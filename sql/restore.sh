#!/bin/bash

if [ `hostname` = "m1530" ]; then
  server="localhost"
  path="/var/www/cms/sql/"
else
  server="isrc_cluster1-mysql"
  path="/apache/www/html/public_html_cms/sql/"
fi

user="cms_admin"
from=$path$1

if [ -z "$1" ]; then
  echo
  echo usage: $0 file_to_restore_from
  echo 
  exit
fi

echo 
echo --- RESTORE ---
echo server: $server
echo from: $from
mysql -h $server -u $user -p < $from
echo finished at: `date`
echo 
