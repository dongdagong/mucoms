#!/bin/bash

databases="cms_common cms_ci1 cms_ci3 cms_ci4"

if [ `hostname` = "m1530" ]; then
  server="localhost"
  path="/var/www/cms/sql/"
else
  server="isrc_cluster1-mysql"
  path="/apache/www/html/public_html_cms/sql/"
fi

user="cms_admin"
to=$path"`date +%y%m%d_%H%M%S`.sql"

if [ "$1" ]; then 
  to=$path"`date +%y%m%d_%H%M%S`_$1.sql"
fi

echo 
echo --- BACKUP ---
echo server: $server
echo databases: $databases
echo to: $to
mysqldump -h $server -u $user -p --databases $databases > $to 
echo finished at: `date`
echo 
