create database cms_common;

use cms_common;

CREATE TABLE Conference (
  ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  FullName CHAR(200) NOT NULL,
  ShortName CHAR(30) NOT NULL,
  URL CHAR(200) NULL,
  ContactEmail CHAR(50) NOT NULL,
  FromEmail CHAR(50) NOT NULL,
  ReplyEmail CHAR(50) NULL,
  CC CHAR(255) NULL,
  BCC CHAR(255) NULL,
  LogEmail BOOL NULL,
  StartDate DATE NULL,
  EndDate DATE NULL,
  Venue CHAR(200) NULL,
  IsMultiTrack BOOL NULL,
  IsConfigured BOOL NULL,
  IsActive BOOL NULL,
  PRIMARY KEY(ID)
)
ENGINE=InnoDB;

CREATE TABLE Administrator (
  ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  UID CHAR(10) NOT NULL,
  Pwd CHAR(50) NOT NULL,
  FName CHAR(30) NULL,
  MName CHAR(30) NULL,
  LName CHAR(30) NOT NULL,
  Email CHAR(60) NOT NULL,
  Email2 CHAR(60) NULL,
  Org CHAR(100) NOT NULL,
  Address CHAR(50) NULL,
  City CHAR(30) NULL,
  Region CHAR(30) NULL,
  Country CHAR(50) NULL,
  Postcode CHAR(20) NULL,
  Phone CHAR(20) NULL,
  PRIMARY KEY(ID)
)
ENGINE=InnoDB;

insert into Administrator set
	UID = 'userName',
	Pwd = sha1('passWord'),
	LName = 'lastName',
	FName = 'firstName',
	Org = 'OrganisationName';

