<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$u = HTTP::sessionate('u');

	if ($u != $_SESSION['id'])
		HTTP::message("Wrong user id specified.", 'no where to go');

	$user = new Users($u);

	$form = new HTMLForm('form');
	$line = new FormElement('line', 'line', 'line');

	$fname = new FormElement('text', 'fname', $user->fname);
	$fname->addLayout('Name', 'First', 'set=31', 'size=15');
	$fname->addRule('maxLen=30');

	$mname = new FormElement('text', 'mname', $user->mname);
	$mname->addLayout('Name', 'Mid', 'set=32', 'size=6');
	$mname->addRule('maxLen=30');

	$lname = new FormElement('text', 'lname', $user->lname);
	$lname->addLayout('Name', '*Last', 'set=33', 'size=15');
	$lname->addRule('required', 'maxLen=30');

	$email = new FormElement('static', 'email', $user->email);
	$email->addLayout('Email', 'Cannot modify email at this stage', 'size=40');

	$email2 = new FormElement('text', 'email2', $user->email);
	$email2->addLayout('Re-enter Email', null, 'size=40');
	$email2->addRule('required', 'equals=email', 'maxLen=60');

	$org = new FormElement('text', 'org', $user->org);
	$org->addLayout('Organization', null, 'size=40');
	$org->addRule('required', 'maxLen=100');

	$address = new FormElement('text', 'address', $user->address);
	$address->addLayout('Address', null, 'size=40');
	$address->addRule('maxLen=50');

	$city = new FormElement('text', 'city', $user->city);
	$city->addLayout('City', null, 'size=40');
	$city->addRule('maxLen=30');

	$region = new FormElement('text', 'region', $user->region);
	$region->addLayout('Region', null, 'size=40' );
	$region->addRule('maxLen=30');

	$country = new FormElement('select', 'country', $user->country, countryValab());
	$country->addLayout('Country');
	$country->addRule('required', 'maxLen=2');

	$postcode = new FormElement('text', 'postcode', $user->postcode);
	$postcode->addLayout('Postcode');
	$postcode->addRule('maxLen=20');

	$phone = new FormElement('text', 'phone', $user->phone);
	$phone->addLayout('Phone');
	$phone->addRule('maxLen=20');

	$submit = new FormElement('submit', null, 'Submit');
	$form->addElement($fname, $mname, $lname, $email, $org, $address, $city, $region, $country,
	$postcode, $phone, $line, $submit);

 	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			$user->fname = $fname->value;
			$user->mname = $mname->value;
			$user->lname = $lname->value;
			//$user->email = $email->value;
			$user->org = $org->value;
			$user->address = $address->value;
			$user->city = $city->value;
			$user->region = $region->value;
			$user->country = $country->value;
			$user->postcode = $postcode->value;
			$user->phone = $phone->value;

			DB::autocommit(false);
			$user->updateProfile();
			DB::commit();

			switch ($_SESSION['role']) {

				case 40:
					$url = 'a_main.php';
					break;
				case 31:
				case 30:
					$url = 'r_main.php';
					break;
				case 20:
					$url = 'c_main.php';
					break;
			}

			HTTP::message('Profile updated successfully.', $url);

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('Change Password', 'change_password.php').'<br />'; ?>
<h3>Edit User Profile</h3>
<? echo REQUIRED_FIELDS; ?>
<p class=red><?php echo $oops; ?>
<?php
	$form->display();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
