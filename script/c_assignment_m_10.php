<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// Manual assignment by paper. Papers ordered by sum of bidding expertises.

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(231);

	function main() {

		$track = new Track(1);
		$pe = $track->getPaperExpertiseValab();

		// a2: <pId>, sumOfPE, title, aNames, {<rId>, rName, numOfAssignments, level}
		//            0        1      2       3 (reviewers assigned)
		$a2 = $track->getAssignmentsByPaper();

		if ($a2 != null) {
			$i = 1;
			foreach ($a2 as $pId=>$a) {

				echo "<a name=$pId>".'<b> '.(DEBUG ? $pId.'. ':'').$a[1].'</b></a>'.downloadLink($pId).'<br />';
				echo '<span class=small>'.$a[2].'</span><br />';
				echo '<span class=small> Sum of bidding expertises assigned: '.warning($a[0], $track->revPerPaper * 2).
				" &nbsp;Number of reviewers assigned: ".warning(count($a[3]), $track->revPerPaper, $track->revPerPaper).
				'</span><br />';

				if (isset($a[3])) {
					asort($a[3]); // order by level;
				}

				$j = 1;
				$data = array();
				if (count($a[3]) > 0) {
					foreach ($a[3] as $rId => $r) {

						$data[$j][0] = (DEBUG ? $rId.'. ':'').$r[0];
						$data[$j][0] = HTML::command($data[$j][0], "c_assignment_m_20.php#$rId");
						$data[$j][1] = '<span class=small>'.$pe[$r[2]].'</span>';
						if ($r[2] == -1)
							$data[$j][1] = makeup($data[$j][1], 'white', 'red');
						$data[$j][2] = $r[1];
						$data[$j][3] = HTML::command('Delete', "c_assignment_m_x.php?p=$pId&r=$rId&a=delete");
						$j++;
					}

					$table = new HTMLTable($data);
					$table->setTitle('Reviewer=550', 'Expertise=140', 'Assignments=100', '=40');
					$table->layout['titleBold'] = 0;
					$table->display();
				}

				echo HTML::command('Add Reviewers', "c_assignment_m_11.php?p=$pId");
				echo '<br /><br />';
				$i++;
			}
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
By Paper | <? echo HTML::command('By Reviewer', 'c_assignment_m_20.php'); ?><br />
<h3>Manual Assignment By Paper</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p></p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
