<?
	// Assuming DB already connected in the including script.
	
	function paperArray() {
		
		$track = new Track(1);
		$ps = new PaperStatus();
		$psValab = $ps->getPaperStatusValab();

		$papers = $track->getNonWithdrawnPapers();
		
		if ($papers == null) return null;
		
		$data = array();
		$i = 0;		
		foreach($papers as $pId) {

			$data[$i][0] = null;	// average
			$data[$i][1] = null;	// weighted average
			$data[$i][2] = null;	// title
			$data[$i][3] = null;	// author
			$data[$i][4] = null;	// links
			$data[$i][5] = null;	// reviewer & score
			$data[$i][6] = null;	// review summary
			$data[$i][7] = null;	// decision
			$data[$i][8] = null;	// sort token - average
			$data[$i][9] = null;	// sort token - weighted average
			$data[$i][10] = $pId;
			
			$p = new Paper($pId);
			$reviewers = $p->getReviewers();
			
			$isInited = $p->isDiscussionInitiated;
			$data[$i][2] = (DEBUG ? $p->id. '. ':'').$p->title;
			$data[$i][3] = $p->getAuthorNames();
			$data[$i][4] .= HTML::command('review', "view_review.php?paperId=$pId");
			$data[$i][4] .= $isInited ? ' '.HTML::command('discuss', "forum_disp_thread.php?pId=$pId"):' ';
			$data[$i][4] .= $isInited ? 
				' '.makeup('&nbsp;d&nbsp', 'white', 'grey'):
				' '.HTML::command('init', "c_init_discuss.php?pId=$pId");
			$data[$i][4] .= ' '.($p->isInConflict() ? makeup('&nbsp;?&nbsp', 'white', 'red'):'');
			$data[$i][7] = $p->paperStatus;

			$j = 0;	
			if ($reviewers != null) {
				$t = 0;	

				$expertise = array();
				foreach ($reviewers as $rId) {
					
					$reviewer = new Reviewer($rId);
					$review = new Review($rId, $pId, 1);
					$overall = $review->getOverallScore($track->overallId);
					
					$o = $overall;
					if ($o == null || $review->progress != 'Finalized')
						$o = makeup("&nbsp;$o&nbsp;", 'white', 'red');
					$data[$i][5] .= $reviewer->getName().': '.$o.'<br />';

					if ($overall > 0) {
						$expertise[] = array($overall, 
						$review->getAnswerByCol($track->reviewerExpertiseId));
						$data[$i][0] += $overall;					
						$j++;
					}
					if ($t == 0) {
						$data[$i][6] .= $review->getQuestionTitle();
						$t = 1;
					}
					$data[$i][6] .= $review->getQuestionAnswer();
					$data[$i][8] += $review->getSortToken();
				}
				
				if ($j != 0) {
					$data[$i][0] = round($data[$i][0] / $j, 2);
					
					$totalExp = 0;
					foreach ($expertise as $exp) {
						$totalExp += $exp[1];
					}
					foreach ($expertise as $exp) {
						$data[$i][1] += $exp[0] * $exp[1] / $totalExp;
//$data[$i][11][] = $exp[1];
					}
					$data[$i][1] = round($data[$i][1], 2);
				}
			}
			
//$data[$i][11] = $data[$i][8];
			$data[$i][6] = '<table>'.$data[$i][6].'</table>';
			
			if ($j != 0) $data[$i][8] = $data[$i][8] / $j;
			$data[$i][9] = $data[$i][8] + $data[$i][1] * 1000;
			$data[$i][8] = $data[$i][8] + $data[$i][0] * 1000;
			
			$data[$i][9] = round($data[$i][9], 2);
			$data[$i][8] = round($data[$i][8], 2);

			$i++;
		}
		if ($data != null)
			arsort($data);
//echobr(count($data));
//printArray($data);
		
		return $data;
	}
?>
