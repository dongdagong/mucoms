<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	/*
		specify conflicts
	*/

	session_start();
	Auth::loginCheck(40);
	DB::connect();

	$paperId = isset($_GET['paperId']) ? $_GET['paperId']:$_SESSION['paperId'];
	$_SESSION['paperId'] = $paperId;

	// paper title

	$p = new Paper($paperId);

	// is an contact author of this paper
	$p->isContactAuthorCheck();

	// track

	$t = new Track($p->trackId);

	// reviewers: id, name, conflict, reasion

	$reviewers = $t->getReviewers();

	$form = new HTMLForm('a_specify_conflicts');
	$data = array();

	$conflictValab = array(
		1 =>'Conflict'
	);
	$i = 0;
	foreach ($reviewers as $rId) {

		$r = new Reviewer($rId);

		$data[$i][0] = '<b>'.$r->getName().'</b>';
		$data[$i][0] .= '<br />'.$r->org;

		DB::query("select Reason from Conflict where ReviewerID = $r->id and PaperID = $p->id", &$result);
		$reasonValue = DB::getCell(&$result);
		if ($reasonValue == null) {
			$conflictValues[0] = 0;
		} else {
			$conflictValues[0] = 1;
		}

		$conflict[$r->id] = new FormElement('checkbox', "conflict$r->id", NULL, $conflictValab, $conflictValues);
		$data[$i][1] = $conflict[$r->id]->getTag();

		$reason[$r->id] = new FormElement('textarea', "reason$r->id", $reasonValue);
		$reason[$r->id]->addLayout(null, null, 'rows=3', 'cols=50');
		//$reason[$r->id]->addRule('required');
		$data[$i][2] = $reason[$r->id]->getTag();

		//$form->addElement($conflict[$r->id], $reason[$r->id]);
		$i++;
	}

	$table = new HTMLTable($data);
	$table->setTitle('Reviewer=190', 'Conflict=135', 'Reason of Conflict=300');

	$submit = new FormElement('submit', null, 'Submit');


	if (HTTP::isLegalPost()) {

		try {

			//$form->validateWithException();

			foreach ($reviewers as $rId) {
				//$reason[$rId]->validate();
				if ($conflict[$rId]->getSelectValue(0) == 0) {
					// delete unticked if they already exist in Conflict
					DB::query("delete from Conflict where ReviewerID = $rId and PaperID = $p->id", &$result);
				} else {
					// update or add ticked
					$theReason = $reason[$rId]->value;
					if (strlen(trim($theReason)) == 0) {
						throw new Exception('Please enter a reason for all selected conflicts.');
					}
					DB::query("update Conflict set Reason = '$theReason' where ReviewerID = $rId
					and PaperID = $p->id", &$result);

					DB::query("select count(*) from Conflict where ReviewerID = $rId and PaperID = $p->id",
					&$result);
					if (DB::getCell(&$result) < 1) {
					//if (DB::getConn()->affected_rows < 1) {
						$id = $_SESSION['id'];
						DB::query("insert into Conflict set PaperID = $p->id, ReviewerID = $rId,
						TrackID = $p->trackId, SpecifiedBy = $id, Reason = '$theReason'", &$result);
					}
				}

			}

			//unset($_SESSION['paperId']);
			HTTP::redirect('a_main.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<h3>Specify Conflicts with Reviewers for <? echo $p->title; ?> </h3>
<p>To <b>add</b> a conflict, check 'Conflict' AND specify the reason. </p>
<p>To <b>delete</b> a conflict, uncheck 'Conflict'.
<p class=red><? echo $oops; ?><br /></p>
<?
	$form->open();
	$table->display();
	echo '<br />';
	$submit->display();
	$form->close();
?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>