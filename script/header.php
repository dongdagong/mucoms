<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Conference.php';

	session_start();

	$welcome = '';		// user
	$shortName = '';	// conference
	$fullName = '';		// conference
	$link = '';			// conference
	$menu = '';			// user
	$switch = '';		// user

	if (isset($_SESSION['role'])) {				// logged in

		$conf = $_SESSION['conference'];

	} elseif (isset($_SESSION['ci'])){			// trying to log in

		try {
			DB::connect('common');
			$conf = new Conference($_SESSION['ci']);
			$_SESSION['conference'] = $conf;
		} catch (Exception $e) {
			$fullName = $e->getMessage();
		}
	}

	if (isset($conf)) {
		$shortName = $conf->shortName;
		$fullName = $conf->fullName;
		$link = $conf->getConferenceLink();
	}

	if (!isset($_SESSION['role'])) {
		$welcome = 'Not logged in';
		$mainmenu = '.';
	} else {									// logged in

		DB::connect();

		// name, current role, edit, logout

		$u = new Users($_SESSION['id']);

		switch ($_SESSION['role']) {
			case 10:
				$role = 'Administrator';
				break;
			case 20:
				$role = 'Chair';
				$menu = array(
					'Home' => 'c_main.php',
					'Configuration' => 'c_configuration.php',
					'Conference Committee' => 'c_conference_committee.php',
					'Programme Committee' => 'c_programme_committee.php',
					'New Line' => '',
					'Phase' => 'c_phase.php',
					'Paper & Author' => 'c_paper.php',
					'Bid' => 'c_bid.php',
					'Assignment' => 'c_assignment.php',
					'Review' => 'c_review.php',
					'Selection' => 'c_selection.php',
//					'Group Emailing' => 'c_group_emailing.php',
					'Report' => 'c_report.php',
					'Advanced Query' => 'c_advanced_query.php'
				);
				break;
			case 30:
				$role = 'Reviewer';
				$menu = array(
					'Home' => 'r_main.php',
					'Bid for Papers' => 'r_bid_1.php',
					'Invite External Reviewer' => 'add_user.php?nur=31',
					'Delegate Paper' => 'r_delegate_paper.php'
				);
				break;
			case 31:
				$role = 'External Reviewer';
				$menu = array(
					'Home' => 'r_main.php',
				);
				break;
			case 40:
				$role = 'Author';
				$menu = array(
					'Home' => 'a_main.php',
					'Submit New Paper' => 'a_submit_paper_1.php',
				);
				break;
		}

		$welcome = 'Welcome <b>'.$u->getName().'</b>, you are logged in as <b>'.$role.'</b>.';
		$edit = '<a href="edit_profile.php?u='.$_SESSION['id'].'">Edit Profile</a>';
		$logout = '<a href="logout.php">Logout</a>';
		$welcome .= ' '.$edit.' | '.$logout.'<br />';

		if (DEBUG)
			$welcome .= 'id:'.$_SESSION['id'].' role:'.$_SESSION['role']. ' ci:'.$_SESSION['ci'];

		$welcome .= ' '.$u->email;

		// switching roles

		$roles = $u->getRolesValab();
		unset($roles[$_SESSION['role']]);

		if (count($roles) > 0) {
			$switch = 'Switch to: ';
			foreach ($roles as $id => $name) {
				$switch .= HTML::command($name, "switch_to.php?r=$id").' | ';
			}
			$switch = substr($switch, 0, strlen($swtich) - 2);
		}

		if (strstr($_SERVER['PHP_SELF'], 'login') == false ) {
			$mainmenu = buildMenu($menu);
			$mainmenu = substr($mainmenu, 0, strlen($mainmenu) - 2);
		}
		else
			$mainmenu = '.';

		if (strstr($_SERVER['PHP_SELF'], 'logout.php') != false) {
			$mainmenu = '.';
			$welcome = '&nbsp;';
			$switch = '&nbsp;';
		}
	}

	function buildMenu($menu){
		if (count($menu) > 0) {

			foreach ($menu as $item=>$link) {
				if ($link == '') {
					$return .= '<br />';
					continue;
				}
				$l = substr($link, 0, strlen($link) - 5);
				$class = stripos($_SERVER['PHP_SELF'], $l) === false ? NULL:'disabledMenuItem';
				$return .= HTML::a($item, $link, $class).' | ';
			}
		} else
			$return = '.';

		return $return;
	}

?>
<? include_once 'header_simple.php'; ?>
<body>
<div class="container">
<div class="header">
<div class="welcome"><? echo $welcome.'<br />'.$switch; ?></div>
<div class="shortname"><? echo $shortName; ?></div>
<div class="fullname"><? echo $fullName; ?></div>
</div>
<div class="conferencelink"><? echo $link; ?></div>
<div class="mainmenu"><? echo $mainmenu; ?></div>
