<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$role = $_SESSION['role'];
	if (!($role == 30 || $role == 31))
		HTTP::message('Access denied');

	
	$r = $_SESSION['role'] == 30 ? new Reviewer($_SESSION['id']) : new ExternalReviewer($_SESSION['id']);
	$papers = $r->getAssignments();	
	
	$t = new Track(1);
	if ($t->phReview != 1 && datediff(date('Y-m-d'), $t->dueReview) > 0)
		HTTP::message("Review phase is not open.");


	// review form
	
	$review = new Review(9999, 9999, 1); 
	$t = new Track(1);
	
	$form = new HTMLForm('form');
	$line = new FormElement('line');

	$q = array();
	
	for ($i = 1; $i <= $review->numQuestions; $i++) {

		$desc = $review->question[$i]->isConfidential == 1 ?
		'(Confidential, won\'t be revealed to the author.)':NULL;

		switch ($review->question[$i]->questionType) {
			case 'Choice':
				foreach ($review->question[$i]->answers as $id => $answer) 
					$review->question[$i]->answers[$id] = "[$id] $answer";
					
				$q[$i] = new FormElement('radio', "q$i", $review->question[$i]->answer,
				$review->question[$i]->answers);
				$q[$i]->addLayout(
				($i == $t->overallId ? '<b>':'').
				$review->question[$i]->id.'. '.$review->question[$i]->question.
				($i == $t->overallId ? '</b>':''),
				$desc);
				break;
			case 'Comment':
				$q[$i] = new FormElement('textarea', "q$i", $review->question[$i]->answer);
				$q[$i]->addLayout($review->question[$i]->id.'. '.$review->question[$i]->question,
				$desc, 'rows=6', 'cols=63');
				$q[$i]->addRule('maxLen=10000');
				break;
		}
		if ($review->question[$i]->isRequired) {
			$q[$i]->addRule('required');
		}
		$form->addElement($q[$i], $line);
	}
?>
<html>
<head>
<title><? echo $_SESSION['conference']->shortName.' Review Form'; ?></title>
<link rel="stylesheet" type="text/css" href="./style/main.css" />
<link rel="stylesheet" type="text/css" href="./style/form.css" />
</head>
<body>
<?
	echo '<div class=right0><b>'.$_SESSION['conference']->shortName.'</b></div>';
	echo 'Review Form ['.REQUIRED_FIELDS.']';
	$form->display('form2');
	echo '<div class=right0>'.SYSTEM_NAME.'</div>';
?>
</body>
</html>
