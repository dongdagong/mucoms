<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$paperId = HTTP::sessionate('paperId');

	// author has this paper
	if ($_SESSION['role'] == 40) {
		$author = new Author($_SESSION['id']);
		if (!$author->hasPaper($paperId))
			HTTP::message('You are not an author of this paper.', 'a_main.php');
	}

	// reviewer has this paper
	if ($_SESSION['role'] == 31) {
		$er = new ExternalReviewer($_SESSION['id']);
		if (!$er->hasPaper($paperId))
			HTTP::message('You do not have this delegation.', 'r_main.php');
	}

	if ($_SESSION['role'] == 30) {
		$r = new Reviewer($_SESSION['id']);
		if (!$r->hasPaper($paperId))
			HTTP::message('You do not have this assignment.', 'r_main.php');
	}

	// chair has the privilege
	if ($_SESSION['role'] == 20) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(240);
	}

	$p = new Paper($paperId);
	$reviewers = $p->getReviewers();
	$t = new Track($p->trackId);

	$tag = '';
	if ($reviewers != null) {

		$reviewId = 1;
		foreach ($reviewers as $reviewerId) {

			$r = new Reviewer($reviewerId);	// instead of Session['id'], which might be an ER's id.
			$review = new Review($r->id, $p->id, $p->trackId);

			if (DEBUG) $tag .= "ReviewerID: $r->id<br />";

			if ($_SESSION['role'] == 40) {
				if ($review->progress == null || $review->progress != 'Finalized')
					continue;
				else
					$tag .= '<h4>Review '.$reviewId++.' </h4>';
			} else {

				$tag .= "<h4>$r->id. ".$r->getName()."</h4>";

				if ($_SESSION['role'] == 20 && $review->progress == 'Finalized') {
					$tag .= HTML::command('Definalize this review',
					"c_definalize.php?p=$paperId&r=$reviewerId");
					$tag .= '<br />';
				}

				if ($review->progress == null) {
					$tag .= 'Review is not submitted. <br />';
					continue;
				}

				if ($review->progress != 'Finalized') {
					$tag .= 'Review is not finalized. <br />';
					if ($_SESSION['role'] != 20)
						continue;
				}
			}

			$data = array();
			for ($i = 1; $i <= $review->numQuestions; $i++) {

				$isConfidential = $review->question[$i]->isConfidential;

				if ($isConfidential && $_SESSION['role'] == 40)
					continue;

				$data[$i][0] =
				($i == $t->overallId ? '<b>':'').
				$review->question[$i]->id.'. '.$review->question[$i]->question.':'.
				($i == $t->overallId ? '</b>':'');
				switch ($review->question[$i]->questionType) {
					case 'Choice':
						$data[$i][1] =
						($i == $t->overallId ? '<b>':'').
						$review->question[$i]->answers[$review->question[$i]->answer].
						($i == $t->overallId ? '</b>':'');
						break;
					case 'Comment':
						$data[$i][1]= str_replace(chr(13), '<br />', $review->question[$i]->answer);
						break;
				}
				$data[$i][1] = ($data[$i][1]);
			} // each question
			$table = new HTMLTable($data);
			$table->setTitle('Question=280', 'Answer=750');
			$tag .= '<br />'.$table->getTag().'<br /><br />';
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<h3>Review of <? echo $p->title; ?></h3>
<? echo $tag; ?>
<p>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>