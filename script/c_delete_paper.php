<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// Delete paper, it's topics, authorship, and conflicts
	// delete all it's authors if they do not have other submissions

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$p = HTTP::sessionate('p');

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(220);

	$paper = new Paper($p);

	// confirm
	HTTP::confirm(
		'Delete <b>'.$paper->title.'</b>?<br /><br />
		Authors of this paper will also be deleted if they do not have other submissions.',
		'c_delete_paper.php',
		'c_paper.php',
		'Yes, delete this paper',
		'No, do not delete'
	);

	try {

		DB::autocommit(false);
		$paper->delete();
		DB::commit();
		HTTP::message('Paper deleted.', 'c_paper.php');

	} catch (Exception $e) {

		$oops = $e->getMessage();
		HTTP::message($e->getMessage().'<br />Deletion failed. Cannot delete the paper at this stage.', $url);
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<p class=red><?php echo $oops.'<br />'; ?><br /></p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
