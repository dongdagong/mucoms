<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck(40);
	DB::connect();

	$paperId = HTTP::sessionate('paperId');
	$p = new Paper($paperId);
	$t = new Track(1);

	// is an contact author of this paper
	$p->isContactAuthorCheck();

	// can edit at this stage
	if (!$p->canWithdraw()) {
		HTTP::message("Cannot withdraw at this stage.", 'a_main.php');
	}

	// ask the user for confirmation
	HTTP::confirm(
		"Withdraw <b>$p->title</b>.<br /><br />
		This operation cannot be undone. <br />
		Do you want to continue?",
		'a_withdraw.php',
		'a_main.php',
		'Yes',
		'Cancel'
	);

	try {
		DB::autocommit(false);

		$p->withdraw();

		$e = new Email('Paper Withdrawn', 'withdraw');
		$e->addPlugin("PaperTitle:$p->title");
		//$u = new User($_SESSION['id']);
		$ca = $p->getAuthors();
		foreach ($ca as $u) {
			if ($u->isContactAuthor == 1) {
				$e->send($u);
			}
		}

		DB::commit();
	} catch (Exception $e) {
		HTTP::message($e->getMessage());
	}

	HTTP::message('Paper withdrawn.', 'a_main.php');
?>