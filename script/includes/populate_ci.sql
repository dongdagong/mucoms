
INSERT INTO Role VALUES
  (10, 'Administrator'),
  (20, 'Chair'),
  (30, 'Reviewer'),
  (31, 'External Reviewer'),
  (40, 'Author');

INSERT INTO PaperStatus
(ID, Name, IsEditable, Template, SendNotification, FinalUpload) VALUES
(-1, 'Withdrawn', 0, null, 0, 0),
(0, 'Decision Pending', 0, null, 0, 0),
(1, 'Accepted', 0, 'decision_accept', 1, 1),
(2, 'Rejected', 0, 'decision_reject', 1, 0);

INSERT INTO PaperExpertise VALUES (4, 'High Expertise');
INSERT INTO PaperExpertise VALUES (3, 'Medium Expertise');
INSERT INTO PaperExpertise VALUES (2, 'Low Expertise');
INSERT INTO PaperExpertise VALUES (1, 'No Expertise');
INSERT INTO PaperExpertise VALUES (0, 'Not Specified');
INSERT INTO PaperExpertise VALUES (-1, 'Conflict of Interest');

INSERT INTO TopicExpertise VALUES (4, 'High Expertise');
INSERT INTO TopicExpertise VALUES (3, 'Medium Expertise');
INSERT INTO TopicExpertise VALUES (2, 'Low Expertise');
INSERT INTO TopicExpertise VALUES (1, 'No Expertise');
INSERT INTO TopicExpertise VALUES (0, 'Not Specified');

INSERT INTO PaperFormat VALUES ('.pdf');
INSERT INTO PaperFormat VALUES ('.ps');
INSERT INTO PaperFormat VALUES ('.doc');

INSERT INTO Privilege VALUES
  (200, 'Configuration', 0),
  (201, 'Conference Committee', 0),
  (202, 'Programme Committee', 0),
  (203, 'Advanced Query', 0),
  (204, 'Phase', 0),
  (220, 'Paper and Author', 1),
  (221, 'Late Submission', 1),
  (230, 'Automatic Assignment', 1),
  (231, 'Manual Assignment', 1),
  (240, 'Review', 1),
  (250, 'Selection', 1),
  (260, 'Group Emailing', 1),
  (270, 'Report', 1);
  
INSERT INTO NotificationLog VALUES
  ('assignment', 'Assignment Notification', null, '2000-01-01');
