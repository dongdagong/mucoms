
DROP TABLE if exists ReviewTrack1;
DELETE FROM FormAnswer;
DELETE FROM FormQuestion;
DELETE FROM Review;

/*

Each FormQuestion of QuestionType choice requires a set of 
correcponding FormAnswer entries.

Each FormQuestion, regardless of its QuestionType, 
requires a single ReviewTrack1 column.

FormQuestions 'Overall' and 'Expertise of Reviewer' are compulsory, in 
that the form need to have at least these two questions using exactly 
the same question names. Though their column positions are up to the 
form designer. 

*/

INSERT INTO FormQuestion (TrackID, ID, Question, QuestionType, IsRequired, IsConfidential) VALUES
(1, 1, "Originality", "choice", 1, 0),
(1, 2, "Significance of Result", "choice", 1, 0),
(1, 3, "Technical Quality", "choice", 1, 0),
(1, 4, "Relevance", "choice", 1, 0),
(1, 5, "Presentation", "choice", 1, 0),
(1, 6, "Overall", "choice", -1, 0),
(1, 7, "Expertise of Reviewer", "choice", -1, 0),
(1, 8, "Amount of Rewriting Required", "choice", 1, 0),
(1, 9, "Nominate for Best Paper Award", "choice", 1, 1),
(1, 10, "Comments to Conference Committee", "comment", 0, 1),
(1, 11, "Feedback to Author <br /> [Main Contributions]", "comment", 1, 0),
(1, 12, "Feedback to Author <br /> [Positive Aspects]", "comment", 1, 0),
(1, 13, "Feedback to Author <br /> [Negative Aspects]", "comment", 1, 0),
(1, 14, "Feedback to Author <br /> [Further Comments]", "comment", 0, 0);

INSERT INTO FormAnswer (TrackID, ID, Value, Label) VALUES
(1, 1, 7, "Strong Accept (award quality)"),
(1, 1, 6, "Accept (I will argue for this paper)"),
(1, 1, 5, "Weak Accept (vote accept, but will not object)"),
(1, 1, 4, "Neutral (not impressed, will not object)"),
(1, 1, 3, "Weak Reject (vote reject, but will not object)"),
(1, 1, 2, "Reject (I will argue against this paper)"),
(1, 1, 1, "Strong Reject"),

(1, 2, 7, "Strong Accept (award quality)"),
(1, 2, 6, "Accept (I will argue for this paper)"),
(1, 2, 5, "Weak Accept (vote accept, but will not object)"),
(1, 2, 4, "Neutral (not impressed, will not object)"),
(1, 2, 3, "Weak Reject (vote reject, but will not object)"),
(1, 2, 2, "Reject (I will argue against this paper)"),
(1, 2, 1, "Strong Reject"),

(1, 3, 7, "Strong Accept (award quality)"),
(1, 3, 6, "Accept (I will argue for this paper)"),
(1, 3, 5, "Weak Accept (vote accept, but will not object)"),
(1, 3, 4, "Neutral (not impressed, will not object)"),
(1, 3, 3, "Weak Reject (vote reject, but will not object)"),
(1, 3, 2, "Reject (I will argue against this paper)"),
(1, 3, 1, "Strong Reject"),

(1, 4, 7, "Strong Accept (award quality)"),
(1, 4, 6, "Accept (I will argue for this paper)"),
(1, 4, 5, "Weak Accept (vote accept, but will not object)"),
(1, 4, 4, "Neutral (not impressed, will not object)"),
(1, 4, 3, "Weak Reject (vote reject, but will not object)"),
(1, 4, 2, "Reject (I will argue against this paper)"),
(1, 4, 1, "Strong Reject"),

(1, 5, 7, "Strong Accept (award quality)"),
(1, 5, 6, "Accept (I will argue for this paper)"),
(1, 5, 5, "Weak Accept (vote accept, but will not object)"),
(1, 5, 4, "Neutral (not impressed, will not object)"),
(1, 5, 3, "Weak Reject (vote reject, but will not object)"),
(1, 5, 2, "Reject (I will argue against this paper)"),
(1, 5, 1, "Strong Reject"),

(1, 6, 7, "Strong Accept (award quality)"),
(1, 6, 6, "Accept (I will argue for this paper)"),
(1, 6, 5, "Weak Accept (vote accept, but will not object)"),
(1, 6, 4, "Neutral (not impressed, will not object)"),
(1, 6, 3, "Weak Reject (vote reject, but will not object)"),
(1, 6, 2, "Reject (I will argue against this paper)"),
(1, 6, 1, "Strong Reject"),

(1, 7, 3, "High"),
(1, 7, 2, "Medium"),
(1, 7, 1, "Low"),

(1, 8, 3, "High"),
(1, 8, 2, "Medium"),
(1, 8, 1, "Low"),

(1, 9, 2, "Yes"),
(1, 9, 1, "No");


CREATE TABLE ReviewTrack1 (
  ReviewerID SMALLINT UNSIGNED NOT NULL,
  PaperID SMALLINT UNSIGNED NOT NULL,
  c1 TINYINT UNSIGNED NULL,
  c2 TINYINT UNSIGNED NULL,
  c3 TINYINT UNSIGNED NULL,
  c4 TINYINT UNSIGNED NULL,
  c5 TINYINT UNSIGNED NULL,
  c6 TINYINT UNSIGNED NULL,
  c7 TINYINT UNSIGNED NULL,
  c8 TINYINT UNSIGNED NULL,
  c9 TINYINT UNSIGNED NULL,
  c10 TEXT NULL,
  c11 TEXT NULL,
  c12 TEXT NULL,
  c13 TEXT NULL,
  c14 TEXT NULL,
  PRIMARY KEY(ReviewerID, PaperID),
  FOREIGN KEY(ReviewerID, PaperID)
    REFERENCES Review(ReviewerID, PaperID)
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
)
ENGINE=InnoDB;
