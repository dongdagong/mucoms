<?

define('DEBUG', false);					// Set to false for production version.
define('DB_HOST', 'localhost');
define('DB_PASSWORD', '123456');		// Set to the same that's used to create cms_admin. 
define('SYSTEM_DOMAIN', 'url');	// Base URL to access the system from the web.
define('SEND_EMAIL', 1);				// Set to 1 to enable sending notification emails, or 0 to disable.
define('SERVER_TIMEZONE', 'UTC');
define('CURRENT_YEAR', '2008');
define('MAX_UPLOAD_FILE_SIZE', 2);		// In megabytes. It should be smaller than PHP's upload_max_filesize. 
										// (Under Ubuntu: /etc/php5/apache2/php.ini, upload_max_filesize)


/////////////// Do not change anything below this line. ///////////////

define('DB_USER', 'cms_admin');
define('DB_PREFIX', 'cms_');

define('UPLOAD_DIR', '../upload/');
define('TEMPLATE_DIR', '../template/');
define('LOG_DIR', '../logs/');

define('SYSTEM_NAME', 'MuCoMS');
define('SYSTEM_VERSION', 'v0.92 beta');
define('REQUIRED_FIELDS', '<b>*</b> denotes required fields.');

define('DETECTION_THRESHOLD', 0.8);			// 0.5 to 1.0, The smaller the number the more conflicts detected.
define('REVIEW_CONFLICT_THRESHOLD', 3.0);	// Any score different from the average by this 
											// number shall make the paper in review conflict.

?>
