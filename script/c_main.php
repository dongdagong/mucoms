<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	try {
		$t = new Track(1);
		$ps = new PaperStatus();
		$psa = $ps->getPaperStatusValab();

		// authors
		$author = new HTMLBox('Authors');
		$author->addLine('Registered: '.$t->statistics('author', 'registered'));
		$author->addLine('Have Submission: '.$t->statistics('author', 'haveSubmission'));
		$author->addLine('Contact Author: '.$t->statistics('author', 'contact'));

		// papers
		$paper = new HTMLBox('Papers');
//		var_dump($psa);
		$paper->addLine('Abstract Submitted: '.$t->statistics('abstractSubmitted'));
		$paper->addLine('Full Paper Submitted: '.$t->statistics('fullSubmitted'));
		foreach ($psa as $psId=>$psName) {
			$paper->addLine("$psName".(DEBUG ? " ($psId)":'').': '.$t->statistics('byPaperStatus', $psId));
		}
		$paper->addLine('Final Paper Submitted: '.$t->statistics('finalSubmitted'));

		// reviewers
		$reviewer = new HTMLBox('Reviewers');
		$reviewer->addLine('Reviewer: '.$t->statistics('reviewer', 'reviewer'));
		$reviewer->addLine('Bidding: '.$t->statistics('reviewer', 'bid'));
		$reviewer->addLine('External Reviewer: '.$t->statistics('reviewer', 'er'));
		$reviewer->addLine('Delegation: '.$t->statistics('reviewer', 'delegation'));

		// reviews
		$review = new HTMLBox('Reviews');
		$review->addLine('Assigned: '.$t->statistics('review', 'assigned'));
		$review->addLine('In Review: '.$t->statistics('review', 'In Review'));
		$review->addLine('Finalized: '.$t->statistics('review', 'Finalized'));

	} catch (Exception $e) {
		$oops = 'Exception: '.$e->getMessage();
	}
?>
<? include_once 'header.php'; ?>

<div class="main">
<h3>Conference Briefing</h3>
<? echo $oops; ?>
<table class=cell width="600" border="0">
  <tr>
    <td valign="top" width="300"><? $author->display(); echo '<br />'; $paper->display(); ?></td>
    <td width="25"></td>
    <td valign="top" width="300"><? $reviewer->display(); echo '<br />'; $review->display(); ?></td>
  </tr>
</table>
<p></p>
</div> <!-- end of main -->

<? include_once 'footer.php'; ?>
