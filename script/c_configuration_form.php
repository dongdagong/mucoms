<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Chair.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);
	
	try {

		$review = new Review(9999, 9999, 1); 
		$t = new Track(1);
		
		$form = new HTMLForm('c_configuration_form');
		$line = new FormElement('line');

		$q = array();
		$moveValab = array();
		
		for ($i = 1; $i <= $review->numQuestions; $i++) {

			$moveValab[$i] = substr($i.'. '.$review->question[$i]->question, 0, 40);
			
			$desc = $review->question[$i]->isConfidential == 1 ?
			'(Confidential, won\'t be revealed to the author.)':NULL;

			if ($t->isFormFinalized != 1) {
				$modifyLink = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.HTML::command('Modify', "c_configuration_form_edit.php?cmd=update&questionNum=$i");
				$deleteLink = HTML::command('Delete', "c_configuration_form_edit.php?cmd=delete&questionNum=$i");
				$link = $i == $t->overallId || $i == $t->reviewerExpertiseId ? '':"<br /><br /> $modifyLink | $deleteLink";
			}
			
			switch ($review->question[$i]->questionType) {
				case 'Choice':
					foreach ($review->question[$i]->answers as $id => $answer) 
						$review->question[$i]->answers[$id] = "[$id] $answer";
					
					$q[$i] = new FormElement('radio', "q$i", $review->question[$i]->answer,
					$review->question[$i]->answers);
					$q[$i]->addLayout(
					($i == $t->overallId ? '<b>':'').
					$review->question[$i]->id.'. '.$review->question[$i]->question.
					($i == $t->overallId ? $link.'</b>':$link.''),
					$desc);//, 'labelAlign=left');
					break;
				case 'Comment':
					$q[$i] = new FormElement('textarea', "q$i", $review->question[$i]->answer);
					$q[$i]->addLayout($review->question[$i]->id.'. '.$review->question[$i]->question. $link,
					$desc, 'rows=6', 'cols=63');//, 'labelAlign=left');
					$q[$i]->addRule('maxLen=10000');
					break;
			}
			if ($review->question[$i]->isRequired) {
				$q[$i]->addRule('required');
			}
			$form->addElement($q[$i], $line);
		}


		$submit = new FormElement('submit', null, 'Submit');


		// move questions

		$moveForm = new HTMLForm('moveForm');
		$submitMove = new FormElement('submit', 'submitMove', 'Move');
		
		$x = new FormElement('select', 'x', 1, $moveValab);
		//$x->addLayout(null, null, 'size=20');
		$x->addRule('isInt', 'minInt=1');

		$dirArray = array(
			-1 => 'before',
			1 => 'after'
		);
		$dir = new FormElement('select', 'dir', 1, $dirArray);
		$dir->addRule('minInt=-1', 'maxInt=1');

		$y = new FormElement('select', 'y', 1, $moveValab);
		//$y->addLayout(null, null, 'size=20');
		$y->addRule('isInt', 'minInt=1');

		$moveForm->addElement($x, $dir, $y);

		if (HTTP::isLegalPost()) {

			if ($t->isFormFinalized == 1)
				HTTP::message('Review form already finalized.', 'c_configuration_form.php');
				
			if (isset($_POST['x']) && isset($_POST['y']) && isset($_POST['dir'])) { 
				
				$q = new FormQuestion(9999, 9999, 1, $x->value);
				DB::autocommit(false);
				$q->move($x->value, $y->value, $dir->value);
				DB::commit();
				
			}
			
			HTTP::redirect('c_configuration_form.php');
		}
	} catch (Exception $e) {
		$oops = $e->getMessage().'<br /><br />';
	}

?>
<? include_once 'header.php'; ?>
<div class="main">
<?
	echo HTML::menu(array(
		'Topics' 		=> array('c_configuration_edit_topics.php', 1),
		'Paper Formats'	=> array('c_configuration_paper_formats.php', 1),
		'Review Form' 	=> array('c_configuration_form.php', 0),
		'Paper Status'	=> array('c_configuration_ps_disp.php?template=hide', 1)
	));
?>
<h3><? echo $t->isFormFinalized == 1? 'Review Form [Finalized]':'Review Form'; ?></h3>
<? 	
	if ($t->isFormFinalized != 1) {
		echo HTML::command('Add a New Choice-type Question', 'c_configuration_form_edit.php?cmd=add&questionType=Choice').' | ';
		echo HTML::command('Add a New Comment-type Question', 'c_configuration_form_edit.php?cmd=add&questionType=Comment').' | ';
	}
	
	$cmd = $t->isFormFinalized == 1 ? 'Definalize':'Finalize';
	echo HTML::command("$cmd Review Form", "c_configuration_form_edit.php?cmd=$cmd").'<br /><br />';
?>
<div class=red><? echo $oops; ?></div>
<?
	echo '<div class=greybox><br />&nbsp;&nbsp;'.REQUIRED_FIELDS.'</div>'; 
	$form->display();
	
	if ($t->isFormFinalized != 1) {
		
		echo '<br />';
		$moveForm->open();
		echo 'Place question '.$x->getTag().' '.$dir->getTag().
		' question '.$y->getTag().' '.$submitMove->gettag();
		echo '<br /><span class=small><span class=red>';
		echo isset($x->errors[0]) ? '1st topic number: '.$x->errors[0].' &nbsp;' : '';
		echo isset($y->errors[0]) ? '2nd topic number: '.$y->errors[0] : '';
		echo '</span></span>';
		$moveForm->close();
		echo '<br />';

		echo HTML::command('Add a New Choice-type Question', 'c_configuration_form_edit.php?cmd=add&questionType=Choice').' | ';
		echo HTML::command('Add a New Comment-type Question', 'c_configuration_form_edit.php?cmd=add&questionType=Comment').' | ';
	} else
		echo '<br />';
	
	$cmd = $t->isFormFinalized == 1 ? 'Definalize':'Finalize';
	echo HTML::command("$cmd Review Form", "c_configuration_form_edit.php?cmd=$cmd").'<br /><br />';

?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
