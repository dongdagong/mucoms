<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php'; 

	session_start();
?>
<?php include_once 'header_simple.php'; ?>
<body>
<div class="main">
<br />
<h3>About The MuCoMS Development Team</h3>

<?php
	echo ' 
        <br />

        <table>
         <colgroup>
          <col width="50px" />
          <col width="150px" />
          <col width="25px" />
          <col width="*" />
         </colgroup>

         <tr>
          <td rowspan="9">&nbsp;</td>
          <th>
           Team Leader:
          </th>
          <td> &nbsp;</td>
          <td>
           <a href="http://work.thekirchbergs.info/" target="markus">Markus Kirchberg</a>, Singapore
          </td>
         </tr>

         <tr>
          <td colspan="3"> &nbsp;</td>
         </tr>

         <tr>
          <th>
           Advisor:
          </th>
          <td> &nbsp;</td>
          <td>
           <a href="http://www.in.tu-clausthal.de/en/personen/aktuelle/prof-dr-sven-hartmann/" target="sven">Sven Hartmann</a>, Germany
          </td>
         </tr>

         <tr>
          <td colspan="3"> &nbsp;</td>
         </tr>

         <tr>
          <th>
           System Analyst:
          </th>
          <td> &nbsp;</td>
          <td>
           vacant
          </td>
         </tr>

         <tr>
          <td colspan="3"> &nbsp;</td>
         </tr>

         <tr>
          <th>
           Programmer:
          </th>
          <td> &nbsp;</td>
          <td>
           <a href="mailto: ">Dagong Dong</a>, New Zealand
          </td>
         </tr>

         <tr>
          <td colspan="3"> &nbsp;</td>
         </tr>

         <tr>
          <th>
           Test Analyst:
          </th>
          <td> &nbsp;</td>
          <td>
           vacant
          </td>
         </tr>
        </table>

        <p> &nbsp;</p>

        <p>
         Contact for general enquiries, bug reports and the lot:
        </p>

        <table>
         <colgroup>
          <col width="50px" />
          <col width="*" />
         </colgroup>

         <tr>
          <td> &nbsp;</td>
          <td>
           <a href="mailto: contact@mucoms.org?subject=MuCoMS Enquiry">contact@mucoms.org</a>
          </td>
         </tr>
        </table>

        <br /><br />
	';

	echo HTML::command('Back', $_SERVER['HTTP_REFERER']);
?>

</div> <!-- end of main -->

<?php include_once 'footer_simple.php'; ?>
