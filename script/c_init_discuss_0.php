<?
	// initialise discussion for the given paper
	
	// definalize the conflict reviews
	// post existing conflicting reviews in the discussion forum
	// send email notifications to all reviewers and ers

	function initDiscuss($pId) {

		$p = new Paper($pId);
		if ($p == null)
			throw new Exception('Invalid pId');
			
		// definalize/definish the reviews
		
		$reviewers =$p->getReviewers();
		if ($reviewers == null)
			throw new Exception('No reviewer assigned');

		$msg = null;
		foreach ($reviewers as $rId) {
			$review = new Review($rId, $pId, 1);
			$review->definalize();
			$reviewer = new Reviewer($rId);
			if ($review->progress != null) {
				$msg .= "<b>By ".$reviewer->getName()." </b><br /><br />";
				$msg .= $review->getReviewText().'<br /><br />';
			}
		}
		$p->initiateDiscussion();

		// post existing reviews in the discussion forum

		$thread = new Thread();
		$thread->userId = $_SESSION['id'];
		$thread->parentId = 'null';
		$thread->paperId = $pId;
		$now = date('Y-m-d H:i:s');
		$thread->timePosted = $now;
		$thread->timeModified = $now;
		$thread->title = 'Please discuss the review conflict as shown below:';
		$thread->msg = '<blockquote>'.htmlentities($msg, ENT_QUOTES).'</blockquote>';

		$thread->add();

		// send email

		$ers = $p->getERs();
		if (is_array($ers))
			$all = array_merge($reviewers,  $ers);
		else
			$all = $reviewers;
		if (count($all) == 0)
			throw new Exception('No review available');

		foreach ($all as $uId) {
			$group[] = new Users($uId);
		}

		$e = new Email('Review Conflict Discussion', 'init_discussion');
		$e->addPlugin("PaperTitle:$p->title");
		$e->groupSend($group);

		return true;
	}
?>
