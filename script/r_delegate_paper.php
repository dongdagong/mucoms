<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Conflict.php';

	session_start();
	Auth::loginCheck(30);
	DB::connect();
	
	$t = new Track(1);
	if ($t->phReview != 1 && datediff(date('Y-m-d'), $t->dueReview) > 0)
		HTTP::message("Review phase is not open.");

	$r = new Reviewer($_SESSION['id']);

	$assignmentValab = $r->getAssignmentValab();

	$ers = $r->getERs();

	$form = new HTMLForm('r_delegate_paper');
	$data = array();

	$i = 0;

	if (count($ers) == 0)
		HTTP::message('Please invite external reviewers first.', 'add_user.php?nur=31');

	foreach ($ers as $erId) {

		$er = new ExternalReviewer($erId);

		$data[$i][0] = (DEBUG ? $erId.'. ':'').'<b>'.$er->getName().'</b><br>'.$er->org.'<div class=small>'.$er->email.'</div>';

		$delegations[$erId] = new FormElement('checkbox', "delegation$erId", NULL,
		$assignmentValab, $er->getAssignments($_SESSION['id']));

		$data[$i][1] = $delegations[$erId]->getTag();

		$i++;
	}

	$table = new HTMLTable($data);
	$table->setTitle('External Reviewer=200', 'Delegation=700');

	$submit = new FormElement('submit', null, 'Submit');


	if (HTTP::isLegalPost()) {

		// Don't delegate the same paper more than once.
		// Also don't delegate a paper to an er who already has that paper from another reviewer.
		// The second pitfall is prevented by table desigin.
		// The first is taken care of here.


		try {

			$er = new ExternalReviewer();
			$conflict = new Conflict(1);

			foreach ($ers as $erId) {

				if ($delegations[$erId]->values != null)
					foreach ($delegations[$erId]->values as $p) {
						if ($conflict->detectReviewerPaper($erId, $p) == 1) {
							HTTP::message('Cannot add this assignment. <br /><br />'.
							$conflict->getDetectedtag(), 'r_delegate_paper.php');
						}
					}
			}

			DB::autocommit(false);
			$er->clearDelegations($_SESSION['id']);
			foreach ($ers as $erId) {

				$er->id = $erId;
				$er->updateDelegations($_SESSION['id'], $delegations[$erId]->values);
			}
			DB::commit();

			HTTP::redirect('r_main.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
			//$oops .= DB::getConn()->error;
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<h3>Delegate Papers</h3>
<p class=red><? echo $oops; ?><br /></p>
<?
	$form->open();
	$table->display();
	echo '<br />';
	$submit->display();
	$form->close();
?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
