<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Thread.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$cmd = HTTP::sessionate('cmd');
	$pId = HTTP::sessionate('pId');
	$parentId = HTTP::sessionate('parentId');
	$threadId = HTTP::sessionate('threadId');
	$relativeId = HTTP::sessionate('relativeId');

	$thread = new Thread();
	try {
		$p = new Paper($pId);
	} catch (Exception $e) {
		HTTP::message($e->getMessage());
	}

	if ($p->isDiscussionInitiated != 1)
		HTTP::message('Group discussion not initiated.');

	if ($cmd == null)
		HTTP::message('cmd is not provided');
	if ($pId == null)
		HTTP::message('pId is not provided');
	if ($parentId == null)
		HTTP::message('parentId is not provided');
	if ($threadId == null)
		HTTP::message('threadId is not provided');

	if (!isset($_SESSION['role']))
		HTTP::message('Please log in first.');

	// check paper belongs to reviewer/er
	if ($_SESSION['role'] == 30) {
		$user = new Reviewer($_SESSION['id']);
		if (!$user->hasPaper($pId))
			HTTP::message('You do not have this assignment.');
	}
	if ($_SESSION['role'] == 31) {
		$user = new ExternalReviewer($_SESSION['id']);
		if (!$user->hasPaper($pId))
			HTTP::message('You do not have this delegation.');
	}

	// check chair has privilege
	if ($_SESSION['role'] == 20) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(250);	// paper selection
	}

	// prepare the values for form and display

	if ($cmd == 'new') {
		$label= "New Message: $p->title";
		$parentId = 0;
	}

	if ($cmd == 'reply') {
		$parentWho = new Users($thread->getUserId($parentId));
		$name = $parentWho->getName();
		$label= "Reply: $p->title";
		$titleValue = "Re: $name (#$relativeId) - ".$thread->getTitle($parentId);
		$msgValue = $thread->getMsg($parentId);
		$threadId = 0;
	}

	if ($cmd == 'edit') {
		// check that msg (edit) belongs to user
		if ($thread->getUserId($threadId) != $_SESSION['id'])
			HTTP::message('This is not your post.');

		$label= "Edit Message: $p->title";
		$titleValue = $thread->getTitle($threadId);
		$msgValue = $thread->getMsg($threadId);
	}

	$form = new HTMLForm('form');
	$line = new FormElement('line', 'line');
	$submit = new FormElement('submit', 'submit', 'Submit');
	$title = new FormElement('text', 'title', ($titleValue));
	if ($cmd == 'edit' and $titleValue != null)
		$title->addLayout(null, null, 'size=83', 'disabled=disabled');
	else
		$title->addLayout(null, null, 'size=83');
	$title->addRule('maxLen=255');
	if ($cmd == 'reply')
		$msgValue = '<blockquote>'.$msgValue.'</blockquote>';
	$msg = new FormElement('textarea', 'msg', $msgValue);
	$msg->addLayout(null, null, 'rows=17', 'cols=71');
	$msg->addRule('maxLen=5000');
	
	$form->addElement($title, $msg);

	if (HTTP::isLegalPost()) {
		
		try {
			
			$thread->title = get_magic_quotes_gpc() ? $title->value : addslashes($title->value);
//echobr("1 ".$thread->title." .".get_magic_quotes_gpc().'.');
			$thread->msg = get_magic_quotes_gpc() ? $msg->value : addslashes($msg->value);
			
			$form->validateWithException();
//echobr("2 ".$thread->title." .".get_magic_quotes_gpc().'.');
			if (strlen(trim($msg->value)) == 0)
				throw new Exception('Message cannot be empty.');

			$now = date('Y-m-d H:i:s');
			$thread->timeModified = $now;

			DB::autocommit(false);

			if ($cmd == 'new' || $cmd == 'reply') {
				$thread->userId = $_SESSION['id'];
				$thread->parentId = $parentId == 0 ? 'null':$parentId;
				$thread->paperId = $pId;
				$thread->timePosted = $now;
				$thread->add();
			}

			if ($_SESSION['cmd'] == 'edit') {
				$thread->update($threadId);
			}

			// group notification (reviewers and external reviewers)

			$reviewers = $p->getReviewers();
			$ers = $p->getERs();
			if ($ers != null)
				$group = array_merge($reviewers, $ers);
			else
				$group = $reviewers;
			$e = new Email('New discussion message posted', 'forum_new_post');
			$e->addPlugin("PaperTitle:$p->title");
			foreach ($group as $uId) {
				if ($uId != $_SESSION['id'])
					$g[] = new Users($uId);
			}
			$e->groupSend($g);

			DB::commit();
//echobr("3 ".$thread->title." .".get_magic_quotes_gpc().'.');

			HTTP::redirect("forum_disp_thread.php?pId=$pId");
		} catch (Exception $e) {
			$oops = $e->getMessage().'<br />';
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3><? echo $label; ?></h3>
<div class=red><? echo $oops.'<br />'; ?></div>
<?php	
	$form->open();
	echo 'Title: <br />';
	$title->display();
	echo '<br />* Message: <br />';
	$msg->display();
	$line->display();
	$submit->display();
	$form->close();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
