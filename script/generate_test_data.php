<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	
	//DB::connect('ci1');

	/*
		Conference instance generation code: .sql
		User entered data: .php
		
		Reviewer	Chair	Paper	Author
		100			5		300		600
		
	*/
	
	// Paper and Author
	
	// # of author	1	2	3	4	5	6	7	8	9	10
	// %			9	40	20	10	6	5	4	3	2	1
	// for each of these authors, 80% chance to be a new user, 20% to be existing user.

/*	
	$R = 0; $ER = 0; $C = 5; $P = 0; // $A = 600;
	$numAuthorsDist = array(
		
		1=>9,
		2=>35,
		3=>25,
		4=>10,
		5=>6,
		6=>5,
		7=>4,
		8=>3,
		9=>2,
		10=>1
	);
	
	// return a number according to it's frequency distribution
	
	function getDistNum($distArray) {

		arsort(&$distArray);
		
		$i = rand(1, 100);
		foreach ($distArray as $num=>$prob) {
		
			if ($i <= $prob)
				return $num;
			else {
				$i = $i - $prob;
			}
		}
	}
	
	echobr(' testing getDistNum() - generate numbers according to their frequency distribution');
	
	$stat = range(1, 10);
	for ($i = 1; $i <= 10; $i++) 
		$stat[$i] = 0;
	for ($i = 1; $i <= 1000; $i++) {
		$numAuthor = getDistNum($numAuthorsDist);
		$stat[$numAuthor]++;
	}
	for ($i = 1; $i <= 10; $i++) 
		echobr($i.' '.$numAuthorsDist[$i]. ' : '.$stat[$i]/10);
		
		
	DB::query('select max(ID) from Paper', &$result);
	$maxPaperId = DB::getCell(&$result);
/////////////////////////////////////////////////////////////////////////////////////////////

	
	// Papers and Authors
if (0) {	
	for ($i = 1; $i <= $P; $i++) {
		echo '<br> generating papers and authors ... paper $i <br>';
		
		$p = new Paper();
		
		$p->trackId = 1;
		$p->paperStatus = 'Decision Pending';
		$p->fullFormat = '.pdf';
		//$p->finalFormat;
		
		$p->title = randStr(80, 'Paper '.($maxPaperId + $i).' ', 1);
		$p->keywords = randStr(100, 'key words ', 1);
		$p->abstract = randStr(140, 'abstract ', 1);
		
		$now = date('Y-m-d H:i:s');
		$p->abst1stSub = $now;
		$p->abstLastModi = $now;
		
		//$p->fullFile;	
		// todo two phase?
		$p->full1stSub = $now;
		$p->fullLastModi = $now;
		//$p->finalFile;
		//$p->final1stSub;
		//$p->finalLastModi;
		//$p->lateAbst;
		//$p->lateFull;
		//$p->lateFinal;
		//$p->isWithdrawn;
		//$p->isDecisionNotified;			
	
		
		//echobr("$i: ".randStr(5));
		
		$numAuthor = getDistNum($numAuthorsDist);
		DB::query('select count(*) from Paper', &$result);
		$x = DB::getCell(&$result);
		for ($j = 1; $j <= $numAuthor; $j++) { 
			
			// no matter new or existing, must add an author
	
			
			// the more number of existing authors, the more likely this 'new' author for this paper
			// already has an author account
			// 20/600 = y/x 
			// when there are 600 authors, the likelihood of the author to be an existing author is 20%.
			// assuming a linear co relation between this likelihood and the number of existing authors.
			
			$y = 20/600 * $x; 
			
			if (rand(1, 100) <= $y) {	// use existing	
				$id = rand(1, $x); // assume the IDs are continuous
				echo ($id.' ');
				$a = new Author($id);
				
			} else {	// generate new
				
				$a = new Author();
				
				$a->fname = randStr(4, 'F');
				//$a->mname = $mname[$i]->value;	
				$a->lname = randStr(4, 'L'); 
				$a->email = $a->fname.'.'.$a->lname.'@'.randStr(4, 'd').'.com'; // highly unlikey to duplicate
				//$a->email2 = $email2->value;
				$a->org = randStr(25, 'Organisation Name ', 1); 	
				$a->country = 'nz';
				//$isContactAuthor = $isContactAuthor[$i]->getSelectValue(0);
				//$a->isContactAuthor = $isContactAuthor == 1 ? 1 : 0;
				//echobr('contact author '.$i.' '. $a->isContactAuthor);
				//echobr('getSelectValue '.$i.' '. $isContactAuthor[$i]->getSelectValue(0));
		
						
			}
			$a->isContactAuthor = 1; //rand(0, 10) > 5 ? 1:0;
			$a->serial = $j;	
	
			$p->authors[] = $a;
		}				
		//var_dump('authors <br>', $p->authors);
	
		$p->priTopic = rand(1, 6);
		$p->secTopics = array(rand(1, 3), rand(4, 6));
		
		DB::autocommit(false);
		$p->add();	
		DB::commit();
		
		//echobr('done');
				
	}

}

	// Reviewer
	// 80 of them do not have Author roles. 20 of them have.

if (0) {	
	DB::query('select count(*) from Users', &$result);
	$numUsers = DB::getCell(&$result);
	
	for ($j = 1; $j <= $R; $j++) { 
		echo "<br>generating reviewer $j ...";
	
		if (rand(1, 100) <= 5) {	// use existing	
			$id = rand(1, $numUsers); // assume the IDs are continuous
			echo ($id.' ');
			$r = new Reviewer($id);
			
		} else {	// generate new
			
			$r = new Reviewer();
			
			$r->fname = randStr(4, 'F');
			$r->lname = randStr(4, 'L'); 
			$r->email = $r->fname.'.'.$r->lname.'@'.randStr(4, 'd').'.com'; // highly unlikey to duplicate
			$r->org = randStr(25, 'Organisation ', 1); 	
			$r->country = 'nz';
					
		}

		DB::autocommit(false);
		$r->add();
		DB::commit();
	}
}	
	echobr('');
		
	// Assignment
	
if (0) {	
	echo 'generating assignments ... <br />';
	
	// for testing reviewer features. just assign 5 papers to a randomly selected reviewer.
	DB::query("select ID from Reviewer limit 8, 2", &$result);
	$rId = DB::getCell(&$result);
	echobr('rId: '.$rId);

	DB::autocommit(false);
	DB::query("insert into Assignment values ($rId, 3, 1), ($rId, 120, 1), ($rId, 97, 1)", &$result);
	DB::query("insert into Member values ($rId, 1, 0)", &$result);
	DB::commit();
}

	// External Reviewer

if (0) {	
	
	$inviterId = 461;
	for ($i = 1; $i <= $ER; $i++) {
		
		echobr("<br />generating external reviewers $i ...");
	
		$r = new ExternalReviewer();
		
		$r->fname = randStr(4, 'E');
		$r->lname = randStr(4, 'R'); 
		$r->email = $r->fname.'.'.$r->lname.'@'.randStr(4, 'd').'.com'; // highly unlikey to duplicate
		$r->org = randStr(25, 'Organisation ', 1); 	
		$r->country = 'us';
		
		try {		
		
			DB::autocommit(false);
			$r->add($inviterId); 
			DB::commit();

		} catch (Exception $e) {
			echobr( $e->getMessage() );
		}


	}	

}
	// Delegation
if (0) {
	echobr('generating delegations ...');

	$r = new Reviewer($inviterId);
	$r->delegate(97, 1057);
	$r->delegate(3, 1057);
}	
	
	
	


	echobr('<br>finished');
	
	
	// returns a string of length $len and starts with $head
	function randStr($len, $head = '_', $hasSpace = 0) {
		
		//$letters = range('a', 'z');
		$return = $head;
		$len = $len - strlen($return);
		while ($len-- > 0) {
			if ($hasSpace == 0)
				$return .= rand(0, 9);
			else
				$return .= rand(1, 10) >= 7 ? ' ':rand(0, 9);
		}
		return $return;
	}
*/				
?>
