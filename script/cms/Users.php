<?php

	class Users {

		public $id;
		public $uid;
		public $pwd;
		public $fname;
		public $mname;
		public $lname;
		public $email;
		public $email2;
		public $org;
		public $address;
		public $city;
		public $region;
		public $country;
		public $postcode;
		public $phone;
		public $pwdSent;

		public $isNewUser;

		function Users($id = null) {

			// return a skeleton if id is not specified
			if ($id == null)
				return;

			$this->id = $id;
			DB::query("select * from Users where ID = $this->id", &$result, 'Users::Users()');
			$row = $result->fetch_object();

			$this->uid = $row->UID;
			$this->pwd = $row->Pwd;
			$this->fname = $row->FName;
			$this->mname = $row->MName;
			$this->lname = $row->LName;
			$this->email = $row->Email;
			$this->email2 = $row->Email2;
			$this->org = $row->Org;
			$this->address = $row->Address;
			$this->city = $row->City;
			$this->region = $row->Region;
			$this->country = $row->Country;
			$this->postcode = $row->Postcode;
			$this->phone = $row->Phone;
			$this->pwdSent = $row->PwdSent;

		}


		// an user can have all the roles Author, Reviewer, Chair at the same time.
		// an External Reviewer must not have any other roles.
		// if the user already exists in the system (identified by email address), simply add the new role.

		// new user: add user, add role
		// existing user: add role only

		public function add($roleId) {

			$this->generateUid();
			$this->pwd = $this->generatePwd();

			// Users Table

			DB::query("select count(*) from Users where Email = '$this->email'", &$result, 'Users::add()');
			$num = DB::getCell(&$result);

			if ($num == 1) {	// existing user, get the id.	(this check is not necessary for ERs)

				$this->isNewUser = 0;
				DB::query("select ID, Pwd from Users where Email = '$this->email'", &$result, 'Users::add()');
				$row = $result->fetch_assoc();
				$this->id = $row['ID'];
				//$this->pwd = $row['Pwd'];

			} else {			// new user, add.

				$this->isNewUser = 1;
				$pwdHash = sha1($this->pwd);
				$query = "
				insert into Users
				values (
					NULL,
					'$this->uid',
					'$pwdHash',
					'$this->fname',
					'$this->mname',
					'$this->lname',
					'$this->email',
					NULL,
					'$this->org',
					'$this->address',
					'$this->city',
					'$this->region',
					'$this->country',
					'$this->postcode',
					'$this->phone',
					'NULL'
				)";
				$query = str_replace("''", 'NULL', $query);

				DB::query($query, &$result, 'Users::add()');
				$this->id = DB::getConn()->insert_id;
			}

			// either way (existing user or not), $this->id represents the user now.

			// UserRole Table (this user must not have this role, otherwise won't reach this method)

			$query = "insert into UserRole set UserID = $this->id, RoleID = $roleId";
			DB::query($query, &$result, 'Users::add()');
		}

		// use EMAIL to check if an user exists AND has the role
		public function isExistingUser($role) {

			$query = "select count(*) from UserRole ur, Users u where u.ID = ur.UserID and u.Email = '$this->email' and ur.RoleID = $role";
			//echobr($query);
			DB::query($query, &$result, 'Users::isExistingUser()');
			$num = DB::getCell(&$result);
			return $num > 0 ? true : false;
		}

		public function isNewUser() {

			$q = "select count(*) from Users where Email = '$this->email'";
			DB::query($q, &$result);
			return DB::getCell(&$result) == 0 ? true : false;
		}

		public function getIdByEmail() {

			$query = "select ID from Users where Email = '$this->email'";
			DB::query($query, &$result, 'Users::getIdByEmail()');
			return DB::getCell(&$result);
		}

		public function getColByEmail($col) {

			$query = "select $col from Users where Email = '$this->email'";
			DB::query($query, &$result, 'Users::getIdByEmail()');
			return DB::getCell(&$result);
		}


		public function generateUid() {

			if ($this->fname != null) {
				$subFname = explode('-', $this->fname);
				for ($i = 0; $i < count($subFname); $i++) {
					$this->uid .= substr($subFname[$i], 0, 1);
				}
			}
			if ($this->mname != null) {
				$subMname = explode('-', $this->mname);
				for ($i = 0; $i < count($subMname); $i++) {
					$this->uid .= substr($subMname[$i], 0, 1);
				}
			}
			$this->uid .= $this->lname;

			// is it unique?

			DB::query("select count(*) from Users where UID like '$this->uid%'", &$result);
			$num = DB::getCell(&$result, 0, 0, 'User::generateUid()');

			if ($num >=1)
				$this->uid .= '.'.($num + 1);

			$this->uid = str_replace("'", '', $this->uid);
			$this->uid = str_replace("&#039;", '', $this->uid);
			$this->uid = str_replace("&quot;", '', $this->uid);
			$this->uid = str_replace(" ", '', $this->uid);
		}

		public function generatePwd() {
			return rand(100000, 999999);
		}

		public function getRoleMenu() {

		}

		public function getMainMenu() {

		}

		public function getName() {

			$query = "select FName, MName, LName from Users where ID = $this->id;";
			DB::query($query, &$result, 'Users::getName()');
			$row = $result->fetch_assoc();
			return $row['FName'].' ' .
			($row['MName'] != null ? $row['MName'].' ' : '').
			$row['LName'];
		}

		// delete reviewers only before bidding, delegating, and assignment
		public function delete($role) {

			DB::query("delete from UserRole where UserID = $this->id and RoleID = $role", &$result);

			switch ($role) {
			case 20:
				// delete chair related records
				DB::query("delete from TrackPrivilege where TrackID = 1 and ChairID = $this->id", &$result);
				DB::query("delete from Coordinate where TrackID = 1 and ChairID = $this->id", &$result);
				DB::query("delete from Chair where ID = $this->id", &$result);

				break;

			case 30:
				// delete reviewer related records
				DB::query("delete from Reviewer where ID = $this->id", &$result);
				break;

			case 40:
				// delete author related records
				DB::query("delete from Author where ID = $this->id", &$result);
				break;
			}

			// delete user if no roles left
			DB::query("select count(*) from UserRole where UserID = $this->id", &$result);
			$rolesLeft = DB::getCell(&$result);
			if ($rolesLeft == 0) {
				DB::query("delete from Users where ID = $this->id", &$result);
			}
		}

		public function updateProfile() {

			$q = "
			update Users set
				FName = '$this->fname',
				MName = '$this->mname',
				LName = '$this->lname',
				Email = '$this->email',
				Org = '$this->org',
				Address = '$this->address',
				City = '$this->city',
				Region = '$this->region',
				Country = '$this->country',
				Postcode = '$this->postcode',
				Phone = '$this->phone'
			where
				ID = $this->id";
			DB::query($q, &$result);
		}

		public function changePassword($oldPwd, $newPwd, $hashOldPwd = true) {

			$pwdHash = $hashOldPwd ? sha1($oldPwd) : $oldPwd;
			$q = "
			select count(*) from Users
			where ID = $this->id and Pwd = '$pwdHash'";
			DB::query($q, &$result);
			if (DB::getCell(&$result) != 1)
				throw new Exception('Old password is wrong.');

			$pwdHash = sha1($newPwd);
			DB::query("update Users set Pwd = '$pwdHash' where ID = $this->id", &$result);
		}

		public function getRolesValab() {

			DB::query("select ur.RoleID, r.Name from UserRole ur, Role r where ur.RoleID = r.ID and
			ur.UserID = $this->id", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['RoleID']] = $row['Name'];
			}
			return $a;
		}

		public function pwdSent() {

			DB::query("update Users set PwdSent = 1 where ID = $this->id", &$result);
		}

		public function isPwdSent() {

			return $this->pwdSent == 1;
		}

	}
?>
