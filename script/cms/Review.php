<?php
	class Review {

		public $reviewerId;
		public $paperId;
		public $trackId;
		public $progress;
		public $isDelegated;
		public $isDelegateFinished;
		public $dateFinalized;
		public $lastModi;

		public $question;		// array of FormQuestion

		public $numQuestions;

		public function Review($reviewerId, $paperId, $trackId) {

			$this->reviewerId = $reviewerId;
			$this->paperId = $paperId;
			$this->trackId = $trackId;

			DB::query("select Progress, IsDelegateFinished, DateFinalized, LastModi from Review where
			ReviewerID = $this->reviewerId and PaperID = $this->paperId and TrackID = $trackId", &$result);
			$row = $result->fetch_object();

			if ($row != NULL) {
				$this->progress = $row->Progress;
				$this->isDelegated = $row->IsDelegated;
				$this->isDelegateFinished = $row->IsDelegateFinished;
				$this->dateFinalized = $row->DateFinalized;
				$this->lastModi = $row->LastModi;
			}

			//echobr("$reviewerId $paperId $trackId");

			DB::query("select * from FormQuestion where TrackId = $trackId", &$result, 'Review::Review()');
			//$row = $result->fetch_row();

			$this->numQuestions = $result->num_rows;
			for ($i = 1; $i <= $this->numQuestions; $i++) {
				$this->question[$i] =  new FormQuestion($reviewerId, $paperId, $trackId, $i);

				//echobr("$this->trackId $i ".$this->question[$i]->question);

				// fetch actual value of each question

			}
		}

		public function isInReview() {
			DB::query("select Progress from Review
			where ReviewerID = $this->reviewerId and PaperID = $this->paperId", &$result, 'Review::update()');
			$isInReview = DB::getCell(&$result);
			return $isInReview == 'In Review';
		}

		public function update() {

			// delete old values, if they exist.
			// order of deletion matters

			DB::query("delete from ReviewTrack$this->trackId where ReviewerID = $this->reviewerId and
			PaperID = $this->paperId", &$result, 'Review::update()');
			DB::query("delete from Review where ReviewerID = $this->reviewerId and PaperID = $this->paperId",
			&$result, 'Review::update()');


			// insert new values

			$now = date('Y-m-d H:i:s');

			if ($this->progress == null) {		// not reviewed yet

				$q = "
				insert into Review set
					ReviewerID = $this->reviewerId,
					PaperID = $this->paperId,
					TrackID = $this->trackId,
					Progress = 'In Review',
					IsDelegateFinished = 0,
					LastModi = '$now'";
			} else {							// already reviewed
				$q .= "
				insert into Review set
					ReviewerID = $this->reviewerId,
					PaperID = $this->paperId,
					TrackID = $this->trackId,
					Progress = '$this->progress',
					IsDelegateFinished = $this->isDelegateFinished,
					LastModi = '$now'";
			}

			DB::query($q, &$result, 'Review::update()');

			// fill the review form

			$query = "insert into ReviewTrack$this->trackId set
				ReviewerID = $this->reviewerId,
				PaperID = $this->paperId ";

			for ($i = 1; $i <= $this->numQuestions; $i++) {

				$answer = $this->question[$i]->answer;
				$query .= ", c$i = '$answer'";
			}

			DB::query($query, &$result, 'Review::update()');
		}

		public function getLastModi() {

			DB::query("select LastModi from Review where ReviewerID = $this->reviewerId and
			PaperID = $this->paperId", &$result);
			$return = DB::getCell(&$result);

			return substr($return, 0, 16);
		}

		public function isDelegated() {
			//return $this->isDelegated == 1 ? true:false;
		}

		public function getAnswerByCol($col) {
			DB::query("select c$col from ReviewTrack$this->trackId
			where ReviewerID = $this->reviewerId
			and PaperID = $this->paperId", &$result);
			return DB::getCell(&$result);
		}
		
		public function getOverallScore($overallId = null) {
			if ($overallId == null) {
				$track = new Track($this->trackId);
				$overallId = $track->overallId;
			}
			return $this->getAnswerByCol($overallId);
/*
			DB::query("select c$overallId from ReviewTrack$this->trackId
			where ReviewerID = $this->reviewerId
			and PaperID = $this->paperId", &$result);
			return DB::getCell(&$result);
*/
		}

		public function isDelegateFinished() {

			return $this->isDelegateFinished == 1 ? true:false;
		}

		public function isFinalized() {

			return $this->progress == 'Finalized' ? true:false;
		}

		public function isEditable() {

			$role = $_SESSION['role'];
			if ($this->isFinalized())
				return false;
			if ($role == 31 && $this->isDelegateFinished == 1)	// more restriction for external reviewer
				return false;

			$t = new Track($this->trackId);
			if ($t->phReview == 0)
				return false;

			return true;
		}

		public function finalize() {

			if ($this->progress == null)
				return;	// does not make sense finalizing a non-exist review

			$now = date('Y-m-d H:i:s');
			DB::query("
				update Review 
				set DateFinalized = '$now', Progress = 'Finalized' 
				where 
					ReviewerID = $this->reviewerId and 
					PaperID = $this->paperId and 
					TrackID = $this->trackId", &$result);
			if ($this->progress != 'Finalized')
				DB::query("
					update Reviewer 
					set NumFinished = NumFinished + 1 
					where ID = $this->reviewerId", &$result);
		}

		public function definalize() {

			if ($this->progress != 'Finalized')
				return;
				
			DB::query("
				update Review 
				set DateFinalized = 'NULL', Progress = 'In Review' 
				where 
					ReviewerID = $this->reviewerId and 
					PaperID = $this->paperId and 
					TrackID = $this->trackId", &$result);
			DB::query("
				update Reviewer 
				set NumFinished = NumFinished - 1 
				where 
					ID = $this->reviewerId and 
					NumFinished > 0", &$result);
			DB::query("
				update Review 
				set IsDelegateFinished = 0, Progress = 'In Review' 
				where 
					ReviewerID = $this->reviewerId and 
					PaperID = $this->paperId and 
					TrackID = $this->trackId and 
					IsDelegated = 1", &$result);
		}

		public function finish() {

			DB::query("
				update Review 
				set IsDelegateFinished = 1, IsDelegated = 1, Progress = 'Delegate Finished' 
				where 
					ReviewerID = $this->reviewerId and 
					PaperID = $this->paperId and 
					TrackID = $this->trackId", &$result);
		}

		public function getDateFinalized() {

			return substr($this->dateFinalized, 0, 16);
		}

		public function getQuestionTitle() {
			$t = new Track($this->trackId);
			foreach ($this->question as $i => $q) {

				if ($q->questionType == 'Choice' )
					$rtn .=
					'<td>'.
					($i == $t->overallId ? '<b>':'').
					substr($q->question, 0, 4).
					($i == $t->overallId ? '</b>':'').
					'</td>';
			}
			return '<tr>'.$rtn.'</tr>';
		}

		public function getQuestionAnswer() {
			foreach ($this->question as $q) {
				if ($q->questionType == 'Choice' ) {
					
					$ans = $q->answer == 0 ? '':$q->answer;
					
					if ($q->question == 'Expertise of Reviewer') {
						switch ($q->answer) {
							case 3: $ans = 'H'; break;
							case 2: $ans = 'M'; break;
							case 1: $ans = 'L'; break;
						}
					}
					if ($q->question == 'Amount of Rewriting Required') {
						switch ($q->answer) {
							case 3: $ans = 'H'; break;
							case 2: $ans = 'M'; break;
							case 1: $ans = 'L'; break;
						}
					}
					
					if ($q->question == 'Nominate for Best Paper Award') {
						switch ($q->answer) {
							//case 0: $ans = ''; break;
							//case 1: $ans = 'Y'; break;
							case 1: $ans = 'N'; break; // form question convention, always starts from 1 (since v0.92).
							case 2: $ans = 'Y'; break;
						}
					}
					
					$rtn .= '<td>';
					if ($q->question == 'Overall') $rtn .= '<b>';
					$rtn .= $ans;
					if ($q->question == 'Overall') $rtn .= '</b>';
					$rtn .= '</td>';
				}
			}
			return "<tr>$rtn</tr>";
		}
		
		public function getSortToken() {
			$rtn = 0;
			foreach ($this->question as $q) {
				if ($q->questionType == 'Choice' ) {
					if ($q->question == 'Originality') $rtn += $q->answer * 100;
					if ($q->question == 'Relevance') $rtn += $q->answer * 10;
					if ($q->question == 'Technical Quality') $rtn += $q->answer * 1;
				}
			}
			return $rtn;
		}

		public function getReview() {
			$t = new Track($this->trackId);
			$data = array();
			for ($i = 1; $i <= $this->numQuestions; $i++) {

				$isConfidential = $this->question[$i]->isConfidential;

				if ($isConfidential == 1 && $_SESSION['role'] == 40)
					continue;

				$data[$i][0] = $this->question[$i]->id.". ".$this->question[$i]->question;
				switch ($this->question[$i]->questionType) {
					case 'Choice':
						$data[$i][1] = $this->question[$i]->answers[$this->question[$i]->answer];
						break;
					case 'Comment':
						$data[$i][1]= "\r\n".$this->question[$i]->answer;
						break;
				}
				$data[$i][1] .= "\r\n";
			} // each question
			return $data;
		}

		public function getReviewText() {

			$data = $this->getReview();
			$text = null;
			foreach ($data as $id => $qa) {
				$text .= "$qa[0]: $qa[1]\r\n";
			}
			return str_replace('<br />', '', $text);
		}
	}
?>
