<?php

// Class Conflict serves two purposes:
// Find all the conflicts and store (add/update) them in Bid. Performed before automatic assignment.
// Detect conflict of a given reviewer paper pair. Used by reviewer delegation and chair manual assignment.

class Conflict {

	// <rId>, <pId>, <aId>, reason
	public $byR = array();		// conflicts specified by reviewers
	public $byA = array();		// conflicts specified by authors
	public $detected = array();	// conflicts detedted by this program

	public $toAdd = array();	// conflicts in $byA or in $detected but not in $byR
								// it has a different format than the three above

	public $strictness;			// setting it to < 1 will return more conflicts
	public $countBid;

	public function Conflict($strictness) {

		$this->strictness = $strictness;
		$this->countBid = true;

		// conflicts specified by reviewers
		DB::query("select b.ReviewerID, b.PaperID from Bid b, Paper p
		where b.PE = -1 and b.PaperID = p.ID and p.PaperStatus <> -1", &$result);
		while ($row = $result->fetch_assoc()) {
			$this->byR[$row['ReviewerID']][$row['PaperID']][] =
			'Specified by reviewer: conflict of interest.';
		}

		// conflicts specified by authors
		DB::query("select ReviewerID, PaperID, SpecifiedBy, Reason from Conflict c, Paper p
		where c.PaperID = p.ID and p.PaperStatus <> -1", &$result);
		while ($row = $result->fetch_assoc()) {
			$this->byA[$row['ReviewerID']][$row['PaperID']][$row['SpecifiedBy']] = $row['Reason'];
		}
	}

	// Return 1 if conflict between the given reviewer and paper.
	// Otherwise return 0
	public function detectReviewerPaper($rId,  $pId) {

		$hit = 0;
		$p = new Paper($pId);
		foreach ($p->authors as $a) {
			//echo " a=".$a->id.' ';
			$hit += $this->detectReviewerAuthor($rId, $a->id, $pId);
		}
	//	echo '<br />';
		return $hit > 1 ? 1:$hit;
	}

	// Return the similarity (in percentage) between the given strings
	function compare($s1, $s2) {

		$dist = levenshtein(strtolower(trim($s1)), strtolower(trim($s2)));
		$len = strlen($s1.$s2) / 2;
		return ($len - $dist) / $len;
	}

	// Return 1 if conflict between the given reviewer and author.
	// Otherwise return 0
	public function detectReviewerAuthor($rId, $aId, $pId) {

		$hit = 0;
		$r = new Users($rId);
		$a = new Users($aId);

		// already in Bid as conflict of interest -1
		if (isset($this->byR[$rId][$pId])) {
			if ($this->countBid) {
				$this->detected[$rId][$pId][$aId] .= ' Conflict recorded in table Bid.';
				$hit = 1;
			} else {
				return 0;
			}
		}

		// same first name and last name. (further test middle name)
		$alike = $this->compare($r->fname.$r->mname.$r->lname, $a->fname.$a->mname.$a->lname);
		if ($alike >= $this->strictness) {
			$this->detected[$rId][$pId][$aId] .= ' Same user name'.
			($this->strictness == 1 || $alike == 1 ? '!':'?');
			$hit = 1;
		}

		// same email address
		$alike = $this->compare($r->email, $a->email);
		if ($alike >= $this->strictness) {
			$this->detected[$rId][$pId][$aId] .= ' Same email'.
			($this->strictness == 1 || $alike == 1 ? '!':'?');
			$hit = 1;
		}

		// same email dommain (academic only)
		$rEmail = explode('@', $r->email);
		$aEmail = explode('@', $a->email);
		$alike = $this->compare($rEmail[1], $aEmail[1]);
		if ($alike >= $this->strictness &&
		 	(!(strpos($rEmail[1], '.edu' ) === false) ||
			 !(strpos($rEmail[1], '.ac') === false))) {
			$this->detected[$rId][$pId][$aId] .= ' Same academic email domain'.
			($this->strictness == 1 || $alike == 1 ? '!':'?');
			$hit = 1;
		}

		// organization
		$alike = $this->compare($r->org, $a->org);
		if ($alike >= $this->strictness) {
			$this->detected[$rId][$pId][$aId] .= ' Same organization'.
			($this->strictness == 1 || $alike == 1 ? '!':'?');
			$hit = 1;
		}

		// address and city

		return $hit;
	}

	public function displayDetected(&$checkbox = null, $withCheckbox = false) {
		echo $this->getDetectedTag(&$checkbox, $withCheckbox);
	}

	public function getDetectedTag(&$checkbox = null, $withCheckbox = false) {

		$checkboxValab = array(
			1 =>' Add this conflict'
		);

		if (count($this->detected) == 0)
			$tag = 'No conflict detected. <br /><br />';
		else {
			foreach ($this->detected as $rId => $r) {
				$reviewer = new Users($rId);
				$tag .= '<b>Reviewer: '.(DEBUG ? $rId.' ':'').$reviewer->getName().
				'</b> [Org: '.$reviewer->org.'] [Email: '.$reviewer->email.']<br /><br />';
				foreach ($r as $pId => $p) {
					$paper = new Paper($pId);

					if ($withCheckbox) {
						$checkboxId = $rId * 1000000 + $pId;
						$checkbox[$checkboxId] =
						new FormElement('checkbox', $checkboxId, null, $checkboxValab, array(1));
						$tag .= $checkbox[$checkboxId]->getTag();
					}

					$tag .= ' Paper: '.(DEBUG ? $pId.' ':'').$paper->title.'<br />';
					foreach ($p as $aId => $reason) {
						$a = new Users($aId);
						$tag .= '<br />Author: '.(DEBUG ? $aId.' ':'').$a->getName().
						' [Org: '.$a->org.'] [Email: '.$a->email.']'.'<br />';
						$tag .= "Reason: $reason";
						$tag .=  '<br />';
					}
					$tag .=  '<br />';
				}
			}
		}
		return $tag;
	}

	// Return 1 if there is at least one conflict across all the reviewers and papers
	// Otherwise return 0.
	public function detectAll() {

		$t = new Track(1);
		$papers = $t->getNonWithdrawnPapers();
		$reviewers = $t->getReviewers();

		$hit = 0;
		foreach ($reviewers as $rId) {
			foreach ($papers as $pId) {
				//echo("r=$rId p=$pId");
				$hit += $this->detectReviewerPaper($rId, $pId);
			}
		}
		return $hit > 1 ? 1:$hit;
	}

	public function generateToAdd($from, $checked = null) {

		if ($from == 'detected') {
			foreach ($this->detected as $rId => $r) {
				foreach ($r as $pId => $p) {
					foreach ($p as $aId => $reason) {
						echo "rId$rId pId$pId aId$aId :: ";
						if (!isset($this->byR[$rId][$pId])) {
							$hit = false;
							foreach ($checked as $pair) {
								if ($pair[0] == $rId && $pair[1] == $pId) {
									$hit = true;
									break;
								}
							}
							if ($hit) {
								if (DEBUG) echo 'detected and added';
								//$this->toAdd[$rId] = $pId;
								// you get one one pId per rId !!!
								$this->toAdd[$rId][$pId] = -1;
							} else
								if (DEBUG) echo 'detected but disapproved';
						} else
							if (DEBUG) echo 'conflict already in table Bid - skip';
						echo '<br />';
					}
				}
			}
//			echo 'byA: '; var_dump($this->byA); echo '<br>';
//			echo 'byR: '; var_dump($this->byR); echo '<br>';
//			echo 'toAdd (detected): '; var_dump($this->toAdd); echo '<br>';
		}

		if ($from == 'byA') {
			foreach ($this->byA as $rId => $r) {
				foreach ($r as $pId => $p) {
					foreach ($p as $aId => $reason) {
						if (DEBUG) echo "rId$rId pId$pId aId$aId :: ";
						if (!isset($this->byR[$rId][$pId])) {
							if (DEBUG) echo 'author specified and added';
							$this->toAdd[$rId][$pId] = -1;
						} else
							if (DEBUG) echo 'conflict already in table Bid - skip';
						echo '<br />';
					}
				}
			}
//			echo 'toAdd (byA): '; var_dump($this->toAdd); echo '<br>';
		}

		if ($from == 'all') {
			$this->generateToAdd('detected', $checked);
			$this->generateToAdd('byA');
		}
	}

	// save $toAdd to Bid
	public function save() {

		if (count($this->toAdd) == 0) {
			echo "toAdd array is empty, nothing to save. <br />";
			return;
		}
		foreach ($this->toAdd as $rId => $r) {
			foreach ($r as $pId => $level) {
				echo "r$rId p$pId ";
				DB::query("select PE from Bid where ReviewerID = $rId and
				PaperID = $pId", &$result);
				$pe = DB::getCell(&$result);
				if ($pe == null) {	// insert
					echo 'insert ';
					DB::query("insert into Bid values ($rId, $pId, 1, -1)", &$result);
				} else {			// update
					echo 'update';
					DB::query("update Bid set PE = -1 where ReviewerID = $rId and
					PaperID = $pId", &$result);
				}
				echo '<br />';
			}
		}
	}

	// delete bids on withdrawn papers from Bid
	public function deleteWithdrawnBids() {

		$q = "
		delete from Bid
		where PaperID in (select ID from Paper where PaperStatus = -1)";
		DB::query($q, &$result);
	}
}
?>
