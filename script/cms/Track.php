<?php
	class Track {

		public $id;
		public $name;
		public $isBlindReview;
		public $isTwoPhase;

		public $dueAbst;
		public $dueFull;
		public $dueTopicExpertise;
		public $duePaperExpertise;
		public $dueReview;
		public $dueGroup;
		public $dueGlobal;
		public $dueFinal;

		public $phAbst;
		public $phFull;
		public $phTopicExpertise;
		public $phPaperExpertise;
		public $phReview;
		public $phGroup;
		public $phGlobal;
		public $phFinal;

		public $revPerPaper;
		public $maxPaperPerRev;
		public $autoAcceptThreshold;
		public $overallId;
		public $reportStatus;
		public $releasePaperStatus;
		public $reviewerExpertiseId;
		public $isFormFinalized;

		public function Track($id) {

			DB::query("select * from Track where ID = $id", &$result, 'Track::Track()');
			$theTrack = $result->fetch_object();

			$this->id = $theTrack->ID;
			$this->name = $theTrack->Name;
			$this->isBlindReview = $theTrack->IsBlindReview;
			$this->isTwoPhase = $theTrack->IsTwoPhase;

			$this->dueAbst = $theTrack->DueAbst;
			$this->dueFull = $theTrack->DueFull;
			$this->dueTopicExpertise = $theTrack->DueTopicExpertise;
			$this->duePaperExpertise = $theTrack->DuePaperExpertise;
			$this->dueReview = $theTrack->DueReview;
			$this->dueGroup = $theTrack->DueGroup;
			$this->dueGlobal = $theTrack->DueGlobal;
			$this->dueFinal = $theTrack->DueFinal;

			$this->phAbst = $theTrack->PhAbst;
			$this->phFull = $theTrack->PhFull;
			$this->phTopicExpertise = $theTrack->PhTopicExpertise;
			$this->phPaperExpertise = $theTrack->PhPaperExpertise;
			$this->phReview = $theTrack->PhReview;
			$this->phGroup = $theTrack->PhGroup;
			$this->phGlobal = $theTrack->PhGlobal;
			$this->phFinal = $theTrack->PhFinal;

			$this->revPerPaper = $theTrack->RevPerPaper;
			$this->maxPaperPerRev = $theTrack->MaxPaperPerRev;
			$this->autoAcceptThreshold = $theTrack->AutoAcceptThreshold;
			$this->overallId = $theTrack->OverallID;
			$this->reportStatus = $theTrack->ReportStatus;
			$this->releasePaperStatus = $theTrack->ReleasePaperStatus;
			$this->reviewerExpertiseId = $theTrack->ReviewerExpertiseID;
			$this->isFormFinalized = $theTrack->IsFormFinalized;
		}

		public function update() {

			$query = "update Track set

				IsBlindReview = $this->isBlindReview,
				IsTwoPhase = $this->isTwoPhase,

				DueAbst = $this->dueAbst,
				DueFull = $this->dueFull,
				DueTopicExpertise = $this->dueTopicExpertise,
				DuePaperExpertise = $this->duePaperExpertise,

				RevPerPaper = $this->revPerPaper,
				MaxPaperPerRev = $this->maxPaperPerRev,

				DueReview = $this->dueReview,
				DueGroup = $this->dueGroup,
				DueGlobal = $this->dueGlobal,
				AutoAcceptThreshold = $this->autoAcceptThreshold,
				DueFinal = $this->dueFinal";

			DB::query($query, &$result, 'Track::update()');
		}

		public function updatePhases() {

			// Dependency check. todo.

			$query = "update Track set
				PhAbst = $this->phAbst,
				PhFull = $this->phFull,
				PhTopicExpertise = $this->phTopicExpertise,
				PhPaperExpertise = $this->phPaperExpertise,

				PhReview = $this->phReview,
				PhGroup = $this->phGroup,
				PhGlobal = $this->phGlobal,
				PhFinal = $this->phFinal";

			DB::query($query, &$result, 'Track::updatePhases()');
		}

		public function isAbstOpen() {
			return $this->phAbst == 1 ? true : false;
		}

		public function isBlindReview() {
			return $this->isBlindReview == 1 ? true : false;
		}

		public function isTwoPhase() {
			return $this->isTwoPhase == 1 ? true : false;
		}

		public function isTopicExpertiseOpen() {
			return $this->phTopicExpertise == 1 ? true : false;
		}

		public function isPaperExpertiseOpen() {
			return $this->phPaperExpertise == 1 ? true : false;
		}

		// Return an array of topics in the form ID->Name
		public function getTopicsValab($cut = 0) {

			$query = "select ID, Name, Serial from Topic where TrackID = $this->id order by Serial";
			DB::query($query, &$result, 'Track::getTopicsValab()');

			$topicArray = array();
			$topicID = '';
			$topicName = '';
			for ($i = 1; $i <= $result->num_rows; $i++) {
				$row = $result->fetch_object();
				if ($row == NULL)
					throw new Exception("Track::getTopicsValab(): Error fetching query result");
				$topicArray[$row->ID] = $cut == 1 && strlen($row->Name) > 80 ?
				substr($row->Name, 0, 80) : $row->Name;
			}
			return $topicArray;

		}

		public function getTopicsValabDefaultNotSelected() {

			$topicArray = array();
			$topicArray = $this->getTopicsValab();
			//$topicArray[0] = '< Not Selected >';
		}

		public function getTopicExpertiseValab() {

			DB::query("select * from TopicExpertise order by ID desc", &$result);
			for ($i = 0; $i < $result->num_rows; $i++) {
				$row = $result->fetch_object();
				$return[$row->ID] = $row->Name;
			}
			return $return;
		}

		public function getPaperExpertiseValab() {

			DB::query("select * from PaperExpertise order by ID desc", &$result);
			for ($i = 0; $i < $result->num_rows; $i++) {
				$row = $result->fetch_object();
				$return[$row->ID] = $row->Name;
			}
			return $return;
		}

		public function getPaperFormatsValab($type) {

			$query = "SELECT PaperFormat FROM TrackPaperFormat WHERE FullorFinal = '$type' AND TrackId = $this->id";
			DB::query($query, &$result, 'Track::getPaperFormatsValab()');

			$formats = array();
			for ($i = 0; $i < $result->num_rows; $i++) {
				$row = $result->fetch_object();
				if ($row === NULL)
					throw new Exception("Track::getPaperFormatsValab(): Error fetching query result.");

				$formats["$row->PaperFormat"] = $row->PaperFormat;
			}
			return $formats;
		}

		public function getReviewers() {

			$reviewers = array();
			//$query = "select ReviewerID from Member where TrackID = '$this->id'";
			$query = "select UserID from UserRole where RoleID = 30";	// impl for single track
			DB::query($query, &$result, 'Track::getReviewers()');
			while ($row = $result->fetch_assoc()) {
				$reviewers[] = $row['UserID'];
			}

			return $reviewers;
		}

		// return an array of id=>expertise pairs of papers having $priTopicId as primary topic
		// and reviewed by $reviewerId
		public function getPaperExpertiseArray($priTopicId, $reviewerId) {

			$papers = array();
			$query = "
			select
				pt.PaperID PaperID, b.PE PE
			from
				PaperTopic as pt left join
				(select PaperID, PE from Bid where ReviewerID = $reviewerId) as b
				using (PaperID)
			where
				pt.TrackID = $this->id and
				pt.IsPrimary = 1 and
				pt.TopicID = $priTopicId
			";
			$query = "
			select
				pt.PaperID PaperID, b.PE PE
			from
				PaperTopic as pt left join
				(select PaperID, PE from Bid where ReviewerID = $reviewerId) as b
				using (PaperID)
			where
				pt.TrackID = $this->id and
				pt.IsPrimary = 1 and
				pt.TopicID = $priTopicId
			";
			DB::query($query, &$result);
			while ($row = $result->fetch_assoc()) {
				$papers[$row['PaperID']] = $row['PE'];
			}
			//echobr($query);
			//var_dump($papers);
			return $papers;
		}

		public function getNonWithdrawnPapers() {

			DB::query("select ID from Paper where TrackID = $this->id and
			PaperStatus <> -1", &$result, 'Track::getPaperIds()');
			while ($row = $result->fetch_assoc()) {
				$return[] = $row['ID'];
			}
			return $return;
		}

		public function getNumPapers() {

			DB::query("select count(*) from Paper where TrackID = $this->id", &$result);
			return DB::getCell(&$result);
		}

		public function getTopicsTextarea() {

			DB::query("select Name, ID from Topic where TrackID = $this->id order by ID", &$result);
			while ($row = $result->fetch_assoc()) {
				//$return .= $row['Name']."\n";	// \n has to be in double quotes
				$return .= $row['Name'].'<br /><br />';
			}
			return $return;
		}

		public function getPaperFormatArray($fullOrFinal) {
			DB::query("select PaperFormat from TrackPaperFormat where TrackID = $this->id and
			FullorFinal = '$fullOrFinal'", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['PaperFormat'];
			}
			return $a;
		}

		public function getPaperFormatText($fullOrFinal) {
			$a = $this->getPaperFormatArray($fullOrFinal);
			foreach ($a as $format) {
				$return .= $format.' ';
			}
			$return = substr($return, 0, strlen($return) - 1);
			return $return;
		}

		public function getPaperFormatTextarea($fullOrFinal) {

			$a = $this->getPaperFormatArray($fullOrFinal);
			foreach ($a as $format) {
				$return .= $format."\n";	// \n has to be in double quotes
			}
			return $return;
		}

		public function isDueAbst() {

			return dateDiff(date('Y-m-d'), substr($this->dueAbst, 0, 10), '-') < 0;
		}

		public function isDueFull() {

			return dateDiff(date('Y-m-d'), substr($this->dueFull, 0, 10), '-') < 0;
		}

		public function isDueFinal() {

			return dateDiff(date('Y-m-d'), substr($this->dueFinal, 0, 10), '-') < 0;
		}


		public function getDueDate($phase, $section) {
/*
		public $dueAbst;
		public $dueFull;
		public $dueTopicExpertise;
		public $duePaperExpertise;
		public $dueReview;
		public $dueGroup;
		public $dueGlobal;
		public $dueFinal;

*/
			switch ($phase) {
				case 'Abst':
					return getDateSection($this->dueAbst, $section);

				case 'Full':
					return getDateSection($this->dueFull, $section);

				case 'TopicExpertise':
					return getDateSection($this->dueTopicExpertise, $section);

				case 'PaperExpertise':
					return getDateSection($this->duePaperExpertise, $section);

				case 'Review':
					return getDateSection($this->dueReview, $section);

				case 'Group':
					return getDateSection($this->dueGroup, $section);

				case 'Global':
					return getDateSection($this->dueGlobal, $section);

				case 'Final':
					return getDateSection($this->dueFinal, $section);
			}
		}

		// Return an array of Rid, Pid, level triples.
		public function getAssignments() {

			$return = array();
			$query = "
			select
				a.ReviewerID as ReviewerID, a.PaperID as PaperID, b.PE as PE
			from
				Assignment a, Bid b
			where
				a.TrackID = $this->id and
				b.TrackID = $this->id and
				a.ReviewerID = b.ReviewerID and
				a.PaperID = b.PaperID";
			//$query = "select ReviewerID, PaperID from Assignment where TrackID = 1";
			DB::query($query, &$result, 'Track::getAssignments()');

			while ($row = $result->fetch_assoc()) {
				$a[0] = $row['ReviewerID'];
				$a[1] = $row['PaperID'];
				//$a[2] = $row['PE'];
				$return[] = $a;
			}

			return $return;
		}

		// Return an array to be used for manaul assignment and selection
		public function getAssignmentsByPaper() {

			// get paper info
			$query = "
			select p.ID, p.Title, u.FName, u.LName, p.IsDiscussionInitiated
			from Paper p, Writes w, Users u
			where
				p.TrackID = 1 and
				p.ID = w.PaperID and
				w.AuthorID = u.ID and
				p.PaperStatus <> -1 
				order by 
					p.ID asc, 
					w.Serial asc";
			DB::query($query, &$result, 'Track::getAssignmentsByPaper()');
			while ($row = $result->fetch_assoc()) {

				$a0[] = array(
					'pId'=>$row['ID'],
					'title'=>$row['Title'],
					'aName'=>$row['FName'].' '.$row['LName'],
					'isDiscussionInitiated' => $row['IsDiscussionInitiated']);
			}

			// get reviviewer info
			$query = "
			select
				a.PaperID, u.ID, u.FName, u.LName, r.NumAssigned, b.PE
			from
				Assignment a, Users u, Reviewer r, Bid b
			where
				a.TrackID = 1 and
				a.ReviewerID = u.ID and
				a.ReviewerID = r.ID and
				a.ReviewerID = b.ReviewerID and
				a.PaperID = b.PaperID";
			$query = "
			select pu.PaperID, pu.ID, pu.FName, pu.LName, pu.NumAssigned, b.PE
			from
				(select
					a.PaperID, u.ID, u.FName, u.LName, r.NumAssigned
				from
					Assignment a, Users u, Reviewer r
				where
					a.TrackID = 1 and
					a.ReviewerID = u.ID and
					a.ReviewerID = r.ID) pu left join Bid b
				on
					(pu.ID = b.ReviewerID and
					pu.PaperID = b.PaperID)";

			//echo($query);
			DB::query($query, &$result, 'Track::getAssignmentsByPaper()');
			while ($row = $result->fetch_assoc()) {
				$pe = $row['PE'] == null ? 0:$row['PE'];
				$a1[] = array(
					'pId'=>$row['PaperID'],
					'rId'=>$row['ID'],
					'numAssigned'=>$row['NumAssigned'],
					'rName'=>$row['FName'].' '.$row['LName'],
					'level'=>$pe
				);
			}

			// build a2 to be returned: <pId>, sumOfPE, title, aNames, {<rId>, rName, numOfAssignments, level}

			// extract author info
			if ($a0 != null) {
				foreach ($a0 as $a) {
					if (isset($a2[$a['pId']])) {
						$a2[$a['pId']][2] .= ', '.$a['aName'];
					} else {

						$a2[$a['pId']][0] = 0;
						$a2[$a['pId']][1] = $a['title'];
						$a2[$a['pId']][2] = $a['aName'];
					}
					$a2[$a['pId']][4] = $a['isDiscussionInitiated'];
				}
			}

			// extract reviewer info
			if ($a1 != null) {
				foreach ($a1 as $a) {
					$a2[$a['pId']][0] += $a['level'];
					$a2[$a['pId']][3][$a['rId']] = array($a['rName'], $a['numAssigned'], $a['level']);
				}
			}

			if ($a2 != null)
				asort($a2);
//			printArray($a2);
			return $a2;
		}

		// a1: <pId>, {<rId>, 'rName, numOfAssignments, level'}
		public function getReviewerValabsByPaper() {

			$query = "
			select pu.PaperID, pu.ID, pu.FName, pu.LName, pu.NumAssigned, b.PE
			from
				(select
					a.PaperID, u.ID, u.FName, u.LName, r.NumAssigned
				from
					Assignment a, Users u, Reviewer r
				where
					a.TrackID = 1 and
					a.ReviewerID = u.ID and
					a.ReviewerID = r.ID) pu left join Bid b
				on
					(pu.ID = b.ReviewerID and
					pu.PaperID = b.PaperID)";

		}
		public function getReviewerValab($paperId) {

			$query = "
			select
				ur.UserID, r.NumAssigned, u.FName, u.LName, b.PE
			from
				UserRole ur, Reviewer r, Users u, Bid b
			where
				ur.RoleID = 30 and
				b.PaperID = $paperId and
				b.PE <> -1 and
				ur.UserID = r.ID and
				ur.UserID = u.ID and
				ur.UserID = b.ReviewerID
			order by b.PE desc, r.NumAssigned";

			$query = "
			select pu.PaperID, pu.ID, pu.FName, pu.LName, pu.NumAssigned, b.PE
			from
				(select
					a.PaperID, u.ID, u.FName, u.LName, r.NumAssigned
				from
					Assignment a, Users u, Reviewer r
				where
					a.TrackID = 1 and
					a.ReviewerID = u.ID and
					a.ReviewerID = r.ID) pu left join Bid b
				on
					(pu.ID = b.ReviewerID and
					pu.PaperID = b.PaperID)";

			DB::query($query, &$result);
			$a[0] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Delete -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			while ($row = $result->fetch_assoc()) {
				$a[$row['UserID']] = $row['UserID'].' '.$row['FName'].' '.$row['LName'].', '.
				$row['NumAssigned'].', '.$row['PE'];
			}

			return $a;
		}

/*
		public function updateAssi($assi) {

			$q1 = "delete from Assignment where ";
			$q2 = "insert into assignment values ";

			foreach ($assi as $a) {

				$pId = $a[0];
				$rId = $a[1];
				$q1 .= "PaperID = $pId or ";
				$q2 .= "($rId, $pId, $this->id),";
			}
			$q1 = substr($q1, 0, strlen($q1) - 4);
			//echo '<br />'.$q2;
			$q2 = substr($q2, 0, strlen($q2) - 1);
			//echo '<br />'.$q2;

			// delete assignemts for all papers in $assi
			DB::query($q1, &$result);

			// add assignments for all papers in $assi
			DB::query($q2, &$result);

			// recalculate Reviewer.NumAssigned and Paper.NumAssi
			$this->updateNumAssi();
		}
*/

		// recalculate Reviewer.NumAssigned and Paper.NumAssi
		public function updateNumAssi() {

			$q = "update Paper set NumAssi = 0";
			DB::query($q, &$result);

			$q = "update Reviewer set NumAssigned = 0";
			DB::query($q, &$result);

			$q = "select ReviewerID, count(PaperID) as cnt from Assignment where TrackID = $this->id group by ReviewerID";
			DB::query($q, &$result1);
			while ($row = $result1->fetch_assoc()) {
				DB::query("update Reviewer set NumAssigned = ".$row['cnt'].' where ID = '.$row['ReviewerID'],
				&$result);
			}

			$q = "select PaperID, count(ReviewerID) as cnt from Assignment where TrackID = $this->id group by PaperID";
			DB::query($q, &$result2);
			while ($row = $result2->fetch_assoc()) {
				DB::query("update Paper set NumAssi = ".$row['cnt'].' where ID = '.$row['PaperID'],
				&$result);
			}
		}

		// Return an array for manual assignment by reviewers
		// Including reviewers who has no assignments.
		// <rId>, numAssigned, name, org, email, te:{<topicId>, topicExpertise}, numFinished
		public function getReviewerDetails() {

			$q = "
			select
				ur.UserID, u.FName, u.LName, r.NumAssigned, r.TopicID, r.TE, r.NumFinished, u.Org, u.Email
			from
				UserRole ur, Users u,
				(select r.ID, r.NumAssigned, r. NumFinished, e.TopicID, e.TE from Reviewer r left join
					(select * from Expertise where TrackID = $this->id) e on (r.ID = e.ReviewerID)) r
			where
				ur.RoleID = 30 and
				ur.UserID = u.ID and
				u.ID = r.ID
			order by r.NumAssigned";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {

				$a[$row['UserID']]['numAssigned'] = $row['NumAssigned'];
				$a[$row['UserID']]['numFinished'] = $row['NumFinished'];
				$a[$row['UserID']]['name'] = $row['FName'].' '.$row['LName'];
				$a[$row['UserID']]['org'] = $row['Org'];
				$a[$row['UserID']]['email'] = $row['Email'];

				$t = $row['TopicID'];
				$e = $row['TE'];
				//echo $row['UserID'].': '.$t.' => '.$e.' ';
				if ($t != NULL) {
					$a[$row['UserID']]['te'][$t] = $e;
				} // reviewers that have zero 'te's appear above those that do have, despite the asort of $a.

				if (isset($a[$row['UserID']]['te']))
					arsort($a[$row['UserID']]['te']);
			}
//			if ($a != null)
//				asort($a);
			return $a;
		}

		// Return an array for manual assignment by reviewers
		// Only reviewers who has assignments
		// <rId>, <pId>, PE, numAssi, title, {<topicId>, isPrimary}, progress, score, PE2 order by rId
		public function getReviewerPapers() {

			// add status and score to be used in c_review.php
			$q = "
			select
				aaa.ReviewerID, aaa.ID, aaa.Title, aaa.TopicID,
				aaa.IsPrimary, aaa.NumAssi, aaa.PE,
				rev.Progress, rev.c$this->overallId, rev.c$this->reviewerExpertiseId 
			from

				(select
					aa.ReviewerID, aa.ID, aa.Title, aa.TopicID,
					aa.IsPrimary, aa.NumAssi, b.PE
				from
					(select a.ReviewerID, p.ID, p.Title, pt.TopicID, pt.IsPrimary, p.NumAssi
					from Paper p, PaperTopic pt, Assignment a
					where
						p.TrackID = 1 and
						pt.TrackID = 1 and
						a.TrackID = 1 and
						a.PaperID = p.ID and
						p.ID = pt.PaperID) aa
						left join Bid b	on
							aa.ID = b.PaperID and
							aa.ReviewerID = b.ReviewerID
				order by ReviewerID, ID) aaa

				left join

				(select rev.ReviewerID, rev.PaperID, rev.Progress, rt.c$this->overallId, rt.c$this->reviewerExpertiseId  
				from Review rev right join ReviewTrack1 rt
				on (rev.ReviewerID = rt.ReviewerID and rev.PaperID = rt.PaperID) where rev.TrackID = 1) rev

				on (aaa.ReviewerID = rev.ReviewerID and aaa.ID = rev.Paperid)";

			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {

				$rId = $row['ReviewerID'];
				$pId = $row['ID'];
				$a[$rId][$pId]['PE'] = $row['PE'];
				$a[$rId][$pId]['numAssi'] = $row['NumAssi'];
				$a[$rId][$pId]['title'] = $row['Title'];
				$a[$rId][$pId]['topics'][$row['TopicID']] = $row['IsPrimary'];
				$a[$rId][$pId]['progress'] = $row['Progress'];
				$a[$rId][$pId]['score'] = $row["c$this->overallId"];
				
				// the expertise specified in the review (instead of in the bidding)
				// have to make 'Reviewer Expertise' compulsory in the review form. todo
				$a[$rId][$pId]['PE2'] = $row["c$this->reviewerExpertiseId"];	
				
				asort($a[$rId][$pId]);
			}
			return $a;
		}

		// Used in c_assignment_m_x.php
		public function getPaperDetails() {

			$q = "
			select
				p.ID PID, p.Title, p.Abstract, p.NumAssi,
				u.ID UID, u.FName, u.LName,
				pt.TopicID,
				w.Serial
			from
				Paper p, Users u, PaperTopic pt, Writes w
			where
				p.TrackID = 1 and
				pt.TrackID = 1 and
				p.ID = w.PaperID and
				pt.IsPrimary = 1 and
				w.AuthorID = u.ID and
				p.ID = pt.PaperID
			order by
				p.ID, w.Serial";

			// extend for c_authors.php
			// a little bit slower than the one above
			$q = "
			select
				p.ID PID, p.Title, p.Abstract, p.NumAssi, p.PaperStatus, p.Full1stSub, p.Final1stSub,
				u.ID UID, u.FName, u.LName,
				pt.TopicID,
				w.Serial, w.IsContactAuthor
			from
				Paper p, Users u, PaperTopic pt, Writes w
			where
				p.TrackID = 1 and
				pt.TrackID = 1 and
				p.ID = w.PaperID and
				pt.IsPrimary = 1 and
				w.AuthorID = u.ID and
				p.ID = pt.PaperID
			order by
				p.ID, w.Serial";

			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {

				$pId = $row['PID'];

				$a[$pId]['priTopicId'] = $row['TopicID'];
				$a[$pId]['title'] = $row['Title'];
				$a[$pId]['abstract'] = $row['Abstract'];
				$a[$pId]['numAssi'] = $row['NumAssi'];
				$a[$pId]['names'] .= $row['FName'].' '.$row['LName'].', ';

				$a[$pId]['paperStatus'] = $row['PaperStatus'];
				$a[$pId]['full1stSub'] = $row['Full1stSub'];
				$a[$pId]['final1stSub'] = $row['Final1stSub'];
			}
			if ($a != null)
				asort($a);
			//var_dump($a); echo '<br />';
			return $a;
		}
		public function addAssi($rId, $pId) {

			DB::query("insert into Assignment set ReviewerID = $rId, PaperID = $pId, TrackID = $this->id",
			&$result);
			$this->updateNumAssi();
		}

		public function delAssi($rId, $pId) {
			
			DB::query("delete from Delegate where FromID = $rId and PaperID = $pId", &$result);
			DB::query("delete from Assignment where ReviewerID = $rId and PaperID = $pId and TrackID = $this->id",
			&$result);
			$this->updateNumAssi();
		}

		// paper selection
		// reututn an array: <pId>, <rId>, score, progress
		// only for papers that HAVE reviews.
		public function getReviewScore() {

			// with progress
			$q = "
			select rt.ReviewerID, rt.PaperID, rt.c$this->overallId, r.Progress
			from ReviewTrack1 rt, Review r
			where rt.ReviewerID = r.ReviewerID and rt.PaperID = r.PaperID";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['PaperID']][$row['ReviewerID']]['score'] = $row["c$this->overallId"];
				$a[$row['PaperID']][$row['ReviewerID']]['progress'] = $row['Progress'];
/*
				$a[$row['PaperID']][$row['ReviewerID']]['expertise'] = $row["c$this->reviewerExpertiseId"];
*/
			}
			return $a;
		}

		public function getDecisionValab() {
			DB::query("select ID, Name from PaperStatus", &$result);
			while ($row = $result->fetch_assoc()) {
				if ($row['ID'] != -1)
					$a[$row['ID']] = $row['Name'];
			}
			return $a;
		}

		public function getDecisions() {
			DB::query("select ID, PaperStatus from Paper where TrackID = $this->id", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['ID']] = $row['PaperStatus'];
			}
			//var_dump($a);
			return $a;
		}

		public function updateDecision($decisionArray) {

			foreach ($decisionArray as $pId => $decision) {
				DB::query("update Paper set PaperStatus = '$decision' where ID = $pId", &$result);
			}
		}

		public function getAuthorsWithNoSubmissions() {

			// such users are only in UserRole but not in Writes

			$q = "
			select ur.UserID from UserRole ur
			where
				ur.RoleID = 40 and
				ur.UserID not in
					(select AuthorID from Writes)";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = new Author($row['UserID']);
			}
			//var_dump($a); exit;
			return $a;
		}

		// return an array of Users according to the specified event $n
		public function getUsers($n) {

			switch ($n) {

				case 'all':
					$q = "
					select ID UserID from Users";
					break;

				// chairs
				case 'allChair':
					$q = "
					select UserID from UserRole
					where RoleID = 20";
					break;

				// reviewers
				case 'allReviewer':	// to all reviewers
				case 'bidOpen':		// to all reviewers
				case 'reviewOpen':	// to all reviewers
					$q = "
					select UserID from UserRole
					where RoleID = 30";
					break;
					
				case 'bidLate':		
					$q = "
					select UserID from UserRole 
					where 
						RoleID = 30 and 
						UserID not in (select ReviewerID from Bid)";
					break;
					
				case 'reviewersWithAssi':	// whose NumAssigned > 0
/*
					$q = "
					select ID from Reviewer // as UsereID!!
					where NumAssigned > 0";
*/
					$q = "
					select ID as UserID from Reviewer 
					where NumAssigned > 0 and ID in (select UserID from UserRole where RoleID = 30);";
					break;
					
				case 'reviewLate':
					$q = "
					select ID UserID from Reviewer 
					where (NumAssigned - NumFinished) > 0";
					break;

				// external reviewers

				case 'allER':
					$q = "
					select UserID from UserRole
					where RoleID = 31";
					break;

				// authors

				case 'allAuthor':
					$q = "
					select UserID from UserRole
					where RoleID = 40";
					break;

				case 'allContactAuthor':
					$q = "
					select distinct ur.UserID
					from UserRole ur, Writes w
					where
						ur.UserID = w.AuthorID and
						w.IsContactAuthor = 1";
					break;

				case 'abstOpen':	// to registered authors who have not submitted any abstracts
					$q = "
					select ur.UserID from UserRole ur
					where
						ur.RoleID = 40 and
						ur.UserID not in
							(select AuthorID from Writes)"; // cannot be there, just to be sure
					break;

				case 'abstLate':	// to registered authors who are late in abstract submission
									// probably better to inform all contact authors, since,
									// what if an author plans to submit another paper?
					$q = "
					select ur.UserID from UserRole ur
					where
						ur.RoleID = 40 and
						ur.UserID not in
							(select AuthorID from Writes)";
					break;

				case 'fullOpen':	// to all contact authors
					$q = "
					select distinct AuthorID UserID from Writes
					where
						IsContactAuthor = 1";
					break;

				case 'fullLate':
					$q = "
					select distinct
						w.AuthorID UserID
					from
						Paper p, Writes w
					where
						w.PaperID = p.ID and
						w.IsContactAuthor = 1 and
						p.FullFormat is NULL and
						p.PaperStatus <> -1";
//						p.PaperStatus not like 'Withdrawn'";
					break;

				case 'decision':	// to all contact authors
					$q = "
					select AuthorID UserID from Writes
					where IsContactAuthor = 1";
					break;

				case 'accepedContactAuthor':
				case 'finalOpen':
					$q = "
					select distinct
						w.AuthorID UserID
					from
						Writes w, Paper p, PaperStatus ps
					where
						w.PaperID = p.ID and
						w.IsContactAuthor = 1 and
						p.PaperStatus = ps.ID and
						ps.FinalUpload = 1";
//						p.PaperStatus like 'Accept%'"; 
					break;

				case 'rejectedContactAuthor':
					$q = "
					select distinct
						w.AuthorID UserID
					from
						Writes w, Paper p
					where
						w.PaperID = p.ID and
						w.IsContactAuthor = 1 and
						p.PaperStatus = 2";
//						p.PaperStatus like 'Reject%'";
					break;

				case 'finalLate':	// to all contact authors who have at least one paper accepted

					$q = "
					select w.AuthorID UserID
					from Paper p, Writes w, PaperStatus ps
					where
						p.ID = w.PaperID and
						w.IsContactAuthor = 1 and
						p.FinalFormat is NULL and
						p.PaperStatus = ps.ID and
						ps.FinalUpload = 1";
//						p.PaperStatus like '%cept%'";
					break;

			} // switch

			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				
//				$a[] = new Author($row['UserID']);
				$a[] = new Author($row['UserID']);
			}

			return $a;
		}


		// <aId>, name, {pId}
		// not including registered authors who are not involved in any submissions
		public function getAuthorPaper() {
			$q = "
			select w.AuthorID, w.PaperID, u.FName, u.LName, u.Org, u.Email
			from Writes w, Users u
			where w.AuthorID = u.ID
			order by AuthorID";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['AuthorID']]['name'] = $row['FName'].' '.$row['LName'];
				$a[$row['AuthorID']]['papers'][] = $row['PaperID'];
				$a[$row['AuthorID']]['org'] = $row['Org'];
				$a[$row['AuthorID']]['email'] = $row['Email'];
			}
			//var_dump($a); exit;
			return $a;
		}

		public function searchUser($role, $key) {

			$key = trim($key);

			if (strlen($key) == 0)
				return null;
			if (!(strpos($key, '%') === false))
				return null;
			if (!(strpos($key, '_') === false))
				return null;

			$keyArray = explode (' ', $key);
			foreach ($keyArray as $k)
				if ($k != '')	// why not ' '?
					$where .= " and concat(u.FName, u.LName, u.Email) like '%$k%' ";
			$q =
			"select u.ID from Users u, UserRole ur
			where ur.RoleID = 40 and ur.UserID = u.ID $where limit 50";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['ID'];
			}
			return $a;
		}

		public function getChairIds() {

			DB::query("select ChairID from Coordinate where TrackID = $this->id", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['ChairID'];
			}
			return $a;
		}

		// same across all tracks
		// <id>, name, isDefault
		public function getPrivilegeValab() {

			DB::query("select ID, Name from Privilege", &$result);
			while ($row = $result->fetch_assoc()) {
				if ($row['Name'] != 'Group Emailing')
					$a[$row['ID']] = $row['Name'];
			}
			return $a;
		}

		public function addTopic($name) {

			DB::query("select count(*) from Topic", &$result);
			$n = DB::getCell(&$result) + 1;

			DB::query("select max(ID) from Topic where TrackID = $this->id", &$result);
			$next = DB::getCell(&$result)  + 1;

			$q = "insert into Topic set TrackID = $this->id, ID = $next, Name = '$name',
			Serial = $n";
			DB::query($q, &$result);

		}

		public function deleteTopic($id) {

			DB::query("select Serial from Topic where ID = $id", &$result);
			$serial = DB::getCell(&$result);

			$q = "delete from Topic where TrackID = $this->id and ID = $id";
			DB::query($q, &$result);

			DB::query("update Topic set Serial = Serial - 1 where Serial > $serial", &$result);
		}

		public function moveTopic($x, $dir, $y) {

			//echo "x$x dir$dir y$y"; //exit;
			DB::query("select count(*) from Topic", &$result);
			$n = DB::getCell(&$result);

			if (($x == $y) ||
				($x == $y + 1 && $dir == 1) ||
				($x == $y - 1 && $dir == -1) ||
				($y < 1) ||
				($y > $n)) {
					return;			// no move

			} elseif ($x > $y) {
				$action = -1;		// move to the front
				if ($dir == 1)
					$y = $y + 1;

			} elseif ($x < $y) {
				$action = 1; 		// move to the end
				if ($dir == -1)
					$y = $y - 1;
			}

			if ($action == -1) {
				DB::query("update Topic set Serial = 0 where Serial = $x", &$result);
				$q = "update Topic set Serial = Serial + 1
				where Serial < $x and Serial >= $y";
				DB::query($q, &$result);
				DB::query("update Topic set Serial = $y where Serial = 0", &$result);
			}

			if ($action == 1) {
				DB::query("update Topic set Serial = 0 where Serial = $x", &$result);
				$q = "update Topic set Serial = Serial - 1
				where Serial <= $y and Serial > $x";
				DB::query($q, &$result);
				DB::query("update Topic set Serial = $y where Serial = 0", &$result);
			}
		}

		public function getPapersByStatus($status) {

			$q = "select ID from Paper where PaperStatus = $status";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['ID']] = $status;
			}
			return $a;
		}

		public function getPapers($type) {

			switch ($type) {

			case 'decided':	// return both ID and PaperStatus

				$q = "select ID, PaperStatus from Paper where PaperStatus <> -1 and PaperStatus <> 0";
				DB::query($q, &$result);
				while ($row = $result->fetch_assoc()) {
					$a[$row['ID']] = $row['PaperStatus'];
				}
				return $a;

			case 'fullOpen':
			case 'fullLate':

				$q = "select ID from Paper where PaperStatus <> -1 and FullFormat is NULL";
				break;

			case 'finalOpen':
			case 'finalLate':

				$q = "
				select p.ID
				from Paper p, PaperStatus ps
				where
					p.PaperStatus = ps.id and
					ps.FinalUpload = 1 and
					p.FinalFormat is NULL";
				break;

			} // switch

			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['ID'];
			}
			return $a;
		}

		public function statistics($type, $subtype = null) {

			// all assume single track

			switch ($type) {
				case 'author':
					switch ($subtype) {
						case 'registered':
							$q = "select count(*) from UserRole where RoleID = 40";
							break;
						case 'haveSubmission':
							$q = "select count(*) from UserRole
							where RoleID = 40 and UserID in (select AuthorID from Writes)";
							break;
						case 'contact':
							$q = "select count(*) from UserRole
							where RoleID = 40 and UserID in (select AuthorID from Writes where IscontactAuthor = 1)";
							break;
					}
					break;

				case 'abstractSubmitted':
					$q = "select count(*) from Paper";
					break;

				case 'fullSubmitted':
					$q = "select count(*) from Paper where Full1stSub <> 'NULL'";
					break;

				case 'finalSubmitted':
					$q = "select count(*) from Paper where Final1stSub <> 'NULL'";
					break;

				case 'byPaperStatus':
					$q = "select count(*) from Paper where PaperStatus = $subtype";
					break;

				case 'reviewer':
					switch ($subtype) {
						case 'reviewer':
							$q = "select count(*) from UserRole where RoleID = 30";
							break;
						case 'bid':
							$q = "select count(*) from Bid where PE <> -1";
							break;
						case 'er':
							$q = "select count(*) from UserRole where RoleID = 31";
							break;
						case 'delegation':
							$q = "select count(*) from Delegate";
							break;
					}
					break;

				case 'review':
					if ($subtype == 'assigned') {
						$q = "select count(*) from Assignment";
					} else {
						if ($subtype == 'In Review') 
							$q = "select count(*) from Review 
							where Progress in ('In Review', 'Delegate Finished')";
						else
							$q = "select count(*) from Review where Progress = '$subtype'";
					}
					break;
			}

			DB::query($q, &$result);
			return DB::getCell(&$result);
		}
		
		
		public function setPaperStatus($flag) {
			
			DB::query("update Track set ReleasePaperStatus = $flag where ID = 1", &$result);
		}

	}
?>
