<?php
	class Conference {

		//public $properties;
		public $id;
		public $fullName;
		public $shortName;
		public $url;
		public $contactEmail;
		public $fromEmail;
		public $replyEmail;
		public $CC;
		public $BCC;
		public $logEmail;
		public $startDate;
		public $endDate;
		public $venue;
		public $isMultiTrack;
		public $isConfigured;
		public $isActive;


		function Conference($ci = null) {	// ciX

			if ($ci != null) {

				if (!is_numeric($ci))
					$id = substr($ci, 2, strlen($ci) - 2);
				else
					$id = $ci;

				DB::query("select * from Conference where ID = $id", &$result, 'Conference::Conference()');
				$row = $result->fetch_object();

				if ($row == null)
					throw new Exception('conference instance does not exist');
				$this->id = $row->ID;
				$this->fullName = $row->FullName;
				$this->shortName = $row->ShortName;
				$this->url = $row->URL;
				$this->contactEmail = $row->ContactEmail;
				$this->fromEmail = $row->FromEmail;
				$this->replyEmail = $row->ReplyEmail;
				$this->CC = $row->CC;
				$this->BCC = $row->BCC;
				$this->logEmail = $row->LogEmail;
				$this->startDate = $row->StartDate;
				$this->endDate = $row->EndDate;
				$this->venue = $row->Venue;
				$this->isMultiTrack = $row->IsMultiTrack;
				$this->isConfigured = $row->IsConfigured;
				$this->isActive = $row->IsActive;
			}
		}

		public function add() {
			// add entry in Common.Conference
			// create conference instance database and its directory structures
			// notify chair

		}

		public function update() {

			$q = "
			update Conference set
				FullName = '$this->fullName',
				ShortName = '$this->shortName',
				URL = '$this->url',
				ContactEmail = '$this->contactEmail',
				FromEmail = '$this->fromEmail',
				ReplyEmail = '$this->replyEmail',
				CC = '$this->CC',
				BCC = '$this->BCC',
				LogEmail = $this->logEmail
			where
				ID = $this->id";

			DB::query($q, &$result);
		}

		public function isMultiTrack() {

			return $this->isMultiTrack == 1 ? true:false;
		}

		public function getConferenceLink() {

			return "[".date('Y-m-d').' '.SERVER_TIMEZONE."] [<a href=$this->url>Conference Website</a>]<br />";
		}

		public function getTracks() {


		}

		public function getOpenTracks() {

		}

		public function getAll() { 	// for Administrator

		}

		public function activate() {

			DB::query("update Conference set IsActive = 1 where ID = $this->id", &$result);
		}

		public function deactivate() {

			DB::query("update Conference set IsActive = 0 where ID = $this->id", &$result);
		}
	}
?>