<?php

	class Administrator {

		public $sqlCreateCI;
		public $sqlPopulateCI;
		public $sqlCreateForm;

		public function Administrator() {
			$this->sqlCreateCI = file_get_contents('./includes/create_ci.sql');
			$this->sqlPopulateCI = file_get_contents('./includes/populate_ci.sql');
			$this->sqlCreateForm = file_get_contents('./includes/create_form.sql');
			$this->sqlCreateCI = str_replace("\\", "", $this->sqlCreateCI);
			$this->sqlPopulateCI = str_replace("\\", "", $this->sqlPopulateCI);
			$this->sqlCreateForm = str_replace("\\", "", $this->sqlCreateForm);
//			echo $this->sqlCreateForm; exit;
		}

		public function addNewCI(Conference $conf, Chair $chair, &$cId) {

			DB::connect('common');
			DB::autocommit(false);

			// get next ci number

			DB::query("select max(ID) from Conference", &$result);
			$maxId = DB::getCell(&$result);
			if ($maxId == null)
				$maxId = 0;
			$id = $maxId + 1;
			$cId = $id;

			// add the new ci in Conference

//			$conf->shortName = $conf->shortName.' '.$id;
			DB::query(
			"insert into Conference set
				ID = $id,
				ShortName = '$conf->shortName',
				Fullname = '$conf->fullName',
				ContactEmail = '$conf->contactEmail',
				FromEmail = '$conf->fromEmail',
				LogEmail = 1,
				IsActive = 1",
			&$result);
			
			DB::commit();	// common

		
			// create the new ci database

			DB::query("create database ".DB_PREFIX."ci$id", &$result);
			DB::connect("ci$id");

			// grant access right
/*			
			Do not have to do this here, privilege granted for all cms_* databases in create_user.
			DB::query("grant all on ".DB_PREFIX."ci$id.* to ".DB_USER.'@'."'".DB_HOST."'".
			" identified by '".DB_PASSWORD."'", &$result);
*/

			DB::autocommit(false);

			$sqlArray = explode(';', $this->sqlCreateCI);
			foreach ($sqlArray as $sql) {
				$sql = trim($sql);
				if (strlen($sql) > 0)
					DB::query(trim($sql), &$result);
			}

			// populate the new ci

			$sqlArray = explode(';', $this->sqlPopulateCI);
			foreach ($sqlArray as $sql) {
				$sql = trim($sql);
				if (strlen($sql) > 0)
					DB::query(trim($sql), &$result);
			}

			DB::query("
				INSERT INTO Track SET
				  ID = NULL,
				  Name = 'Track 01',
				  IsBlindReview = 1,
				  IsTwoPhase = 1,
				  DueAbst = '2008-01-01',
				  DueFull = '2008-01-01',
				  DueTopicExpertise = '2008-01-01',
				  DuePaperExpertise = '2008-01-01',
				  DueReview = '2008-01-01',
				  DueGroup = '2008-01-01',
				  DueGlobal = '2008-01-01',
				  DueFinal = '2008-01-01',
				  PhAbst = 0,
				  PhFull = 0,
				  PhTopicExpertise = 0,
				  PhPaperExpertise = 0,
				  PhReview = 0,
				  PhGroup = 0,
				  PhGlobal = 0,
				  PhFinal = 0,
				  RevPerPaper = 3,
				  MaxPaperPerRev = 8,
				  AutoAcceptThreshold = 6.0,
				  ReportStatus = '10000000000',
				  ReleasePaperStatus = 0", &$result);

			DB::query("INSERT INTO TrackPaperFormat VALUES (1, '.pdf', 'full')", &$result);
			DB::query("INSERT INTO TrackPaperFormat VALUES (1, '.doc', 'full')", &$result);
			DB::query("INSERT INTO TrackPaperFormat VALUES (1, '.ps', 'full')", &$result);
			DB::query("INSERT INTO TrackPaperFormat VALUES (1, '.pdf', 'final')", &$result);
			DB::query("INSERT INTO TrackPaperFormat VALUES (1, '.ps', 'final')", &$result);

			// create chair's account and add previleges

			$chair->generateUid();
			$chair->pwd = $chair->generatePwd();
			$chair->pwd = '123456';
			$sql = "
				INSERT INTO Users SET
				  ID = NULL,
				  UID = '$chair->uid',
				  Pwd = sha1('$chair->pwd'),
				  FName = '$chair->fname',
				  MName = '$chair->mname',
				  LName = '$chair->lname',
				  Email = '$chair->email',
				  Org = '$chair->org',
				  Country = '$chair->country',
				  PwdSent = 1";
			DB::query($sql, &$result); // Pwd is 123456 hashed using sha1

			DB::query("INSERT INTO UserRole VALUES (1, 20)", &$result);
			DB::query("INSERT INTO Chair VALUES (1)", &$result);
			DB::query("INSERT INTO Coordinate VALUES (1, 1)", &$result);

			DB::query("
				INSERT INTO TrackPrivilege VALUES
				   (1, 1, 200),
				   (1, 1, 201),
				   (1, 1, 202),
				   (1, 1, 203),
				   (1, 1, 204),
				   (1, 1, 220),
				   (1, 1, 221),
				   (1, 1, 230),
				   (1, 1, 231),
				   (1, 1, 240),
				   (1, 1, 250),
				   (1, 1, 260),
				   (1, 1, 270)",
				&$result);

			// create review form

			$sqlArray = explode(';', $this->sqlCreateForm);
			foreach ($sqlArray as $sql) {
				$sql = trim($sql);
				if (strlen($sql) > 0)
					DB::query(trim($sql), &$result);
			}

			// update OverallID and ReviewerExpertiseID

			DB::query("select ID, Question from FormQuestion", &$result);
			$overallId = null;
			$reviewerExpertiseId = null;
			while ($row = $result->fetch_assoc()) {
				if (strtolower($row['Question']) == 'overall') {
					$overallId = $row['ID'];
				}
				if (strtolower($row['Question']) == 'expertise of reviewer') {
					$reviewerExpertiseId = $row['ID'];
				}
			}
			if ($overallId == null)
				throw new Exception('missing overall column in review form');
			if ($reviewerExpertiseId == null)
				throw new Exception('missing reviewerExpertiseId column in review form');
				
			DB::query("update Track set OverallID = $overallId, 
			ReviewerExpertiseID = $reviewerExpertiseId", &$result);

			DB::commit();
		}
		
		public function deleteCI($id) {
			
			// clean common.Conference
			DB::connect('common');
			DB::autocommit(false);
			DB::query("delete from Conference where ID = $id", &$result);
			DB::commit();
			
			// clean instance database
			DB::query("drop database ".DB_PREFIX."ci$id", &$r);

			// clean folders
			exec('rm '.TEMPLATE_DIR."ci$id -r");
			exec('rm '.  UPLOAD_DIR."ci$id -r");

/*
			unlink("../template/ci$id/*");	
			rmdir("../template/ci$id");	
			rmdir("../cms/upload/ci$id");
*/
		}

		public function getActiveCIs() {
			DB::query("select ID, ShortName, FullName, IsActive from Conference where IsActive = 1", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row;
			}
			return $a;
		}

		public function getCIs() {
			DB::query("select ID, ShortName, FullName, IsActive from Conference", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row;
			}
			return $a;
		}
	}
?>
