<?php
	class Reviewer extends Users {

		public $numAssigned;
		public $numFinished;
		public $numDelegated;
		public $maxAssi;
		public $releaseReport;

		public function Reviewer($id = null) {

			parent::Users($id);

			if ($id != null) {

				DB::query("select NumAssigned, NumFinished, NumDelegated, MaxAssi, ReleaseReport from Reviewer
				where ID = $id", &$result);
				$row = $result->fetch_assoc();
				if ($row == null)
					throw new Exception("No reviewer has id $id");
				$this->numAssigned = $row['NumAssigned'];
				$this->numFinished = $row['NumFinished'];
				$this->numDelegated = $row['NumDelegated'];
				$this->maxAssi = $row['MaxAssi'];
				$this->releaseReport = $row['ReleaseReport'];
			}
		}

		public function add() {

			if ($this->isExistingUser(30)) {

				throw new Exception( "$this->email is already associated with a Reviewer.");
			}

			// External Reviewers cannot have any other roles
			if ($this->isExistingUser(31)) {
				throw new Exception("$this->email is already associated with an External Reviewer.");
			}

			parent::add(30);

			// Reviewer

			$query = "insert into Reviewer set ID = $this->id, NumAssigned = 0, NumFinished = 0, NumDelegated = 0";
			DB::query($query, &$result, 'Reviewer::add()');
		}

		public function getAssignments() {

			$papers = array();

			$query = "select PaperID, TrackID from Assignment where ReviewerID = $this->id order by TrackID";

			DB::query($query, &$result, 'Reviewer::getAssignments()');
			while ($row = $result->fetch_assoc()) {
				$papers[] = $row['PaperID'];
			}

			return $papers;
		}

		// has assignment
		public function hasPaper($pId) {

			$q = "select count(*) from Assignment where ReviewerID = $this->id and TrackID = 1 and
			PaperID = $pId";
			DB::query($q, &$result);
			if (DB::getCell(&$result) == 1)
				return true;
			return false;
		}

		public function getAssignmentValab() {

			$valab = array();

			$query = "
			select
				p.ID as ID, p.Title as Title
			from
				Assignment a, Paper p
			where
				a.PaperID = p.ID and
				ReviewerID = $this->id
			";

			DB::query($query, &$result, 'Reviewer::getAssignmentValab()');
			while ($row = $result->fetch_assoc()) {
				$valab[$row['ID']] = $row['Title'];
			}

			return $valab;
		}

		public function getTopicExpertiseArray() {

			$a = array();
			DB::query(
				"select TopicID, TE from Expertise where ReviewerID = $this->id and TrackID = 1", 
				&$result
			);

			while ($row = $result->fetch_assoc()) {
				$a[$row['TopicID']] = $row['TE'];
			}			
			return $a;
		}

		// $topicExpertiseArray in the form of topicId=>topicExpertise
		public function updateTopicExpertise($trackId, $topicExpertiseArray) {

			// delete all

			DB::query("delete from Expertise where ReviewerID = $this->id and TrackID = $trackId", &$result);

			// insert all

			foreach ($topicExpertiseArray as $topicId=>$te) {

				DB::query("insert into Expertise set
					ReviewerID = $this->id,
					TrackID = $trackId,
					TopicID = $topicId,
					TE = $te", &$result);
			}
		}

		public function updatePaperExpertise($trackId, $paperExpertiseArray) {

			// delete all

			DB::query("delete from Bid where ReviewerID = $this->id and TrackID = $trackId", &$result);

			// insert all

			foreach ($paperExpertiseArray as $paperId=>$pe) {

				if ($pe != 0) {
					DB::query("insert into Bid set
						ReviewerID = $this->id,
						PaperID = $paperId,
						TrackID = $trackId,
						PE = $pe", &$result);
				}
			}

		}

		public function delegate($paperId, $erId) {

			DB::query("insert into Delegate set FromID = $this->id, ToID = $erId, PaperID = $paperId", &$Rresult);

		}

		public function getToName($paperId) {

			$toId = $this->getToId($paperId);
			if ($toId != NULL) {
				$r = new ExternalReviewer($toId);
				return (DEBUG ? "$toId. ":'').$r->getName();
			} else
				return NULL;
		}

		public function getToId($paperId) {

			DB::query("select ToID from Delegate where PaperID = $paperId and FromID = $this->id", &$result);
			$toId = DB::getCell(&$result);
			return $toId;
		}

		public function getERs() {

			DB::query("select InviteeID from Invite where InviterID = $this->id", &$result);
			while ($row = $result->fetch_assoc()) {
				$return[] = $row['InviteeID'];
			}
			return $return;
		}

		// <pId>, level
		public function getBidExpertise($trackId) {

			$q = "select PaperID, PE from Bid where TrackID = $trackId and ReviewerID = $this->id";
			DB::query($q, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['PaperID']] = $row['PE'];
			}
			return $a;
		}

		public function getPaperExpertise($pId) {

			$q = "select PE from Bid where TrackID = 1 and ReviewerID = $this->id and PaperID = $pId";
			DB::query($q, &$result);
			return DB::getCell(&$result);
		}

	}
?>
