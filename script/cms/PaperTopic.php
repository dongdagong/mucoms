<?php
	class PaperTopic {

		public $paperId;
		public $trackId;
		public $topicId;
		public $isPrimary;

		public function add($paperId, $trackId, $topicId, $isPrimary) {

			$query =
			"INSERT INTO PaperTopic SET
				PaperID = $paperId,
				TrackID = $trackId,
				TopicID = $topicId,
				IsPrimary = $isPrimary";

			DB::query($query, &$result, 'PaperTopic::add()');
		}

		public function update($paperId, $trackId, $topicId, $isPrimary) {

			$query =
			"update PaperTopic set
				TopicID = $topicId
			where
				PaperID = $paperId,
				TrackID = $trackId,
				IsPrimary = $isPrimary";
			DB::query($query, &$result, 'PaperTopic::update()');
		}

		// return ids of paper having topicId as the primary topic
		public function getPaperIds($trackId, $topicId) {

			$paperIdArray = array();
			$query = "select PaperID from PaperTopic where TrackID = $trackId and
			TopicID = $topicId and IsPrimary = 1";
			DB::query($query, &$result, 'PaperTopic::getPaperIds()');

			for ($i = 1; $i <= $result->num_rows; $i++) {
				$row = $result->fetch_object();
				if ($row == NULL)
					throw new Exception("PaperTopic::getePaperIds(): Error fetching query result");
				$topicArray[] = $row->PaperID;
			}
			return $topicArray;
		}
	}
?>