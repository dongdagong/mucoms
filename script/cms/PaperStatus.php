<?php
	class PaperStatus {

		public $id;
		public $name;
		public $isEditable;
		public $template;
		public $sendNotification;
		public $finalUpload;

		public $templateContent;

		public function PaperStatus($id = null) {
			if ($id === null) return;	// !! == null won't display for id = 0

			DB::query("select * from PaperStatus where ID = $id", &$result);
			$row = $result->fetch_object();
			if ($row == null)
				throw new Exception('no such paper');

			$this->id = $row->ID;
			$this->name = $row->Name;
			$this->isEditable = $row->IsEditable;
			$this->sendNotification = $row->SendNotification;
			$this->finalUpload = $row->FinalUpload;

			$this->template = TEMPLATE_DIR.$_SESSION['ci']."/decision_$this->id.txt";
		}

		public function add() {
			DB::query("select max(ID) from PaperStatus", &$result);
			$id = DB::getCell(&$result);
			$id++;
			DB::query("
			insert into PaperStatus set
				ID = $id,
				Name = '$this->name',
				IsEditable = $this->isEditable,
				Template = '$this->template',
				SendNotification = $this->sendNotification,
				FinalUpload = $this->finalUpload", &$result);
			$this->id = $id;

			$this->template = TEMPLATE_DIR.$_SESSION['ci']."/decision_$this->id.txt";
			if ($this->sendNotification == 1) $this->putTemplateContent();
		}

		public function delete() {
			DB::query("delete from PaperStatus where ID = $this->id", &$result,
			'Deletion fail: check whether some paper have been set to this paper status.');

			if (file_exists($this->template))
				if (!unlink ($this->template))
					throw new Exception('Error deleting template file');
		}

		public function update() {
			DB::query("
			update PaperStatus set
				Name = '$this->name',
				IsEditable = $this->isEditable,
				Template = '$this->template',
				SendNotification = $this->sendNotification,
				FinalUpload = $this->finalUpload
			where ID = $this->id", &$result);

			if ($this->sendNotification == 1) $this->putTemplateContent();
		}

		public function getPaperStatusValab() {
			DB::query("select * from PaperStatus", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[$row['ID']] = $row['Name'];
			}
			return $a;
		}

		public function getTemplateContent() {
			if (!file_exists($this->template)) return;
			$fileArray = file($this->template);
			$this->templateContent = null;
			foreach ($fileArray as $line) {
				$this->templateContent .= $line;
			}
		}

		public function putTemplateContent(){
			if (!$f = fopen($this->template, 'w'))
				throw new Exception('Cannot open template file');
			if (fwrite($f, $this->templateContent) === false)
				throw new Exception('Cannot write to template file');
			fclose($f);
		}
	}
?>