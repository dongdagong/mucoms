<?php
	class Author extends Users {

		public $isContactAuthor;
		public $serial;		// used when adding authorship ordering into writes


		public function add() {

			if ($this->isExistingUser(40)) {

				$this->id = $this->getIdByEmail();
				$this->uid = $this->getColByEmail('UID');  // for email notifications to existing users
				$this->pwd = $this->getColByEmail('Pwd');  // for email notifications to existing users
				return 1; 	// existing user who already has an Author role
			}

			// External Reviewers cannot have any other roles
			if ($this->isExistingUser(31)) {
				throw new Exception("$this->email is already associated with an External Reviewer.");
			}

			parent::add(40);

			// Author Table (must not exist in Author table)

			$query = "insert into Author values ($this->id)";
			DB::query($query, &$result, 'Author::add()');
		}

		// Return an array of IDs of papers written by this author.
		public function getPapers() {

			$papers = array();
			$query = "select PaperID from Writes where AuthorID = $this->id";

			DB::query($query, &$result, 'Author::getPapers()');
			while ($row = $result->fetch_assoc()) {
				$papers[] = $row['PaperID'];
			}

			return $papers;
		}

		public function getPapersByStatus($status) {

			$query = "
			select distinct
				p.ID
			from
				Paper p, Writes w
			where
				p.ID = w.PaperID and
				w.AuthorID = $this->id and
				p.PaperStatus = $status";

			DB::query($query, &$result);
			while ($row = $result->fetch_assoc()) {
				$papers[] = $row['ID'];
			}

			return $papers;
		}


		public function hasPaper($pId) {

			DB::query("select count(*) from Writes where AuthorID = $this->id and PaperID = $pId", &$result);
			if (DB::getCell(&$result) == 0)
				return false;
			return true;
		}

		public function isReviewReady($pId) {

			// at least one finalized review exists
//			$q = "select count(Progress) from Review where PaperID = $pId and Progress = 'Finalized'";
//			DB::query($q, &$result);
//			if (DB::getCell(&$result) == 0)
//				return false;
//			return true;
		}

		public function getNumPapers() {
			DB::query("select count(distinct PaperID) num from Writes where AuthorID = $this->id", &$result);
			return DB::getCell(&$result);
		}
	}
?>
