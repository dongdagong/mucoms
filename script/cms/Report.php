<?php

	// Stored in Track.ReportStatus. 11 characters, 1st char (0th) always set to 1.
	// The ith report corresponds to the ith char (1: activate, 0: iniactivate).

	class Report {

		public $id;
		public $status;

		public function Report($id) {
			$this->id = $id;
		}

		public function isActive() {
			$t = new Track(1);
			$this->status = $t->reportStatus;
			$s = substr($this->status, $this->id, 1);
//			echo $this->status.' '.$s.'<br />';
			return $s == 1;
		}

		public function activate() {
			$status = '11000000000';
			DB::query("update Track set ReportStatus = '$status'", &$result);
		}

		public function deactivate() {
			$status = '10000000000';
			DB::query("update Track set ReportStatus = '$status'", &$result);
		}
	}
?>