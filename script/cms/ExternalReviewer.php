<?php
	class ExternalReviewer extends Users {

		public function add($inviterId) {

			// an External Reviewer can only have this SINGLE ROLE in the system.

			if ($this->isExistingUser(20) || $this->isExistingUser(30) || $this->isExistingUser(40)) {

				throw new Exception("$this->email is already associated with an existing non External Reviewer.");

			} else {	// ok to add this external reviewer

				if (!$this->isExistingUser(31)) {	// new user

					parent::add(31);

					$query = "insert into Reviewer set ID = $this->id";
					DB::query($query, &$result);

				} else {	// reviewer and external revierwer have a m:m relationship

					$this->id = $this->getIdByEmail();
					DB::query("select count(*) from Invite where InviterID = $inviterId and
					InviteeID = $this->id", &$result);
					if (DB::getCell(&$result) >= 1)
						throw new Exception('You have already invited this user.');
				}

				// record this invitation

				DB::query("insert into Invite set InviterID = $inviterId, InviteeID = $this->id", &$result);
			}
		}

		// same name as used in Reviewer, but different data source.
		// for external reviewers, the 'assignments' are delegations.

		// return an array of paper ids delegated to this er from all reviewers.
		public function getAssignments($inviterId = NULL) {

			$papers = array();

			$query = "select PaperID from Delegate where ToID = $this->id";
			$query .= $inviterId == NULL ? '':" and FromID = $inviterId";

			DB::query($query, &$result, 'ExternalReviewer::getAssignments()');
			while ($row = $result->fetch_assoc()) {
				$papers[] = $row['PaperID'];
			}

			return $papers;
		}

		// has delegation
		public function hasPaper($pId, $inviterId = 0 ) {

			$inviterRestriction = $inviterId == 0 ? '':" and FromID = $inviterId";
			$q = "select count(*) from Delegate where ToID = $this->id and PaperID = $pId $inviterRestriction";
			DB::query($q, &$result);
			if (DB::getCell(&$result) == 1)
				return true;
			return false;
		}

		public function getFromName($paperId) {

			$fromId = $this->getFromId($paperId);
			$r = new Reviewer($fromId);
			return (DEBUG ? "$fromId. ":'').$r->getName();
		}

		public function getFromId($paperId) {

			DB::query("select FromID from Delegate where PaperID = $paperId and ToID = $this->id", &$result);
			$fromId = DB::getCell(&$result);
			return $fromId;
		}

		public function clearDelegations($inviterId) {

			// delete
			DB::query("delete from Delegate where FromID = $inviterId", &$result);
		}

		public function updateDelegations($inviterId, $delegations) {


			// insert

			if ($delegations == NULL)
				return;

			foreach ($delegations as $paperId) {

				$query = "insert into Delegate (ToID, PaperID, FromID) values";
				$query .= " ($this->id, $paperId, $inviterId)";

				DB::query("select Title from Paper where ID = $paperId", &$result);
				$title = DB::getCell(&$result);

				DB::query($query, &$result, "Could not delegae '$title'. <br />Either you delegated it more than once, or the intended external reviewer already got that paper from another reviewer.");
			}
		}
	}
?>
