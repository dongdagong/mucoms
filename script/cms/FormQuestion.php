<?php
	class FormQuestion {

		public $trackId;
		public $id;
		public $question;
		public $questionType;
		public $isRequired;
		public $isConfidential;

		public $answer;
		public $answers;

		public $reviewerId;
		public $paperId; 
		
		public function FormQuestion($reviewerId, $paperId, $trackId, $id) {

			$this->overallIdRemoved = false;
			$this->reviewerExpertiseIdRemoved = false;
			
			$this->reviewerId = $reviewerId;
			$this->paperId = $paperId;
			$this->trackId = $trackId;
			$this->id = $id;
			
			if ($id == 9999) return;

			DB::query("select * from FormQuestion where TrackID = $trackId and ID = $id", &$result, 'FormQuestion::FormQuestion()');
			$row = $result->fetch_object();
			//var_dump($row);
			//echobr( $row->Question);
			$this->question = $row->Question;
			$this->questionType = $row->QuestionType;
			$this->isRequired = $row->IsRequired;
			$this->isConfidential = $row->IsConfidential;

			$query = "select c$id from ReviewTrack$trackId
			where ReviewerID = $reviewerId and PaperID = $paperId";
			DB::query($query, &$result, 'FormQuestion::FormQuestion()');
			//echobr($query);
			$this->answer = DB::getCell($result);
			//echobr($this->answer);

			DB::query("select Value, Label from FormAnswer where TrackID = $trackId and ID = $id", &$result, 'FormQuestion::FormQuestion()');

/*
			$numRows = $result->num_rows;
			for ($i = 0; $i < $numRows; $i++) {
				$row = $result->fetch_object();
				$this->answers[$row->Value] = $row->Label;
			}
*/
			while ($row = $result->fetch_assoc()) {
				$this->answers[$row['Value']] = $row['Label'];
			}
		}
		
		public function add() {
			
			// FormQuestion
			
			DB::query('select max(ID) from FormQuestion', &$result);
			$this->id = 1 + DB::getCell($result);
			$q = "insert into FormQuestion set 
				TrackID = 1,
				ID = $this->id,
				Question = '$this->question',
				QuestionType = '$this->questionType',
				IsRequired = $this->isRequired,
				IsConfidential = $this->isConfidential";
			DB::query($q, &$result);			
			// if (DEBUG) echobr($q);
							
			
			// ReviewTrack
			
			$decoration = $this->type == 'Choice' ? 'TINYINT UNSIGNED NULL':'TEXT NULL';
			$q = "alter table ReviewTrack1 add column c$this->id $decoration";
			DB::query($q, &$result);
			// if (DEBUG) echobr($q);
			
			
			// ReviewAnswer (if it is a Choice question)
			
			if ($this->questionType == 'Choice') {
				$this->addAnswers();
			}
		}
		
		public function addAnswers() {
			foreach ($this->answers as $value => $label) {
				$q = "insert into FormAnswer set 
					TrackID = 1, 
					ID = $this->id, 
					Value = $value, 
					Label = '$label'";
				DB::query($q, &$result);
				// if (DEBUG) echobr($q);
			}
		}
		
		public function update() {
				
			DB::query("update FormQuestion set 
				Question = '$this->question',
				IsRequired = $this->isRequired,
				IsConfidential = $this->isConfidential where ID = $this->id",
				&$result
			);		
				
			if ($this->questionType == 'Choice') {
				DB::query("delete from FormAnswer where TrackID = 1 and ID = $this->id", &$result);
				$this->addAnswers();
			}
		}
		
		public function delete($isMoving = false) {
			
			$t = new Track(1);
			if ($isMoving == false) 
				if ($this->id == $t->overallId || $this->id == $t->reviewerExpertiseId)
					throw new Exception('overallId and reviewerExpertiseId are compulsory.');

			DB::query('select max(ID) from FormQuestion', &$result);
			$max = DB::getCell($result);

			// ReviewTrack
			
			DB::query("alter table ReviewTrack1 drop column c$this->id", &$result);
			
			for ($i = $this->id + 1; $i <= $max; $i++) {
				$new = $i - 1;
				DB::query("select QuestionType from FormQuestion where ID = $i", &$result);
				$colType = DB::getCell($result) == 'Choice' ? 'TINYINT UNSIGNED NULL':'TEXT NULL';
				$q = "alter table ReviewTrack1 change c$i c$new $colType";
				//echobr('delete '.$q);
				DB::query($q, &$result);
			};
			
			
			// FormAnswer
			// FormQuestion
			
			DB::query("delete from FormAnswer where TrackID = 1 and ID = $this->id", &$result);
			DB::query("delete from FormQuestion where TrackID = 1 and ID = $this->id", &$result);
			
			DB::query("SET @@foreign_key_checks = 0", &$result);
			
			$q = "update FormAnswer set ID = ID - 1 where ID > $this->id";
			//echobr($q);
			DB::query($q, &$result);
			
			$q = "update FormQuestion set ID = ID - 1 where ID > $this->id";
			//echobr($q);
			DB::query($q, &$result);

			DB::query("SET @@foreign_key_checks = 1", &$result);

			
			// OverallID and ReviewerExpertiseID
			
			if ($this->id < $t->overallId)
				DB::query("update Track set OverallID = OverallID -1", &$result);
			if ($this->id < $t->reviewerExpertiseId)
				DB::query("update Track set ReviewerExpertiseID = ReviewerExpertiseID - 1", &$result);
		}
		
		public function move($x, $y, $dir) {
			
			if ($x == $y)	throw new Exception('Moving: destination should be different.');
			if ($x > $y)	$z = $dir == 1 ? ($y + 1):$y;
			if ($x < $y)	$z = ($dir == 1 ? ($y + 1):$y) - 1;
			if ($z == $x)	throw new Exception('Destination should be different.');

			//echobr("x:$x y:$y dir:$dir z:$z");
			
			// backup column content
/*
			DB::query("select ReviewerID, PaperID, c$x from ReviewTrack1", &$result);
*/

			$t = new Track(1); // before del and ins!!
			
			$this->delete(true);
			$this->insert($z);
			
			if ($x == $t->overallId)
				DB::query("update Track set OverallID = $z", &$result);
			if ($x == $t->reviewerExpertiseId)
				DB::query("update Track set ReviewerExpertiseID = $z", &$result);
			
			// restore column content. string formats todo
/*
			while ($row = $result->fetch_assoc()) {
				$q = 
					"update ReviewTrack1 set c$z = ".$row["c$x"].
					" where ReviewerID = ".$row['ReviewerID']." and PaperID = ".$row['PaperID'];
				echobr($q);
				DB::query($q,&$result2);
			}
*/
		}
		
		public function insert($z){	// inserted element will have position $z
			
			DB::query('select max(ID) from FormQuestion', &$result);
			$max = DB::getCell($result);

			// ReviewTrack
			
			//for ($i = $z + 1; $i <= $max; $i++) {
			for ($i = $max; $i >= $z; $i--) {
				$new = $i + 1;
				DB::query("select QuestionType from FormQuestion where ID = $i", &$result);
				$colType = DB::getCell($result) == 'Choice' ? 'TINYINT UNSIGNED NULL':'TEXT NULL';
				$q = "alter table ReviewTrack1 change c$i c$new $colType";
				//echobr('+ '.$q);
				DB::query($q, &$result);
			};
			
			$colType = $this->questionType == 'Choice' ? 'TINYINT UNSIGNED NULL':'TEXT NULL';
			$afterCol = $z == 1 ? 'first' : 'after c'.($z - 1);
			$q = "alter table ReviewTrack1 add column c$z $colType $afterCol";
			//echobr($q);
			DB::query($q, &$result);
			
			
			// FormAnswer
			// FormQuestion
			
			DB::query("SET @@foreign_key_checks = 0", &$result);
			//DB::query("SET @@UNIQUE_CHECKS = 0", &$result);

			//DB::query('alter table FormQuestion drop primary key', &$result);
			for ($i = $max; $i >= $z; $i--) {
				$q = "update FormQuestion set ID = ID + 1 where ID = $i";
				//echobr($q);
				DB::query($q, &$result);
			}
			//DB::query('alter table FormQuestion add primary key (TrackID, ID)', &$result);

			for ($i = $max; $i >= $z; $i--) {
				$q = "update FormAnswer set ID = ID + 1 where ID = $i";
				//echobr($q);
				DB::query($q, &$result);
			}
			
			if ($this->questionType == 'Choice') {
				$q = null;
				foreach ($this->answers as $value => $label) {
					$q .= $q == null ? '':', ';
					$q .= "(1, $z, $value, ".'"'.$label.'")';
				}
				$q = "insert into FormAnswer values $q";
				//echobr($q);
				DB::query($q, &$result);
			}
			
			$q = "insert into FormQuestion set 
				TrackID = 1, 
				ID = $z, 
				Question = '$this->question', 
				QuestionType = '$this->questionType', 
				IsRequired = $this->isRequired, 
				IsConfidential = $this->isConfidential";
			//echobr($q);
			DB::query($q, &$result);
			
			DB::query("SET @@foreign_key_checks = 1", &$result);
			//DB::query("SET @@UNIQUE_CHECKS = 1", &$result);
			
			
			// OverallID and ReviewerExpertiseID
			
			$t = new Track(1);
			if ($z <= $t->overallId)
				DB::query("update Track set OverallID = OverallID + 1", &$result);
			if ($z <= $t->reviewerExpertiseId)
				DB::query("update Track set ReviewerExpertiseID = ReviewerExpertiseID + 1", &$result);
		}
	}
?>
