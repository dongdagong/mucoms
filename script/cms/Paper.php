<?php
	class Paper {

		public $id;
		public $trackId;
		public $paperStatus;
		public $fullFormat;
		public $finalFormat;

		public $title;
		public $keywords;
		public $abstract;
		public $abst1stSub;
		public $abstLastModi;
		public $fullFile;
		public $full1stSub;
		public $fullLastModi;
		public $finalFile;
		public $final1stSub;
		public $finalLastModi;
		public $lateAbst;
		public $lateFull;
		public $lateFinal;
		public $isWithdrawn;
		public $isDecisionNotified;
		public $numAssi;
		public $isDiscussionInitiated;
		public $isStudentPaper;

		public $priTopic;
		public $secTopics;
		public $authors;	// array of Authors

		public $reviewers;
		public $averageScore;

		public $isFullFileRequired;

		public function Paper($paperId = null) {

			if ($paperId == null)
				return;

			$this->id = $paperId;
			$query = "select * from Paper where ID = $this->id";
			DB::query($query, &$result, 'Paper::Paper()');
			$row = $result->fetch_object();
			if ($row == null)
				throw new Exception('no such paper');

			$this->trackId = $row->TrackID;
			$this->paperStatus = $row->PaperStatus;
			$this->fullFormat = $row->FullFormat;
			$this->finalFormat = $row->FinalFormat;

			$this->title = $row->Title;
			$this->keywords = $row->Keywords;
			$this->abstract = $row->Abstract;
			$this->abst1stSub = $row->Abst1stSub;
			$this->abstLastModi = $row->AbstLastModi;
			$this->fullFile = $row->FullFile;
			$this->full1stSub = $row->Full1stSub;
			$this->fullLastModi = $row->FullLastModi;
			$this->finalFile = $row->FinalFile;
			$this->final1stSub = $row->Final1stSub;
			$this->finalLastModi = $row->FinalLastModi;
			$this->lateAbst = $row->LateAbst;
			$this->lateFull = $row->LateFull;
			$this->lateFinal = $row->LateFinal;
			$this->isWithdrawn = $row->IsWithdrawn;
			$this->isDecisionNotified = $row->IsDecisionNotified;
			$this->numAssi = $row->NumAssi;
			$this->isDiscussionInitiated = $row->IsDiscussionInitiated;
			$this->isStudentPaper = $row->IsStudentPaper;

			$this->priTopic = $this->getPriTopic();		// integer
			$this->secTopics = $this->getSecTopics();	// array of integers
			$this->authors = $this->getAuthors();		// array of Authors
			//var_dump('<br>authors 1: ', $this->authors);
		}

		public function getAuthors() {

			$authors = array();
			$query = "select AuthorID, IsContactAuthor, Serial from Writes where PaperID = '$this->id'
			order by Serial";
			DB::query($query, &$result, 'Paper::getAuthors()');
			while ($row = $result->fetch_assoc()) {
				$author = new Author($row['AuthorID']);
				$author->isContactAuthor = $row['IsContactAuthor'];
				$authors[] = $author;
				// directly assign the new author to $this->authors doesn't work.
			}
			return $authors;
		}

		public function getAuthorNames() {

			foreach ($this->authors as $author) {
				$authorNames .= $author->getName() . ', ';
			}
			$authorNames = substr($authorNames, 0, strlen($authorNames) - 2);
			return $authorNames;
		}

		public function getPriTopic() {

			$query = "select t.ID from PaperTopic pt, Topic t where pt.PaperID = '$this->id' and pt.IsPrimary = 1 and pt.TrackID = t.TrackID and pt.TopicID = t.ID";

			DB::query($query, &$result, 'Paper::getPriTopic()');

			return DB::getCell(&$result);
		}

		public function getPriTopicName() {

			$query = "select Name from Topic where TrackID = $this->trackId and ID = $this->priTopic";
			DB::query($query, &$result, 'Paper::getPriTopicName()');
			return DB::getCell(&$result);
		}

		public function getSecTopics() {

			$return = array();

			$query = "select t.ID from PaperTopic pt, Topic t where pt.PaperID = '$this->id' and pt.IsPrimary = 0 and pt.TrackID = t.TrackID and pt.TopicID = t.ID";

			DB::query($query, &$result, 'Paper::getSecTopics()');
			while ($row = $result->fetch_assoc()) {
				$return[] = $row['ID'];
			}
			return $return;
		}

		public function getSecTopicNames() {
			$return = NULL;
			foreach ($this->secTopics as $secTopic) {
				$query = "select Name from Topic where TrackID = $this->trackId and ID = $secTopic";
				DB::query($query, &$result, 'Paper::getPriTopicName()');
				$return .= DB::getCell(&$result) . ', ';
			}
			$return = substr($return, 0, strlen($return) - 2);
			return $return;

		}

		public function getTopicNames() {

			return $this->getPriTopicName() . ', ' . $this->getSecTopicNames();
		}

		public function getTrackName() {

		}

		// returns an array of dates for various submission items
		public function getDates() {

			$track = new Track($this->trackId);

			$dueAbst = $track->dueAbst.($this->lateAbst == 0 ? '' : ' + '.$this->lateAbst.' days extension');
			$dueFull = $track->dueFull.($this->lateFull == 0 ? '' : ' + '.$this->lateFull.' days extension');
			$dueFinal = $track->dueFinal.($this->lateFinal == 0 ? '' : ' + '.$this->lateFinal.' days extension');

			$dates = array(
				array('Abstract', substr($this->abst1stSub,0,16), substr($this->abstLastModi,0,16), $dueAbst),
				array($this->getFullLink('Full Paper'), substr($this->full1stSub,0,16), substr($this->fullLastModi,0,16), $dueFull),
				array($this->getFinalLink('Final Paper'), substr($this->final1stSub,0,16), substr($this->finalLastModi,0,16), $dueFinal)
			);

			if (!$this->isSelected())
				unset($dates[2]);


			return $dates;

		}

		public function isDueAbst() {

			$track = new Track($this->trackId);
			return dateDiff(date('Y-m-d'), substr($track->dueAbst, 0, 10), '-') + $this->lateAbst < 0;
		}

		public function isDueFull() {

			$track = new Track($this->trackId);
			return dateDiff(date('Y-m-d'), substr($track->dueFull, 0, 10), '-') + $this->lateFull < 0;
		}

		public function isDueFinal() {

			$track = new Track($this->trackId);
			return dateDiff(date('Y-m-d'), substr($track->dueFinal, 0, 10), '-') + $this->lateFinal < 0;
		}

		public function isSelected() {

			return ($this->paperStatus == 1 || $this->paperStatus > 2);
		}

		public function isFullUploaded() {

			return $this->full1stSub != NULL;
		}
		public function isFinalUploaded() {

			return $this->final1stSub != NULL;
		}

		public function isContactAuthor() {

			// authors array already populated
			foreach ($this->authors as $author) {
				if ($author->id == $_SESSION['id'] && $author->isContactAuthor == 1)
					return true;
			}
			return false;
		}

		public function isContactAuthorCheck() {

			if (!$this->isContactAuthor())
				HTTP::message('You are not a contact author of this paper.');
		}

		public function isReviewer() {

			$q = "
			select count(*) from Assignment
			where
				PaperID = $this->id and
				ReviewerID = ".$_SESSION['id']." and
				TrackID = 1";
			DB::query($q, &$result);
			if (DB::getCell(&$result) != 1)
				return false;
			return true;
		}

		public function isReviewerCheck() {

			if (!$this->isReviewer())
				HTTP::message('You are not assigned to this paper.');
		}

		public function isExternalReviewer() {

			$q = "
			select count(*) from Delegate
			where
				PaperID = $this->id and
				ToID = ".$_SESSION['id'];
			DB::query($q, &$result);
			if (DB::getCell(&$result) != 1)
				return false;
			return true;
		}

		public function isExternalReviewerCheck() {

			if (!$this->isExternalReviewer())
				HTTP::message('You are not a delegate for this paper.');
		}

		public static function isExistingPaper($pId, $trackId) {

			DB::query("select count(*) from Paper where ID = $id and TrackID = $trackId", &$result);
			if (DB::getCell(&$result) == 0)
				return false;
			return true;
		}

		public function add() {

			$query =

			"INSERT INTO Paper SET

				PaperStatus = $this->paperStatus,
				TrackID = $this->trackId,
				FullFormat = NULL,
				FinalFormat = NULL,
				Title = '$this->title',
				Keywords = '$this->keywords',
				Abstract = '$this->abstract',
				Abst1stSub = '$this->abst1stSub',
				AbstLastModi = '$this->abstLastModi',
				FullFile = NULL,
				Full1stSub = NULL,
				FullLastModi = NULL,
				FinalFile = NULL,
				Final1stSub = NULL,
				FinalLastModi = NULL,
				LateAbst = 0,
				LateFull = 0,
				LateFinal = 0,
				IsWithdrawn = 0,
				IsDecisionNotified = 0,
				IsStudentPaper = $this->isStudentPaper";

			DB::query($query, &$result, 'Paper::add()');
			$id = DB::getConn()->insert_id;
			$this->id = $id;

			// PaperTopics

			$paperTopic = new PaperTopic();

			$paperTopic->add($id, $this->trackId, $this->priTopic, 1);

			if ($this->secTopics != NULL) {

				foreach ($this->secTopics as $secTopic) {
					if ($secTopic != $this->priTopic)
						$paperTopic->add($id, $this->trackId, $secTopic, 0);
				}
			}


			// Authors & Writes

			foreach ($this->authors as $a) {

				$isNewUser = false;
				if ($a->isNewUser()) {
					$isNewUser = true;
					$e = new Email('Author', 'new_account_author');
				}

				$a->add();	// do not throw exceptions if author already exist
							// since this is batch add() of authors

				if ($isNewUser)
					$e->send($a);

				$query = "insert into Writes values ($id, $a->id, $a->isContactAuthor, $a->serial)";
				DB::query($query, &$result, 'Paper::add()');
			}
		}

		public function update() {

			// Paper
			$query =
			"update Paper set
				Title = '$this->title',
				Abstract = '$this->abstract',
				Keywords = '$this->keywords',
				AbstLastModi = '$this->abstLastModi',
				IsStudentPaper = $this->isStudentPaper 
			where ID = $this->id";
			//echobr($query);
			DB::query($query, &$result, 'Paper::update()');

			// PaperTopic
			$paperTopic = new PaperTopic();

			// delete all topics
			$query = "delete from PaperTopic where PaperID = $this->id and TrackID = $this->trackId";
			DB::query($query, &$result, 'Paper::update()');

			// add all 'new' topics
			$id = $this->id;
			$paperTopic->add($id, $this->trackId, $this->priTopic, 1);
			if ($this->secTopics != NULL) {
				foreach ($this->secTopics as $secTopic) {
					if ($secTopic != $this->priTopic)	// automatically skip if it is primary topic
						$paperTopic->add($id, $this->trackId, $secTopic, 0);
				}
			}
		}

		public function setCol($colName, $colValue) {

			$query = "update Paper set $colName = '$colValue' where ID = $this->id";
			DB::query($query, &$result, 'Paper::setFullLastModi()');
		}

		public function extension($colName, $colValue) {

			switch ($colName) {
				case 'LateAbst':


					break;
				case 'LateFull':

					break;

				case 'LateFinal':

					break;
			}

			$this->setCol($colName, $colValue);
		}

		public function setFullLastModi() {

			$now = date('Y-m-d H:i:s');
			$query = "update Paper set FullLastModi = '$now' where ID = $this->id";
			DB::query($query, &$result, 'Paper::setFullLastModi()');
		}
		public function setFinalLastModi() {

			$now = date('Y-m-d H:i:s');
			$query = "update Paper set FinalLastModi = '$now' where ID = $this->id";
			DB::query($query, &$result, 'Paper::setFullLastModi()');
		}

		public function withdraw() {

//			// check if the paper is in any bids (in table Bid)
//			// this check is not in canWithdraw for performance reason
//			DB::query("select count(*) from Bid where PaperID = $this->id", &$result);
//			if (DB::getCell(&$result) > 0)
//				throw new Exception("Paper $this->id has biddings. Cannot withdraw.");

			// withdraw
			$query = "update Paper set PaperStatus = -1 where ID = $this->id";
			DB::query($query, &$result, 'Paper::withdraw()');
		}

		public function getFullPaperName() {	// file extension extracted from the uploaded file.

			return 'full'.$this->id; //.$this->fullFormat;
		}

		public function getFinalPaperName() {	// file extension extracted from the uploaded file.

			return 'final'.$this->id.$this->fullFormat;
		}

		public function getTitleLink() {

			return $this->isFullUploaded() ? HTML::a($this->title, UPLOAD_DIR.$_SESSION['ci'].'/'.$this->getFullPaperName()) : 'Full Paper';
		}

		public function getFullLink($label = 'Full') {

			return $this->isFullUploaded() ? HTTP::d($label, 'full'.$this->id.$this->fullFormat) : $label;

		}

		public function getFinalLink($label = 'Final') {

			return $this->isFinalUploaded() ? HTTP::d($label, 'final'.$this->id.$this->finalFormat) : $label;
		}

		public function isEditable() {

			$t = new Track($this->trackId);

			if ($this->paperStatus != 0)
				return false;

			// too late
			if ($this->isDueAbst())
				return false;

			// too early
			if ($t->phAbst == 0 && $t->isDueAbst() == false)
				return false;

			return true;
		}

		public function isUploadable($type) {

			$t = new Track($this->trackId);

			switch ($type) {

				case 'full':
					if ($this->paperStatus != 0)
						return false;

					// too late
					if ($this->isDueFull())
						return false;

					// too early
					if ($t->phFull == 0 && $t->isDueFull() == false)
						return false;

					break;

				case 'final':

					// final paper submission due for this paper
					if ($this->isDueFinal())
						return false;

					// paper status does not require final upload
					$ps = new PaperStatus($this->paperStatus);
					if ($ps->finalUpload == 0)
						return false;
//					paper status is not one of the 'accepted' types
//					if (stristr($this->paperStatus, 'accept') == false)
//						return false;

					// too early
					if ($t->phFinal == 0 && $t->isDueFinal() == false)
						return false;

					break;
			}

			return true;
		}

		public function canSpecifyConflicts() {

			return $this->isUploadable('full');
		}

		public function canWithdraw() {

			$t = new Track($this->trackId);

			return $this->isEditable();
		}

		public function getReviewers() {
			$query = "select ReviewerID from Assignment where TrackID = 1 and PaperID = $this->id";
			DB::query($query, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['ReviewerID'];
			}
			return $a;
		}

		public function getERs() {
			$query = "select ToID from Delegate where PaperID = $this->id";
			DB::query($query, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['ToID'];
			}
			return $a;
		}

		public function delete() {

			// Writes
			DB::query("delete from Writes where PaperID = $this->id", &$result);

			// PaperTopic
			DB::query("delete from PaperTopic where PaperID = $this->id", &$result);

			// Conflict
			DB::query("delete from Conflict where PaperID = $this->id", &$result);

			// Paper
			DB::query("delete from Paper where ID = $this->id", &$result);

			// Author and Users

			foreach ($this->authors as $a) {
				$num = $a->getNumPapers();
				if ($num == 0) {		// no other assignments
					$a->delete(40);
					$subject = 'Paper and Author Account Deleted';
					$template = 'delete_paper_author';
				} else {
					$subject = 'Paper Deleted';
					$template = 'delete_paper';
				}
				if ($a->isContactAuthor == 1) {
					$e = new Email($subject, $template);
					$e->addPlugin("PaperTitle:$this->title");
					$e->send($a);
				}
			}
		}

		public function deleteAuthor($aId) {

			DB::query("select Serial from Writes where PaperID = $this->id and AuthorID = $aId", &$result);
			$delSerial = DB::getCell(&$result);

			// Writes
			DB::query("delete from Writes where PaperID = $this->id and AuthorID = $aId", &$result);

			// Role
			$a = new Author($aId);
			$num = $a->getNumPapers();
			if ($num == 0) {		// no other assignments
				$a->delete(40);
			}

			// Serial
			DB::query("
				update Writes
				set Serial = Serial - 1
				where
					PaperID = $this->id and
					Serial > $delSerial",
				&$result
			);
		}

		public function addAuthor($a) {

			DB::query("select max(Serial) max from Writes
			where PaperID = $this->id", &$result);
			$maxSerial = DB::getCell(&$result);
			$maxSerial++;
			$query = "insert into Writes values ($this->id, $a->id, $a->isContactAuthor, $maxSerial)";
			DB::query($query, &$result);
		}

		public function moveAuthor($x, $dir, $y) {

			DB::query("select count(*) from Writes where PaperID = $this->id", &$result);
			$n = DB::getCell(&$result);

			if (($x == $y) ||
				($x == $y + 1 && $dir == 1) ||
				($x == $y - 1 && $dir == -1) ||
				($y < 1) ||
				($y > $n)) {
					return;			// no move

			} elseif ($x > $y) {
				$action = -1;		// move to the front
				if ($dir == 1)
					$y = $y + 1;

			} elseif ($x < $y) {
				$action = 1; 		// move to the end
				if ($dir == -1)
					$y = $y - 1;
			}

			if ($action == -1) {
				DB::query("update Writes set Serial = 0 where Serial = $x and PaperID = $this->id", &$result);
				$q = "update Writes set Serial = Serial + 1
				where Serial < $x and Serial >= $y and PaperID = $this->id";
				DB::query($q, &$result);
				DB::query("update Writes set Serial = $y where Serial = 0 and PaperID = $this->id", &$result);
			}

			if ($action == 1) {
				DB::query("update Writes set Serial = 0 where Serial = $x and PaperID = $this->id", &$result);
				$q = "update Writes set Serial = Serial - 1
				where Serial <= $y and Serial > $x and PaperID = $this->id";
				DB::query($q, &$result);
				DB::query("update Writes set Serial = $y where Serial = 0 and PaperID = $this->id", &$result);
			}
		}

		public function getAverageScore() {

			$this->reviewers = $this->getReviewers();
			$count = 0;
			$this->averageScore = 0;
			
			if ($this->reviewers == null) return null;
			
			foreach ($this->reviewers as $rId) {
				$review = new Review($rId, $this->id, 1);
				$overall = $review->getOverallScore();
				if ($overall != null) {
					$count++;
					$this->averageScore += $overall;
				}
			}
			$this->averageScore = $count > 0 ? $this->averageScore/$count:0;
			return $this->averageScore;
		}

		public function isInConflict() {

			$this->getAverageScore();
			
			if ($this->reviewers == null) return null;
			
			foreach ($this->reviewers as $rId) {
				$review = new Review($rId, $this->id, 1);
				$overall = $review->getOverallScore();
				if ($overall != null)
					if (abs($overall - $this->averageScore) >= REVIEW_CONFLICT_THRESHOLD)
						return true;
			}
			return false;
		}

		public function initiateDiscussion() {
			DB::query("update Paper set IsDiscussionInitiated = 1 where ID = $this->id", &$result);
		}
	}
?>
