<?php
	class Chair extends Users {
		
		public $trackID;
		public $privileges;
		
		public function add() {	 
			
			if ($this->isExistingUser(20)) {

				throw new Exception( "$this->email is already associated with a Chair.");
			}
				
			// External Reviewers cannot have any other roles
			if ($this->isExistingUser(31)) {
				throw new Exception("$this->email is already associated with an External Reviewer.");
			} 

			parent::add(20);
			
			// Chair 
			
			$query = "insert into Chair set ID = $this->id";
			DB::query($query, &$result, 'Chair::add()');	
			
			$query = "insert into Coordinate set ChairID = $this->id, TrackID = 1";
			DB::query($query, &$result, 'Chair::add()');	
		}
		
		public function privilegeCheck($privId) {
			
			DB::query("select count(*) from TrackPrivilege where TrackID = 1 and ChairID = $this->id and 
			PrivilegeID = $privId", &$result);
			if (DB::getCell(&$result) != 1)
				HTTP::message('You do not have the access privilege to this page.', 'c_main.php');
		}
	
		public function getPrivilegeArray() {
			
			DB::query("select PrivilegeID from TrackPrivilege 
			where TrackID = 1 and ChairID = $this->id", &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row['PrivilegeID'];
			}
			return $a;		
		}
		
		public function updatePrivileges($privileges) {
			
			DB::query("delete from TrackPrivilege where TrackID = 1 and ChairID = $this->id", &$result);
			
			$q = "insert into TrackPrivilege values ";
			if (count($privileges) > 0) {
				foreach ($privileges as $privId) {
					$q .= "(1, $this->id, $privId),";
				}
				$q = substr($q, 0, strlen($q) - 1);
				DB::query($q, &$result);			
			}
		}
	}
?>
