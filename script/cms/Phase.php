<?

class Phase	{

	public $name;
	
	public function Phase() {
		
		$this->name['abst'] = 'Abstract Submission';
		$this->name['full'] = 'Full Paper Submission';
		$this->name['bid'] = 'Bidding';
		$this->name['review'] = 'Review Submission';
		$this->name['final'] = 'Final Paper Submission';
		
	}
	
}
