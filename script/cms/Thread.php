<?
	class Thread {

		public $id;
		public $userId;
		public $parentId;
		public $paperId;

		public $timePosted;
		public $timeModified;
		public $title;
		public $msg;

		public $lastPost;
		public $replies;
		public $views;

		public function add() {
			DB::query("
				insert into Thread set
					UserID = $this->userId,
					ParentID = $this->parentId,
					TrackID = 1,
					PaperID = $this->paperId,

					TimePosted = '$this->timePosted',
					TimeModified = '$this->timeModified',
					Title = '$this->title'",
				&$result);
			$id = DB::getConn()->insert_id;
			DB::query("insert into ThreadMsg set ThreadID = $id, Msg = '$this->msg'", &$result);
		}

		public function update($id) {
			$title = ($this->title);
			DB::query("
				update Thread set
					TimeModified = '$this->timeModified',
					Title = '$title'
				where ID = $id",
				&$result);
			DB::query("
				update ThreadMsg set Msg = '$this->msg' where ThreadID = $id", &$result);
		}

		public function getUserId($id) {
			DB::query("select UserID from Thread where ID = $id", &$result);
			return DB::getCell(&$result);
		}

		public function getTitle($id) {
			DB::query("select Title from Thread where ID = $id", &$result);
			return DB::getCell(&$result);
		}

		public function getMsg($id) {
			DB::query("select Msg from ThreadMsg where ThreadID = $id", &$result);
			return DB::getCell(&$result);
		}

		public function getThread($pId) {
			$sql = "
				select
					t.UserID,
					concat(u.FName, ' ', u.LName) Who,
					t.TimePosted,
					t.TimeModified,
					t.Title,
					m.Msg,
					t.ParentID,
					t.ID
				from Thread t, Users u, ThreadMsg m
				where
					t.PaperID = $pId and
					t.UserID = u.ID and
					t.ID = m.ThreadID
				order by t.ID";	// some names won't show up at all if having middle name in concat()
			DB::query($sql, &$result);
			while ($row = $result->fetch_assoc()) {
				$a[] = $row;
			}
			return $a;
		}
	}
?>
