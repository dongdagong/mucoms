<?php

	class Notification {

///////////////////////////////////////////////////////////////////////

		public $code;
		public $name;
		public $isSent;
		public $sentTime;
		
		function Notification($code = null) {
			
			if ($code == null) return;
			
			DB::query("select * from NotificationLog where Code = '$code'", &$result);
			$row = $result->fetch_assoc();
			
			if ($row == null) return;
			
			$this->code = $row['Code'];
			$this->name = $row['Name'];
			$this->isSent = $row['IsSent'];
			$this->sentTime = $row['SentTime'];		
		}

		public function writeSentLog() {
			
			$now = date('Y-m-d H:i:s');
			DB::query("update NotificationLog set IsSent = 1, SentTime = '$now' 
			where Code = '$this->code'", &$result);
		}
		
		public function isSent() {
			
			return $this->isSent == 1 ? true:false;
		}			
		
		
///////////////////////////////////////////////////////////////////////

		
		public function send($event, $uId = null) {

			$this->writeSentLog();	

			$t = new Track(1);
			$to = array();

			switch ($event) {

				case 'abstOpen':

					$to = $t->getAuthorsWithNoSubmissions();
					$e = new Email('Abstract submission open', 'abstract_submission_open');
					$e->groupSend($to);
					break;

				case 'abstLate':

					$to = $t->getusers('abstLate');
					$e = new Email('Abstract submission about to close', 'abstract_submission_late');
					$e->addPlugin("DueAbst:$t->dueAbst");
					$e->groupSend($to);
					break;

				case 'fullOpen':

//					$to = $t->getUsers('fullOpen');
//					$e = new Email('Full paper submission open', 'full_submission_open');
//					$e->groupSend($to);

					$papers = $t->getPapers('fullOpen');
					$e = new Email('Full paper submission open', 'full_submission_open');
					$this->paperSend($papers, $e);
					break;

				case 'fullLate':

//					$to = $t->getusers('fullLate');
//					$e = new Email('Full paper submission about to close', 'full_submission_late');
//					$e->addPlugin("DueFull:$t->dueFull");
//					$e->groupSend($to);

					$papers = $t->getPapers('fullLate');
					$e = new Email('Full paper submission about to close', 'full_submission_late');
					$e->addPlugin("DueFull:$t->dueFull");
					$this->paperSend($papers, $e);
					break;

				case 'bidOpen':

					$to = $t->getUsers('bidOpen');
					$e = new Email('Paper bidding open', 'bid_open');
					$e->groupSend($to);
					break;

				case 'bidLate':

					$to = $t->getUsers('bidLate');
					$e = new Email('Bidding about to close', 'bid_late');
					$e->addPlugin("DueTopicExpertise:$t->dueTopicExpertise");
					$e->groupSend($to);
					break;

				case 'assignment':

					$to = $t->getUsers('reviewersWithAssi');
					$e = new Email('Assignment avaialble', 'assignment');
					$e->groupSend($to);
					break;

				case 'reviewOpen':

					$to = $t->getUsers('reviewOpen');
					$e = new Email('Review submission open', 'review_open');
					$e->groupSend($to);
					break;

				case 'reviewLate':

					$to = $t->getusers('reviewLate');
					$e = new Email('Review submission about to close', 'review_late');
					$e->addPlugin("DueReview:$t->dueReview");
					$e->groupSend($to);
					break;

				case 'decision':

					// release paper status
					
					DB::autocommit(false);
					$t->setPaperStatus(1);
					DB::commit();
					
					// for each PAPER that has a decision (not withdrawn or pending)
					// send a SINGLE email.

					if ($_SESSION['_sendDecisionToOnlyOnePaper'] == true) { 

						$onePaper = new Paper($_SESSION['_pId']);
						$papers[$onePaper->id] = $onePaper->paperStatus;
						unset($_SESSION['_sendDecisionToOnlyOnePaper']);
						unset($_SESSION['_pId']);
												
					} else
						$papers = $t->getPapers('decided');

					if ($papers == null) return;
					foreach ($papers as $pId => $psId) {
						
						$paper = new Paper($pId);
						if ($paper->isDecisionNotified == 1) continue;
						
						DB::query("update Paper set IsDecisionNotified = 1 where ID = $pId", &$result);
						DB::commit();
						
						$ps = new PaperStatus($psId);
						if ($ps->sendNotification != 1)	continue;
						$e = new Email("Decision - $ps->name", "decision_$ps->id");
						$authors = $paper->getAuthors();
						$a1 = null;
						foreach ($authors as $author) {
/*							// wrong if the first author is NOT a contact author!
							if ($a1 == null) 
								$a1 = $author;
							elseif ($author->isContactAuthor == 1)
								$e->addCC($author->email);
*/
							if ($author->isContactAuthor != 1) continue;
							
							if ($a1 == null)
								$a1 = $author;
							else
								$e->addCC($author->email);
						}
						if ($a1 != null) {
							$e->addPlugin("PaperTitle:$paper->title");
							$e->send($a1);
						}
					}
					break;

				case 'finalOpen':

					$papers = $t->getPapers('finalOpen');
					$e = new Email('Final paper submission open', 'final_submission_open');
					$this->paperSend($papers, $e);
					break;

				case 'finalLate':

//					$to = $t->getusers('finalLate');
					$papers = $t->getPapers('finalLate');
					$e = new Email('Final paper submission about to close', 'final_submission_late');
					$this->paperSend($papers, $e);
//					$e->addPlugin("DueFinal:$t->dueFinal");
//					$e->groupSend($to);
					break;

				case 'inviteChair':

					$to = new Users($uId);
					$e = new Email('Chair Invitation', 'new_account_chair');
					if (!$to->isPwdSent()) {
						$newPwd = $to->generatePwd();
						$to->changePassword($to->pwd, $newPwd, false);
						$to->pwd = $newPwd;
						$to->pwdSent();
						$e = new Email('Chair Invitation', 'new_account_chair');
					} else {
						$e = new Email('Chair Invitation', 'new_account_chair', null, true, false);
					}
					$e->send($to);
					break;

				case 'inviteReviewer':
					$to = new Users($uId);
					if (!$to->isPwdSent()) {
						$newPwd = $to->generatePwd();
						$to->changePassword($to->pwd, $newPwd, false);
						$to->pwd = $newPwd;
						$to->pwdSent();
						$e = new Email('Reviewer Invitation', 'new_account_reviewer');
					} else {
						$e = new Email('Reviewer Invitation', 'new_account_reviewer', null, true, false);
					}
					$e->send($to);
					break;

			}
		} // send

		public function paperSend($papers, $e0) {

//			$cc = $e->cc;
			if ($papers == null) return;
			foreach ($papers as $pId) {
//				$e->cc = $cc;
				$e = clone $e0;
				$paper = new Paper($pId);
				$authors = $paper->getAuthors();
				$a1 = null;
				foreach ($authors as $author) {
					if ($author->isContactAuthor != 1) continue;
					if ($a1 == null)
						$a1 = $author;
					else
						$e->addCC($author->email);
				}
				if ($a1 != null) {
					$e->addPlugin("PaperTitle:$paper->title");
					$e->send($a1);
				}
			}
		}
		
	}
?>
