<?

	// requires include_once 'cms/Notification.php';

	class Assignment {

		public function canPerformAutoAssignment(&$msg) {
			
			$msg = null;
			
			$noti = new Notification('assignment');
			if ($noti->isSent())
				$msg = 'Assignment notifications has been sent.<br />';
//return 'aaa';
			
			$t = new Track(1);
			if ($t->phReview == 1)
				$msg .= 'Review submission phase is open.<br />';
				
			DB::query('select count(*) C from Review', &$result);
			if (DB::getCell(&$result) > 0)
				$msg .= 'Reviews have been entered.<br />';
				
			return ($msg == null);
		}
	}
?>
