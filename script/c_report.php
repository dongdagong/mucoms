<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Report.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(270);

	$form = new HTMLForm('c_report');
	$submit = new FormElement('submit', null, 'Submit');
	$line = new FormElement('line', 'line');

	$table = null;
	$track = new Track(1);

	$a1 = $track->getReviewers();

	if ($a1 !=  null) {

		$i = 0;
		foreach ($a1 as $rId) {

			$r = new Reviewer($rId);
			
			$yesNoValab = array(1 =>'');
			$yesNoValues = array();
			//echobr($rId.' '.$r->releaseReport.' '.$_GET['select'].'_');
			if ($_GET['select'] == 'all' || $r->releaseReport == 1)
				$yesNoValues[0] = 1;
			if ($_GET['select'] == 'none')
				$yesNoValues[0] = 0;

			$data[$i][0] = new FormElement('checkbox', "release$i", null, $yesNoValab, $yesNoValues);
			$data[$i][1] = $r->id;
			$data[$i][2] .= (DEBUG ? $rId.'. ':'').$r->getName();
			$data[$i][3] .= $r->org.'<br /><span  class=smallgrey>'.$r->email.'</span>';

			$form->addElement($data[$i][0]);
			$i++;
		}
		if ($data != null) sort($data);

		$table = new HTMLTable($data, 1, 'asc');
		$table->setSortOffCols(array(0));
		$table->setTitle('=30', 'ID=30', 'Reviewer=200', 'Organization and Email=650');
	}

	if (HTTP::isLegalPost()) {
	
		try {
			
			$form->validateWithException();

			DB::autocommit(false);

			foreach ($data as $row) {
			
				$rId = $row[1];
				$isReleased = $row[0]->values[0] == 1 ? 1:0;				
				DB::query("update Reviewer set ReleaseReport = $isReleased where ID = $rId", &$result);
			}		

			DB::commit();
			$msg = 'Release status saved.<p />';

		} catch (Exception $e) {
		
			$oops = $e->getMessage();
		}
	}

/*
	$cmd = HTTP::sessionate('cmd');
	$rpt = HTTP::sessionate('rpt');

	if ($cmd == 'activate') {
		$report = new Report($rpt);
		$report->activate();
	}

	if ($cmd == 'deactivate' && is_numeric($rpt)){
		$report = new Report($rpt);
		$report->deactivate();
	}

	$report = new Report(1);
	

	unset($_SESSION['cmd']);
	unset($_SESSION['rpt']);
*/
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::command('Display Report', 'report_1.php').'  ';
/*
	echo ($report->isActive() ? '[available':'[unavailable').' to reviewers] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$cmd = $report->isActive() ? 'deactivate':'activate';
	echo HTML::command($cmd, "c_report.php?cmd=$cmd&rpt=1");
*/
	echo '<br />';
?>
<h3>Release report to selected reviewers</h3>
<div class=red><? echo $oops; ?></div>
<?php
	echo $msg;
	echo HTML::command('Select All',  'c_report.php?select=all').' | ';
	echo HTML::command('Select None', 'c_report.php?select=none').'<p />';
	if ($a1 != null) {
		$form->open(); 
		$table->display();
		echo '<br />';
		$submit->display();
		$form->close();
	} else
		echo 'No reviewers.';
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
