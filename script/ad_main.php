<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Administrator.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::adminCheck();
	DB::connect('common');  

/*
	$cmd = HTTP::sessionate('cmd');
	$ciId = HTTP::sessionate('ciId');
*/
	$cmd = $_GET['cmd'];
	$ciId = $_GET['ciId'];
	
	if ($cmd != null && $ciId != null) {
		
		$conf = new Conference($ciId);

		DB::autocommit(false);
		if ($cmd == 'Activate') {
			$conf->activate();
		}

		if ($cmd == 'Deactivate') {
			$conf->deactivate();
		}
		DB::commit();
	}

	$admin = new Administrator();
	$cis = $admin->getCIs();

	$i = 0;
	
	if ($cis == null) {
		echo '.';
	} else {
	
		foreach ($cis as $ci) {
			$data[$i][0] = '&nbsp;'.$ci['ID'];

			$cmd = ($ci['IsActive']== 1) ? 'Deactivate':'Activate';
			$state = ($ci['IsActive']== 1) ?
				makeup('&nbsp;:)&nbsp;', 'white', 'green'):
				makeup('&nbsp;(:&nbsp;', 'white', 'lightgrey');

			$data[$i][1] = $state;

			$data[$i][2] = '<h2>'.$ci['ShortName'].'</h2>';
			$data[$i][2] .= $ci['FullName'];

			$data[$i][3] = HTML::command("$cmd", "ad_main.php?cmd=$cmd&ciId=".$ci['ID']);
			$data[$i][4] = HTML::command("Delete", 'ad_delete_ci.php?id2Del='.$ci['ID']);

			$i++;
		}

		$table = new HTMLTable($data);
		$table->setTitle('&nbsp;#=40', 'Status=55', 'Conference Instances=800', 'Switch=90', '=50');
	}
?>
<?php include 'header_simple.php'; ?>

<div class="main">
<? echo '<div class=right0>'.HTML::command('Logout', 'ad_logout.php').'</div><br />'; ?>
<h3><? echo SYSTEM_NAME. ' Administration'; ?></h3>
<?
	if (isset($table)) 
		$table->display();
	else
		echobr('No conference instances.');

	echo '<br /><br />';
	echo HTML::command('Add a new conferfence instance', 'ad_add_ci.php');
?>
</div> <!-- end of main -->

<?php include 'footer_simple.php'; ?>
