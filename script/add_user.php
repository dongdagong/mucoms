<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	$nur = HTTP::sessionate('nur');	// nur: New User Role

	$pId = HTTP::sessionate('pId'); // directed from a_edit_authors
	if ($pId != null && !is_numeric($pId))
		HTTP::message('Invalid pId.');
	$paper = null;

	switch ($nur) {		// rur: Requesting User Role

		case 40:
			$rur = 40;	// author signup or edit authors list
			$title = 'Create a new author account';
			break;
		case 31:
			$rur = 30;	// reviewer invite external reviewer
			$title = 'Invite an External Reviewer';
			break;
		case 30:
			$rur = 20;	// chair invite reviewer
			$title = 'Create new Reviewer Account';
			break;
		case 20:
			$rur = 20;	// chair invite chair
			$title = 'Create new Chair Account';
			break;
	}

	// check login type except for signing up author
	if ($rur != 40)
		Auth::loginCheck($rur);
	elseif (isset($_SESSION['role']) && $pId == null)
		HTTP::message('To register as author you need to log off your current role first.');

	DB::connect();
	$t = new Track(1);

	if ($pId != null && $_SESSION['role'] == 40) {
		$paper = new Paper($pId);
		$title = "Add an author for $paper->title";

		// is an contact author of this paper
		$paper->isContactAuthorCheck();
		// can edit at this stage
		if (!$paper->isEditable()) {
			HTTP::message('Cannot edit paper information at this stage.', 'a_main.php');
		}
	}

	// cannot register new authors after full paper submissoin is closed
	if ($rur == 40 && $t->isDueFull()) {
		HTTP::message('Author registration is closed.');
	}

	// check chair privilege
	if ($rur == 20) {

		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck($nur == 20 ? 201 : 202);
	}

	$form = new HTMLForm('form');
	$line = new FormElement('line', 'line', 'line');

	$fname = new FormElement('text', 'fname');
	$fname->addLayout('Name', 'First', 'set=31', 'size=15');
	$fname->addRule('maxLen=30');

	$mname = new FormElement('text', 'mname');
	$mname->addLayout('Name', 'Mid', 'set=32', 'size=6');
	$mname->addRule('maxLen=30');

	$lname = new FormElement('text', 'lname');
	$lname->addLayout('Name', '*Last', 'set=33', 'size=15');
	$lname->addRule('required', 'maxLen=30');

	$email = new FormElement('text', 'email');
	$email->addLayout('Email', '<b>Email cannot be changed once this user account is created</b>, please make sure it is correct.<br />Login <b>ID and password</b> will be sent to this email address.', 'size=40');
	$email->addRule('required', 'email', 'maxLen=60');

	$email2 = new FormElement('text', 'email2');
	$email2->addLayout('Re-enter Email', null, 'size=40');
	$email2->addRule('required', 'equals=email', 'maxLen=60');

	$org = new FormElement('text', 'org');
	$org->addLayout('Organization', null, 'size=40');
	$org->addRule('required', 'maxLen=100');

	$address = new FormElement('text', 'address');
	$address->addLayout('Address', null, 'size=40');
	$address->addRule('maxLen=50');

	$city = new FormElement('text', 'city');
	$city->addLayout('City', null, 'size=40');
	$city->addRule('maxLen=30');

	$region = new FormElement('text', 'region');
	$region->addLayout('Region', null, 'size=40' );
	$region->addRule('maxLen=30');

	$country = new FormElement('select', 'country', '', countryValab());
	$country->addLayout('Country');
	$country->addRule('required', 'maxLen=2');

	$postcode = new FormElement('text', 'postcode');
	$postcode->addLayout('Postcode');
	$postcode->addRule('maxLen=20');

	$phone = new FormElement('text', 'phone');
	$phone->addLayout('Phone');
	$phone->addRule('maxLen=20');

	$submit = new FormElement('submit', null, 'Submit');
	$form->addElement($fname, $mname, $lname, $email, $email2, $org, $address, $city, $region, $country,
	$postcode, $phone);

	if ($pId != null && $_SESSION['role'] == 40) {
//	if ($pId != null) {
		$isContactValab = array(
			1 => 'Check if this is a contact author'
		);
		$isContactAuthor =
			new FormElement('checkbox', "isContact", null, $isContactValab, null);
		$isContactAuthor->addLayout("Contact Author");
		$form->addElement($isContactAuthor);
	}

	$form->addElement($line, $submit);

 	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			switch ($nur) {

				case 40:
					$u = new Author();
					$url = 'a_main.php';
					break;
				case 31:
					$u = new ExternalReviewer();
					$url = 'r_delegate_paper.php';
					break;
				case 30:
					$u = new Reviewer();
					$url = 'c_programme_committee.php';
					break;
				case 20:
					$u = new Chair();
					$url = 'c_privilege.php';
					break;
			}

			$u->fname = $fname->value;
			$u->mname = $mname->value;
			$u->lname = $lname->value;
			$u->email = $email->value;
			$u->email2 = $email2->value;
			$u->org = $org->value;
			$u->address = $address->value;
			$u->city = $city->value;
			$u->region = $region->value;
			$u->country = $country->value;
			$u->postcode = $postcode->value;
			$u->phone = $phone->value;
			if ($pId != null && $_SESSION['role'] == 40)
				$u->isContactAuthor = $isContactAuthor->getSelectValue(0);

			DB::autocommit(false);

			switch ($nur) {

				case 40:
					if ($u->isExistingUser(20) || $u->isExistingUser(30) && 
					$_SESSION['role'] != 40 ) {
						$_SESSION['newAuthor4RC'] = $u;
						HTTP::redirect('add_user_rc.php');
					}
					
					$result = $u->add();
					if ($result == 1) {

						if ($pId == null) {
							$ci = $_SESSION['ci'];
							$message = "The Email is already registered with an existing Author.
							Click <a href=a_login.php?ci=$ci>here</a> to log in.";
							throw new Exception($message);
						}
					}
					$subject = 'Author';
					$template = 'new_account_author';

					// Add author for paper
					if ($pId != null && $_SESSION['role'] == 40) {
					//if ($pId != null ) {
						$paper->addAuthor($u);
					}

					break;
				case 31:
					$u->add($_SESSION['id']);
					$subject = 'External Reviewer';
					$template = 'new_account_external_reviewer';
					break;
				case 30:
					$u->add();
					$subject = 'Reviewer';
					$template = 'new_account_reviewer';
					break;
				case 20:
					$result = $u->add();
					$subject = 'Chair';
					$template = 'new_account_chair';
					$url .= "?c=$u->id";
					break;
			}

			// notification

			if ($nur == 40 || $nur == 31) {	// chair and reviewer notifications are not sent immediately
				if ($u->isNewUser == 1) {
					$e = new Email($subject, $template);
					$u->pwdSent();
				} else {
					$newSubject = 'New '.$subject.' Role';
					$template = 'new_role';
					$u = new Users($u->getIdByEmail());
					if (!$u->isPwdSent()) {
						$newPwd = $u->generatePwd();
						$u->changePassword($u->pwd, $newPwd, false);
						$u->pwd = $newPwd;
						$u->pwdSent();
						$e = new Email($newSubject, $template);
					} else {
						$e = new Email($newSubject, $template, null, true, false);
					}
					$e->addPlugin("NewRole:$subject");
				}
				$e->send($u);
			}

			DB::commit();

			// direct to main page if this is an author
			if ($nur == 40 && $pId == null) {
				if ($u->isNewUser == 1) {
					Auth::login($u->email, $u->pwd, 40, $_SESSION['ci']);
					$msg = "Your ID and password has been send to <b>$u->email</b>.<br />To set a new password, choose Edit Profile on the upper right.";
				}
				else {
/*
					$pwdHash = $u->getColByEmail('Pwd');
					Auth::login($u->email, $pwdHash, 40, $_SESSION['ci'], false);
*/
					$msg = "Your role as an author has been added successfully.";
					$_SESSION['authorIdForExistingRole'] = $u->uid;
					$url = "a_login.php?ci=".$_SESSION['ci'];
				}

				HTTP::message($msg, $url);
			}

			headerLocation($url);

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br /><br />';
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<h3><? echo $title; ?></h3>
<? echo REQUIRED_FIELDS; ?>
<p class=red><?php echo $oops; ?>
<?php $form->display(); ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
