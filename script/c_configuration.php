<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$track = new Track(1);

	$form = new HTMLForm('c_configuration');
	$submit = new FormElement('submit', null, 'Submit');
	$line = new FormElement('line', 'line');

	$yesNo = array(
		1 => 'Yes ',
		0 => 'No '
	);

	$isBlindReview = new FormElement('radio', 'isBlindReview', $track->isBlindReview, $yesNo);
	$isBlindReview->addLayout('Blind Review', NULL, 'radio=h');
	$isBlindReview->addRule('required');

	$isTwoPhase = new FormElement('radio', 'isTwoPhase', $track->isTwoPhase, $yesNo);
	$isTwoPhase->addLayout('Two Phase Submission', NULL, 'radio=h');
	$isTwoPhase->addRule('required');

//	$paperFormat = new FormElement('textarea', 'paperFormat', $track->getPaperFormatTextarea('Full'));
//	$paperFormat->addLayout('Paper Format', 'Please start each format in a new line.', 'cols=6', 'rows=5', 'wraps=off');

	$days = array(1 => '1', 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30, 31);
	$months = array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
	'October', 'November', 'December');
	
	for ($y = 0; $y <= 2; $y++)
		$years[CURRENT_YEAR + $y] = CURRENT_YEAR + $y;

	$dueAbstDay = new FormElement('select', 'dueAbstDay', $track->getDueDate('Abst', 'day'), $days);
	$dueAbstDay->addLayout('Abstract Submission Due', NULL, 'set=31');
	$dueAbstDay->addRule('required');

	$dueAbstMonth = new FormElement('select', 'dueAbstMonth', $track->getDueDate('Abst', 'month'), $months);
	$dueAbstMonth->addLayout('Abstract Submission Due', NULL, 'set=32');
	$dueAbstMonth->addRule('maxInt=12');
	$dueAbstMonth->addRule('required');

	$dueAbstYear = new FormElement('select', 'dueAbstYear', $track->getDueDate('Abst', 'year'), $years);
	$dueAbstYear->addLayout('Abstract Submission Due', NULL, 'set=33');
	$dueAbstYear->addRule('required', 'isInt', 'minInt=2006');

	$dueFullDay = new FormElement('select', 'dueFullDay', $track->getDueDate('Full', 'day'), $days);
	$dueFullDay->addLayout('Full Paper Submission Due', NULL, 'set=31');
	$dueFullDay->addRule('required');

	$dueFullMonth = new FormElement('select', 'dueFullMonth', $track->getDueDate('Full', 'month'), $months);
	$dueFullMonth->addLayout('Full Paper Submission Due', NULL, 'set=32');
	$dueFullMonth->addRule('required', 'maxInt=12');

	$dueFullYear = new FormElement('select', 'dueFullYear', $track->getDueDate('Full', 'year'), $years);
	$dueFullYear->addLayout('Full Paper Submission Due', NULL, 'set=33');
	$dueFullYear->addRule('required', 'isInt', 'minInt=2006');

//	$dueTopicExpertiseDay = new FormElement('select', 'dueTopicExpertiseDay', $track->getDueDate('TopicExpertise', 'day'), $days);
//	$dueTopicExpertiseDay->addLayout('Bidding Due - Topic Expertise', NULL, 'set=31');
//
//	$dueTopicExpertiseMonth = new FormElement('select', 'dueTopicExpertiseMonth', $track->getDueDate('TopicExpertise', 'month'), $months);
//	$dueTopicExpertiseMonth->addLayout('Bidding Due - Topic Expertise', NULL, 'set=32');
//	$dueFullMonth->addRule('maxInt=12');
//
//	$dueTopicExpertiseYear = new FormElement('select', 'dueTopicExpertiseYear', $track->getDueDate('TopicExpertise', 'year'), $years);
//	$dueTopicExpertiseYear->addLayout('Bidding Due - Topic Expertise', NULL, 'set=33');
//	$dueTopicExpertiseYear->addRule('isInt', 'minInt=2006');

	$duePaperExpertiseDay = new FormElement('select', 'duePaperExpertiseDay', $track->getDueDate('PaperExpertise', 'day'), $days);
	$duePaperExpertiseDay->addLayout('Bidding Due', NULL, 'set=31');
	$duePaperExpertiseDay->addRule('required');

	$duePaperExpertiseMonth = new FormElement('select', 'duePaperExpertiseMonth', $track->getDueDate('PaperExpertise', 'month'), $months);
	$duePaperExpertiseMonth->addLayout('Bidding Due', NULL, 'set=32');
	$dueFullMonth->addRule('required', 'maxInt=12');

	$duePaperExpertiseYear = new FormElement('select', 'duePaperExpertiseYear', $track->getDueDate('PaperExpertise', 'year'), $years);
	$duePaperExpertiseYear->addLayout('Bidding Due', NULL, 'set=33');
	$duePaperExpertiseYear->addRule('required', 'isInt', 'minInt=2006');

	$revPerPaper = new FormElement('text', 'revPerPaper', $track->revPerPaper);
	$revPerPaper->addLayout('Reviews per Paper', NULL, 'size=4');
	$revPerPaper->addRule('required', 'isInt', 'minInt=1');

	$maxPaperPerRev = new FormElement('text', 'maxPaperPerRev', $track->maxPaperPerRev);
	$maxPaperPerRev->addLayout('Maximum Papers per Reviewer', NULL, 'size=4');
	$maxPaperPerRev->addRule('required', 'isInt', 'minInt=1');

	$dueReviewDay = new FormElement('select', 'dueReviewDay', $track->getDueDate('Review', 'day'), $days);
	$dueReviewDay->addLayout('Review Submission Due', NULL, 'set=31');
	$dueReviewDay->addRule('required');

	$dueReviewMonth = new FormElement('select', 'dueReviewMonth', $track->getDueDate('Review', 'month'), $months);
	$dueReviewMonth->addLayout('Review Submission Due', NULL, 'set=32');
	$dueReviewMonth->addRule('required', 'maxInt=12');

	$dueReviewYear = new FormElement('select', 'dueReviewYear', $track->getDueDate('Review', 'year'), $years);
	$dueReviewYear->addLayout('Review Submission Due', NULL, 'set=33');
	$dueReviewYear->addRule('required', 'isInt', 'minInt=2006');

//	$dueGroupDay = new FormElement('select', 'dueGroupDay', $track->getDueDate('Group', 'day'), $days);
//	$dueGroupDay->addLayout('Group Discussion Due', NULL, 'set=31');
//
//	$dueGroupMonth = new FormElement('select', 'dueGroupMonth', $track->getDueDate('Group', 'month'), $months);
//	$dueGroupMonth->addLayout('Group Discussion Due', NULL, 'set=32');
//	$dueGroupMonth->addRule('maxInt=12');
//
//	$dueGroupYear = new FormElement('select', 'dueGroupYear', $track->getDueDate('Group', 'year'), $years);
//	$dueGroupYear->addLayout('Group Discussion Due', NULL, 'set=33');
//	$dueGroupYear->addRule('isInt', 'minInt=2006');
//
//	$dueGlobalDay = new FormElement('select', 'dueGlobalDay', $track->getDueDate('Global', 'day'), $days);
//	$dueGlobalDay->addLayout('Global Discussion Due', NULL, 'set=31');
//
//	$dueGlobalMonth = new FormElement('select', 'dueGlobalMonth', $track->getDueDate('Global', 'month'), $months);
//	$dueGlobalMonth->addLayout('Global Discussion Due', NULL, 'set=32');
//	$dueGlobalMonth->addRule('maxInt=12');
//
//	$dueGlobalYear = new FormElement('select', 'dueGlobalYear', $track->getDueDate('Global', 'year'), $years);
//	$dueGlobalYear->addLayout('Global Discussion Due', NULL, 'set=33');
//	$dueGlobalYear->addRule('isInt', 'minInt=2006');

	$autoAcceptThreshold = new FormElement('text', 'autoAcceptThreshold', $track->autoAcceptThreshold);
	$autoAcceptThreshold->addLayout('Automatic Acceptence Threshold', NULL, 'size=4');
	$autoAcceptThreshold->addRule('required', 'isInt');

	$dueFinalDay = new FormElement('select', 'dueFinalDay', $track->getDueDate('Final', 'day'), $days);
	$dueFinalDay->addLayout('Final Paper Submission Due', NULL, 'set=31');
	$dueFinalDay->addRule('required');

	$dueFinalMonth = new FormElement('select', 'dueFinalMonth', $track->getDueDate('Final', 'month'), $months);
	$dueFinalMonth->addLayout('Final Paper Submission Due', NULL, 'set=32');
	$dueFinalMonth->addRule('required', 'maxInt=12');

	$dueFinalYear = new FormElement('select', 'dueFinalYear', $track->getDueDate('Final', 'year'), $years);
	$dueFinalYear->addLayout('Final Paper Submission Due', NULL, 'set=33');
	$dueFinalYear->addRule('required', 'isInt', 'minInt=2006');

//echo '000';
//	$co = new Conference($_SESSION['ci']); // ???
//echo '111';

	$conf = $_SESSION['conference'];

	$shortName = new FormElement('text', 'shortName', $conf->shortName);
	$shortName->addLayout('Conference Short Name', '', 'size=25');
	$shortName->addRule('required', 'maxLen=30');

	$fullName = new FormElement('text', 'fullName', $conf->fullName);
	$fullName->addLayout('Conference Full Name', '',  'size=70');
	$fullName->addRule('required', 'maxLen=200');

	$url = new FormElement('text', 'url', $conf->url);
	$url->addLayout('Conference Website', '', 'size=40');
	$url->addRule('required', 'maxLen=200');

	$contact = new FormElement('text', 'contact', $conf->contactEmail);
	$contact->addLayout('Contact Email', '', 'size=40');
	$contact->addRule('required', 'email', 'maxLen=50');

	$from = new FormElement('text', 'from', $conf->fromEmail);
	$from->addLayout('From Email', '', 'size=40');
	$from->addRule('required', 'email', 'maxLen=50');

//	$reply = new FormElement('text', 'reply', $conf->replyEmail);
//	$reply->addLayout('Reply Email', '', 'size=40');
//	$reply->addRule('required', 'email', 'maxLen=50');

	$CC = new FormElement('text', 'CC', $conf->CC);
	$CC->addLayout('CC', 'Use , to separate multiple CC email addresses', 'size=70');
	$CC->addRule('maxLen=255');

	$BCC = new FormElement('text', 'BCC', $conf->BCC);
	$BCC->addLayout('BCC', 'Use , to separate multiple BCC email addresses', 'size=70');
	$BCC->addRule('maxLen=255');

	$logEmail = new FormElement('checkbox', 'logEmail', null,
	array(1 => 'Log outgoing emails in a text file'), array($conf->logEmail));
	$logEmail->addLayout('Log Email', null);

//	$venue = new FormElement('text', 'venue', $conf->venue);
//	$venue->addLayout('Venue', null, 'size=40');
//	$venue->addRule('maxLen=200');

	$form->addElement(
		$shortName,
		$fullName,
		$line,

		$url,
		$contact,
		$from,
//		$reply,
		$CC,
		$BCC,
		$logEmail,

//		$venue,
		$line,

		$isBlindReview,
		$isTwoPhase,
//		$topics,
		$line,

		$dueAbstDay,
		$dueAbstMonth,
		$dueAbstYear,
		$dueFullDay,
		$dueFullMonth,
		$dueFullYear,
		$duePaperExpertiseDay,
		$duePaperExpertiseMonth,
		$duePaperExpertiseYear,
		$revPerPaper,
		$maxPaperPerRev,
		$dueReviewDay,
		$dueReviewMonth,
		$dueReviewYear,
//			$dueGroupDay,
//			$dueGroupMonth,
//			$dueGroupYear,
//			$dueGlobalDay,
//			$dueGlobalMonth,
//			$dueGlobalYear,
//			$autoAcceptThreshold,
		$dueFinalDay,
		$dueFinalMonth,
		$dueFinalYear,
		$line,
		$submit
	);

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();
			$CCs = explode(',', $CC->value);
			foreach ($CCs as $cc) {
				if ($cc != null) {
					if (!ereg('^[a-zA-Z0-9 \._\-]+@([a-zA-Z0-9-][a-zA-Z0-9\-]*\.)+[a-zA-Z]+$',
					$cc)) {
						$CC->errors[] = 'Contains Invalid Email';
						throw new Exception('Please correct errors in the form.');
					}
				}
			}
			$BCCs = explode(',', trim($BCC->value));
			foreach ($BCCs as $bcc) {
				if ($bcc != null) {
					if (!ereg('^[a-zA-Z0-9 \._\-]+@([a-zA-Z0-9-][a-zA-Z0-9\-]*\.)+[a-zA-Z]+$', $bcc)) {
						$BCC->errors[] = 'Contains Invalid Email';
						throw new Exception('Please correct errors in the form.');
					}
				}
			}

			$track->isBlindReview = $isBlindReview->value;
			$track->isTwoPhase = $isTwoPhase->value;

			// topics
			$topics = $topics->value;

			$track->dueAbst = '"'.$dueAbstYear->value.'-'.$dueAbstMonth->value.'-'.$dueAbstDay->value.'"';
			$track->dueFull = '"'.$dueFullYear->value.'-'.$dueFullMonth->value.'-'.$dueFullDay->value.'"';
			$track->dueTopicExpertise = '"'.$duePaperExpertiseYear->value.'-'.$duePaperExpertiseMonth->value.'-'.$duePaperExpertiseDay->value.'"';
			$track->duePaperExpertise = '"'.$duePaperExpertiseYear->value.'-'.$duePaperExpertiseMonth->value.'-'.$duePaperExpertiseDay->value.'"';
			$track->dueReview = '"'.$dueReviewYear->value.'-'.$dueReviewMonth->value.'-'.$dueReviewDay->value.'"';
//			$track->dueGroup = '"'.$dueGroupYear->value.'-'.$dueGroupMonth->value.'-'.$dueGroupDay->value.'"';
//			$track->dueGlobal = '"'.$dueGlobalYear->value.'-'.$dueGlobalMonth->value.'-'.$dueGlobalDay->value.'"';
			$track->dueFinal = '"'.$dueFinalYear->value.'-'.$dueFinalMonth->value.'-'.$dueFinalDay->value.'"';


			$track->revPerPaper = $revPerPaper->value;
			$track->maxPaperPerRev = $maxPaperPerRev->value;
//			$track->autoAcceptThreshold = $autoAcceptThreshold->value;

			$conf->shortName = $shortName->value;
			$conf->fullName = $fullName->value;
			$conf->url = $url->value;
			$conf->contactEmail = $contact->value;
			$conf->fromEmail = $from->value;
//			$conf->replyEmail = $reply->value;
			$conf->CC = $CC->value;
			$conf->BCC = $BCC->value;
			$conf->logEmail = $logEmail->values[0] == null ? 0:1;
//echobr('bcc: '.$_POST['BCC']);
//echobr(isset($_POST['BCC']) ? 'yes':'no');
//exit;
			DB::autocommit(false);
			$track->update();
			DB::commit();

			DB::connect('common');
			DB::autocommit(false);
			$conf->update();
			DB::commit();

			$_SESSION['conference'] = new Conference($_SESSION['ci']);

			HTTP::message('Configuration updated successfully. Please continue to config topics, paper formats, and paper status.', "c_configuration.php");

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::menu(array(
		'Topics' 		=> array('c_configuration_edit_topics.php', 1),
		'Paper Formats'	=> array('c_configuration_paper_formats.php', 1),
		'Review Form' 	=> array('c_configuration_form.php', 1),
		'Paper Status'	=> array('c_configuration_ps_disp.php?template=hide', 1)
	));
?>
<h3>Conference Configuration</h3>
<p class=red><?php echo $oops; ?></p>
<?php
	$form->display();
	echo '<br /><br />';
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
