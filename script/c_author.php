<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperStatus.php';

	// List assignments ordered by sum of expertise levels

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(220);

	function main() {

		$track = new Track(1);
		$ps = new PaperStatus();
		$psValab = $ps->getPaperStatusValab();

		$a0 = $track->getAuthorsWithNoSubmissions();
		$a1 = $track->getAuthorPaper();
		$a2 = $track->getPaperDetails();

		if ($a0 != null) {

			$i = 1;
			foreach ($a0 as $a) {
				echo $i++.'. '.(DEBUG ? $a->id.'. ':'').$a->getName().' ('.$a->email.') has not submitted any abstracts yet. ';
				echo HTML::command('<span class=small>Delete</span>', "delete_user.php?u=$a->id&r=40").'<br />';
			}
		}

		echo '<br />';

		if ($a1 != null) {
			$j = 1;
			asort($a1);

			foreach ($a1 as $aId => $a) {
				$i = 0;
				unset($data);

				echo '<b>'.(DEBUG ? $aId.'. ':'').$a['name'].'</b>';
				echo ' <span class=small>'.$a['org'].' ('.$a['email'].') </span><br />';
				//todo
				//echo HTML::command('Delete', "delete_user.php?u=$aId&r=40").'</span>';
				foreach ($a['papers'] as $pId) {
					$data[$i][0] .= $a2[$pId]['title'].downloadLink($pId).'<br ><div class=small>'.$a2[$pId]['names'].'</div>';
					$status = $psValab[$a2[$pId]['paperStatus']];
					if ($a2[$pId]['paperStatus'] == -1)
						$status = makeup($status, 'white', 'red');
					$data[$i][1] = '<div class=small>'.$status.'</div>';
					$data[$i][2] = '<div class=small>'.substr($a2[$pId]['full1stSub'], 0, 10).'</div>';
					$data[$i][3] = '<div class=small>'.substr($a2[$pId]['final1stSub'], 0, 10).'</div>';
					$i++;
				}

				$table = new HTMLTable($data);
				$table->setTitle('Paper=600', 'Status=150', 'Full Paper=90', 'Final Paper=100');
				$table->layout['titleBold'] = 0;
				$table->display();
				echo '<br />';
			}
		} // end if
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('Paper', 'c_paper.php').' | '; ?>
<? echo HTML::command('Author', 'c_author.php', 'disabled').' | '; ?>
<? echo HTML::command('Late Abstract Notification', 'c_notification.php?n=abstLate').' | '; ?>
<? echo HTML::command('Late Full Paper Notification', 'c_notification.php?n=fullLate').' | '; ?>
<? echo HTML::command('Extension', 'c_extension.php'); ?>
<br />
<h3>Authors and their submissions</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
