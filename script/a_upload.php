<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(40);
	DB::connect(); 

	$paperId = isset($_GET['paperId']) ? $_GET['paperId']:$_SESSION['paperId']; $_SESSION['paperId'] = $paperId;
	$type = isset($_GET['type']) ? $_GET['type']:$_SESSION['type']; $_SESSION['type'] = $type;

	$t = new Track(1);
	$p = new Paper($paperId);

	// is an contact author of this paper
	$p->isContactAuthorCheck();

	// can edit at this stage
	if (!$p->isUploadable($type)) {
		HTTP::message("Cannot upload $type paper at this stage.");
	}


	$form = new HTMLForm('uploadForm');
	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Upload');

	$file = new FormElement('file', 'file');
	$desc = 'Maximum size: <b>'.MAX_UPLOAD_FILE_SIZE.'MB</b><br />';
	$desc .= $t->isBlindReview() ? '<b>Blind-review</b> conference, please do not include any authorship information in the file.<br />' : NULL;
	$desc .= 'Acceptable paper formats: <b>'.$t->getPaperFormatText($type).'</b>.<br />';
	$file->addLayout("Upload $type paper file", $desc);
	$file->addRule('required', 'MAX_FILE_SIZE='.MAX_UPLOAD_FILE_SIZE);
	$file->rules['formats'] = $t->getPaperFormatArray($type);

	$form->addElement($line, $file, $line, $submit);

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			DB::autocommit(false);
			$now = date('Y-m-d H:i:s');

			switch ($type) {
				case 'full':
					if ($p->full1stSub == NULL) {
						//echobr('full1stsub is null');
						$p->setCol('Full1stSub', $now);
					} else {
						$p->setCol('FullLastModi', $now);
					}
					$p->setCol('FullFormat', FILE::getUploadFileExt($file->name));
					$p->setCol('FullLastModi', $now);
					$fileName = 'full'.$p->id;
					$subject = 'Full Paper Submission';
					$template = 'full_submission';
					break;

				case 'final':
					if ($p->final1stSub == NULL) {
						//echobr('final1stsub is null');
						$p->setCol('Final1stSub', $now);
					} else {
						$p->setCol('FinalLastModi', $now);
					}
					$p->setCol('FinalFormat', FILE::getUploadFileExt($file->name));
					$p->setCol('FinalLastModi', $now);
					$fileName = 'final'.$p->id;
					$subject = 'Final Paper Submission';
					$template = 'final_submission';
					break;
			}

			//$delCmd = 'del '.File::getDefaultUploadDir().$fileName.'.*';
			//echobr($delCmd);
			// old file will be overwritten.
			// new format will be recorded, old format will stay there, but not referred to any further

			File::upload($file->name, $fileName);
			DB::commit();	// if file move command fails, db doesn't get updated.

                        $e = new Email($subject, $template);
                        $e->addPlugin("PaperTitle:$p->title");
                        $author = new Author($_SESSION['id']);
                        $e->send($author);

			HTTP::message("Your $subject was successful.", 'a_main.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<h3>
<?
	echo $type == 'full' ? 'Upload Full Paper File' : 'Upload Final Paper File';
	echo ': '.$p->title;
?>
</h3>
<p class=red><? echo $oops; ?><br /></p>
<? $form->display(); ?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
