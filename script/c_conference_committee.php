<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(201);

	$track = new Track(1);

	$chairIds = $track->getChairIds();

	if ($chairIds !=  null) {

		$i = 0;
		foreach ($chairIds as $cId) {

			$c = new Chair($cId);

			$data[$i][0] = $cId;
			$data[$i][1] .= (DEBUG ? $cId.'. ':'').$c->getName();
			$data[$i][2] .= $c->org.'<br /><span  class=smallgrey>'.$c->email.'</span>';
			$data[$i][3] = HTML::command('Privilege', "c_privilege.php?c=$cId").' | ';
			$data[$i][3] .= HTML::command('Send Invitation', "c_notification.php?n=inviteChair&uId=$cId").' | ';
			$data[$i][3] .= HTML::command('Delete', "delete_user.php?u=$cId&r=20");

			$i++;
		}
		if ($data != null) sort($data);

		$table = new HTMLTable($data, 0, 'asc');
		$table->setTitle('ID=30', 'Chair=200', 'Organization and Email=360','=400');
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::command('Add a Chair', 'add_user.php?nur=20').' | ';
	echo HTML::command('Send All Chair Invitation', "c_notification.php?n=inviteAllChair");
?>
<h3>Conference Committee</h3>
<?
	$table->display();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
