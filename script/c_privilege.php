<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// Phase control

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$chair = new Chair($_SESSION['id']);
	$chair->privilegeCheck(201);

	$cId = HTTP::sessionate('c');
	$c = new Chair($cId);
	$track = new Track(1);

	$form = new HTMLForm('c_privilege');
	$line = new FormElement('line', 'line');
	$submit = new FormElement('submit', null, 'Submit');

	$a1 = $track->getPrivilegeValab();
	$a2 = $c->getPrivilegeArray();

	if ($a1 != null) {

		$privCheckbox = new FormElement('checkbox', 'privilege', null, $a1, $a2);
		$form->addElement($privCheckbox, $line, $submit);

	}

	if (HTTP::isLegalPost()) {

		try {

			$form->validate();
			DB::autocommit(false);
			$c->updatePrivileges($privCheckbox->values);
			DB::commit();

			headerLocation("c_conference_committee.php");
		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Chair Privilege</h3>
<p class=red><?php echo $oops; ?>
<?php
	echo '<h4>'.$c->getName().'</h4>';
	$form->display()
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>