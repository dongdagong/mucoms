<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$user = new Users($_SESSION['id']);

	$form = new HTMLForm('form');
	
	$p0 = new FormElement('password', 'p0');
	$p0->addLayout('Old password:');
	$p0->addRule('required', 'passwordLen=6', 'maxLen=15');

	$p1 = new FormElement('password', 'p1');
	$p1->addLayout('New password:');
	$p1->addRule('required', 'passwordLen=6', 'maxLen=15');

	$p2 = new FormElement('password', 'p2');
	$p2->addLayout('Re-enter new password:');
	$p2->addRule('required', 'passwordLen=6', 'equals=p1', 'maxLen=15');


	$line = new FormElement('line', 'line');
	$submit = new FormElement('submit', 'change', 'Change');
	$form->addElement($p0, $p1, $p2, $line, $submit);

 	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			$e = new Email('New Password', 'change_password');
			DB::autocommit(false);
			$user->changePassword($p0->value, $p1->value);
			$user = new Users($_SESSION['id']);
			$user->pwd = $p1->value;
			$e->send($user);
			DB::commit();

			switch ($_SESSION['role']) {

				case 40:
					$url = 'a_main.php';
					break;
				case 31:
				case 30:
					$url = 'r_main.php';
					break;
				case 20:
					$url = 'c_main.php';
					break;
			}

			HTTP::message('Your password has been changed successfully.', $url);

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Change Password</h3>
<? echo REQUIRED_FIELDS; ?>
<p>Minimum length of password: 6</p>
<p class=red><?php echo $oops; ?>
<?php 	$form->display(); ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
