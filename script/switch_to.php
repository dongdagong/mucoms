<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	DB::connect();

	$role = $_GET['r'];
	$id = $_SESSION['id'];
	$ci = $_SESSION['ci'];
	$u = new Users($id);

	Auth::logout();

	switch ($role) {

		case 20:
			$url = 'c_main.php';
			break;
		case 30:
		case 31:
			$url = 'r_main.php';
			break;
		case 40:
			$url = 'a_main.php';
			break;
	}

	try {
		session_start();
		Auth::login($u->uid, $u->pwd, $role, $ci, false);
		HTTP::redirect($url);

	} catch (Exception $e) {

		$oops = $e->getMessage();
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Switching Roles</h3>
<p class=red><?php echo $oops.'<br />'; ?>
<?php ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>