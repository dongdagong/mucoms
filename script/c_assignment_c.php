<?
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Conflict.php';

	session_start();
	DB::connect();
	
	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(230);

	if (!HTTP::isLegalPost())
		HTTP::confirm(
			'This operation detects potential conflicts between authors and reviewers. <br />
			You will have an option whether to save or ignore the detected conficts. <br /><br />
			This operation requires Paper/Abstract Submission and Bidding phases to be closed. ',
			'c_assignment_c.php',
			'c_assignment.php',
			'Start detecting',
			'Return to previous page'
		);
	
	function title($msg, $newLine = 1) {
		echo "<b class=big>$msg</b>";
		if ($newLine ==1)
			echo '<br />';
	}

	$t = new Track(1);
	if ($t->phAbst == 1 || $t->phFull == 1 || $t->phPaperExpertise == 1)
		HTTP::message('Please close Paper/Abstract Submission and Bidding phases first.');
?>
<html>
<body>
<?
	$conflict = new Conflict(DETECTION_THRESHOLD);
	$conflict->countBid = false;

	// detect

	if (HTTP::isLegalPost()) {
		$conflict->detected = $_SESSION['detectedConflicts'];
		//unset($_SESSION['detectedConflicts']);
	} else {
		echo "comparison strictness = $conflict->strictness <br />";
		$conflict->detectAll();
		$_SESSION['detectedConflicts'] = $conflict->detected;
	}

	// display

	title(HTTP::isLegalPost() ? 'Checked conflicts are to be saved: ' : 'Conflicts detected:');
	$form = new HTMLForm('detectedConflicts');
	$submit = new FormElement('submit', 'submit', 'Save Checked Conflicts (along with Author Specified Conflicts)');
	$checkbox = array();
	$form->open();
	$conflict->displayDetected(&$checkbox, true);
	if (!HTTP::isLegalPost()) {
		echo $submit->getTag();
		echo '<br /><br />'.HTML::command('Return without saving', 'c_assignment.php');
	}
	$form->close();

	$checked = array();
	foreach($checkbox as $checkboxId => $cb ){
		if ($cb->values[0] == 1) {
			$pId = $checkboxId % 1000000;
			$checked[] = array(($checkboxId - $pId) / 1000000, $pId);	// rId => pId
		}
	}

	// save

	if (HTTP::isLegalPost()) {

		title('Saving detected and author specified conflicts: ');
		$conflict->generateToAdd('all', $checked);
		try {
			$conflict->save();
		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}
		title('success');

		title('Deleting biddings on withdrawn papers:');
		try {
			$conflict->deleteWithdrawnBids();
		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
		}
		title('success');

		echo "<b class=big><a href='c_assignment.php'>Return</a></b>";
	}

	if (HTTP::isLegalPost())
		unset($_SESSION['detectedConflicts']);
?>
</body>
</html>
<style type="text/css">
<!--
.red {
	color: #FF0000;
}
.blue {
	color: #0000FF;
}
.big {
	color: #FF0000;
	font-size: 26px;
}
-->
</style>
