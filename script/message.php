<?php 
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	
	session_start();
	
	if (isset($_SESSION['pageFormat'])) {
		$h = 'header'.$_SESSION['pageFormat'].'.php';
		$f = 'footer'.$_SESSION['pageFormat'].'.php';
	} else {
		$h = 'header.php';
		$f = 'footer.php';
	}
	unset($_SESSION['pageFormat']);

?>
<? include_once $h; ?>
<div class="main">
<p><p><p>
<?
	echo '<div class=dialogBox><br />';
	echo $_GET['message'];
	echo '<br /><br />';
	
	if (isset($_GET['nextURL']))
		echo 'Click <a href = "'.$_GET['nextURL'].'">here</a> to proceed.';
		
	echo '</div>';
?>
<p><p><p>
</div> <!-- end of main -->
<? include_once $f; ?>
