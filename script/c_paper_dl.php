<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$ps = new PaperStatus();
	$psValab = $ps->getPaperStatusValab();
	$pId = $_GET['p'];
	$paper = new Paper($pId);
	
	$tag = '<div class=a4widthGrey>';
	$tag .= '<br />ID: '.$pId.'<br /><br /><b>'.$paper->title.'</b><br />';
	$tag .= '<b></b> '.$paper->getAuthorNames().'<br /><br />';

	$tag .= '<b>Status:</b> '.$psValab[$paper->paperStatus].'<br /><br />';
	$tag .= '<b>Topic:</b> '.$paper->getTopicNames().'<br />';
	$tag .= '<span class=smallgrey>First one is the primary topic.</span><br /><br />';
	$tag .= '<b>Abstract:</b> '.$paper->abstract.'<br /><br />';

	$tag .= '<b>Download Paper:</b> '.$paper->getFullLink().' '.$paper->getFinalLink();
	$tag .= '<br /><span class=smallgrey>Link active once the full or final paper file is uploaded.</span><br />';
	$tag .= '<br /></div>';
?>
<? include 'header_simple.php'; ?>
<div class="main">
<? echo $tag; ?>
</div>
<? include 'footer_simple.php'; ?>
