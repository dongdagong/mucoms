<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(202);

	$form = new HTMLForm('form');
	$submit = new FormElement('submit', 'submit', 'Add');
	$text = new FormElement('textarea', 'test',
'Firstname1, Middlename1, Lastname1, Email1@email.com, Organization One, nz
, , Lastname2, Email2@email.com, Organization Two');
	$text->addLayout(null, null, 'rows=20', 'cols=85');
	
	$form->addElement($text);
	$rData = array();

	if (HTTP::isLegalPost()) {

		try {
			
			$form->validateWithException();
			$txt = trim($text->value);
			if (strlen($txt) == 0) throw new Exception('No input.');
			
			$rArray = explode(chr(13), $txt);
			$i = 0;
			DB::autocommit(false);
			foreach ($rArray as $rLine) {
				$u = new Reviewer();
				$rLine2 = explode(',', trim($rLine));
				$rData[$i][0] = $rLine2[0];
				$rData[$i][1] = $rLine2[1];
				$rData[$i][2] = $rLine2[2];
				$rData[$i][3] = $rLine2[3];
				$rData[$i][4] = $rLine2[4];
				$rData[$i][5] = $rLine2[5];

				$u->fname = trim($rLine2[0]);
				$u->mname = trim($rLine2[1]);
				$u->lname = trim($rLine2[2]);
				$u->email = trim($rLine2[3]);
				$u->org = trim($rLine2[4]);
				$u->country = trim($rLine2[5]);

				if (strlen($u->lname) == 0) throw new Exception('Last name missing: '.$rLine);
				if (strlen($u->email) == 0) throw new Exception('Email missing: '.$rLine);
				if (strlen($u->org) == 0) throw new Exception('Organization missing: '.$rLine);
				
				$ev = new FormElement('text', 'ev', $u->email);
				$ev->addRule('required', 'email', 'maxLen=50');
				if (!$ev->validate())
					throw new Exception("Invalid email <b>$u->email</b>: $rLine");
					
				$u->add();
				$i++;
			}
			DB::commit();
		} catch (Exception $e) {
			$oops = $e->getMessage().' <br />Number of reviewers added in this batch: 0 <br /><br />';
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Batch Add Reviewers</h3>
<div class=red><? echo $oops; ?></div>
<?php
	if (HTTP::isLegalPost() && $oops == null) {

		$i = 1;
		foreach ($rData as $rLine) {
			echo "Added: $i. ";
			foreach ($rLine as $r) {
				echo $r.', ';
			}
			echo '<br />';
			$i++;
		}

		echo '<br />';
		echo HTML::command('Return', 'c_programme_committee.php');
		echo '<br /><br />';
	} else {

		$form->open();
		echo 'FirstName, MiddleName, *LastName, *Email, *Organization, CountryCode [carriage return]<p />
		Each line represents one reviewer.<br />
		Only LastName, Email, and Organization are required fields. <br />
		You may leave other fields out but the commas should be kept (except the trailing ones).<p />';
		$text->display();
		$submit->display();
		$form->close();
	}
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
