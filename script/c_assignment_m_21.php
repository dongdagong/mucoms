<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(231);

	$reviewer = new Reviewer($_GET['r']);
	$reviewerName = $reviewer->getName();

	$track = new Track(1);
	$pe = $reviewer->getBidExpertise(1);

	$data = array();
	if (true) {

		$tag = '';

		$all = $track->getPaperDetails();
		$withdrawn = $track->getPapersByStatus(-1);
		$got = $reviewer->getAssignmentValab();
		$numGot = count($got);
		if ($numGot > 0)
			$tag = "<b>$numGot paper".($numGot > 1 ? 's':'').' already assigned: </b><br />';
		foreach ($got as $pId=>$title){
			$tag .= (DEBUG ? $pId.'. ':'').$all[$pId]['title'].'<br />';
			unset($all[$pId]);
		}
		$tag .= '<br />';

		if ($all != null) {
			$i = 1;
			foreach ($all as $pId=>$p) {

				if (isset($withdrawn[$pId]) && DEBUG) {
					$tag .= "<br />Debug: paper $pId is withdrawn<br /><br />";
					continue;
				}

				//$i = count($data);
				$data[$i][0] .= $pe[$pId];
				$data[$i][1] .=  '<div><b>'.(DEBUG ? $pId.'. ':'').$all[$pId]['title'].'</b>'.downloadLink($pId).'<br ></div>';
				$data[$i][1] .= "<div class=small>";
				$data[$i][1] .=  $all[$pId]['names'].'<br />';
				$data[$i][1] .= 'Abstract: '.$all[$pId]['abstract'].'<br />';
				$data[$i][1] .= '</div>';
				$data[$i][2] .= $p['numAssi'];
				$data[$i][3] = HTML::command("Add this paper", "c_assignment_m_x.php?p=$pId&r=".$_GET['r']."&a=add");
				$i++;
			}
		}
		arsort($data);
		$pe = $track->getPaperExpertiseValab();
		for ($i = 1; $i <= count($data); $i++) {
			$data[$i][0] = $pe[$data[$i][0]];
			$data[$i][0] = '<span class=small>'.$data[$i][0].'</span>';
		}
		$table = new HTMLTable($data);
		$table->setTitle('Expertise=100', 'Paper to Add=750', 'Assignments=100', '=100');
		$tag .= $table->getTag();
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('By Paper', 'c_assignment_m_10.php').' | '; ?>
<? echo HTML::command('By Reviewer', 'c_assignment_m_20.php#'.$_GET['r']).'<br />'; ?>
<h3>Add Papers to <? echo $reviewerName; ?> </h3>
<div class=red><? echo $oops; ?></div>
<?php echo $tag; ?>
</div>
<!-- end of main -->

<?php include 'footer.php'; ?>