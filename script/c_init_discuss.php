<?
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Thread.php';
	include_once 'c_init_discuss_0.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(250);

	$pId = HTTP::sessionate('pId');	

	try {
		
		if (is_numeric($pId)) {
			
			$paper = new Paper($pId);

			if ($paper->isInConflict())
				$confirmQuestion = 'Do you want to continue?';
			else
				$confirmQuestion = makeup(
					'This paper is not in review conflict. Initiate discussion anyway?',
					'red',
					'#EEEEEE'
				);
			
			HTTP::confirm(
				"Initiate discussion for $paper->title: <br /><br />
				This operation will send email notifications to all reviewers and external <br />
				reviewers of this paper. The involved reviews will be definialized as well.<br /><br />
				$confirmQuestion",
				'c_init_discuss.php',
				'c_selection.php',
				'Yes',
				'Cancel'
			);
			
			
			DB::autocommit(false);
			initDiscuss($pId);
			DB::commit();

			$_SESSION['pId'] = null;
			unset($_SESSION['pId']);
   					
			HTTP::message("Discussion initiated for $paper->title.", "forum_disp_thread.php?pId=$pId");
		
		} 
		
		if ($pId == 'all') {
			
			HTTP::confirm(
				"This operation will send email notifications to all reviewers and external <br />
				reviewers who have conflicting reviews. The involved reviews will be definialized as well.<br /><br />
				Do you want to continue?",
				'c_init_discuss.php',
				'c_selection.php',
				'Yes',
				'Cancel'
			);
			
			$track = new Track(1);
			$papers = $track->getNonWithdrawnPapers();
			
			if ($papers == null) return;
			
			$errMsg = null;
			foreach($papers as $pId) {
				
				$paper = new Paper($pId);
				if ($paper->isInConflict()) {
					try {
						DB::autocommit(false);
						initDiscuss($pId);
						DB::commit();
					} catch (Exception $e) {
						
						DB::rollback();
						$errMsg .= "An error occured fininalising $paper->title<br />";
						$errMsg .= 'Error message: '.$e->getMessage().'<br /><br />';
					}
				}
			}
			if ($errMsg != null) 
				throw new Exception('Discussion not initiated for the 
				following papers: <br /><br />'.$errMsg);					
			
			HTTP::message('Discussion initiated for conflicting papers.', 'c_selection.php');
		}

	} catch (Exception $e) {
		$oops = $e->getMessage().'<br /><br />';
	}
?>
<? include 'header.php'; ?>

<div class="main">
<h3>Initialize discussions for papers with conflicting reviews</h3>
<div class=red><? echo $oops; ?></div>
</div> <!-- end of main -->

<? include 'footer.php'; ?>
