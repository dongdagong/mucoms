<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(40);
	DB::connect();
	//echobr('php_self: '.$_SERVER['PHP_SELF']);

	function main() {

		$author = new Author($_SESSION['id']);
		$papers = $author->getPapers();

		if (count($papers) == 0) {
			echo 'No submission yet.';
			return;
		}

		$ps = new PaperStatus();
		$psValab = $ps->getPaperStatusValab();

		$i = 1;
		foreach ($papers as $paperId) {

			$p = new Paper($paperId);
			$t = new Track($p->trackId);

			echobr($i++.'. '.'<b>'.(DEBUG ? $p->id.'. ':'').$p->title.'</b>');
			echobr($p->getAuthorNames());
			
			//$maskedPaperStatus = $t->releasePaperStatus == 1 ? $p->paperStatus : 0;
			$maskedPaperStatus = $p->isDecisionNotified == 1 || $p->paperStatus == -1 ? $p->paperStatus : 0;
			echobr('<br />Paper Status: <b>'.$psValab[$maskedPaperStatus].'</b>');

			// track info only if multi track

			echobr("Topic: ".$p->getTopicNames());

			echo '<br />';

			$dates = new HTMLTable($p->getDates());
			$dates->setTitle('=120', 'First Submission=150', 'Last Update=150', 'Due=210');
			$dates->layout['titleBold'] = 0;
			$dates->display();

			echo '<br/>';

			// commands

			if ($p->paperStatus != -1 && $p->isContactAuthor()) {

				// for each command: label, link, status

				// edit
				echo HTML::command('Edit', "a_edit.php?paperId=$p->id", $p->isEditable() ? 'normal':'disabled' );

				// full paper
				$cmd = $p->isFullUploaded() ? 'Update Full Paper' : 'Upload Full Paper';
				echo ' | '.HTML::command($cmd, "a_upload.php?paperId=$p->id&type=full",
				$p->isUploadable('full') ? 'normal':'disabled' );

				// specify conflict
				if ($t->isBlindReview == 1) { // ??
					echo ' | '.HTML::command('Specify Conflicts', "a_specify_conflicts.php?paperId=$p->id",
					$p->canSpecifyConflicts() ? 'normal':'disabled');
				}

				// final paper
				if ($p->isSelected()) {
					$final = $p->isFinalUploaded() ? 'Update Final Paper' : 'Upload Final Paper';
					echo ' | '.HTML::command($final,  "a_upload.php?paperId=$p->id&type=final",
					$p->isUploadable('final') ? 'normal':'disabled' );
				}

				// display review
				echo ' | '.HTML::command('Display Review', "view_review.php?paperId=$p->id",
//				$author->isReviewReady($paperId) ? 'normal':'disabled');
				($t->phFinal == 1) ? 'normal':'disabled');

				// withdraw
				echo ' | '.HTML::command('Withdraw', "a_withdraw.php?paperId=$p->id",
				$p->canWithdraw() ? 'normal':'disabled');
			}

			echo ('<hr size=1 /><p>');
		}
	}
?>
<? include_once 'header.php'; ?>

<div class="main">
<h3>Submitted Abstracts/Papers</h3>
<? main(); ?>
</div> <!-- end of main -->

<? include_once 'footer.php'; ?>
