<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$role = $_SESSION['role'];
	if (!($role == 30 || $role == 31))
		HTTP::message('Access denied');

	$r = $_SESSION['role'] == 30 ? new Reviewer($_SESSION['id']) : new ExternalReviewer($_SESSION['id']);
/*
	Cannot add this feature because the chair will later finalise all 
	if ($role == 30) {
		$cnt = "You have finished $r->numFinished of $r->numAssigned assignments. ";
	}
*/
	
	$t = new Track(1);
	$today = date('Y-m-d');
	$dDiff = dateDiff($today, $t->dueReview);
	$dueReview = "Review Submission Due [$t->dueReview]";
	if ($dDiff <= 30 && $dDiff >= 0)
		$dueReview = makeup($dueReview.' in '.$dDiff.' '.($dDiff > 1 ? 'days':'day'),
		'black', 'orange');
	elseif ($dDiff < 0)
		$dueReview = makeup($dueReview, 'white', 'red');
	else
		$dueReview = $dueReview;

//	$dueReview = $cnt.$dueReview;
	
	$ps = new PaperStatus();
	$psValab = $ps->getPaperStatusValab();

	function main() {

		global $r, $today, $t;
		$papers = $r->getAssignments();	// it's actually getDelegations for external reviewers.

		if ($t->phReview != 1 && datediff($today, $t->dueReview) > 0) {
			echo "Review phase is not open.";
		} else {
			$i = 1;
			if ($papers != null) {
				sort($papers);
				foreach ($papers as $p) {
					paperInfo($i, $p);
					$i++;
				}
			}
		}
	}

	function isER() {

		return $_SESSION['role'] == 31 ? true:false;
	}

	function paperInfo($i, $paperId) {

		global $r, $t, $psValab;

		$p = new Paper($paperId);
		$reviewerId = isER() ? $r->getFromId($paperId) : $r->id;
		$review = new Review($reviewerId, $paperId, $p->trackId);

		// commands

		echo '<div class=right>';

		// discuss

		if ($p->isDiscussionInitiated == 1) {
			echo makeup(HTML::command('<b>Discuss</b>', "forum_disp_thread.php?pId=$p->id"), 'white', '#DDDDDD');
			echo '<br />';
		}

		if ($t->phReview == 0)
			echo 'Review phase: close <br />';
		else {

			// edit review

			$status = $review->isEditable() ? 'normal':'disabled';
			echo HTML::command('Edit Review', "r_edit_review.php?reviewerId=$reviewerId&paperId=$p->id", $status);
			echo '<br />';

			// last review time

			$lastModi = $review->getLastModi() == null ? 'Not reviewed yet': 'Last Reviewed:<br />'.
			$review->getLastModi();
			echo "<div class=smallgrey>$lastModi</div>";

			// external reviewer finish review

			if (isER()) {
				echo HTML::command('Finish Review',
				"r_finish.php?reviewerId=$reviewerId&paperId=$paperId&trackId=$p->trackId",
				$review->isDelegateFinished() || $review->isFinalized() ? 'disabled':'normal');
				echo '<br />';
			}

			// reviewer finalize review

			if (!isER()) {
				echo HTML::command('Finalize Review',
				"r_finalize.php?paperId=$paperId&trackId=$p->trackId",
				$review->isFinalized() || $review->getLastModi() == null ? 'disabled':'normal');
				echo '<br />';
			}

			// date finalized

			if (!isER() && $review->isFinalized()) {
				echo '<div class=smallgrey>Date Finalized:<br />'.$review->getDateFinalized().'</div>';
			}
		}

		// view all reviews
		$status = $review->isFinalized() ? 'normal':'disabled';
		echo HTML::command('View All Reviews', "view_review.php?paperId=$p->id", $status);
		echo '<br />';

		echo '</div>';


		// paper info

		echo '<div class=a4width>';

		//echo '<b>';

		if ($review->isFinalized())
			echo makeup('[Finalized]', 'white', 'green').'<br />';

		if (isER()) {
			echo '[Delegated From: '.$r->getFromName($paperId).']';
		} else {
			if ($r->getToName($paperId) != NULL){
				echo '[Delegated To: '.$r->getToName($paperId).']';
			}
		}
		if ($review->isDelegateFinished())
			echo ' [Delegate Finished]';

		//echo '</b>';


		echo '<div class=titlecell>';
		//echo $i.'. ';
		echo "[ID:$p->id] ";
		echo '<b>'.$p->title.'</b> ';
		echo ' <span class=small>'.$p->getFullLink().' '.$p->getFinalLink().'<br /></span>';
		echo '</div>';

		if ($t->isBlindReview != 1) {
			echo $p->getAuthorNames();
			//echo ' (<span class=small>authorship info for testing only)</span><br /> ';
		}

		//$maskedPaperStatus = $t->releasePaperStatus == 1 ? $p->paperStatus : 0;
		$maskedPaperStatus = $p->isDecisionNotified == 1 ? $p->paperStatus : 0;
		echobr('<br />Paper Status: <b>'.$psValab[$maskedPaperStatus].'</b>');

		// track info only if multi track

		echobr("Topic: ".$p->getTopicNames());
		if ($_GET['abst'] == 'show')
			echobr("<br />Abstract: $p->abstract");
		echo '<br /><br /><br />';

		echo '</div>';
		echo ('<hr size=1 /><p>');
	}
?>
<? include_once 'header.php'; ?>

<div class="main">
<?
	echo '<span class=right0>'.$dueReview.'</span>';
	if ($t->phReview == 1) echo ' <a href="r_form.php" target="_blank">Printable Review Form</a> | ';
	$cmdLabel = $_GET['abst'] == 'show' ? 'Hide Abstract' : 'Show Abstract';
	$cmdLink = $_GET['abst'] == 'show' ? 'r_main.php' : 'r_main.php?abst=show';
	echo HTML::command($cmdLabel, $cmdLink).' | ';
	echo HTML::command('Review Result Report', 'report_1.php').'<br />';
?>
<h3><? 
	if ($t->phReview != 1 && datediff($today, $t->dueReview) > 0)
		$num = null;
	else
		$num = "[$r->numAssigned papers]";
	echo $_SESSION['role'] == 30 ? "Assignments $num" : ' Delegations'; 
?></h3>
<? main(); ?>
</div> <!-- end of main -->

<? include_once 'footer.php'; ?>
