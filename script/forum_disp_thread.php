<?php
//update thread set ParentID = null;
//delete FROM threadmsg;
//delete FROM thread;
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Thread.php';

	session_start();
	DB::connect();

	$pId = HTTP::sessionate('pId');
	if ($pId == null)
		HTTP::message('pId is not provided');

	$thread = new Thread();
	$p = new Paper($pId);


	if ($_SESSION['role'] == 40 || !isset($_SESSION['role'])) {
		HTTP::message('Access denied.');
	}

	// check paper belongs to reviewer/er
	if ($_SESSION['role'] == 30) {
		$user = new Reviewer($_SESSION['id']);
		if (!$user->hasPaper($pId))
			HTTP::message('You do not have this assignment.');
	}
	if ($_SESSION['role'] == 31) {
		$user = new ExternalReviewer($_SESSION['id']);
		if (!$user->hasPaper($pId))
			HTTP::message('You do not have this delegation.');
	}

	// check chair has privilege
	if ($_SESSION['role'] == 20) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(250);	// paper selection
	}

	function main() {
		global $pId, $thread, $p;
		try {
			$i = 1;
			$a = $thread->getThread($pId);
			if ($a != null) {
				foreach ($a as $m) {
					//$color = $i % 2 == 0 ? 'cell':'shadedcell';
					//echo "<div class=$color>";
					
					echo '<div class=greybox>';

					echo '<br />';
					echo '<span class=right><b>#'.$i.'</b></span>';

					echo '<div class=a4width>'; 
					if ($m['Title'] != null) echo '<b>'.($m['Title']).'</b><br />';
					echo '<b>'.
						$m['Who'].'</b> <span class=small>'.
						$m['TimePosted'].'</span><br /><br />';
					$msg = html_entity_decode(str_replace(chr(13), '<br />', $m['Msg']));
					$msg = str_replace('<blockquote>', '<div class=blockquote>', $msg);
					$msg = str_replace('</blockquote>', '</div>', $msg);
					echo html_entity_decode(str_replace(chr(13), '<br />', $msg)).'<br />';

					if ($m['TimePosted'] != $m['TimeModified'])
						echo '<span class=small><br />Last modifed: '.$m['TimeModified'].'</span><br />';
					
					echo '</div>'; // a4width

					echo '<span class=right>';
					echo HTML::command('Reply',
						"forum_add_msg.php?cmd=reply&pId=$pId&parentId=".$m['ID'].
						'&threadId=0'."&relativeId=$i");
					if ($m['UserID'] == $_SESSION['id']) { 
						echo ' | ';
						echo HTML::command('Edit',
						"forum_add_msg.php?cmd=edit&pId=$pId&parentId=0&threadId=".$m['ID']).'<br />';
					}
					echo '</span>';

					if (DEBUG)
						echo '<br /><span class=small>ID: '.$m['ID'].' ParentID: '.
						$m['ParentID'].' PaperID: '.$pId.'</span><br />';
					echo '<br />';
					echo '<br />';
					//echo '</div>';	// color
					echo '</div>';
					echo '<br />';
					$i++;
				}
			} else
				echo 'No message posted yet.<br /> ';
			echo '<br />';
			echo HTML::command('Post new message',
				"forum_add_msg.php?cmd=new&pId=$pId&parentId=0&threadId=0").'<br />';
		} catch (Exception $e) {
			$oops = $e->getMessage().'<br />';
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3><? echo "Discussion thread for: $p->title"; ?></h3>
<div class=red><? echo $oops; ?></div>
<?
	main();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
