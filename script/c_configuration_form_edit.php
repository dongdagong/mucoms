<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Chair.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Notification.php';
	include_once 'cms/Assignment.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$t = new Track(1);

	$cmd = HTTP::sessionate('cmd');
	
	if ($cmd == 'Finalize') {
		DB::query('update Track set IsFormFinalized = 1', &$result);
		HTTP::message('Review form finalized.', 'c_configuration_form.php');
	}
	
	if ($cmd == 'Definalize') {
		
		$assi = new Assignment();
		$msg = null;
		$canAuto = $assi->canPerformAutoAssignment(&$msg);
		if ($canAuto != true)
			HTTP::message($msg.'Cannot definalize review form at this stage.', 'c_configuration_form.php');
		HTTP::confirm(
			"Warning! <p />
			Definalizing review form makes it editable again. Deleting or rearranging 
			review questions in the form then will damage review data. <br /><br />
			Do you want to continue?",
			"c_configuration_form_edit.php?cmd=Definalize",
			'c_configuration_form.php',
			'Yes',
			'Cancel'
		);
		DB::query('update Track set IsFormFinalized = 0', &$result);
		HTTP::message('Review form definalized.', 'c_configuration_form.php');
	}
	
	if ($t->isFormFinalized == 1)
		HTTP::message('Review form already finalized.', 'c_configuration_form.php');
			
	$title = '';
	if ($cmd == 'add') {
		$questionType = HTTP::sessionate('questionType');
		$title = 'New Form Question - '.$questionType;
		if ($questionType != 'Choice' && $questionType != 'Comment') HTTP::message('questionType missing');
		$isConfidential = 0;
		$isRequired = 1;
		$q = new FormQuestion(9999, 9999, 1, 1);
	}
	
	if ($cmd == 'update' || $cmd == 'delete') {
		$questionNum = HTTP::sessionate('questionNum');
		if (!is_numeric($questionNum)) HTTP::message('questionNum missing');
		DB::query('select max(ID) from FormQuestion', &$result);
		$max = DB::getCell($result);
		if ($questionNum > $max || $questionNum < 1)
			HTTP::message('Invalid questionNum.');
			
		$q = new FormQuestion(9999, 9999, 1, $questionNum);
		$questionType = $q->questionType;
		$title = 'Update Form Question - '.$questionType;
		$isRequired = $q->isRequired;
		$isConfidential = $q->isConfidential;
		
		if ($questionType == 'Choice') {	
			$choicesText = null;
			foreach ($q->answers as $value => $label) {
				//$choicesText .= $label.chr(13);	// leads to heading invisible chars
				$choicesText .= "$label\r\n";
			}		
		}
	}
	
	try {
		if ($cmd == 'delete') {
			HTTP::confirm(
				"Delete $q->question?",
				"c_configuration_form_edit.php?cmd=delete&questionNum=$q->id",
				'c_configuration_form.php',
				'Yes',
				'Cancel'
			);
			DB::autocommit(false);
			$q->delete();
			DB::commit();
			HTTP::redirect('c_configuration_form.php');
		}		
	} catch (Exception $e) {
		HTTP::message($e->getMessage());
	}
	
	// display
	
	$form = new HTMLForm('c_configuration_form');
	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Submit');
	
	$question = new FormElement('text', 'question', $cmd == 'update' ? $q->question:null);
	$question->addLayout('Question', null, 'size=64');
	$question->addRule('required', 'maxLen=50');
	$form->addElement($question, $line); 	
	
	if ($questionType == 'Comment') {
		
	}
	
	if ($questionType == 'Choice') {
		
		$choices = new FormElement('textarea', 'choices', $cmd == 'update' ? $choicesText:null);
		$choices->addLayout(
			'Choices', 
			'<b>Represent each option as a new line.<br />
			If the question is about selection eligibility, list the options in increasing order of acceptance.</b> <br />
			For example, a three-option question will be listed as Reject (line 1), Neutral (line 2), and Accept (line 3).', 
			'rows=8', 'cols=55'
		);
		$choices->addRule('required');
		$form->addElement($choices);
	}
	
	$yesNoValab = array(1 =>'Confidential, answers to this question will not be viewed by the authors.');
	$yesNoValues[0] = $isConfidential == 1 ? 1:0;
	$isConfidential = new FormElement('checkbox', 'isConfidential', null, $yesNoValab, $yesNoValues);
	
	$yesNoValab = array(1 =>'Required, reviewer must provide answer to this question.');
	$yesNoValues[0] = $isRequired == 1 ? 1:0;
	$isRequired = new FormElement('checkbox', 'isRequired', null, $yesNoValab, $yesNoValues);
	
	$form->addElement($line, $isConfidential, $isRequired, $line, $submit);

	
	// add or update  
	
	if (HTTP::isLegalPost()) {
		
		try {
		
			$form->validateWithException();
			
			DB::autocommit(false);
			
			$q->question = $question->value;
			$q->questionType = $questionType;
			
			$q->isConfidential = $isConfidential->values[0] == 1 ? 1:0;
			$q->isRequired = $isRequired->values[0] == 1 ? 1:0;
			
			if ($questionType == 'Choice') {
				$choicesArray = explode("\r\n", $choices->value);
				$q->answers = array();
				$i = 1;
				foreach ($choicesArray as $value => $label) {
					$row = trim(str_replace("\r\n", '', $label));
					if ($row != '')
						$q->answers[$i++] = $row;
				}
				if (count($q->answers) < 2) {
					throw new Exception('Please enter at least two options.');
				}
			}
			
			if ($cmd == 'add') 
				$q->add();
			if ($cmd == 'update')
				$q->update();
			
			DB::commit();

			unset($_SESSION['cmd']);
			unset($_SESSION['questionType']);
			unset($_SESSION['questionNum']);
			
			HTTP::redirect('c_configuration_form.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<h3><? echo $title; ?></h3>
<? echo REQUIRED_FIELDS; ?>
<p class=red><? echo $oops; ?></p>
<?
	$form->display();
?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
