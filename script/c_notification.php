<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Notification.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(220);

	// input check
	$n = HTTP::sessionate('n');
	$uId = HTTP::sessionate('uId');
	$uIds = array();

	// full name of the notification
	// return url
	switch ($n) {

		case 'abstLate':
			$name = 'Send late abstract submission notification?';
			$url = 'c_author.php';
			break;

		case 'fullLate':
			$name = 'Send late full paper submission notification?';
			$url = 'c_author.php';
			break;

		case 'bidLate':
			$name = 'Send late bidding notification?';
			$url = 'c_bid.php';
			break;

		case 'assignment':
			$name = "Send assignment notification? <br />
			Reviewers will not see their assignments until Review Submission phase is open.";
			$url = 'c_assignment.php';
			break;

		case 'reviewLate':
			$name = 'Send late review submission notification?';
			$url = 'c_review.php';
			break;

		case 'decision':
			if ($_SESSION['_sendDecisionToOnlyOnePaper'] == true) { 
				$onePaper = new Paper($_SESSION['_pId']);
				$title = $onePaper->title.'<p />';
			}
			$name = $title.'Release paper status and send decision notification?';
			$url = 'c_selection.php';
			break;

		case 'finalLate':
			$name = 'Send late final paper submission notification?';
			$url = 'c_selection.php';
			break;

		case 'inviteChair':
			$u = new Users($uId);
			$name = 'Send chair invitation to '.$u->getName().'?';
			$url = 'c_conference_committee.php';
			break;

		case 'inviteReviewer':
			$u = new Users($uId);
			$name = 'Send reviewer invitation to '.$u->getName().'?';
			$url = 'c_programme_committee.php';
			break;

		case 'inviteAllChair':
			$t = new Track(1);
			$uIds = $t->getChairIds();
			$name = 'Send chair invitations?';
			$url = 'c_conference_committee.php';
			break;

		case 'inviteAllReviewer':
			$t = new Track(1);
			$uIds = $t->getReviewers();
			$name = 'Send reviewer invitations?';
			$url = 'c_programme_committee.php';
			break;


	} // switch

	// confirm
	HTTP::confirm(
		$name,
		'c_notification.php',
		$url,
		'Yes',
		'Cancel'
	);

	// send the notification
	try {
		$noti = new Notification($n);
		if ($n == 'inviteAllChair' or $n == 'inviteAllReviewer') {
			foreach ($uIds as $uId) {
				if ($uId != $_SESSION['id']) {
					//$u = new Users($uId);
					$noti->send($n == 'inviteAllChair' ? 'inviteChair':'inviteReviewer', $uId);
				}
			}
		} else {
			
			$noti->send($n, $uId);
		}

		HTTP::message('Message sent.', $url);
	} catch (Exception $e) {
		$oops = $e->getMessage();
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Notification</h3>
<div class=red><? echo $oops; ?></div>
<?php //echo $tag; ?>
</div>
<!-- end of main -->

<?php include 'footer.php'; ?>
