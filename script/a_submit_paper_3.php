<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	//	main step of paper submission: paper information

	session_start();
	Auth::loginCheck(40);
	DB::connect();

	if (!isset($_SESSION['numAuthors'])) {
		HTTP::redirect('a_submit_paper_2.php');
	}

	$myNum = isset($_SESSION['myNum']) ? $_SESSION['myNum'] : -1;

	$form = new HTMLForm('a_submit_paper_3', null, null, 'multipart/form-data');

	$line = new FormElement('line');

	$title = new FormElement('text', 'title');
	$title->addLayout('Paper Title', null, 'size=65');
	$title->addRule('required', 'maxLen=150');

	$form->addElement($title, $line);

	$isContactValab = array(
		1 => 'Check if you want this author to reveive email notifications and be able to update this submission.'
	);
	
	$isStudentPaperValab = array(
		1 => 'Check if this is a student paper. Please refer to the submission notification email for more information.'
	);

	$me = new Author($_SESSION['id']);

	for ($i = 1; $i <= $_SESSION['numAuthors']; $i++) {

		$authorLabel = "Author <b>#$i</b><br />";

		$fname[$i] = new FormElement('text', "fname$i", $i == $myNum ? $me->fname:NULL);
		$fname[$i]->addLayout($authorLabel, 'First', 'set=31', 'size=15');
		$fname[$i]->addRule('maxLen=30');

		$mname[$i] = new FormElement('text', "mname$i", $i == $myNum ? $me->mname:NULL);
		$mname[$i]->addLayout($authorLabel, 'Middle', 'set=32', 'size=7' );
		$mname[$i]->addRule('maxLen=30');

		$lname[$i] = new FormElement('text', "lname$i", $i == $myNum ? $me->lname:NULL);
		$lname[$i]->addLayout($authorLabel, '* Last', 'set=33', 'size=15');
		$lname[$i]->addRule('required', 'maxLen=30');

		$email[$i] = new FormElement('text', "email$i", $i == $myNum ? $me->email:NULL);
		$email[$i]->addLayout('Email', NULL, 'size=40');
		$email[$i]->addRule('required', 'email', 'maxLen=60');

		$email2[$i] = new FormElement('text', "email2$i", $i == $myNum ? $me->email:NULL);
		$email2[$i]->addLayout('Re-enter Email', NULL, 'size=40');
		$email2[$i]->addRule('required', 'equals=email'.$i, 'maxLen=60');

		$org[$i] = new FormElement('text', "org$i", $i == $myNum ? $me->org:NULL);
		$org[$i]->addLayout('Organization', null, 'size=40');
		$org[$i]->addRule('required', 'maxLen=100');

		$country[$i] = new FormElement('select', "country$i",  $i == $myNum ? $me->country:NULL, countryValab());
		$country[$i]->addLayout("Country", null, 'checkbox=h');
		$country[$i]->addRule('required');

		$isContactAuthor[$i] = new FormElement('checkbox', "isContact$i", null, $isContactValab, $i == $myNum ? array(1=>1):NULL);
		$isContactAuthor[$i]->addLayout("Contact Author");

		$form->addElement($fname[$i], $mname[$i], $lname[$i], $email[$i], $email2[$i], $org[$i], $country[$i], $isContactAuthor[$i], $line);
	}

	$abstract = new FormElement('textarea', 'abstract');
	$abstract->addLayout('Abstract', 'Maximum 3000 characters.<br /><br />', 'cols=64', 'rows=4');
	$abstract->addRule('required', 'maxLen=3000');

	$keywords = new FormElement('text', 'keywords');
	$keywords->addLayout('Keywords', 'Use comma to seperate the keywords.', 'size=65');
	$keywords->addRule('required', 'maxLen=255');

	$track = new Track($_SESSION['selectedTrack']);

	$temp = $track->getTopicsValab(1);
	$priTopicValab[-1] = '- Not Selected -';
	foreach ($temp as $tId => $tName)
		$priTopicValab[$tId] = $tName;
	$priTopic = new FormElement('select', 'priTopic', -1, $priTopicValab);
	$priTopic->addLayout('Primary Topic', null, 'size=65');
	$priTopic->addRule('required', 'minInt=1');

	$secTopics = new FormElement('checkbox', 'secTopics', null, $track->getTopicsValab());
	$secTopics->addLayout('Secondary Topics');
	
/*
	// Required only if the track uses single phase submission.
	$fullFile = new FormElement('file', 'fullFile');
	$desc = $track->isTwoPhase()? 'Paper submission is <b>two-phase</b>, you may choose to upload full paper later on.<br />' : NULL;
	$desc .= $track->isBlindReview() ? '<b>Blind-review</b> conference, please do not include any authorship information in the full paper file.<br />' : NULL;
	$desc .= 'Acceptable full paper formats: <b>'.$track->getPaperFormatText('full').'</b>.<br />';
	$fullFile->addLayout('Full Paper File', $desc);
	if (!$track->isTwoPhase())
		$fullFile->addRule('required');
	$fullFile->addRule('MAX_FILE_SIZE='.MAX_UPLOAD_FILE_SIZE); // MB
	$fullFile->rules['formats'] = $track->getPaperFormatArray('full');
*/

	$isStudentPaper = new FormElement('checkbox', "isStudentPaper", null, $isStudentPaperValab, null);
	$isStudentPaper->addLayout('Student Paper');

	$submit = new FormElement('submit', null, 'Submit Abstract');
	$form->addElement($abstract, $keywords, $line, $priTopic, $line, $secTopics);
	
	if ($_SESSION['ci'] == 'ci5') $form->addElement($line, $isStudentPaper);
	
	$form->addElement($line, $submit);
	

	// track already selected in step 0.

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			// save the paper info
			$p = new Paper();
			$p->trackId = $_SESSION['selectedTrack'];
			$p->paperStatus = 0;
			$p->title = $title->value;
			$p->keywords = $keywords->value;
			$p->abstract = $abstract->value;

			$now = date('Y-m-d H:i:s');
			$p->abst1stSub = $now;
			$p->abstLastModi = $now;
			$p->priTopic = $priTopic->value;

			$p->secTopics = $secTopics->values; 

			$foundContactAuthor = false;
			for ($i = 1; $i <= $_SESSION['numAuthors']; $i++) {

				$a = new Author();

				$a->fname = $fname[$i]->value;
				$a->mname = $mname[$i]->value;
				$a->lname = $lname[$i]->value;
				$a->email = $email[$i]->value;
				$a->org = $org[$i]->value;
				$a->country = $country[$i]->value;
				$a->isContactAuthor = $isContactAuthor[$i]->getSelectValue(0);
				if ($a->isContactAuthor == 1) $foundContactAuthor = true;
				$a->serial = $i;

				$p->authors[] = $a;
			}

			if (!$foundContactAuthor)
				throw new Exception('Please check at least one author as the Contact Author.');
			
			$p->isStudentPaper = $isStudentPaper->getSelectValue(0); 
			
			DB::autocommit(false);
			
			$p->add();

			$e = new Email('Abstract Submission', 'abstract_submission');
			$e->addPlugin("PaperTitle:$p->title");
			foreach ($p->authors as $author) {
				if ($author->isContactAuthor == 1)
					$e->send($author);
			}

			DB::commit();

			if ($track->isTwoPhase == 0) {
				HTTP::redirect("a_upload.php?paperId=$p->id&type=full");
			}

			HTTP::message(urlencode("Your abstract submission for <b>".
			$p->title."</b> is saved."), 'a_main.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<?php include_once 'header.php'; ?>
<div class="main">
<h3>Paper Submission - Step 2</h3>
<? echo REQUIRED_FIELDS; ?>
<p class=red><?php echo $oops; ?></p>
<?php $form->display(); ?>
</div> <!-- end of main -->
<?php include_once 'footer.php'; ?>
