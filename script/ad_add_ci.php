<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Administrator.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php'; 
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::adminCheck();

	$done = false;

	$form = new HTMLForm('form');
	$line = new FormElement('line', 'line', 'line');
	$submit = new FormElement('submit', null, 'Submit');

	$cShort = new FormElement('text', 'cShort');	// using shortName instead of cShort crashes this program. strange
	$cShort->addLayout('Conference Short Name', '',  'size=25');
	$cShort->addRule('required', 'maxLen=30');

	$fullName = new FormElement('text', 'fullName');
	$fullName->addLayout('Conference Full Name', '',  'size=70');
	$fullName->addRule('required', 'maxLen=200');

	$fname = new FormElement('text', 'fname');
	$fname->addLayout('1st Chair Name', 'First', 'set=31', 'size=15');
	$fname->addRule('maxLen=30');
	$mname = new FormElement('text', 'mname');
	$mname->addLayout('1st Chair Name', 'Mid', 'set=32', 'size=6');
	$mname->addRule('maxLen=30');
	$lname = new FormElement('text', 'lname');
	$lname->addLayout('1st Chair Name', '*Last', 'set=33', 'size=15');
	$lname->addRule('required', 'maxLen=30');

	$email = new FormElement('text', 'email');
	$email->addLayout('1st Chair Email',
	'<b>Login ID and password</b> will be sent to this email address.', 'size=40');
	$email->addRule('required', 'email', 'maxLen=60');

	$org = new FormElement('text', 'org');
	$org->addLayout('Conference Organization', '', 'size=40');
	$org->addRule('required', 'maxLen=100');

	$form->addElement($cShort, $fullName, $line, $fname, $mname, $lname, $email, $line,
	$org, $line, $submit);

	$cId = null;
	$admin = new Administrator();
	$ok = true;
	
 	if (HTTP::isLegalPost()) {
		try {
			$form->validateWithException();
			$ok = false;

			$conf = new Conference();
			$chair = new Chair();

			// create conference instance

			$conf->shortName = $cShort->value;
			$conf->fullName = $fullName->value;
			$conf->contactEmail = $email->value;
			$conf->fromEmail = $email->value;

			$chair->fname = $fname->value;
			$chair->mname = $mname->value;
			$chair->lname = $lname->value;
			$chair->email = $email->value;
			$chair->org = 'Please update';
			$chair->country = 'Please update';

			$admin->addNewCI($conf, $chair, &$cId);
			
			// create directories and copy templates
			// may need chmod !!

			mkdir('../upload/ci'.$cId);
			mkdir('../template/ci'.$cId);	
			chmod('../upload/ci'.$cId, 0770);
			chmod('../template/ci'.$cId, 0770);
			$src = '../template/ci0';
			$dest = '../template/ci'.$cId.'/';
			$dir = dir($src);
			while (false !== $entry = $dir->read()) {
				if ($entry == '.' || $entry == '..') {
					continue;
				}
				copy("$src/$entry", "$dest/$entry");
			}
			touch('../logs/ci'.$cId.'_notification_log.txt');
			touch('../logs/ci'.$cId.'_log.txt');
			chmod('../logs/ci'.$cId.'_notification_log.txt', 0200);
			chmod('../logs/ci'.$cId.'_log.txt', 0200);
			$dir->close();

			// send email notification of 1st chair's account

/*
			$e = new Email('New Chair Account', 'new_account_1st_chair');
			$e->addPlugin("InviterName:Admin");
			$e->send($chair);
*/

			$tag = "Conference instance for $conf->shortName has been created successfully. <br /><br />";
			$tag .= "Please manually email these login info to the 1st chair. <br /><br />";
			$tag .= "ID: $chair->uid <br />";
			$tag .= "Password: $chair->pwd <br /><br />";
			$tag .= "Email: $chair->email <br />";

			$tag .= '<br />Click '.HTML::command('here', 'ad_main.php').' to continue.';
			$done = true;

		} catch (Exception $e) {
			$oops = $e->getMessage();
			if ($ok != true)
				$admin->deleteCI($cId);
		}
	}

?>
<?php include 'header_simple.php'; ?>

<div class="main">
<h3>Add a new conference instance</h3>
<?php echo '<p class=red>'.$oops.'</p>'; ?>
<?php
	if (!$done) {
		$form->display();
		echo '<br />'.HTML::command('Return', 'ad_main.php');
	} else
		echo $tag;
?>
</div> <!-- end of main -->

<?php include 'footer_simple.php'; ?>
