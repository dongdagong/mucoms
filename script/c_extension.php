<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// List assignments ordered by sum of expertise levels

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(221);

	if (isset($_POST['sel']) && isset($_POST['days']) && isset($_POST['type'])) {

		try {
			$p = new Paper($_POST['sel']);
			DB::autocommit(false);
			$p->setCol($_POST['type'], $_POST['days']);
			DB::commit();
			HTTP::message('Extension granted.', 'c_extension.php');
		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

	function main() {

		$track = new Track(1);

		$name = new FormElement('text', 'name');
		$name->addLayout('Author', null, 'size=40');
		$nameForm = new HTMLForm('nameForm');
		$search = new FormElement('submit', 'submit', 'Search');

		$nameForm->open();
		echo 'Author name or email: '.$name->getTag().' '; $search->display();
		$nameForm->close(); echo '<br />';

		$authors = $track->searchUser(40, $name->value);

		if (count($authors) > 0){

			$grantForm = new HTMLForm('grantForm');
			$submit = new FormElement('submit', 'grant', 'Grant');
			$days = new FormElement('text', 'days');
			$days->addLayout(null, null, 'size=3');
			$days->addRule('isInt', 'minInt=1', 'maxInt=90');
			$typeValab = array(
//				'LateAbst' => 'late abstract',
				'LateFull' => 'late full paper',
				'LateFinal' => 'late final paper'
			);
			$type = new FormElement('select', 'type', 1, $typeValab);

			$grantForm->open();
			foreach ($authors as $aId) {
				author($aId);
				echo '<hr size=1 />';
			}
			echo '<br /><br />';
			echo 'Allow the selected paper '.$days->getTag().' more days for '.$type->getTag().' submission. ';
			$submit->display();
			$grantForm->close();
		}
	}

	function author($aId) {

		$a = new Author($aId);
		$papers = $a->getPapers();

		echo '<h4>'.(DEBUG ? $a->id.'. ':'').$a->getName().'</h4>';

		if (count($papers) == 0)
			echo 'No submission<br /><br />';

		$i = 1;
		foreach ($papers as $paper) {

			$paperValab = array(
				$paper => '',
			);
			$sel = new FormElement('radio', 'sel', '', $paperValab);
			echo str_replace('div', 'span', str_replace('<br />', '', $sel->getTag()));

			echo $i.'. ';
			$i++;
			paperInfo($paper);
		}
	}

	function paperInfo($paperId) {

		$p = new Paper($paperId);
		$t = new Track($p->trackId);

		echobr('<b>'.$p->title.'</b>');
		echobr($p->getAuthorNames());
		echobr("<br />Paper Status: <b>$p->paperStatus</b>");

		// track info only if multi track

		echobr("Topic: ".$p->getTopicNames());

		echo '<br />';

		$dates = new HTMLTable($p->getDates());
		$dates->setTitle('=120', 'First Submission=150', 'Last Modification=150', 'Due=210');
		$dates->layout['titleBold'] = 0;
		$dates->display();
		echo '<br />';
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('Paper', 'c_paper.php').' | '; ?>
<? echo HTML::command('Author', 'c_author.php').' | '; ?>
<? echo HTML::command('Late Abstract Notification', 'c_notification.php?n=abstLate').' | '; ?>
<? echo HTML::command('Late Full Paper Notification', 'c_notification.php?n=fullLate').' | '; ?>
<? echo HTML::command('Extension', 'c_extension.php', 'disabled'); ?>
<br /><br />
<h3>Grant extension on selected paper</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>