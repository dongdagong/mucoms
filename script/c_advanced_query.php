<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();
	$conn = DB::getConn();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(203);

	$form = new HTMLForm('form');
	$submit = new FormElement('submit', 'submit', 'Submit');
	$text = new FormElement(
		'textarea', 
		'text',
"select u.FName, u.LName, p.Title, u.Email 
from Users u, Writes w, Paper p 
where 
  p.PaperStatus > 0 and
  p.ID = w.PaperID and 
  u.ID = w.AuthorID  
order by p.Title, u.LName;"
	);
	$text->addLayout(null, 'Only SELECT clauses are allowed.
	Use ; to separate multiple queries. ', 'rows=10', 'cols=85');

?>
<?php include 'header.php'; ?>

<div class="main">
<h3>SQL Queries</h3>
<div class=red><? echo $oops; ?></div>
<?php
	$text->value = stripslashes($text->value);
	echo '<a href="c_db_schema.php" target="_blank">List Tables</a><br /><br />';

	$form->open();
	$text->display();
	echo '<br />';
	$submit->display();
	$form->close();

	// display

	if (HTTP::isLegalPost()) {

		try {

			// validate input

			$words = array(
				'connect',
				'create',
				'alter',
				'drop',
				'update',
				'insert',
				'delete'
			);

			$queries = explode(';', $text->value);

			foreach ($queries as $q) {


				if (strlen(trim($q)) == 0)
					continue;

				foreach ($words as $word) {
					if (!strstr(strtolower(trim($q)), $word.' ') === FALSE) {
						throw new Exception("<b>$word</b> is not allowed in the query.<br />");
					}
				}

				DB::query($q, &$result);

				echo "<br />Number of rows returned: $result->num_rows <br />";
				echo '<br /><table>';

				// title

				$fields = $result->fetch_fields();
				$title = '<tr>';
				foreach ($fields as $field) {
					$len = $field->max_length;
					$title .= "<td class=titlecell><b>".
					nbspStr($field->name, $len > 50 ? 50:$len)."</b></td>";
				}
				$title .= '</tr>';

				// rows

				$i = 0;
				$j = 0;
				while ($row = $result->fetch_assoc()){
					if ($j++ % 50 == 0)
						echo $title;

					echo '<tr>';
					foreach ($row as $cell) {
						echo '<td class='.($i == 0 ? 'cell':'shadedcell').">$cell</td>";
					}
					$i = $i == 0? 1:0;
					echo '</tr>';
				}

				echo '</table><br />';
			}

		} catch (Exception $e) {
			echo '<br /><span class=red>Error: '.$e->getMessage().'</span><br />';
		}
	}
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
