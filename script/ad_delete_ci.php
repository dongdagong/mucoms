<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Administrator.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php'; 
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::adminCheck();

	$id2Del = HTTP::sessionate('id2Del');
	$admin = new Administrator();
	
	DB::connect('common');
	$conf = new Conference($id2Del);
	
/*
	DB::connect("ci$id2Del");
	
	$_SESSION['pageFormat'] = '_simple';
	HTTP::confirm(
		"This operation deletes all the databases and folders/files of conference instance $conf->shortName. <br />
		<br />Do you want to continue?",
		'ad_delete_ci.php',
		'ad_main.php',
 		"Yes, delete $conf->shortName",
		'Cancel'
	);
	unset($_SESSION['pageFormat']);
*/

	if ($_GET['token'] == sha1($id2Del + 1)) {
		try {
			//DB::connect("ci$id2Del");
			$admin->deleteCI($id2Del);
			$tag = "Conference instance $conf->shortName deleted.<br /><br />";
			$tag .= "Click <a href=ad_main.php>here</a> to continue.";
			
		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}
	else {
		$tag = "<b>
			Warning! <p />
			This operation deletes all the databases and folders/files of conference instance $conf->shortName. <br />
			<br />Do you want to continue? </b><br /><br /><p />".
			
			HTML::command("Yes, delete $conf->shortName", "ad_delete_ci.php?id2Del=$id2Del&token=".sha1($id2Del + 1)).' | '.
			HTML::command('Cancel', 'ad_main.php');
	}
?>
<?php include 'header_simple.php'; ?>

<div class="main">
<?php echo '<p class=red>'.$oops.'</p>'; ?>
<?php
	echo "<div class=dialogBox>$tag</div>";
?>
</div> <!-- end of main -->

<?php include 'footer_simple.php'; ?>
