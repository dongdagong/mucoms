<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	session_start();

	if (isset($_SESSION['role']))
		HTTP::message('You are already logged in.');

	$tag = '<br />';
	$oops = '';

	if (!isset($_GET['ci'])) {

		$tag .= '<h3>Welcome to MuCoMS</h3>';

		$tag .= '
        <br />

        <p>
         Welcome to the Multi Conference Management System (MuCoMS). You are accessing the main MuCoMS installation hosted at <a href="http://www.mucoms.org/">http://www.mucoms.org/</a>.
        </p>

        <p>
         Currently, we have the following active conference management system instances:
        </p>

        <menu>
         <li>
          <p>
           The Sixth Asia-Pacific Conference on Conceptual Modelling (<a href="index.php?ci=ci5" target="_top">APCCM 2009</a>)
          </p>
         </li>
        </menu>

        <p> &nbsp;</p>

        <p>
         Previoulsy, the following events have been organised using the MuCoMS system:
        </p>

        <menu>
         <li>
          <p>
           The Fifth Asia-Pacific Conference on Conceptual Modelling (APCCM 2008)
          </p>
         </li>

         <li>
          <p>
           The Fifth International Symposium on Foundations of Information and Knowledge Systems (FoIKS 2008)
          </p>
         </li>

         <li>
          <p>           The First International Symposium on Asset Management (ISAM 2007)
          </p>
         </li>
         <li>
          <p>
           The First International Workshop on Conceptual Modelling for Life Sciences Applications (CMLSA 2007)
          </p>
         </li>
        </menu>
		';

	} else {

		$ci = $_GET['ci'];
		try {
			DB::connect('common');
			$conf = new Conference($ci);
			if ($conf->id == null)
				throw new Exception('Conference instance does not exist');

			$conf->shortName = $conf->shortName;
			$tag .= "<h3>$conf->shortName - MuCoMS Roles</h3>";

			$tag .= "
</p>
<br />
<p>
Welcome to the Multi Conference Management System (MuCoMS) instance
for $conf->shortName. Please, take one of the following roles that are
supported by MuCoMS:
</p>

<ol>
	<li>
		<b>Author</b>: You can create an author account, which will permit
		the submission of abstracts and papers to $conf->shortName. <br /> <br />
		<ul>
			<li> <a href=\"a_login.php?ci=$ci\">Author Login page</a> <br /></li>
		</ul>
	</li>

	<li>
		<b>Chair</b> (by invitation only): As a chair, you can configure
		and use the $conf->shortName submission and review system in accordance
		with the privileges associated with your chair account. <br /> <br />
		<ul>
			<li> <a href=\"login.php?ci=$ci&amp;r=20\">Member Login page</a> <br /></li>
		</ul>
	</li>

	<li>
		<b>External Reviewer</b> (by invitation only): As an external
		reviewer, you can access a small number of papers that have been
		delegated to you for review. <br /> <br />
		<ul>
			<li> <a href=\"login.php?ci=$ci&amp;r=31\">Member Login page</a> <br /></li>
		</ul>
	</li>

	<li>
		<b>Reviewer</b> (by invitation only): As a reviewer, you can
		participate in the paper bidding process and download, delegate,
		review and discuss papers that have been assigned to you for
		review. <br /> <br />
		<ul>
			<li> <a href=\"login.php?ci=$ci&amp;r=30\">Member Login page</a> <br /></li>
		</ul>
	</li>
</ol>
			";
		} catch (Exception $e) {
			$oops = 'Error fetching conference instance';
			$tag = '';
		}
	}
?>
<? include_once 'header_simple.php' ?>
<body>
<div class="main">
<?
	echo $oops;
	echo $tag;
?>
</div> <!-- end of main -->

<?php include_once 'footer_simple.php'; ?>
