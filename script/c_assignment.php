<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::command("Conflict Detection", 'c_assignment_c.php').' | ';
	echo HTML::command("Auto-Assignment", 'c_assignment_a_1.php').' | ';
	echo HTML::command("Backup", 'c_assignment_br.php?assignmentData=bak').' | ';
	echo HTML::command("Restore", 'c_assignment_br.php?assignmentData=res').' | ';
	echo HTML::command("Manual By Paper", 'c_assignment_m_10.php').' | ';
	echo HTML::command("Manual By Reviewer", 'c_assignment_m_20.php').' | ';
	echo HTML::command("Assignment Notification", 'c_notification.php?n=assignment');
?>
<br />
<h3>Steps to assign papers to reviewers</h3>
<div class=a4width>
<ol>
<li><b>Conflict Detection</b><p>
Conflicts between papers and reviewers might have already been specified by authors (specify conflicts) 
or reviewers (bidding). Conflict Detection discovers the  missing ones. These conflict information 
will be used in automatic assignment to avoid unwanted assignments. 

<li><b>Automatic Assignment </b><p>
Two options are available for this step. Choose 'new assignment' to generate a new set of assignments
and override existing ones. Choose 'incremental assignment' to generate additional assignments on top 
of existing ones (to fill the void producted by manual assignment). You can 
<? echo HTML::command('set a cap', 'c_programme_committee_set_max_assi.php'); ?>
 on the number of assignments for each individual reviewer.

<li><b>Backup/Restore Assignment</b><p>
Choose Backup to save the current assignment in a separate space. Choose Restore to replace the current
assignment with the saved backup. 

<li><b>Manual Assignment by Paper/Reviewer </b><p>
This is where manual (and hopefully minor) assignment adjustments are made. Steps 2 to 4 can be iterated 
until a satisfiable assignment is made. 

<li><b>Assignment Notification </b><p>
No notification emails to reviewers are sent until this is performed. After this step you will not be 
able to Restore assignment.
</ol>
</div>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
