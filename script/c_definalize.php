<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Review.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/FormQuestion.php';


	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(240);

	$p = HTTP::sessionate('p');
	$r = HTTP::sessionate('r');
	try {
		$paper = new Paper($p);
	} catch (Exception $e) {
		HTTP::message($e->getMessage());
	}

	$review = new Review($r, $p, 1);
	$reviewer = new Reviewer($r);

	// confirmation
	HTTP::confirm(
		'Reviewer: '.$reviewer->getName().'<br />'.
		"Paper: $paper->title <br /><br />".
		'Definalize this review so the reviewer can make changes to it?<br />',
		'c_definalize.php',
		'view_review.php?paperId='.$p,
		'Yes',
		'Cancel'
	);

	DB::autocommit(false);
	$review->definalize();
	DB::commit();

	HTTP::redirect('view_review.php?paperId='.$p);
?>