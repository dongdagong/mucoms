<?
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	session_start();
	DB::connect('common');

	$showForm = true;

	$form = new HTMLForm('adminLogin');
	$uid = new FormElement('text', 'uid');
	$uid->addLayout('ID', null, 'size=25');
	$pwd = new FormElement('password', 'pwd');
	$pwd->addLayout('Password', '', 'size=25');
	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Log in');
	$form->addElement($uid, $pwd, $line, $submit);

	try {
		if (isset($_SESSION['adminUID'])) {
			$showForm = false;
			throw new Exception('You are already logged in as administrator.');
		}
	} catch (Exception $e) {
		$oops = $e->getMessage();
	}

	if (HTTP::isLegalPost()) {
		try {
			$form->validateWithException();

			$query = "select count(*) num from Administrator
			where UID = '$uid->value' and Pwd = sha1('$pwd->value')";
			DB::query($query, &$result);
			$num = DB::getCell(&$result);
			if ($num != 1) {
				throw new Exception('Invalid ID or password, please try again.');
			}
			$_SESSION['adminUID'] = $uid->value;

			HTTP::redirect('ad_main.php');

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}
?>

<? include_once 'header_simple.php'; ?>

<div class="main">
<h3><? echo SYSTEM_NAME; ?> Administrator Login</h3>
<p class=red><?php echo $oops; ?><br /></p>
<?
	if ($showForm)
		$form->display();
	else
		echo 'Click '.HTML::command('here', 'ad_main.php').' to continue.';
?>
<p>
</div> <!-- end of main -->

<? include_once 'footer_simple.php'; ?>
