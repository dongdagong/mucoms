<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperStatus.php';

	// List all the papers

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(220);

	function main() {

		$track = new Track(1);
		$topicsValab = $track->getTopicsValab();
asort($topicsValab);
		$total = 0;
		$ps = new PaperStatus();
		$psValab = $ps->getPaperStatusValab();
		
		$i = 0;
		$data = array();
		foreach ($topicsValab as $topicId=>$topicName) {
			$paperTopic = new PaperTopic();
			$paperIds = $paperTopic->getPaperIds($track->id, $topicId);
			if ($paperIds != null) {
				foreach ($paperIds as $paperId) {

					$paper = new Paper($paperId);
					$data[$i][0] = $paperId;
					$data[$i][1] = $paper->title.' <span class=small>'.downloadLink($paperId).' ';
					$data[$i][1] .= HTML::command('Delete', 'c_delete_paper.php?p='.$paperId).'</span><br />';
					$data[$i][1] .= '<span class=small>'.$paper->getAuthorNames().'<br />';
					//$data[$i][1] .= 'Abstract: '.$paper->abstract.'</span><br /><br />';

					//$data[$i][2] = warning($paper->numAssi, $track->revPerPaper);
					$data[$i][2] = $paper->numAssi;
					$data[$i][3] = $paper->isFullUploaded() ? 'submitted' : makeup('not submitted', 'white', 'red');
					$data[$i][3] = '<span class=small>'.$data[$i][3].'</span>';
					$data[$i][4] = $paper->isFinalUploaded() ? 'submitted' : 'not submitted';
					$data[$i][4] = '<span class=small>'.$data[$i][4].'</span>';
					$data[$i][5] = $paper->paperStatus == -1 ?
						makeup($psValab[$paper->paperStatus], 'white', 'red'):$psValab[$paper->paperStatus];
					$data[$i][5] = '<span class=small>'.$data[$i][5].'</span>';
					
					$data[$i][6] = $topicName;
					
					$i++;
					$total++;
				}
			}
		}

		$table = new HTMLTable($data, 6, 'asc');
		$table->setCategory(6, 'Primary Topic');
		$table->setTitle("ID=30", "Paper=650", 'Assignment=90', 'Full=70', 'Final=70', 'Status=200', 'Primary Topic=80');
		$table->layout['titleBold'] = 0;

		if (count($table->data) > 0)
			$table->display();
		echo "<br /><br /><b>$total papers in total.</b>";
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('Paper', 'c_paper.php', 'disabled').' | '; ?>
<? echo HTML::command('Author', 'c_author.php').' | '; ?>
<? echo HTML::command('Late Abstract Notification', 'c_notification.php?n=abstLate').' | '; ?>
<? echo HTML::command('Late Full Paper Notification', 'c_notification.php?n=fullLate').' | '; ?>
<? echo HTML::command('Extension', 'c_extension.php'); ?>
<br />
<h3>Submitted Papers</h3>
<?php main(); ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
