<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(202);
	
	global $cnt;
	$track = new Track(1);

	$a1 = $track->getReviewers();

	if ($a1 !=  null) {

		$i = 0;
		foreach ($a1 as $rId) {

			$r = new Reviewer($rId);

			$data[$i][0] = $rId;
			$data[$i][1] .= (DEBUG ? $rId.'. ':'').$r->getName();
			$data[$i][2] .= $r->org.'<br /><span  class=smallgrey>'.$r->email.'</span>';
			$data[$i][3] = HTML::command('Send Invitation', "c_notification.php?n=inviteReviewer&uId=$rId").' | ';
			$data[$i][3] .= HTML::command('Delete', "delete_user.php?u=$rId&r=30");

			$i++;
		}
		if ($data != null) sort($data);

		$table = new HTMLTable($data, 0, 'asc');
		$table->setTitle('ID=30', 'Reviewer=200', 'Organization and Email=360','=400');
	}
	
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::menu(
		array(
			'Add a Reviewer' => array('add_user.php?nur=30', 1),
			'Batch Add Reviewers' => array('c_batch_add.php', 1),
			'Set Maximum Assignments' => array('c_programme_committee_set_max_assi.php', 1),
			'Send All Reviewer Invitation' => array("c_notification.php?n=inviteAllReviewer", 1)
		)
	);	
?>
<h3><? echo 'Programme Committee ['.count($data).' members]'; ?></h3>
<div class=red><? echo $oops; ?></div>
<?
	if (isset($table)) $table->display();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
