<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Report.php';
	include_once 'c_selection_0.php';	
	include_once 'cms/PaperStatus.php';

	session_start();
	$roleId = $_SESSION['role'];
	Auth::loginCheck($roleId);
	DB::connect();

	switch ($roleId) {
		case 20:
			$c = new Chair($_SESSION['id']);
			$c->privilegeCheck(270);
			break;

		case 30:
			break;

		case 31:
		case 40:
			HTTP::message('Access denied.');
	}

	if ($roleId == 30) {
/*
		$report = new Report(1);
		if (!$report->isActive()) {
*/
		$reviewer = new Reviewer($_SESSION['id']);
		if ($reviewer->releaseReport != 1) {
			HTTP::message('This report is not activated.');
		}
	}

	$track = new Track(1);
	$pa = paperArray();

	$data = null;
	if ($pa != null) {
		
		$decisionValab = $track->getDecisionValab();
		
		foreach ($pa as $i => $row) {

			$pId = $row[10];
			
			$data[$i][0] = makeup($row[8], '#EEEEEE', 'white').'<br/> '.$row[0];
			$data[$i][1] = makeup($row[9], '#EEEEEE', 'white').'<br/> '.$row[1];
			$data[$i][2] = "$row[2]<br /><span class=smallgrey>$row[3]</span>";
			$data[$i][3] = "<span class=small>$row[6]</span>";
			$data[$i][4] = '<span class=small>'.$decisionValab[$row[7]].'</span>';

			$p = new Paper($pId);
			$isAuthor = false;
			foreach ($p->authors as $a) {
				if ($a->id == $_SESSION['id']) {
					$isAuthor = true;
					break;
				}
			}
			if ($_SESSION['role'] == 30 && $isAuthor) {
				$data[$i][0] = '<span class=small>hidden</span>';
				$data[$i][1] = '<span class=small>hidden</span>';
				$data[$i][3] = '<span class=small>hidden</span>';
				$data[$i][4] = '<span class=small>hidden</span>';
			}
			$i++;
		}
		if ($data != null) arsort($data);
		
		foreach ($pa as $i => $row) {
			if ($data[$i][0] != '<span class=small>hidden</span>') {
				$data[$i][0] = $row[0];
				$data[$i][1] = $row[1];
			}
		}
		
		$table = new HTMLTable($data, 0, 'desc');
		$table->setSortOffCols(array(3));
		$table->setTitle('Avg=40', 'Weighted Avg=165', 'Title & Author=630', 'Review=320',  'Status=120');
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Review Result Report</h3>
<div class=red><? echo $oops; ?></div>
<? if (isset($table)) $table->display(); ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
