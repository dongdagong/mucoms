<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// delete user's role. delete user if no role left. done in Class User's method

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$id = HTTP::sessionate('u');
	$role = HTTP::sessionate('r');

	if ($role == 20) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(201);
		$url = 'c_conference_committee.php';
	}
	if ($role == 30) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(202);
		$url = 'c_programme_committee.php';
	}
	if ($role == 40) {
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(220);
		$url = 'c_author.php';
	}

	if ($id == 1 && $role == 20)
		HTTP::message("Cannot delete first chair account", 'c_conference_committee.php');


	$u = new Users($id);

	// confirm
	HTTP::confirm(
		'Delete <b>'.$u->getName()."</b>? <br />
		The user account will remain if the user still has other roles.", 
		'delete_user.php',
		$url,
		'Yes, delete this account/role',
		'Cancel'
	);

	try {

		DB::autocommit(false);
		$u->delete($role);
		DB::commit();
		HTTP::message('Account/role deleted', $url);

	} catch (Exception $e) {

		$oops = $e->getMessage();
		HTTP::message($e->getMessage().'<br />Deletion failed. Cannot delete the account/role at this stage.', $url);
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<p class=red><?php echo $oops.'<br />'; ?><br /></p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
