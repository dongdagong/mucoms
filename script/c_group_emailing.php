<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	exit;
	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(260);

	$t = new Track(1);

	$groups = array(
		-1 => 'Please select a user group',

		'all' => 'All Users (Chairs, Reviewers and External Reviewers, Authors)',
		'allChair' => 'All Chairs',

		'allReviewer' => 'All reviewers',
		'bidLate' => '&nbsp;&nbsp;Reviewers - Late Bidding',
		'reviewLate' => '&nbsp;&nbsp;Reviewers - Late Review Submission',

		'allAuthor' => 'All authors',
		'allContactAuthor' => '&nbsp;&nbsp;All Contact Authors',
		'abstLate' => '&nbsp;&nbsp;Contact Authors - Late Abstract Submission',
		'fullLate' => '&nbsp;&nbsp;Contact Authors - Late Full Paper Submission',
		'accepedContactAuthor' => '&nbsp;&nbsp;Contact Authors - Has Accepted Papers',
		'rejectedContactAuthor' => '&nbsp;&nbsp;Contact Authors - Has Rejected Papres',
		'finalLate' => '&nbsp;&nbsp;Contact Authors - Late Final Paper Submission',
	);

	$form = new HTMLForm('form');
	$line = new FormElement('line', 'line');
	$submit = new FormElement('submit', 'submit', 'Preview');

	$to = new FormElement('select', 'to', -1, $groups);
	$to->addLayout('To', null, 'size=70');
	$to->addRule('required');

	$subject = new FormElement('text', 'subject');
	$subject->addLayout(Subject, null, 'size=70');
	$subject->addRule('required');

	$body = new FormElement('textarea', 'body', '
Dear {UserName},


Your Message Here.


Best regards,
{ConferenceShortName} Committee
{ConferenceFullName}
');
	$body->addLayout('Message', null, 'rows=17', 'cols=70');
	$body->addRule('required');

	$form->addElement($to, $subject, $body, $line, $submit);

	if (HTTP::isLegalPost()) {		// preview

		try {
			$form->validateWithException();
			if ($to->value == -1)
				throw new Exception('Please select a To User Group.');

			$_SESSION['groupEmail']['to'] = $to->value;
			$_SESSION['groupEmail']['subject'] = $subject->value;
			$_SESSION['groupEmail']['body'] = $body->value;

			$toArray = $t->getusers($to->value);
			$e = new Email($subject->value, $body->value, null, false);
			$num = count($toArray);
			$tag = '<b>To:</b> ('.$num.' users) ';
			if ($toArray != null) {
				foreach ($toArray as $u) {
					$tag .= $u->getName();
					$tag .='<'.$u->email.'>, ';
				}
				$tag = noTail($tag, 2);
			}
			$tag .= '<br />'.str_replace(chr(13), '<br />', $e->headers).'<br />';
			$tag .= "<b>Subject</b>: $e->subject<br /><br />";
			$tag .= str_replace(chr(13), '<br />', $e->body);
			$tag .= '<br /><br />';
			if ($num > 0)
				$tag .= HTML::command('Send', 'c_group_emailing.php?a=s').' | ';
			$tag .= HTML::command('Modify', 'c_group_emailing.php?a=m').'<br />';
			$tag .= '<span class=small>{UserName} will be replaced by real names in each email.</span><br />';

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br /><br />';
		}
	} elseif ($_GET['a'] == 'm') {	// modify

		$to->value = $_SESSION['groupEmail']['to'];
		$subject->value = $_SESSION['groupEmail']['subject'];
		$body->value = $_SESSION['groupEmail']['body'];

	} elseif ($_GET['a'] == 's') {	// send

		$to->value = $_SESSION['groupEmail']['to'];
		$subject->value = $_SESSION['groupEmail']['subject'];
		$body->value = $_SESSION['groupEmail']['body'];

		try {
			$toArray = $t->getusers($to->value);
			$e = new Email($subject->value, $body->value, null, false);
			$e->groupSend($toArray);

			HTTP::message('Emails sent.', 'c_main.php');

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br />';
		}

	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Group Emailing</h3>
<div class=red><? echo $oops; ?></div>
<?php
	if (HTTP::isLegalPost() && $oops == null) {
		echobr ($tag);
	} else {
		$form->display();
	}
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
