<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	session_start();

	if (Auth::isLogin(40) || Auth::isLogin(31) || Auth::isLogin(30) ||Auth::isLogin(20) )
		HTTP::message('You are already logged in.');

	$_SESSION['ci'] = isset($_GET['ci']) ? $_GET['ci'] : $_SESSION['ci'];

	if (!isset($_SESSION['ci'])) {
		$msg = '';
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') > 0) { // user is using IE v6
			$msg = "This error may be due to how Internet Explorer 6 handles user sessions, please try the following and then revisit this page: <br /><br />1. In Internet Explorer, click Tools, and then click Internet Options.<br />2. Click the Privacy tab, and then click Advanced.<br />3. Click to select the Override automatic cookie handling check box.<br />4. Click Ok twice to save the setting. <br /><br />";
		}
		HTTP::message("Conference instance is not specified<br /><br />$msg");
	}

	$form = new HTMLForm('signup');

	$uid = new FormElement('text', 'uid');
	$uid->addLayout('ID or Email', null, 'size=25');

	$pwd = new FormElement('password', 'pwd');
	$pwd->addLayout('Password', HTML::command('Forgot your password?',
	'forget_password.php?loginURL=login.php'), 'size=25');

	$roleValab = array(
		'20'=>'Chair',
		'31'=>'External Reviewer',
		'30'=>'Reviewer',
	);

	$role = new FormElement('radio', 'role', isset($_GET['r'])? $_GET['r']:20, $roleValab);
	$role->addLayout('Login as', null);

	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Log in');

	$form->addElement($uid, $pwd, $line, $role, $line, $submit);

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			switch ($role->value) {
				case 30:
					$next = 'r_main.php'; break;
				case 31:
					$next = 'r_main.php'; break;
				case 20:
					$next = 'c_main.php'; break;
			}
			$role = $role->value;

			Auth::login($uid->value, $pwd->value, $role, $_SESSION['ci']);
			HTTP::redirect($next);
			//exit();

		} catch (Exception $e) {

			$oops = $e->getMessage();

		}
	}
?>
<?php include_once 'header.php'; ?>

<div class="main">
<h3>Committee Member Login</h3>
<p class=red><?php echo $oops; ?></p>
<?php $form->display(); ?><br />
<?
	echo
	'<span style="float:right">'.
	HTML::command('Go to Author Login page', 'a_login.php?ci='.$_SESSION['ci']).
	'</span>';
?>
</div> <!-- end of main -->

<?php include_once 'footer.php'; ?>
