<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck(30);
	DB::connect();

	$conf = $_SESSION['conference'];
	$t = new Track(1);
	$r = new Reviewer($_SESSION['id']);
	$TEArray = $r->getTopicExpertiseArray();	 

	if (!$t->isTopicExpertiseOpen()) { 
		HTTP::message("Bidding is not open at this stage.");
	}

	$topicsValab = $t->getTopicsValab();
	$topicExpertiseValab = $t->getTopicExpertiseValab();

	$today = date('Y-m-d');
	$dDiff = dateDiff($today, $t->duePaperExpertise);
	if ($dDiff <= 30 && $dDiff >= 0)
		$dueBid = makeup("Bidding Due [$t->duePaperExpertise]".' in '.$dDiff.' '.($dDiff > 1 ? 'days':'day'),
		'black', 'orange');
	elseif ($dDiff < 0)
		$dueBid = makeup("Bidding Due [$t->duePaperExpertise]", 'white', 'red');
	else
		$dueBid = "Bidding Due [$t->duePaperExpertise]";
	
	$_SESSION['r_bid_dueBid'] = $dueBid;

	$form = new HTMLForm('r_bid_1');
	$data = array();

	$i = 1;
	foreach ($topicsValab as $topicId=>$topicName) {

		$data[$topicId][0] = $i++;
		$data[$topicId][1] = $topicName;

		$topicExpertise[$topicId] = 
		new FormElement('select', "topicExpertise$topicId",	$TEArray[$topicId], $topicExpertiseValab);
		//$r->getTopicExpertise(1, $topicId),	$topicExpertiseValab);

		$data[$topicId][2] = $topicExpertise[$topicId]->getTag();
	}

	$table = new HTMLTable($data);
	$table->setTitle(' =20', 'Topic=800', 'Expertise in Topic=140');

	$submit = new FormElement('submit', null, 'Next - Specify Paper Expertise');


	if (HTTP::isLegalPost()) {

		try {

			foreach ($topicsValab as $topicId=>$topicName) {

				$topicExpertiseArray[$topicId] = $topicExpertise[$topicId]->value;
			}

			DB::autocommit(false);
			$r->updateTopicExpertise(1, $topicExpertiseArray);
			DB::commit();
			
			//printArray($topicExpertiseArray); exit;
			//$_SESSION['topicExpertiseArray'] = $topicExpertiseArray;

			HTTP::redirect('r_bid_2.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<? echo $dueBid; ?>
<h3>Bidding - Specify Topic Expertise</h3>
<? echo $oops; ?>
<?
$instr = '<div class=greybox><div class=a4width><br />';
	
$instr .= 'Bidding takes place in two steps. First, you may enter your default expertise per paper topic. If you choose to enter a default, this value is then assigned to all papers that have the respective topic as their primary paper topic. Subsequently, you will have the opportunity to refine your expertise on a per-paper basis.

Note: You may skip the first step if you prefer to specify all your expertise values on a per-paper basis.

Important: If you wish to update your biddings, you may do so until the bidding due date has been reached. It is important to note that expertise values, which have been entered on a per-paper basis, are not lost (or overwritten) in the event that you specify a default expertise value for a corresponding primary topic when you update your biddings.';

$instr .= '</div><br /></div><br />';

echo str_replace(chr(13), '<br />', $instr);

$form->open();
$table->display();
echo '<br />';
$submit->display();
$form->close();
?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
