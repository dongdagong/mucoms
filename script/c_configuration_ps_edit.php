<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$cmd = HTTP::sessionate('cmd');
	$psId = HTTP::sessionate('psId');

	if (!($cmd == 'add' || $cmd == 'edit' || $cmd == 'delete'))
		HTTP::message('cmd is invalid');
	if ($cmd == 'edit' || $cmd == 'delete') {
		if (!is_numeric($psId))
			HTTP::message('psId is invalid');
	}

	try {

		if ($cmd == 'delete') {
			$title = "Delete $ps->name";
			$ps = new PaperStatus($psId);
			HTTP::confirm(
				"Delete paper status <b>$ps->name</b>?",
				'c_configuration_ps_edit.php',
				'c_configuration_ps_disp.php',
				'Yes',
				'Cancel'
			);
			if ($ps->isEditable == 0)
				HTTP::message("Cannot delete $ps->name");
			$ps->delete();
			HTTP::message("Paper status $ps->name deleted.", 'c_configuration_ps_disp.php');
		}

		if ($cmd == 'add') {
			$title = 'Add a paper status';
			$ps = new PaperStatus();
			$ps->sendNotification = 1;
			$ps->isEditable = 1;
		}
		if ($cmd == 'edit') {
			$title = 'Edit paper status';
			$ps = new PaperStatus($psId);
			$ps->getTemplateContent();
		}
	} catch (Exception $e) {
		$oops = $e->getMessage().'<br /><br /';
	}

	unset($psValab[-1]);

	$form = new HTMLForm('form');
	$submit = new FormElement('submit', 'submit', 'Submit');
	$line = new FormElement('line', 'line', 'line');
	$yesNo = array(
		1 => 'Yes ',
		0 => 'No '
	);

	$label = new FormElement('text', "label", $ps->name);
	$label->addLayout("Paper status", null, 'size=81');
	if ($ps->isEditable == 0) $label->disable();
	$label->addRule('required', 'maxLen=30');

	$sendNotification = new FormElement('radio', "sendNoti", $ps->sendNotification, $yesNo);
	$sendNotification->addLayout('Send email notification', null, 'radio=h');
	if ($ps->isEditable == 0) $sendNotification->disable();
	$sendNotification->addRule('isInt', 'maxInt=1', 'minInt=0');

	$finalUpload = new FormElement('radio', "filalUpload", $ps->finalUpload, $yesNo);
	$finalUpload->addLayout('Require final paper upload', null, 'radio=h');
	if ($ps->isEditable == 0) $finalUpload->disable();
	$finalUpload->addRule('isInt', 'maxInt=1', 'minInt=0');

	$form->addElement($label, $line, $sendNotification, $finalUpload);

	$templateContent = new FormElement('textarea', "templateContent", $ps->templateContent);
	$templateContent->addLayout('Email template', 'Changes to this field will <b>only</b>
	be saved if <b>Send email notification</b> is set to yes.', 'rows=15', 'cols=70');
	$form->addElement($line, $templateContent);

	$form->addElement($line, $submit);

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			DB::autocommit(false);

			$ps->name = $label->value;
			$ps->sendNotification = $sendNotification->value;
			$ps->finalUpload = $finalUpload->value;
			$ps->templateContent = $templateContent->value;

			if ($ps->sendNotification == 1) {
				if (strlen($ps->templateContent) <= 10)
					throw new Exception('Please fill in email template.');
			}

			unset($_SESSION['cmd']);
			unset($_SESSION['psId']);

			if ($cmd == 'add') {
				$ps->isEditable == 1;	// all chair added ones should be editable
				$ps->add();
			}

			if ($cmd == 'edit') {
				$ps->update();
			}
			DB::commit();

			HTTP::redirect("c_configuration_ps_disp.php?template=$ps->id#$ps->id");

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br /><br />';
		}

	}
?>
<?php include 'header.php'; ?>
<div class="main">
<h3><? echo $title; ?></h3>
<? echo $oops; ?>
<?
	if ($oops == null || $cmd == 'edit' || $cmd == 'add') $form->display();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
