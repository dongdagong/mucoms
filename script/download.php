<?php
include_once 'library/html.php';
include_once 'library/basic.php';
include_once 'cms/Users.php';
include_once 'cms/Author.php';
include_once 'cms/Reviewer.php';
include_once 'cms/ExternalReviewer.php';
include_once 'cms/Track.php';
include_once 'cms/Paper.php';
include_once 'cms/Review.php';
include_once 'cms/FormQuestion.php';
include_once 'cms/PaperTopic.php';
include_once 'cms/Conference.php';
include_once 'cms/Chair.php';

session_start();
Auth::loginCheck($_SESSION['role']);
DB::connect();

// file name check

if (!isset($_GET['f']))
	HTTP::message('No file name specified.');

$f = $_GET['f'];
$type = null;

if (substr($f, 0, 4) == 'full') {
	$type = 'full';
	$f = substr($f, 4, strlen($f) - 4);
}
if (substr($f, 0, 5) == 'final') {
	$type = 'final';
	$f = substr($f, 5, strlen($f) - 5);
}

$a = explode('.', $f);
$f = $a[0];
if (!is_numeric($f))
	HTTP::message('Invalid file name.');

try {
	$p = new Paper($f);
} catch (Exception $e) {

	die ($e->getMessage());
}

// user has the paper

$role = $_SESSION['role'];

switch ($role) {
	case 20:
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(220);
		break;
	case 40:
		$p->isContactAuthorCheck();
		break;
	case 30:
		$p->isReviewerCheck();
		break;
	case 31:
		$p->isExternalReviewerCheck();
		break;
}

define('BASE_DIR', UPLOAD_DIR.$_SESSION['ci'].'/');
define('LOG_DOWNLOADS',false);
define('LOG_FILE', 'downloads.log');

// Allowed extensions list in format 'extension' => 'mime type'
// If myme type is set to empty string then script will try to detect mime type
// itself, which would only work if you have Mimetype or Fileinfo extensions
// installed on server.

$t = new Track(1);
$paperFormats = $t->getPaperFormatsValab($type);

$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'ps'  => 'application/postscript',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',

  // executables
  'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);

foreach ($allowed_ext as $ext => $mime) {
	if (!isset($paperFormats[".$ext"]))
		unset($allowed_ext[$ext]);
}

set_time_limit(0);

if (!isset($_GET['f']) || empty($_GET['f'])) {
  die("Please specify file name for download.");
}

$fname = basename($_GET['f']);


// Check if the file exists
// Check in subfolders too
function find_file ($dirname, $fname, &$file_path) {

  $dir = opendir($dirname);

  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

}

// get full file path (including subfolders)
$file_path = '';
find_file(BASE_DIR, $fname, $file_path);

if (!is_file($file_path)) {
  die("<br />File '$fname' does not exist.");
}

// file size in bytes
$fsize = filesize($file_path);

// file extension
$fext = strtolower(substr(strrchr($fname,"."),1));

// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("Not allowed file type.");
}

// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// Browser will try to save file with this filename, regardless original filename.
// You can override it if needed.

if (!isset($_GET['fc']) || empty($_GET['fc'])) {
  $asfname = $fname;
}
else {
  $asfname = $_GET['fc'];
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$asfname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
@readfile($file_path);

// log downloads
if (!LOG_DOWNLOADS) die();

$f = @fopen(LOG_FILE, 'a+');
if ($f) {
  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
  @fclose($f);
}

?>
