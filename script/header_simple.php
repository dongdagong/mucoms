<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
<link rel="stylesheet" type="text/css" href="./style/main.css" />
<link rel="stylesheet" type="text/css" href="./style/form.css" />
<title>
<?
	if (strstr($_SERVER['PHP_SELF'], 'ad_') === false) {
		echo $shortName;
		if (DEBUG && isset($_SESSION['id'])) {
			DB::connect();
			$me = new Users($_SESSION['id']);
			echo ' ['.$_SESSION['role'];
			echo '.'.$me->getName().']';
		}
	} else
		echo SYSTEM_NAME;
?>
</title>
</head>
