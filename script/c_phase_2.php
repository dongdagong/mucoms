<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Notification.php';
	include_once 'cms/Phase.php';

	// Entering this page because at least one phase has been set open.

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(204);

	$track = new Track(1);
	$phase = new Phase();
	$opened = $_SESSION['openedArray'];

	$msg = 'Phase status saved. <br  />';
	$msg .= 'The following phases have been opened. <br />
	To send phase opening notifications, check the phases and click send.<br /><br />';
	
	$form = new HTMLForm('c_phase');
	$line = new FormElement('line', null);
	$submit = new FormElement('submit', null, 'Send');

	$openedValab = null;
	foreach ($opened as $o) {
		$openedValab[$o] = $phase->name[$o];
	}

	$checkedPhase = new FormElement('checkbox', 'checkedPhase', null, $openedValab);
	$checkedPhase->addLayout('', NULL);
	
	$form->addElement($checkedPhase, $line, $submit);

	if (HTTP::isLegalPost()) {

		try {

			$noti = new Notification();
			
			if ($checkedPhase->values != null){
			
				foreach ($checkedPhase->values as $phaseCode) {
					$noti->send($phaseCode.'Open');
					$sentMsg .= $phase->name[$phaseCode].' open notification sent.<br />';
				}
			} else {
				$sentMsg = 'No notification sent.';
			}
			
			unset($_SESSION['openedArray']);
			
			HTTP::message($sentMsg, 'c_phase.php');

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Phase Opening Notifications</h3>
<p class=red><?php echo $oops; ?></p>
<?php
	echo $msg;
	$form->display();
	echo '<br />'.HTML::command('Return without sending notification', 'c_phase.php');
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
