<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/FormQuestion.php';


	session_start();
	Auth::loginCheck(30);
	DB::connect();

	$paperId = HTTP::sessionate('paperId');
	$trackId = HTTP::sessionate('trackId');
	try {
		$p = new Paper($paperId);
	} catch (Exception $e) {
		HTTP::message($e->getMessage());
	}

	$review = new Review($_SESSION['id'], $paperId, $trackId);

	// confirmation
	HTTP::confirm(
		"Finalize the review of $p->title <br />You cannot edit the review once it is finalized. <br /><br />
		Do you want to continue?",
		'r_finalize.php',
		'r_main.php',
		'Yes',
		'Cancel'
	);

	$r = new Reviewer($_SESSION['id']);

	if (!$r->hasPaper($paperId))
		HTTP::message('You do not have this assignment.');

	DB::autocommit(false);
	$review->finalize();
	DB::commit();

	HTTP::redirect('r_main.php');
?>
