<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	session_start();

	$flag = null;
	
	// Following the password-resetting link
	
	if (isset($_GET['uid']) && isset($_GET['token'])) {
		
		$flag = 'reset';
		
		if (!isset($_GET['ci']))
			HTTP::message('No conference instance specified.');
		
		HTTP::sessionate('ci');
		
		if (sha1($_GET['uid']) != $_GET['token'])
			HTTP::message('Invalid token');
		
		try {
			DB::connect('common');
			$conf = new Conference($_GET['ci']);
			if ($conf->isActive == 0) {
				HTTP::message("Conference instance is inactive.");
			}
			$_SESSION['conference'] = $conf;
			
			DB::connect($_GET['ci']);
			DB::query("select ID from Users where UID = '".$_GET['uid']."'", &$result);
			$row = $result->fetch_assoc();
			$u = new Users($row['ID']);
			
			DB::autocommit(false);
			$newPwd = $u->generatePwd();
			$u->changePassword($u->pwd, $newPwd, false);
			$e = new Email('New Password', 'reset_password_2');
			$u->pwd = $newPwd;
			$e->send($u);
			DB::commit();

			unset($_SESSION['conference']);
			unset($_SESSION['ci']);
			
			$_SESSION['pageFormat'] = '_simple';
			HTTP::message("New password sent to: $u->email"); //, $_GET['loginURL']."?ci=".$_GET['ci']);
			
		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	
	// Requesting to send the link
		
	} else {		
			
		DB::connect();

		$loginURL = HTTP::sessionate('loginURL');
		switch ($loginURL) {
			case 'a_login.php':
				$type = ' - Author';
				break;
			case 'login.php':
				$type = ' - Member';
				break;
			default:
				HTTP::message('login URL missing');
		}

		if (!isset($_SESSION['ci']))
			HTTP::message('No conference instance specified.');

		if (isset($_SESSION['role']))
			HTTP::message('To reset password log off first.');
			
		$form = new HTMLForm('form');

		$key = new FormElement('text', 'key');
		$key->addLayout('Email or User ID', null, 'size=30');
		$key->addRule('required');

		$submit = new FormElement('submit', 'submit', 'Submit');
		$form->addElement($key);
		
		if (HTTP::isLegalPost()) {

			try {

				$form->validateWithException();

				DB::query("select ID from Users where UID = '$key->value' or Email = '$key->value'", &$result);
				$row = $result->fetch_assoc();
				$id = $row['ID'];

				if ($id == null) {
					throw new Exception('Invalid ID or Email, please try again.');
				} else {
					$u = new Users($id);
					$uid = $u->getColByEmail('UID');
					// $token = sha1(rand(1, 1000)); // store it in db. todo
					$link = SYSTEM_DOMAIN.'forget_password.php?ci='.$_SESSION['ci']."&loginURL=$loginURL".
					"&uid=$uid&token=".sha1($uid);
					$e = new Email('Reset Password', 'reset_password_1');
					$e->addPlugin("LinkToResetPassword:$link");
					$e->send($u);
				}

				HTTP::message('A link has been sent to your email address. Please follow that link to reset your password.');
				//"$loginURL?ci=".$_SESSION['ci']);

			} catch (Exception $e) {
				$oops = $e->getMessage();
			}
		}
	}
?>


<?php include_once 'header.php'; ?>

<div class="main">
<h3>Reset Password <? echo $type; ?></h3>
<p class=red><?php echo $oops; ?><br /></p>
<?
	if ($flag == 'reset') {
		
	} else {
		$form->open();
		echo 'Please enter your user ID or email below:<br /><br />'.
		$key->getTag().' '.$submit->getTag().'<br />';
		$form->close();
	}
?>
</div> <!-- end of main -->

<?php include_once 'footer.php'; ?>
