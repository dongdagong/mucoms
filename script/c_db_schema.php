<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(203);

?>
<html>
<body>
<?
	$tables = array(
		Assignment,
		Author,
		Bid,
		Chair,
		Conflict,
		Coordinate,
		Delegate,
		EmailTemplate,
		Expertise,
		FormAnswer,
		FormQuestion,
		Invite,
		Member,
		Paper,
		PaperExpertise,
		PaperFormat,
		PaperStatus,
		PaperTopic,
		Privilege,
		Review,
		Reviewer,
		ReviewTrack1,
		Role,
		Thread,
		ThreadMsg,
		Topic,
		TopicExpertise,
		Track,
		TrackPaperFormat,
		TrackPrivilege,
		UserRole,
		Users,
		Writes,
	);

	echo '<b>List of Tables </b><br /><br />';

	$n = 1;
	foreach ($tables as $table) {

		$q = "describe $table";
		DB::query($q, &$result);

		echo "<br />$n. <b>$table</b> <br />";
		$n++;
		echo '<br /><table border="1">';

		// title

		$fields = $result->fetch_fields();
		$title = '<tr>';
		foreach ($fields as $field) {
			$len = $field->max_length;
			$title .= "<td class=titlecell>".
			nbspStr($field->name, $len > 50 ? 50:$len)."</td>";
		}
		$title .= '</tr>';

		// rows

		$i = 0;
		$j = 0;
		while ($row = $result->fetch_assoc()){
			if ($j++ % 100 == 0)
				echo $title;

			echo '<tr>';
			foreach ($row as $cell) {
				echo '<td class='.($i == 0 ? 'cell':'shadedcell').">$cell</td>";
			}
			$i = $i == 0? 1:0;
			echo '</tr>';
		}

		echo '</table><br />';
	}
	echo '<br /><b>End of Table Listing</b>';
?>
</body>
</html>