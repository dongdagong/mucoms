<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(231);

	function main() {
		$paper = new Paper($_GET['p']);
		$track = new Track(1);

		$all = $track->getReviewers();
		$got = $paper->getReviewers();

		echo "<h3>Add Reviewer to Paper $paper->title</h3>";
		$numGot = count($got);

		$Got = array();
		if ($numGot > 0) {
			echo "<b>$numGot reviewer".($numGot > 1 ? 's':'').' already assigned: </b><br />';

			foreach ($got as $rId) {
				$r = new Reviewer($rId);
				echo (DEBUG ? $r->id.'. ':'').$r->getName().'<span class=small> '.$r->org.' &nbsp;'.$r->email.'</span><br />';
				$Got[$rId] = true;	// to test existence in $all
			}
		}
		echo '<br />';

		if ($all != null) {
			$i = 1;
			$data = array();
			foreach ($all as $rId) {

				if (isset($Got[$rId]))
					continue;

				$r = new Reviewer($rId);
				$data[$i][0] = $r->getPaperExpertise($_GET['p']);
				$data[$i][1] = (DEBUG ? $r->id.'. ':'').$r->getName(); //.'<br /><span class=small>'.$r->org.' &nbsp;'.$r->email.'</span>';
				$data[$i][2] = $r->org.'<br /><span class=small>'.$r->email.'</span>';
				$data[$i][3] = $r->numAssigned;
				$data[$i][4] = HTML::command("Add this reviewer", "c_assignment_m_x.php?r=$rId&p=".$_GET['p']."&a=add");
				$i++;
			}
			arsort($data);
			$pe = $track->getPaperExpertiseValab();
			for ($i = 1; $i <= count($data); $i++) {
				$data[$i][0] = $pe[$data[$i][0]];
				$data[$i][0] = '<span class=small>'.$data[$i][0].'</span>';
			}
			$table = new HTMLTable($data);
			$table->setTitle('Expertise=120', 'Reviewer=220', 'Organization and Email=300', 'Assignments=50', '=110');
			$table->display();

		}


		$withdrawn = $track->getPapersByStatus(-1);

		$table = new HTMLTable($data);
		//$table->setTitle('Expertise=30', 'Paper=750', '# Assignment=100', '=100');
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? echo HTML::command('By Paper', 'c_assignment_m_10.php#'.$_GET['p']).' | '; ?>
<? echo HTML::command('By Reviewer', 'c_assignment_m_20.php').'<br />'; ?>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
</div>
<p></p>
<!-- end of main -->

<?php include 'footer.php'; ?>