<?

/*

Assign papers to reviewers such that:

1. The sum of the expertise levels of the assignments is maximized.
2. All conflicts MUST be avoided.
3. No reviewer gets more than $maxP papers (Since v0.92 this number is different for each reviewer).
4. No paper gets less than $minR reviews (impossible if there are no enough assignment slots).

Start from the highest expertise level. Assign the most urgent 'needs' of that
level. Move to the next level until level 1 is finished.Then assign the remainng
papers to reviewers, start from those reviewers who has the least number of assignmnets.

*/

include_once 'library/html.php';
include_once 'library/basic.php';
include_once 'cms/Users.php';
include_once 'cms/Reviewer.php';
include_once 'cms/Track.php';
include_once 'cms/Paper.php';
include_once 'cms/Chair.php';
include_once 'cms/Author.php';
include_once 'cms/Notification.php';
include_once 'cms/Assignment.php';

session_start();
Auth::loginCheck(20);
DB::connect();

$c = new Chair($_SESSION['id']);
$c->privilegeCheck(230);

$assi = new Assignment();
$msg = null;
$canAuto = $assi->canPerformAutoAssignment(&$msg);
if ($canAuto != true)
	HTTP::message($msg.'Cannot perform automatic assignment at this stage.', 'c_assignment.php');

$type = HTTP::sessionate('assignmentType');
if ($type != 'new' && $type != 'inc')
	HTTP::message('Assignment type missing', 'c_assignment.php');

if ($type == 'new')	
	HTTP::confirm(
		"Generate new assignments. <br />All existing assignments will be overriden. <br /><br />
		Do you want to continue?",
		'c_assignment_a_2.php?c=new',
		'c_assignment_a_1.php',
		'Yes',
		'Cancel'
	);
	
if ($type == 'inc')	
	HTTP::confirm(
		"Generate additional assignments on top of existing ones. <br /><br />
		Do you want to continue?",
		'c_assignment_a_2.php?c=inc',
		'c_assignment_a_1.php',
		'Yes',
		'Cancel'
	);

title('['.($type == 'new' ? 'New':'Incremental').']');


// Variables

$P = array();	// Paper: {(<Pid>, bid, a)}
				// Pid: paper id, <> denotes Pid implemented as array index
				// bid: number of bids
				// a: number of assignments
				// Withdrawn papers excluded

$R = array();	// Reviewer	{(<Rid>, e:{(Pid, l)}, a)}
				// Rid: reviewer id, <> denotes Rid implemented as array index
				// e: expertise levels array
				// l: expertise level
				// a: number of assignments

$A 	= array();	// Assignment: {(Rid, Pid)}
$A0 = array();	// Existing assignment: {(<Rid>, Pid)}

$maxL = 4;		// Maximum expertise level. A level of -1 indicates a conflict.
$minR = 3;		// Minimum number of reviewers per paper
$maxP = array();// Maximum number of papers for each reviewer 

$R0 = array();	// A copy for $R
$P0 = array();	// A copy for $P


// Read $P, $R, and $maxP from database

function readDB() {

	title('Reading from database');

	global $R, $P, $A, $A0, $R0, $P0, $maxL, $maxP, $minR, $type;

	$track = new Track(1);
	
	// Read $A0
	if ($type == 'inc') {
		DB::query("select ReviewerID, PaperID from Assignment", &$result);	
		while ($row = $result->fetch_assoc()) {
			$A0[$row['ReviewerID']] [$row['PaperID']] = true;
		}	
		//printArray($A0);
	}
	
	// Read $P
	$papers = $track->getNonWithdrawnPapers();
	if ($papers != null) {
		foreach ($papers as $Pid) {
			$P[$Pid]['bid'] = 0;
			if ($type == 'new')
				$P[$Pid]['a'] = 0;
			else {
				$p = new Paper($Pid);
				$P[$Pid]['a'] = $p->numAssi == null ? 0:$p->numAssi;
			}
			if ($P[$Pid]['a'] >= $minR)
				unset($P[$Pid]);
		}
	}
	$numP = count($P);

	// Read $R: {(<Rid>, {(Pid, l)}, a)}
	$reviewers = $track->getReviewers();
	foreach ($reviewers as $Rid) {

		$r = new Reviewer($Rid);

		// Precondition of getBidExpertise():
		// conflicts detected (from Conflict) and stored in Bid as -1s.
		$bids = $r->getBidExpertise(1);	// <Pid>, level

		if (count($bids) > 0 ) {
			foreach ($bids as $Pid => $l) {

				// Add the (Pid, e) pair
				$R[$Rid]['e'][$Pid] = $l;
			}
			arsort($R[$Rid]['e']);
		}
		if ($type == 'new')
			$R[$Rid]['a'] = 0;
		else 
			$R[$Rid]['a'] = $r->numAssigned == null ? 0:$r->numAssigned;
		
		$maxP[$Rid] = $r->maxAssi === null ? $track->maxPaperPerRev:$r->maxAssi;
		if ($maxP[$Rid] == 0)
			unset($R[$Rid]);
		if ($R[$Rid]['a'] >= $maxP[$Rid])
			unset($R[$Rid]);
	}

	// Calculate number of bids for each $P
	foreach ($R as $r) {
		if (isset($r['e'])) {
			foreach ($r['e'] as $Pid => $l) {
				if ($l != -1 && isset($P[$Pid]))
					$P[$Pid]['bid']++;
			}
		}
	}

	$P0 = $P;
	$R0 = $R;
}


function generateTestingData() {

	global $R, $P, $A, $R0, $P0, $maxL, $maxP, $minR, $type;

	$numHot = 15;				// Number of hot papers
	$maxNumExpertises = 40; 	// Per reviewer

	$maxRid = 20;				// Maximum VALUE of reviewer id
	$maxPid = 60;				// Maximum VALUE of paper id

	$numR = $maxRid;			// How many reviewers
	$numP = $maxPid;			// How many papers

	// Notes to the above four variables: papers may be withdrawn so paper ids may
	// not be continuous. Setting $maxPid = 350, for example, will better represent the actual scenario.

	//$maxP = $numP * $minR / $numR;	// Maximum number of papers per reviewer
	$Pids = range(1, $maxPid);			// Possible values for paper ids
	shuffle($Pids);
	$Rids = range(1, $maxRid); 			// Possible values for reviewer ids
	shuffle($Rids);
	echo '$numR = '.$numR.' $numP = '.$numP.' $maxNumExpertises = '.$maxNumExpertises.' $maxP = '.$maxP.'<br/>';


	// Generate $P

	for ($i = 0; $i < $numP; $i++) {	// The first $numP items of $Pids will be used as the paper ids

		$P[$Pids[$i]] = array(	// Pid is the array index
			'a' => 0,			// Number of assignments
			'bid' => 0,			// Number of bids
		);
	}

	// Generate $R

	echo '<br>'.$numHot.' hot papers: ';
	for ($i = 0; $i <$numHot; $i++)	// The first $numHot numbers are used as hot paper ids
		echo $Pids[$i].' ';
	echo '<br>';

	for ($i = 0; $i < $numR; $i++) {

		$R[$Rids[$i]] = array(	// Rid is the array index
			'a' => 0,			// Number of assignments
			'e' => array(),		// Array of Expertises of the form {(Pid, level)}
		);

		// Generate the expertise list
		$thisNumExpertises = $numP; //rand(0, $maxNumExpertises);
		//$thisNumExpertises = rand(0, $maxNumExpertises);
		for ($j = 0; $j < $thisNumExpertises; $j++) {
			// Paper id
			if (rand(0, 1) > 0.75)
				$pid = rand(0, $numHot);
			else
				$pid = rand(0, $numP - 1);
			// Expertise Level
			$level = rand(1, 10) > 9 ? -1 : rand(1, $maxL);	// Rarely there is a conflict (-1)

			// Add the (Pid, level) pair
			$R[$Rids[$i]]['e'][$Pids[$pid]] = $level;
			arsort($R[$Rids[$i]]['e']);
		}
	}

	// Calculate number of bids for each $P

	foreach ($R as $Rid => $r) {
		foreach ($r['e'] as $Pid => $l) {
			if ($l != -1)
				$P[$Pid]['bid']++;
		}
	}

	$P0 = $P;
	$R0 = $R;
}


// Print papers

function printP() {

	global $P;

	echo "<b>Paper (Pid, #bids, #assi), #papers = ".count($P)." </b><br>";
	foreach ($P as $Pid => $p) {
		echo "($Pid, ".$p['bid'].",".$p['a'].") ";
	}
	echo '<br>';
}


// Print reviewers

function printR() {

	global $R, $P;

	echo '<b>Reviewer (Rid, #assi, {(Pid, level)}) #reviewers = '.count($R).' </b><br>';
	foreach ($R	as $Rid => $r) {
		echo '<b>'.$Rid.', </b> a='.$r['a'].', ';
		if (count($r['e']) > 0) {
			foreach ($r['e'] as $Pid=>$l) {
				echo '('.$Pid.', ';
				if ($l == -1) echo '<b> ';
				echo $l.') ';
				if ($l == -1) echo '</b> ';
			}
		}
		echo '<br>';
	}
}


// Print assignments

function printA() {

	global $A, $R0, $P0;

	sort($A);
	title('Assignment result');
	echo "<b>#assignments = ".count($A)."</b><br>";

	// by R
	$R3 = array();				// {(Rid, #assignments, {(Pid, Level)})}
	foreach ($A as $a) {
		$Rid = $a[0];
		$Pid = $a[1];
		$R3[$Rid][0]++;			// number of assignments
		$R3[$Rid][1][$Pid] = $R0[$Rid]['e'][$Pid];
	}
	echo '<b>by Reviewer #R3='.count($R3).'</b><br>';

	foreach ($R3 as $Rid => $a) {
		if (count($R0[$Rid]['e']) > 0)
			arsort($R0[$Rid]['e']);
		$numBids = 0;
		if (count($R0[$Rid]['e']) > 0) {
			foreach ($R0[$Rid]['e'] as $notused => $ll)
				if ($ll > 0)
					$numBids++;
		}
		echo"<a name=R$Rid>$Rid</a> #bids = ".$numBids.' ';

		if (count($R0[$Rid]['e']) > 0) {

			foreach ($R0[$Rid]['e'] as $Pid => $l) {
				//if ($l == -1) echo '<b> ';

				if (!array_key_exists($Pid, $R3[$Rid][1]) &&$R0[$Rid]['e'][$Pid] != -1 ) {
					$left = '[';
					$right = ']';
				} else {
					$left = '';
					$right = '';
				}
				echo "(<a href=#P$Pid>$left$Pid$right</a>, $l) ";
				//if ($l == -1) echo '</b> ';
			}
		}
		if (count($a[1]) > 0)
			arsort($a[1]);
		echo "<br><span class=blue><b>$Rid</b></span> #assi = $a[0] ";
		$i = 0;
		foreach ($a[1] as $p => $l) {
			if ($R0[$Rid]['e'][$p] >= 1)
				$i++;
			if ($l >= 1) echo '<b> ';
			echo "(<a href=#P$p>$p</a>, ".$R0[$Rid]['e'][$p].") ";
			if ($l >= 1) echo '</b> ';
		}
		if ($i < $numBids) {
			$lost = $numBids - $i;
			echo " <b class=red> [lost $lost/$numBids]</b>";
		}
		echo '<br >';
	}

	// by P

	$P3 = array(); 	// {('Pid', a, {(Rid, l)})}
	foreach ($A as $a) {
		$Rid = $a[0];
		$Pid = $a[1];
		$P3[$Pid][0]++;			// number of assignments
		$P3[$Pid][1][$Rid] = $R0[$Rid]['e'][$Pid];
	}
	ksort($P3);


	echo '<b>by Paper #P3='.count($P3).'</b><br>';


	foreach ($P3 as $Pid => $a) {
		$e1 = 0;	// number of assignments where expertise >= 1
		$e2 = 0;	// number of bids where expertise >= 1

		arsort($a[1]);
		echo "<a class=blue name=P$Pid>$Pid</a> #assi = $a[0] ";
		foreach ($a[1] as $r => $l) {
			if ($R0[$r]['e'][$Pid] >=1)
				$e1++;
			if ($l >= 1) echo '<b> ';
			echo "(<a href=#R$r>$r</a>, ".$R0[$r]['e'][$Pid].") ";
			if ($l >= 1) echo '</b> ';
		}

		$e2 = $P0[$Pid]['bid'];
		$highlight = $e1 < 3 ? 'class=red':'';
		if (($e1 < 3) && ($e1 < $e2))
			$highlight = 'class=blue';
		echo "<b $highlight> $e1/$e2 </b>";
		echo '<br>';
	}
}


// R2: {(urgency, Rid, Pid)}, all the Rid Pid pairs at the given level.

function getR2($level) {

	global $R, $P, $A, $R0, $maxL, $maxP, $minR, $type;

	$i = 0;
	$R2 = array();
	foreach ($R as $Rid => $r) {	// Check all the remainig reviewers in $R

		if (count($r['e']) > 0) {
			foreach ($r['e'] as $Pid => $l) {
				if (!isset($P[$Pid])) continue;
				
// Pid is not $Pid!!!

// Reviewer	{(<Rid>, e:{(Pid, l)}, a)}
// Rid: reviewer id, <> denotes Rid implemented as array index
// e: expertise levels array
// l: expertise level
// a: number of assignments

				if ($l == $level) {

					// Favor papers having less number of bids (more urgent).
					$urgency = $P[$Pid]['bid'];

					$R2[] = array(
						'urgency' => $urgency,
						'Rid' => $Rid,
						'Pid' => $Pid
					);

					// Move reviewers who have very few bids UP the list. Possible jump from lower levels.
					// So there will be less number of pissed off reviewers who are too lazy to bid thoroughly but
					// complain they don't get what they wanted.
					// Kind of unfair to those who DO bid thoroughly. Balance.
				}
			}
		}
	}

	if ($R2 != null)
		sort($R2);

	echo "<b>R2 (Rid, Pid, urgency) @ level $level</b><br>";

	if ($R2 != null) {
		foreach ($R2 as $r) {
			echo '('.$r['Rid'].', '.$r['Pid'].', '.$r['urgency'].') ';
		}
	}
	echo '<br />';
	return $R2;
}


function generateAssignment() {

	global $R, $P, $A, $A0, $R0, $maxL, $maxP, $minR, $type;
	
	title('Generating assignment');

	for ($level = $maxL; $level >= 1; $level--) {

		$R2 = getR2($level);

		if (count($R2) <= 0)	// No more PREFERRED papers at this level
			continue;

		foreach ($R2 as $r) {

			$Rid = $r['Rid'];
			$Pid = $r['Pid'];
			
			$rtn = candidateCheck($Rid, $Pid);
			if ($rtn == 'pass') {
			
				echo "p$Pid -> r$Rid<br>";
				$A[] = array($Rid, $Pid);		// New assignment
//echobr(111); printP(); 
				bookkeeping($Rid, $Pid);
//echobr(222); printP(); 
			} else
				echobr($rtn);			
		}
	}

	printP();
	printR();

	
	title('Matching the remaining reviewers and papers');

	$k = 1;
	while ($k <= count($R) * count($P)) {

		if (count($P) == 0) {
			title('Paper slot exhausted');
			break;
		}

		if (count($R) == 0) {
			title('Reviewer slot exhausted');
			break;
		}

		asort($P); // todo randomize
		asort($R);

		foreach ($P as $Pid => $p) {
			foreach ($R as $Rid => $r) {
				
				$rtn = candidateCheck($Rid, $Pid);
				if ($rtn != 'pass') {
					echo "(r$Rid, p$Pid) $rtn $k <br />";
					$k++;
					continue;
				}
				
				// already assigned?
				
				$already = false;
				foreach ($A as $a) {
					if ($a[0] == $Rid && $a[1] == $Pid) {
						echo "(r$Rid, p$Pid) already in A, continue [k:$k r:".count($R).' p:'.count($P)."]<br />";
						$already = true;
						$k++;
					}
				}
				if ($already)
					continue;

				// conflict?
				
				if ($R0[$Rid]['e'][$Pid] == -1) {
					echo " (r$Rid, p$Pid) conflict, continue $k <br />";
					$k++;
					continue;
				}				
				
				echo "p$Pid -> r$Rid<br>";
				$A[] = array($Rid, $Pid);	// New Assignment

				bookkeeping($Rid, $Pid);
					
			} // R
			
		} // P
	}
}

function candidateCheck($Rid, $Pid) {
	
	global $R, $P, $A, $A0, $R0, $maxL, $maxP, $minR, $type;
	
	$rtn = 'pass';
	
	if (!isset($R[$Rid])) {
		$rtn = "r$Rid is already removed; ";
	}
	if (!isset($P[$Pid])) {
		$rtn .= "p$Pid is already removed; ";
	}

	if ($type == 'inc' && $A0[$Rid][$Pid]) {
		$rtn .= "(r$Rid, p$Pid) is already in database;";
	}

	return $rtn;
}

function bookkeeping($Rid, $Pid) {
	
	global $R, $P, $A, $A0, $R0, $maxL, $maxP, $minR, $type;
	
	$R[$Rid]['a']++;
	$P[$Pid]['a']++;

	unset($R[$Rid]['e'][$Pid]);		// One less bid for reviewer
	$P[$Pid]['bid']--;				// One less bid for paper

	// R
	if ($R[$Rid]['a'] >= $maxP[$Rid]) {

		echo "removing reviewer r$Rid, #assi = ".$R[$Rid]['a'].' <br>';

		// inform Papers that lose bid from this Reviewer
		foreach ($R[$Rid]['e'] as $id => $l) {
			if ($id != $Pid and $l != -1)
				if (isset($P[$id])) {
					$P[$id]['bid']--;
				}
		}

		unset($R[$Rid]);
	}

	// P
	if ($P[$Pid]['a'] >= $minR) {

		echo "removing paper p$Pid, #assi = ".$P[$Pid]['a']." <br>";

		// inform Reviewers who have bids for this Paper
		foreach ($R as $id => $r) {
			if (count($r['e']) > 0) {
				foreach ($r['e'] as $p =>$l) {
					if ($p == $Pid and $l != -1) {
						unset($R[$id]['e'][$Pid]); // remove the (Pid, l) pair from this reviewer
					}
				}
			}
		}

		unset($P[$Pid]);
	}
}

function statistics() {

	global $R0, $A, $type;

	// total points of expertise before assignment
	$total1 = 0;
	foreach ($R0 as $Rid => $r) {
		if (count($r['e']) > 0)
			foreach ($r['e'] as $Pid => $l) {
				if ($l != -1)
					$total1 += $l;
			}
	}

	// total points of expertise utilised
	$total2 = 0;
	foreach ($A as $a) {
		$Rid = $a[0];
		$Pid = $a[1];
		$total2 += $R0[$Rid]['e'][$Pid];
	}

	//the more expertises reviewers specify, the less its utilization.
	//the more hot papers there are, the less utilization.
	
	if ($total1 != 0)
		$ratio = substr($total2/$total1, 0, 5);
		
	echo '<br />';
	
	if ($type == 'new') echo "total expertise points = $total1 <br />";
	echo "used expertise points = $total2 <br />";
	if ($type == 'new') echo "used/total = $ratio<br>";

	return $ratio;
}

function writeDB() {

	global $A, $P0, $R0, $type;

		try {

		if (count($A) == 0) {
			title('No assignment generated');
			return;
		}

		title('Writing assignment results to database: ', 0);
		
		DB::autocommit(false);

		if ($type == 'new') 
			DB::query("delete from Assignment", &$result);
			
		$query = "insert into Assignment values ";

		foreach ($A as $a) {

			$query .= "($a[0], $a[1], 1),";

			$P0[$a[1]]['a']++;
			$R0[$a[0]]['a']++;
		}

		$query = substr($query, 0, strlen($query) - 1);
		DB::query($query, &$result, 'writeDB()');

		$track = new Track(1);
		$track->updateNumAssi();
		
		DB::commit();
		title('success');

	} catch (Exception $e) {
		echo '<b class=red>Exception: '.$e->getMessage().'</b><br />';
	}
}

function title($msg, $newLine = 1) {
	echo "<br /><b class=big>$msg</b>";
	if ($newLine ==1)
		echo '<br />';
}


readDB();
//printArray($maxP);

ksort($R); ksort($P);
printP();
printR();

generateAssignment();

printP();
printR();
printA();
statistics();

writeDB();

echo '<br /><b class=big>';
echo HTML::command('By Paper', 'c_assignment_m_10.php').' |  ';
echo HTML::command('By Reviewer', 'c_assignment_m_20.php');
echo '</b><p />';
?>


<style type="text/css">
<!--
a:link {
	color: #333333;
	text-decoration: none;
}
a:visited {
	color: #000000;
	text-decoration: none;
}
a:hover {
	color: #FF0000;
	text-decoration: none;
}
a:active {
	color: #333333;
	text-decoration: none;
}
.red {
	color: #FF0000;
}
.blue {
	color: #0000FF;
}
.big {
	color: #FF0000;
	font-size: 26px;
}
-->
</style>
