<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Conflict.php';

	// add/delete assignments

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	try {	// adding a new assignment might run into conflicts.

		// Privilege check
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(231);

		$r = $_GET['r'];
		$p = $_GET['p'];
		$a = $_GET['a'];

		// todo: check input

		if (!(isset($r) && isset($p) && isset($a)))
			HTTP::message('Missing inputs.');

		$track = new Track(1);
		$reviewer = new Reviewer($r);
		$pe = $reviewer->getBidExpertise(1);
		$reviewerName = $reviewer->getName();

		if ($_GET['confirm'] == 'yes') {		// back from the confirmation page
			$referer = $_SESSION['referer'];
			unset($_SESSION['referer']);

		} else {
			$referer = referer();
			switch ($referer) {
				case 'c_assignment_m_20.php':	// for 20 and 10, tell the page to park on the anchor
					$referer .= "#$r";
					break;
				case 'c_assignment_m_10.php':
					$referer .= "#$p";
					break;
				case 'c_assignment_m_21.php':	// for 21 and 11, tell the page to whom/paper to add
					$referer .= "?r=$r";
					break;
				case 'c_assignment_m_11.php':
					$referer .= "?p=$p";
					break;
			}
			$_SESSION['referer'] = $referer;
		}

		if ($a == 'delete') {
			
			$paper = new Paper($p);
			
			// check if the paper is delegated to some external reviewer
			
			DB::query("select ToID from Delegate where PaperID = $p and FromID = $r", &$result);
			while ($row = $result->fetch_assoc()) {	// either none or just one
				$erId = $row['ToID'];
			}
			$extraMsg = null;
			if ($erId != null) {					// just one
				$er = new Reviewer($erId);
				$extraMsg = '<br /><br /><div class=red>This paper is delegated to external reviewer '.$er->getName().
				'. The delegation will also be deleted.</div>';
			}
			
			HTTP::confirm(
				"Delete the following assignment? <br /><br />
				Paper: $paper->title <br />
				Reviewer: $reviewerName".$extraMsg,
				"c_assignment_m_x.php?r=$r&p=$p&a=$a",
				$referer,
				'Yes',
				'Cancel'
			);
			
			DB::autocommit(false);
			$track->delAssi($r, $p);
			DB::commit();
			HTTP::redirect($referer);
		}

		if ($a == 'add') {
			$conflict = new Conflict(1);
			if ($conflict->detectReviewerPaper($r, $p) > 0) {
				//echobr("r$r p$p");
				HTTP::confirm(
					makeup('There is a conflict: ', 'red', 'white').'<br /><br />'.$conflict->getDetectedtag(),
					"c_assignment_m_x.php?r=$r&p=$p&a=$a",
					$referer,
					'Add it anyway',
					'Do not add'
				);
			}

			DB::autocommit(false);
			$track->addAssi($r, $p);
			DB::commit();
			HTTP::redirect($referer);
		}
	} catch (Exception $e) {
		$oops = $e->getMessage();
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3><? echo 'Action: '.$_GET['c'].'Reviewer: '.$reviewerName; ?> </h3>
<div class=red><? echo $oops; ?></div>
<?php echo $tag; ?>
</div>
<!-- end of main -->

<?php include 'footer.php'; ?>
