<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// List assignments ordered by sum of expertise levels

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(240);

	function main() {

		$track = new Track(1);
		$topics = $track->getTopicsValab();
		$te = $track->getTopicExpertiseValab();
/*
		$pe = $track->getPaperExpertiseValab();
*/		
		$pe = array(	// read from db. todo
			3 => 'High',
			2 => 'Medium',
			1 => 'Low'
		);

		$a1 = $track->getReviewerDetails();	// <rId>, numAssigned, name, te:{<topicId>, topicExpertise}, numFinished
		$a2 = $track->getReviewerPapers();	// <rId>, <pId>, PE, numAssi, title, {<topicId>, isPrimary}, progress, score
		//printArray($a2); 
		if ($a1 != null) {

			foreach ($a1 as $rId=>$r) {
				// rating is between 0 and 1. reviewers having smaller ratings appears on the top.
				$a0[$rId]['rating'] = $r['numAssigned'] == 0 ? 0 : $r['numFinished'] / $r['numAssigned'];
				$a0[$rId]['numAssigned'] = $a1[$rId]['numAssigned'];
				$a0[$rId]['name'] = $a1[$rId]['name'];
				$a0[$rId]['te'] = $a1[$rId]['te'];
				$a0[$rId]['numFinished'] = $a1[$rId]['numFinished'];
			}
			asort($a0);
		}

		if ($a0 == null) return;
		
		foreach ($a0 as $rId=>$r) {

			echo '<b>'.(DEBUG ? $rId.'. ':'').$r['name'].'</b><br />';
			echo '<div  class=small>';

			if (HTML::switchStatus('Topic Info') == 'Show') {
				if (isset($r['te']) && count($r['te']) > 0) {
					foreach ($r['te'] as $topic=>$level) {
						if ($level > 0)
							echo $topics[$topic].', '.$te[$level].'<br />';
					}
				}
			}
			echo 'Finalized ('.$r['numFinished'].') / Assigned ('.$r['numAssigned'].'): ';
			echo warning(substr($r['rating'], 0, 4), 1, 1);
			echo '<br />';
			echo '</div>';

			if (isset($a2[$rId])) {

				unset($data);
				foreach ($a2[$rId] as $pId=>$p) {
					$data[$pId][0] = (DEBUG ? $pId.'. ':'').$p['title'].downloadLink($pId).
					' '.HTML::command('review', "view_review.php?paperId=$pId").
					'<br />';
					arsort($p['topics']);
					if (HTML::switchStatus('Topic Info') == 'Show') {
						$data[$pId][0] .= '<span class=smallgrey>';
						foreach ($p['topics'] as $topic=>$isPrimary) {
							$data[$pId][0] .=  $topics[$topic].'<br />';
						}
						$data[$pId][0] .= '</span>';
					}

					$data[$pId][1] = '<span class=small>'.$pe[$p['PE2']].'</span>';
					$data[$pId][2] = '<span class=small>'.$p['progress'].'</span>';
					$data[$pId][3] = $p['score'];
				}
				$table = new HTMLTable($data);
				$table->setTitle('Papers Assigned=650', 'Expertise=80', 'Progress=100', 'Score=20');
				$table->layout['titleBold'] = 0;
				$table->display();
			}

			echo '<br /><br />';
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? 
	echo HTML::switchCommand('Topic Info');
	echo HTML::command('Late Review Notification', 'c_notification.php?n=reviewLate'); 
?>

<br />
<h3>Reviews<? echo $reviewerName; ?> </h3>
<?php main(); ?>
<p>
</div>
<!-- end of main -->

<?php include 'footer.php'; ?>
