<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/FormQuestion.php';


	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$reviewerId = HTTP::sessionate('reviewerId');
	$paperId = HTTP::sessionate('paperId');
	$trackId = HTTP::sessionate('trackId');
	$review = new Review($reviewerId, $paperId, $trackId);
	$p = new Paper($paperId);

	// confirmation
	HTTP::confirm(
		"Finish the review of $p->title <br />You cannot edit the review once it is finished. <br /><br />
		Do you want to continue?",
		'r_finish.php',
		'r_main.php',
		'Yes',
		'Cancel'
	);

	$er = new ExternalReviewer($_SESSION['id']);

	if (!$er->hasPaper($paperId, $reviewerId))
		HTTP::message('You do not have this delegation.');

	DB::autocommit(false);
	$review->finish();
	DB::commit();

	HTTP::redirect('r_main.php');

?>
