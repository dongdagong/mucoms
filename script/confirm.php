<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	DB::connect();

	// calling script must have its reciving query string variables sessionized.

	$msg = urldecode($_GET['msg']);
	$yesURL = $_GET['yesURL'];
	$noURL = $_GET['noURL'];
	$yesCmd = urldecode($_GET['yesCmd']);
	$noCmd = urldecode($_GET['noCmd']);

	$yesURL = urldecode($yesURL);
	$noURL = urldecode($noURL);

	$mark = strpos($yesURL, '?') === false ? '?':'&';
	$yes = "<a href=$yesURL".$mark."confirm=yes>$yesCmd</a>";
	$mark = strpos($noURL, '?') === false ? '?':'&';
	$no = "<a href=$noURL".$mark."confirm=no>$noCmd</a>";
	
	// Calling page sets pageFormat to '_simple' or '',
	// calling page unsets pageFormat (in case user chooses OK),
	// this page unsets pageFormat (in case user chooses Cancel).
	
	if (isset($_SESSION['pageFormat'])) {
		$h = 'header'.$_SESSION['pageFormat'].'.php';
		$f = 'footer'.$_SESSION['pageFormat'].'.php';
	} else {
		$h = 'header.php';
		$f = 'footer.php';
	}
	unset($_SESSION['pageFormat']);
	
?>
<? include_once $h; ?>
<div class="main">
<p><p><p>
<?
	echo '<div class=dialogBox>';
	echo '<b>'.$msg.'</b><br />';
	echo '<br /><p />';
	echo $yes.' | '.$no.'<br />';
	echo '</div>';
?>
<p><p><p>
</div> <!-- end of main -->
<? include_once $f; ?>
