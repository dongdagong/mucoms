<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$template = HTTP::sessionate('template');	// show, hide, or a ps ID

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$ps = new PaperStatus();
	$psValab = $ps->getPaperStatusValab();
	unset($psValab[-1]);

	$tag = null;
	$i = 1;
	foreach ($psValab as $psId => $psName) {

		$ps = new PaperStatus($psId);
		$ps->getTemplateContent();

		$tag .= '<div class=a4width>';
		$color = $i % 2 == 0 ? 'cell':'shadedcell';
		$tag .= "<div class=$color><br />";

		$tag .= '<span class=right>';
		if ($psId != 0)
			$tag .= HTML::command('Edit', "c_configuration_ps_edit.php?psId=$psId&cmd=edit");
		if ($ps->isEditable == 1)
			$tag .= ' | '.HTML::command('Delete', "c_configuration_ps_edit.php?psId=$psId&cmd=delete");
		$tag .= '<br />';
		$tag .= '</span>';

		$tag .= "<b><a name=$ps->id>$i</a> ".(DEBUG ? $ps->id:'')." $ps->name </b><br />";
		$tag .= 'Send email notification: '.($ps->sendNotification == 1 ? '<b>yes</b>':'no').'<br />';
		$tag .= 'Require final paper upload: '.($ps->finalUpload == 1 ? '<b>yes</b>':'no').'<br />';
		$content = str_replace('  ', '&nbsp;&nbsp;&nbsp;', str_replace(chr(13), '<br />', $ps->templateContent));

		if ($template == 'show' || $template == $ps->id) {
			$tag .= 'Template: <br />'.
				'<div class=leftmargin>'.
				$content.
				'</div>';
		}

		$tag .= '<br /></div></div>'; // a4width, color

		if ($ps->isEditable == 1);
		$i++;
	}

	$tag .= '<br />'.HTML::command('Add a new paper status', "c_configuration_ps_edit.php?cmd=add");
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	if ($template == 'show') {
		$label = 'Hide email templates';
		$template = 'hide';
	} else {
		$label = 'Show email templates';
		$template = 'show';
	}

	echo HTML::menu(array(
		'Topics' 		=> array('c_configuration_edit_topics.php', 1),
		'Paper Formats'	=> array('c_configuration_paper_formats.php', 1),
		'Review Form' 	=> array('c_configuration_form.php', 1),
		'Paper Status'	=> array('c_configuration_ps_disp.php?template=hide', 0),
		$label 			=> array("c_configuration_ps_disp.php?template=$template", 1)
	));
?>
<h3>Paper Status</h3>
<?
	echo $tag;
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
