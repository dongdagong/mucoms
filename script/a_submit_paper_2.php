<?php 
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	/*
		specify the number of authors
	*/
	
	session_start();
	
	Auth::loginCheck(40);	
	
	if (!isset($_SESSION['selectedTrack'])) {
		HTTP::redirect('a_submit_paper_1.php');
	}
	
	$maxNumAuthors = 15;
	$form = new HTMLForm('a_submit_paper_2');
	
	for ($i = 1; $i <= $maxNumAuthors ; $i++) {
		$numArray[$i] = $i;
		if ($i == 1)
			$myNumArray[$i] = '1st'; 
		else if ($i == 2)
			$myNumArray[$i] = '2nd';
		else if ($i == 3)
			$myNumArray[$i] = '3rd'; 
		else 
			$myNumArray[$i] = $i.'th';
	}
	
	$numAuthors = new FormElement('select', 'numAuthors', 1, $numArray);
	$numAuthors->addLayout('Number of Authors: ', null, 'cols=20');
	$numAuthors->addRule('isInt', 'maxInt='."$maxNumAuthors", 'minInt=1');
	$myNum = new FormElement('select', 'myNum', 1, $myNumArray);
	$myNum->addLayout('Number of Authors: ', null, 'cols=20');
	$myNum->addRule('isInt', 'maxInt='."$maxNumAuthors", 'minInt=1');
	$submit = new FormElement('submit', null, 'Next');

	
		
	if (HTTP::isLegalPost()) {
	
		try {
			$form->validate();
			$_SESSION['numAuthors'] = $numAuthors->value;
			$_SESSION['myNum'] = $myNum->value;
			HTTP::redirect('a_submit_paper_3.php'); 
		
		} catch (Exception $e) {
		
			$oops = $e->getMessage();
		}
	}
?>
	
	
<?php include_once 'header.php'; ?>

<div class="main">
<h3>Paper Submission - Step 1</h3>
<p class=red><?php echo $oops; ?><br /></p>
<?php 
	$form->open(); 
	echo 'There are '.$numAuthors->getTag().' authors for this paper, ';
	echo "and I'm the ".$myNum->getTag().' author. '. $submit->getTag();
	$form->close();
?>
</div> <!-- end of main -->

<?php include_once 'footer.php'; ?>
