<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// Manual assignment by reviewer. Reviewers ordered by number of assignments

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(231);

	function main() {

		$track = new Track(1);
		$topics = $track->getTopicsValab();
		$te = $track->getTopicExpertiseValab();
		$pe = $track->getPaperExpertiseValab();

		$a1 = $track->getReviewerDetails();	// <rId>, numAssigned, name, te:{<topicId>, topicExpertise}
		$a2 = $track->getReviewerPapers();	// <rId>, <pId>, PE, numAssi, title, {<topicId>, isPrimary}

		if ($a1 != null) {
			foreach ($a1 as $rId=>$r) {

				echo "<a name=$rId>".'<b>'.(DEBUG ? $rId.'. ':'').$r['name'].'</b> &nbsp;<span class=small>'.
				$r['org'].' &nbsp;'.$r['email'].'</span><br /></a>';
				
				echo '<div  class=small>';
				
				if (HTML::switchStatus('Topic Info') == 'Show') {
					if (isset($r['te']) && count($r['te']) > 0) {
						foreach ($r['te'] as $topic=>$level) {
							if ($level > 0)
								echo $topics[$topic].', '.$te[$level].'<br />';
						}
					}
				}
				
				echo 'Number of papers assigned: '.warning($r['numAssigned'], $track->maxPaperPerRev, $track->maxPaperPerRev);
				echo '<br />';

				echo '</div>';

				if (isset($a2[$rId])) {

					unset($data);
					foreach ($a2[$rId] as $pId=>$p) {
						$data[$pId][0] = (DEBUG ? $pId.'. ':'').$p['title'];
						$data[$pId][0] = HTML::command($data[$pId][0], "c_assignment_m_10.php#$pId").
						' '.downloadLink($pId).'<br /><div class=small>';
						arsort($p['topics']);
						if (HTML::switchStatus('Topic Info') == 'Show') {
							foreach ($p['topics'] as $topic=>$isPrimary) {
								$data[$pId][0] .=  $topics[$topic].'<br />';
							}
						}
						$data[$pId][1] = '<span class=small>'.$pe[$p['PE']].'</span>';
						if ($p['PE'] == -1)
							$data[$pId][1] = makeup($data[$pId][1], 'white', 'red');
						$data[$pId][2] = $p['numAssi'];
						$data[$pId][3] = HTML::command('Delete', "c_assignment_m_x.php?p=$pId&r=$rId&a=delete");
					}
					$table = new HTMLTable($data);
					$table->setTitle('Paper=650', 'Expertise=110', 'Assignments=100', '=20');
					$table->layout['titleBold'] = 0;
					$table->display();
				}

				echo HTML::command('Add Papers', "c_assignment_m_21.php?r=$rId");
				echo '<br /><br />';
			}
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? 
	echo HTML::switchCommand('Topic Info');
	echo HTML::menu(
		array(
			'By Paper' => array('c_assignment_m_10.php', 1),
			'By Reviewer' => array('', 0)
		)
	);
?>
<h3>Manual Assignment By Reviewer</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
