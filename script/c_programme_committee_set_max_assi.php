<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(202);

	$form = new HTMLForm('c_programme_committee');
	$submit = new FormElement('submit', null, 'Submit');
	$line = new FormElement('line', 'line');

	$table = null;
	$track = new Track(1);

	$a1 = $track->getReviewers();

	if ($a1 !=  null) {

		$i = 0;
		foreach ($a1 as $rId) {

			$r = new Reviewer($rId);
			$list[-1] = 'default';
			for ($j = 0; $j <= 30; $j++) {
				$list[$j] = $j;
			}

			$data[$i][0] = $rId;
			$data[$i][1] .= (DEBUG ? $rId.'. ':'').$r->getName();
			$data[$i][2] .= $r->org.'<br /><span  class=smallgrey>'.$r->email.'</span>';
			$value = $r->maxAssi === NULL ? -1:$r->maxAssi;
			$data[$i][3] = new FormElement('select', "$rId", $value, $list);

			$i++;
		}
		if ($data != null) sort($data);
/*
		for ($i = 0; $i < count($data); $i++) { // <= will hang the system
			$data[$i][0] = $i + 1;
		}
*/

		$table = new HTMLTable($data, 0, 'asc');
		$table->setSortOffCols(array(3));
		$table->setTitle('ID=30', 'Reviewer=200', 'Organization and Email=350', 'Maximum Number of Assignments=360');
	}

	if (HTTP::isLegalPost()) {
	
		try {
			
			$form->validateWithException();

			foreach ($data as $row) {
				if ($row[3]->value != -1 && $row[3]->value != $track->maxPaperPerRev)
					$rtn[$row[3]->name] = $row[3]->value;
			}
			//printArray($rtn);
			
			DB::autocommit(false);

			DB::query("update Reviewer set MaxAssi = NULL", &$result);
			if ($rtn != null)
				foreach ($rtn as $rId => $max) {
					DB::query("update Reviewer set MaxAssi = $max where ID = $rId", &$result);
				}		

			DB::commit();
							
			HTTP::message('Maximum assignment saved.', 'c_programme_committee.php'); 
		
		} catch (Exception $e) {
		
			$oops = $e->getMessage();
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Programme Committee - Set Maximum Assignments</h3>
<? 
	echo "The default maximum number of assignments per reviewer is set 
	to $track->maxPaperPerRev ";
	echo '('.HTML::command('modify', 'c_configuration.php').').<br /><br />';
?>
<div class=red><? echo $oops; ?></div>
<?php
	if ($a1 != null) {
		$form->open(); 
		$table->display();
		echo '<br />';
		$submit->display();
		$form->close();
	} else
		echo 'Please add reviewers first.';

?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
