<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/PaperFormat.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$form = new HTMLForm('c_configuration_paper_formats');
	$submit = new FormElement('submit', null, 'Submit');
	$line = new FormElement('line', 'line');

	$track = new Track(1);
	$pf = new PaperFormat();
	$paperFormatsFull = $track->getPaperFormatsValab('full');
	$paperFormatsFinal = $track->getPaperFormatsValab('final');
	$paperFormatsAll = $pf->names;
	
	$singleCheckbox = array(
		1 => ''
	);
	
	$data = null;
	$tag = null;
	
	if ($paperFormatsAll !=  null) {
		
		foreach ($paperFormatsAll as $format => $format2) {
			
			$data[$format][0] = "&nbsp;&nbsp;<b>$format</b>";
			
			$format2 = substr($format2, 1, strlen($format2) - 1);
			$isFull = isset($paperFormatsFull[$format]) ? array(1):null;
			$data[$format][1] = new FormElement('checkbox', "full$format2", null, $singleCheckbox, $isFull);
			$isFinal = isset($paperFormatsFinal[$format]) ? array(1):null;
			$data[$format][2] = new FormElement('checkbox', "final$format2", null, $singleCheckbox, $isFinal);
			
		}

		$table = new HTMLTable($data);
		$table->setTitle('=100', 'Full Paper=200', 'Final Paper=200');
	}
	
	if (HTTP::isLegalPost()) {
	
		try {
			
			$form->validate();
			
			foreach ($paperFormatsAll as $format => $format2) {
/*
				echobr('.'.$data[$format][1]->values[0].'.');
				echobr('.'.$data[$format][2]->values[0].'.');
*/
				
				if ($data[$format][1]->values[0] == 1)
					$pfFull[] = $format;
				if ($data[$format][2]->values[0] == 1)
					$pfFinal[] = $format;
			}
			
/*
			printArray($pfFull);
			printArray($pfFinal);
*/
			
			if (count($pfFull) == 0 || count($pfFinal) == 0)
				throw new Exception('At least one format is needed for each submission phase. Please try again.');
						
			DB::autocommit(false);
			
			DB::query("delete from TrackPaperFormat", &$r);
			
			$q = null;
			foreach($pfFull as $format) {
				$q .= ($q != null? ', ':' ')."(1, '$format', 'full')";
			}
			$q = 'insert into TrackPaperFormat values '.$q;
			DB::query($q, &$r);
			
			$q = null;
			foreach($pfFinal as $format) {
				$q .= ($q != null? ', ':' ')."(1, '$format', 'final')";
			}
			$q = 'insert into TrackPaperFormat values '.$q;
			DB::query($q, &$r);
			
			DB::commit();
			
			$tag = 'Paper formats saved.<br />';
					
		} catch (Exception $e) {
		
			$oops = '<span class=red>'.$e->getMessage().'</span><br />';
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::menu(array(
		'Topics' 		=> array('c_configuration_edit_topics.php', 1),
		'Paper Formats'	=> array('c_configuration_paper_formats.php', 0),
		'Review Form' 	=> array('c_configuration_form.php', 1),
		'Paper Status'	=> array('c_configuration_ps_disp.php?template=hide', 1)
	));
?>
<h3>Paper Formats</h3>
<? echo $oops; ?>
<?
	echobr($tag);
	$form->open();
	$table->display();
	$line->display();
	$submit->display();
	$form->close();	
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
