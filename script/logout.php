<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
?>
<?php include_once 'header.php'; ?>
<div class="main">
<?php

	$shortName = $_SESSION['conference']->shortName;
	
	$instanceURL = 'index.php?ci='.$_SESSION['ci'];
	$siteURL = "index.php";
	
	Auth::logout();

	echo '<br />You have logged out of '.$shortName.'.<br /><br />';
	echo 'Go to '.HTML::command($shortName, $instanceURL).' login page.<br />';
	echo 'Go to '.HTML::command(SYSTEM_NAME, $siteURL).' site.';
?>
<p>
</div> <!-- end of main -->
<?php include_once 'footer.php'; ?>
