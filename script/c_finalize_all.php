<?
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// finalize all reviews
	// increase reviews' finalized count

	session_start();
	Auth::loginCheck(20);
	DB::connect();
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(250);

	HTTP::confirm(
		"This operation will finalize all the reviews.<br /><br />
		Do you want to continue?",
		'c_finalize_all.php',
		'c_selection.php',
		'Yes',
		'Cancel'
	);

	function main() {

		try {
			DB::autocommit(false);

			$t = new Track(1);
			$reviewers = $t->getReviewers();
			foreach ($reviewers as $rId) {
				$r = new Reviewer($rId);
				$assignments = $r->getAssignments();
				foreach ($assignments as $pId) {
					$review = new Review($rId, $pId, 1);
					$review->finalize();
				}
			}

			DB::commit();
			echo 'Reviews finalized <br /><br />';
			echo 'Click '.HTML::command('here', 'c_selection.php').' to continue.';
		} catch (Exception $e) {
			$oops = $e->getMessage().'<br /><br />';
		}
	}

?>
<? include 'header.php'; ?>

<div class="main">
<h3>Finalize all reivews</h3>
<? main(); ?>
<div class=red><? echo $oops; ?></div>
</div> <!-- end of main -->

<? include 'footer.php'; ?>