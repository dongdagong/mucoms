<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Notification.php';
	include_once 'cms/Assignment.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(230);

	$assi = new Assignment();
	$msg = null;
	$canAuto = $assi->canPerformAutoAssignment(&$msg);
	if ($canAuto != true)
		HTTP::message($msg.'Cannot perform automatic assignment at this stage.', 'c_assignment.php');
		
?>
<?php include 'header.php'; ?>
<div class="main">
<h3>Automatic Assignment</h3>
<?
	echo '<div class=dialogBox>';
	echo '<b>'.HTML::command('New Assignment', 'c_assignment_a_2.php?assignmentType=new').'</b><br />';
	echo '<p>Generate a new set of assignments and <b>override</b> existing ones.';
	
	echo '<br /><br /><br />';
	
	echo '<b>'.HTML::command('Incremental Assignment', 'c_assignment_a_2.php?assignmentType=inc').'</b><br />';
	echo '<p>Generate additional assignments on top of existing ones.';
	echo '</div>';
	echo '<br />'.HTML::command('Return to previous page', 'c_assignment.php');
?>
<p></p>
</div> <!-- end of main -->
<?php include 'footer.php'; ?>
