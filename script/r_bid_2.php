<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$t = new Track(1);
	$role = $_SESSION['role'];
	
	if ((!$t->isTopicExpertiseOpen()) && $role == 30) { 
		HTTP::message("Bidding is not open at this stage.");
	}

	if ($role == 40 || $role ==31)
		HTTP::message('Access denied.');
		
	if ($role == 30) 
		$rId = $_SESSION['id'];

	if ($role == 20) {
		$rId  = HTTP::sessionate('rId');
		if ($rId == null)
			HTTP::message('Missing rId.');
	}
		
	$r = new Reviewer($rId);

	$paperExpertiseValab = $t->getPaperExpertiseValab();
	$topics = $t->getTopicsValab();

	$topicExpertise = array(); 
	$paperExpertise = array();
	

	function main() {

		global $t;
		global $r;

		$topicExpertiseArray = $r->getTopicExpertiseArray();
		arsort($topicExpertiseArray);

		if ($topicExpertiseArray == null && $_SESSION['role'] == 30) {
			
			echobr('Please '.HTML::command('specify expertise in topics', 'r_bid_1.php').' first.');
			return;
		}
	
		if ($topicExpertiseArray == null && $_SESSION['role'] == 20) {
			$topicExpertiseArray = $t->getTopicsValab();
			foreach ($topicExpertiseArray as $topicId => $expertise)
				$topicExpertiseArray[$topicId] = 0;
		}
		
		$form = new HTMLForm('r_bid_2');
		$form->open();
		
		foreach ($topicExpertiseArray as $topicId=>$expertise) {

			priTopicPapers($t->id, $topicId, $expertise);
			echo '<br />';
		}

		$submit = new FormElement('submit', null, 'Submit');
		$submit->display();
		$form->close();
	}

	// display a table of papers which have $topicId as the primary topic

	function priTopicPapers($trackId, $priTopicId, $te) {
		global $r;
		global $paperExpertiseValab;
		global $topicExpertise;
		global $t;
		global $topics;

		$data = array();
		$paperExpertiseArray = $t->getPaperExpertiseArray($priTopicId, $r->id);
		$wPapers = $t->getPapersByStatus(-1);

		if ($paperExpertiseArray == null) 
			$cnt = 0;
		else
			$cnt = count($paperExpertiseArray);

		echo ("<h3>Primary Topic: $topics[$priTopicId] [$cnt papers] </h3>");
		if  ($cnt == 0) return;
		
		$i = 0;
		foreach ($paperExpertiseArray as $paperId=>$expertise) {

			if (isset($wPapers[$paperId])) {
				echobr("Paper ID:$paperId is withdrawn.");
				continue;
			}
			$p = new Paper($paperId);
			$data[$i][0] = '<div class=a4width><b>'.$p->title.'</b><br />';
			//$data[$i][0] = '<b>'.($i+1).'. '.$p->title.' </b>'.$p->getFullLink('f').' '.$p->getFinalLink('F').'<br />';
			if (!$t->isBlindReview())
				$data[$i][0] .= $p->getAuthorNames().'<br />';
			$data[$i][0] .= '<span class=darkgrey>Abstract: '.$p->abstract.'</span></div><br />';

			if ($expertise == 0 || $expertise == NULL)
				$expertise = $te;

			$paperExpertise[$paperId] = new FormElement('select', "paperExpertise$paperId",
			$expertise,	$paperExpertiseValab);

			$data[$i][1] = $paperExpertise[$paperId]->getTag();

			$i++;
		}

		$table = new HTMLTable($data);
		$table->setTitle("&nbsp;=800", 'Expertise in Paper=125');
		$table->layout['titleBold'] = 0;

		if (count($data) > 0)
			$table->display();
	}

	if (HTTP::isLegalPost()) {

		try {

			foreach ($_POST as $id=>$expertise) {
				$paperExpertiseArray[substr($id, 14, strlen($id) - 14)] = $expertise;
			}

			DB::autocommit(false);
			$r->updatePaperExpertise(1, $paperExpertiseArray);
			DB::commit();

			HTTP::message('Biddings saved successfully.', $role == 30 ? 'r_main.php':'c_bid.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<? 
	echo $_SESSION['r_bid_dueBid'];

	echo $_SESSION['role'] == 30 ?
		'<h3>Bidding - Specify Paper Expertise</h3>' :
		'<h3>Biddings - '.$r->getName().'</h3>';
		
	echo '<p>In descending order of primary topic expertise:';
?>
<? echo $oops; ?><br /><br />
<? 	main(); ?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
