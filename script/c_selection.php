<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'c_selection_0.php';	
	include_once 'cms/PaperStatus.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(250);

	function main() {
		
		$form = new HTMLForm('c_selection');
		$submit = new FormElement('submit', null, 'Submit');
		$track = new Track(1);
		
		$pa = paperArray();

		if ($pa == NULL)
			echobr('No paper data yet.<br />');
		else {
			echo makeup('&nbsp;&nbsp;', 'red', 'red').' Review not submitted; '.
			makeup('&nbsp;score&nbsp;', 'white', 'red').' Review submitted but not finalized; '.
			makeup('&nbsp;?&nbsp;', 'white', 'red').' Review conflict; '.
			makeup('&nbsp;d&nbsp;', 'white', 'grey').' Discussion initiated; '.
			makeup('&nbsp;n&nbsp;', 'white', 'grey').' Notification sent<br /><br />';
		}

		$data = null;
		if ($pa != null) {
			
			$decisionValab = $track->getDecisionValab();
			
			foreach ($pa as $i => $row) {

				$pId = $row[10];
				
				$data[$i][0] = makeup($row[8], '#EEEEEE', 'white').'<br/> '.$row[0];
				$data[$i][1] = makeup($row[9], '#EEEEEE', 'white').'<br/> '.$row[1];
				$download = downloadLink($pId);
				$notify = $row[7] == 0 ? '':HTML::command('notify', "c_decision_notification.php?pId=$pId");
				
				DB::query("select IsDecisionNotified from Paper where ID = $pId", &$result);
				$notify = DB::getCell(&$result) == 1 ? makeup('&nbsp;n&nbsp;', 'white', 'grey'):$notify;
				$data[$i][2] = "$row[2]<br /><span class=small><span class=right>$download $notify $row[4]</span></span><span class=smallgrey>$row[3]</span>";
				$data[$i][3] = "<span class=small>$row[5]</span>";

				$data[$i][4] = new FormElement('select', "$pId", $row[7], $decisionValab);

			}

			$table = new HTMLTable($data, 0, 'desc');
			$table->setSortOffCols(array(3, 4));
			$table->setTitle('Avg=40', 'Weighted Avg=165', 'Title & Author=650', 'Reviewer & Score=350',  'Status=100');

			$form->open();
			$table->display();
			echo '<br />';
			$submit->display();
			$form->close();
		}
	}

	if (HTTP::isLegalPost()) {

		$track = new Track(1);
		try {
			//var_dump($_POST);
			foreach ($_POST as $pId=>$decision) {
				//if ($rId != 0) {
					$decisionArray[$pId] = $decision;
				//}
			}

			DB::autocommit(false);
			$track->updateDecision($decisionArray);
			DB::commit();

			HTTP::message('Selection saved. <br />
			No notification to authors is sent. <br />
			To send notifications, choose Decision Notification.', 'c_selection.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<?
	echo HTML::command('Initiate Discussion for All Conflicting Reviews', 'c_init_discuss.php?pId=all').' | ';
	echo HTML::command('Finalize All Reviews', 'c_finalize_all.php').' | ';
	echo HTML::command('Decision Notification', 'c_decision_notification.php').' | ';
	echo HTML::command('Late Final Paper Notification', 'c_notification.php?n=finalLate');
?>

<br />
<h3>Paper Selection</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p></p>
</div> <!-- end of main -->
<?php include 'footer.php'; ?>
