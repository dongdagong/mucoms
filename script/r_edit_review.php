<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	session_start();
	Auth::loginCheck($_SESSION['role']);
	DB::connect();

	$role = $_SESSION['role'];
	if (!($role == 30 || $role == 31))
		HTTP::message('Access denied');

	$paperId = HTTP::sessionate('paperId');
	$reviewerId = HTTP::sessionate('reviewerId');
	$choiceDispMode = HTTP::sessionate('choiceDispMode');
	
	$dispModeSwitcher = false;
	try {

		$r = new Reviewer($reviewerId);	// instead of Session['id'], which might be an ER's id.
		$p = new Paper($paperId);
		$review = new Review($r->id, $p->id, $p->trackId);
		$t = new Track($p->trackId);

		// reviewer has assignment
		if ($role == 30) {
			if (!$r->hasPaper($paperId))
				HTTP::message('You do not have this assignment.');
			if ($reviewerId != $_SESSION['id'])
				HTTP::message("You are not reviewer $reviewerId.");
		}

		// external reviewer has delegation
		if ($role == 31) {
			$er = new ExternalReviewer($_SESSION['id']);
			if (!$er->hasPaper($paperId, $reviewerId))
				HTTP::message('You do not have this delegation.');
		}

		if (!$review->isEditable())
			HTTP::message('Review not editable at this stage.');

		// for each review form question, i'm going to generate a corresponding HTML form element.

		$form = new HTMLForm('r_edit_review');
		$line = new FormElement('line');

		//var_dump($review);

		$q = array();

		for ($i = 1; $i <= $review->numQuestions; $i++) {
			//echobr("$i ". $review->question[$i]->questionType);

			$desc = $review->question[$i]->isConfidential == 1 ?
			'(Confidential, won\'t be revealed to the author.)':NULL;

			switch ($review->question[$i]->questionType) {
			case 'Choice':
				foreach ($review->question[$i]->answers as $id => $answer) 
					$review->question[$i]->answers[$id] = "[$id] $answer";
					
				$elementType = 'select';
				if ($choiceDispMode != null) {
					$elementType = $choiceDispMode;
				} 
				
				$cnt = count($review->question[$i]->answers);
				if ($cnt > 3) $dispModeSwitcher = true;

				if ($elementType == 'select') {
					if (count($review->question[$i]->answers) > 3) {
						$review->question[$i]->answers[null] = ' ';
						ksort($review->question[$i]->answers);
					} else
						$elementType = 'radio';
				}
				
				$q[$i] = new FormElement($elementType, "q$i", $review->question[$i]->answer, $review->question[$i]->answers);
				$q[$i]->addLayout(
					($i == $t->overallId ? '<b>':'').
					$review->question[$i]->id.'. '.$review->question[$i]->question.
					($i == $t->overallId ? '</b>':''),
					$desc);//, 'labelAlign=left');
				break;
			case 'Comment':
				$q[$i] = new FormElement('textarea', "q$i", $review->question[$i]->answer);
				$q[$i]->addLayout($review->question[$i]->id.'. '.$review->question[$i]->question,
				$desc, 'rows=10', 'cols=63'); //, 'labelAlign=left');
				$q[$i]->addRule('maxLen=10000');
				break;
			}
			if ($review->question[$i]->isRequired) {
				$q[$i]->addRule('required');
				if ($elementType == 'select')
					$q[$i]->addRule('minInt=0');
			}
			$form->addElement($q[$i], $line);
		}

		$sendEmailValab = array(
			1 =>'Send me an email copy of this review.'
		);
		$sendEmailValues[0] = 1;
		$sendEmail = new FormElement('checkbox', "sendEmail", NULL, $sendEmailValab, $sendEmailValues);


		$submit = new FormElement('submit', null, 'Submit');
		$form->addElement($sendEmail, $line, $submit);

		if (HTTP::isLegalPost()) {

			for ($i = 1; $i <= $review->numQuestions; $i++) {
				$review->question[$i]->answer = get_magic_quotes_gpc() ? $q[$i]->value : addslashes($q[$i]->value);
			}

			try {

				$form->validateWithException();

				DB::autocommit(false);
				$review->update();
				DB::commit();

				// send email

				if ($sendEmail->getSelectValue(0) == 1) {

					$review = new Review($reviewerId, $paperId, 1);
					$reviewText .= $review->getReviewText();

					$e = new Email('Review Updated', 'review_update');
					$e->addPlugin("PaperTitle:$p->title");
					$e->addPlugin("Review:$reviewText");
					$e->send(new Users($_SESSION['id']));
				}

				HTTP::redirect('r_main.php');

			} catch (Exception $e) {

				$oops = $e->getMessage();
			}
		}
	} catch (Exception $e) {
		$oops = $e->getMessage();
	}

?>
<? include_once 'header.php'; ?>
<div class="main">
<h3>Review of <? echo $p->title; ?></h3>
<? 
	if ($dispModeSwitcher) {
		echo '<div class=right0>';
		echo 'Display: '.HTML::command('Radio', 'r_edit_review.php?choiceDispMode=radio', $choiceDispMode == 'radio' ? 'disabled':'normal').' | ';
		echo HTML::command('Dropdown List', 'r_edit_review.php?choiceDispMode=select', $choiceDispMode == 'select' || $choiceDispMode == null ? 'disabled':'normal');
		echo '<br /><span class=small>Please <b>save your changes</b> before changing display mode</span></div><br />';
	}
	$timeout = ini_get('session.gc_maxlifetime') / 60;
	echobr("This page times out in $timeout minutes of inactivity."); 
	echo REQUIRED_FIELDS;
?>
<p class=red><? echo $oops; ?>
<?
	if (isset($form))
		$form->display();
?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
