<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Notification.php';
	include_once 'cms/Phase.php';

	// Phase control

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(204);


	$track = new Track(1);

	$form = new HTMLForm('c_phase');
	$submit = new FormElement('submit', null, 'Submit');

	$today = date('Y-m-d');
	$data = array();

	$openClose = array(
		1 => 'Open ',
		0 => 'Close '
	);

	$phAbst = new FormElement('radio', 'phAbst', $track->phAbst, $openClose);
	$phAbst->addLayout('Abstract Submission', NULL, 'radio=h');

	$data[0][0] = 'Abstract Submission';
	$data[0][1] = $phAbst->getTag();
	$data[0][2] = $track->dueAbst;
	if (dateDiff($today, $data[0][2]) < 0)
		$data[0][2] = makeup($data[0][2], 'white', 'red');

	$phFull = new FormElement('radio', 'phFull', $track->phFull, $openClose);
	$phFull->addLayout('Full Paper Submission', NULL, 'radio=h');

	$data[1][0] = 'Full Paper Submission';
	$data[1][1] = $phFull->getTag();
	$data[1][2] = $track->dueFull;
	if (dateDiff($today, $data[1][2]) < 0)
		$data[1][2] = makeup($data[1][2], 'white', 'red');

	$phTopicExpertise = new FormElement('radio', 'phTopicExpertise', $track->phTopicExpertise, $openClose);
	$phTopicExpertise->addLayout('Topic Expertise Specification', NULL, 'radio=h');

	$data[2][0] = 'Bidding';
	$data[2][1] = $phTopicExpertise->getTag();
	$data[2][2] = $track->dueTopicExpertise;
	if (dateDiff($today, $data[2][2]) < 0)
		$data[2][2] = makeup($data[2][2], 'white', 'red');

	// as long as topic expertise specification is open, paper expertise specification must be open.
	// do not use it.
//	$phPaperExpertise = new FormElement('radio', 'phPaperExpertise', $track->phPaperExpertise, $openClose);
//	$phPaperExpertise->addLayout('Paper Expertise Specification', NULL, 'radio=h');
//
//	$data[3][0] = 'Paper Expertise Specification';
//	$data[3][1] = $phPaperExpertise->getTag();
//	$data[3][2] = $track->duePaperExpertise;

	// set to 1 after assignment result notification.

	$phReview = new FormElement('radio', 'phReview', $track->phReview, $openClose);
	$phReview->addLayout('Review Submission', NULL, 'radio=h');

	$data[4][0] = 'Review Submission';
	$data[4][1] = $phReview->getTag();
	$data[4][2] = $track->dueReview;
	if (dateDiff($today, $data[4][2]) < 0)
		$data[4][2] = makeup($data[4][2], 'white', 'red');

//	$phGroup = new FormElement('radio', 'phGroup', $track->phGroup, $openClose);
//	$phGroup->addLayout('Group Discussion', NULL, 'radio=h');
//
//	$data[5][0] = 'Group Discussion';
//	$data[5][1] = $phGroup->getTag();
//	$data[5][2] = $track->dueGroup;
//
//	$phGlobal = new FormElement('radio', 'phGlobal', $track->phGlobal, $openClose);
//	$phGlobal->addLayout('Global Discussion', NULL, 'radio=h');
//
//	$data[6][0] = 'Global Discussion';
//	$data[6][1] = $phGlobal->getTag();
//	$data[6][2] = $track->dueGlobal;

	$phFinal = new FormElement('radio', 'phFinal', $track->phFinal, $openClose);
	$phFinal->addLayout('Final Paper Submission', NULL, 'radio=h');

	$data[7][0] = 'Final Paper Submission';
	$data[7][1] = $phFinal->getTag();
	$data[7][2] = $track->dueFinal;
	if (dateDiff($today, $data[7][2]) < 0)
		$data[7][2] = makeup($data[7][2], 'white', 'red');

	$table = new HTMLTable($data);
	$table->setTitle('Phase=200', ' Status=320', 'Due Date=90');

	if (HTTP::isLegalPost()) {

		try {

			$track->phAbst = $phAbst->value;
			$track->phFull = $phFull->value;
			$track->phTopicExpertise = $phTopicExpertise->value;
			//$track->phPaperExpertise = $phPaperExpertise->value;

			$track->phReview = $phReview->value;
			//$track->phGroup = $phGroup->value;
			//$track->phGlobal = $phGlobal->value;
			$track->phFinal = $phFinal->value;

			$trackOld = new Track(1);	// original phase open/close status

			$opened = null;	

			DB::autocommit(false);
			$track->updatePhases();
			
			if ($trackOld->phAbst < $track->phAbst) {
				$opened[] = 'abst';
				//$noti->send('abstOpen');
				//$reply .= 'Abstract submission open notification sent.<br />';
			}

			if ($trackOld->phFull < $track->phFull) {
				$opened[] = 'full';
				//$noti->send('fullOpen');
				//$reply .= 'Full paper submission open notification sent.<br />';
			}

			if ($trackOld->phTopicExpertise < $track->phTopicExpertise) {
				$opened[] = 'bid';
				//$noti->send('bidOpen');
				//$reply .= 'Bidding open notification sent.<br />';
			}

			if ($trackOld->phReview < $track->phReview) {
				$opened[] = 'review';
				if ($track->isFormFinalized != 1)
					HTTP::message('Cannot open review submission. Please finalize review form first.', 'c_configuration_form.php');
				//$noti->send('reviewOpen');
				//$reply .= 'Review submission open notification sent.<br />';
			}
			
			DB::commit();

			if ($trackOld->phFinal < $track->phFinal) {
				$opened[] = 'final';
				//$noti->send('finalOpen');
				//$reply .= 'Final paper submission open notification sent.<br />';
			}

			$reply .= 'Phase status saved.<br /><br />';
			
			if ($opened != null) {
				$_SESSION['openedArray'] = $opened;
				HTTP::redirect('c_phase_2.php');
			}

		} catch (Exception $e) {
			$oops = $e->getMessage();
		}
	}

?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Phase Management</h3>
<p class=red><?php echo $oops; ?></p>
<?php
	echo $reply;

	$form->open();
	$table->display();
	echo '<br />';
	$submit->display();
	$form->close();
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
