<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';
	include_once 'cms/Notification.php';
	include_once 'cms/Assignment.php';
	
	session_start();
	
	try {
		DB::connect();
		
		$c = new Chair($_SESSION['id']);
		$c->privilegeCheck(231);

		$cmd = HTTP::sessionate('assignmentData');
		
		if ($cmd != 'bak' && $cmd != 'res')
			HTTP::message('Parameter missing', 'c_assignment.php');

		if ($cmd == 'bak') {
			$title = 'Backup Assignment';
			
			HTTP::confirm(
				'Backup assignment data?',
				'c_assignment_br.php',
				'c_assignment.php',
				'Yes',
				'Cancel'
			);
			
			DB::autocommit(false);
			DB::query('delete from Assignment2', &$result);
			DB::query('insert into Assignment2 select * from Assignment', &$result);
			DB::commit();
			
			HTTP::message('Assignment backup successful', 'c_assignment.php');
		}	
		
		if ($cmd == 'res') {
			$title = 'Restore Assignment';
			
/*
			$noti = new Notification('assignment');
			if ($noti->isSent())
*/
			$assi = new Assignment();
			$msg = null;
			$canAuto = $assi->canPerformAutoAssignment(&$msg);
			if ($canAuto != true)
				HTTP::message($msg.'Cannot perform automatic assignment at this stage.', 'c_assignment.php');
				
			HTTP::confirm(
				'Restoring the backup assignment will <span class=red>override the current assignment</span> . 
				Do you want to continue? ',
				'c_assignment_br.php',
				'c_assignment.php',
				'Yes',
				'Cancel'
			);
			
			DB::autocommit(false);

			DB::query('delete from Assignment', &$result);
			DB::query('insert into Assignment select * from Assignment2', &$result);

			$track = new Track(1);
			$track->updateNumAssi();
		
			DB::commit();
			
			HTTP::message('Assignment restore successful.', 'c_assignment.php');
		}
	} catch (Exception $e) {
		$oops = $e->getMessage();
	}


?>
<?php include 'header.php'; ?>

<div class="main">
<h3><? echo $title; ?></h3>
<? echo $oops; ?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
