<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	// List assignments ordered by sum of expertise levels

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	// Privilege check
	$c = new Chair($_SESSION['id']);
	//$c->privilegeCheck(999);

	function main() {

		$track = new Track(1);
//		$topics = $track->getTopicsValab();
		$te = $track->getTopicExpertiseValab();
		$pe = $track->getPaperExpertiseValab();

		$reviewers = $track->getReviewers();
		$data = array();

		if ($reviewers != null) {

			$i = 0;
			foreach ($reviewers as $rId) {

				$r = new Reviewer($rId);
				$bids = $r->getBidExpertise(1);
				$positiveBid = 0;
				$negativeBid = 0;
				if ($bids != null) {
					foreach($bids as $pId => $pe){
						if ($pe != -1)
							$positiveBid++;
						else
							$negativeBid++;
					}
				}

				
				//$data[$i][0] = warning($positiveBid, $track->maxPaperPerRev);
				$data[$i][0] = $positiveBid;
				$data[$i][1] = $negativeBid;
				$data[$i][2] = (DEBUG ? $r->id.'. ':'').$r->getName();
				$data[$i][3] = $r->org.'<br /><span class=smallgrey>'.$r->email.'</span>';
				$data[$i][4] = HTML::command('View/Update', "r_bid_2.php?rId=$r->id");

				$i++;
			} // foreach reviewers

			//asort($data);
			$table = new HTMLTable($data, 0, 'asc');
			$table->setTitle('Num of Bids=110', 'Num of Conflicts=140', 'Reviewer=240', 
			'Organization=500', '=90');
			$table->display();
		} // if
	}
?>
<?php include 'header.php'; ?>

<div class="main">
<? 
	echo HTML::command('Late Bidding Notification', 'c_notification.php?n=bidLate');
?>
<br />
<h3>Reviewer Biddings</h3>
<div class=red><? echo $oops; ?></div>
<?php main(); ?>
<p>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
