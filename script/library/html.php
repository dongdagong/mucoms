<?php
/*
	An I/O library handling presentation tasks in HTML.
	It does not access databases directly.
*/

/*
	HTMLForm Design goals:

	Handle input the way it is done in standalone applications.

	Generate
	Initialise
	Display (retain old values. error messages. css selectors. flexible placement into html pages)
	Validate
	Bind/Get value(s) so the form data can be processed/populated

	Usage Notes:

	addLayout before addRule

*/
	// Globals used by element sets to buffer HTML output
	$GLOBALS['error'] = array();
	$GLOBALS['desc'] = array();
	$GLOBALS['tag'] = array();


	class FormElement {

		public $type;
		public $name;
		public $value;
		public $values = array(); // indexed array of selected values.
		public $valab = array(); // associative array of pairs 'VAlue'=>'LABel'

		public $label;	// not to be confused with the labels in select, checkbox, and radio.
		public $desc;
		public $layout = array();
		public $attrib;	// usability performance trade off. the users use addLayout to set attrib as well

		public $rules = array();		// an associative array of 'rulename=>detail' pairs
		public $errors = array();


		function FormElement($type = null, $name = null, $value = null, $valab = null, $values = null) {

			$this->type = $type;
			$this->name = $name;
			//$this->value = $_POST["$name"] == null ? $value : $_POST["$name"];
			$this->value = !isset($_POST["$name"]) ? $value : $_POST["$name"];
			$this->valab = $valab;
			//$this->values = !isset($_POST["$name"]) ? $values : $_POST["$name"];
			$this->values = $_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST["$name"] : $values;

			// default layouts
			$this->layout['label'] = 'h';
			$this->layout['radio'] = 'v';
			$this->layout['checkbox'] = 'v';
			$this->layout['labelAlign'] = 'right';

			// the layouts will affect the output of getTag


		}

		function disable() {
			$this->attrib .= 'disabled ';
		}

		function addLayout($label = null, $desc = null) { // optional 'key=value' pairs.

			$this->label = $label;
			$this->desc = $desc;

			$numArgs = func_num_args();
			if ($numArgs > 2) {
				for ($i = 2; $i < $numArgs; $i++) {
					$arg = func_get_arg($i);
					$pair = explode('=', trim($arg));
					$this->layout[$pair[0]] = $pair[1];
					if ($pair[0] != 'set' && $pair[0] != 'class') {
						$this->attrib .= $arg.' ';
					}
				}
			}
		}

		function addRule() {
			$numArgs = func_num_args();
			for ($i = 0; $i < $numArgs; $i++) {
				$arg = func_get_arg($i);
				$pair = explode('=', trim($arg));
				$this->rules[$pair[0]] = $pair[1];
				if ($pair[0] == 'required')
					$this->label = '<b>*</b> '.$this->label;
			}
			//var_dump('<br/>', $this->rules, '<br />');
			//$this->validate();
		}

		function isRequired() {
			foreach ($this->rules as $r=>$rule) {
				if ($r == 'required')
					return true;
			}
			return false;
		}

		function cleanup($string) {

			$string = trim($string);

			// injection check

			$badChars= array(
				'--',
				//';'
			);

			foreach ($badChars as $bad) {
				if (!strpos(strtolower($string), $bad) === false)
					throw new Exception("$bad is not allowed in the input");
			}

			$string = str_replace(';', '.', $string);

			$string = get_magic_quotes_gpc() ? stripslashes($string) : $string;
			$string = htmlspecialchars($string, ENT_QUOTES);

			return $string;
		}

		function validate() {

			$success = true;

			if ($this->type == 'checkbox') {
				if (! (count($this->values) > 0) && $this->isRequired()) {
					$this->errors[] = 'Required';
					$success = false;
				}

				// todo check each value in the checkbox values array

			} elseif ($this->type == 'file') {
				//echo $_FILES[$this->name]['type'];

				// check if this upload is required
				$isRequired = false;
				foreach ($this->rules as $r=>$rule) {
					if ($r == 'required') {
						$isRequired = true;
						break;
					}
				}

				// check if the file is successfully stored in the tmp directory
				$input = $this->name;
				$file = $_FILES[$this->name]['name'];
				if ($_FILES[$this->name]['error'] > 0) {
					$this->errors[] = "$file";
					switch ($_FILES[$input]['error']) {
						case 1:
						case 2:
							$this->errors[] = "File too big. Maximum: ".$this->rules['MAX_FILE_SIZE'].'MB';
							break;
						case 3:
							$this->errors[] = "File only partially uploaded";
							break;
					}
					$success = false;

					// check that the upload IS there
					if ($_FILES[$this->name]['error'] == 4 && $isRequired) {
						$this->errors[] = "No file uploaded";
						$success = false;
					}

				} else {

					// no error, check file format
					if (isset($this->rules['formats']) && isset($_FILES[$input])) {
						$lastDotPos = strrpos($file, '.');
						$tmpFileFormat = substr($file, $lastDotPos, strlen($file) - $lastDocPos);
						//echo '<h2>_'.$tmpFileFormat.'_</h2>';
						$hit = false;
						foreach ($this->rules['formats'] as $format) {
							//echo '_'.$format.'_<br >';
							if (strtolower($tmpFileFormat) == strtolower($format))
								$hit =  true;
							$acceptedFormats .= $format.' ';
						}
						if (!$hit) {
							$this->errors[] = "Wrong file format. Acceptable formats: $acceptedFormats";
							$success = false;
						}
					}
				}

			} else {	// all the other simpler form element types

				// cleanup
				$this->value = $this->cleanup($this->value);

				foreach ($this->rules as $r=>$rule) {
					if ($r == 'required' && $this->value == null) {
						$this->errors[] = "Required";
						$success = false;
						break;
					}
					switch ($r) {
						case 'maxLen':
							if (strlen($this->value) > intval($rule)) {
								$this->errors[] = "Maximum $rule characters";
								$success = false;
							}
							break;
						case 'minLen':
							if (strlen($this->value) < intval($rule)) {
								$this->errors[] = "Minimum $rule characters";
								$success = false;
							}
							break;
						case 'regExp':
							if (!ereg($rule, $this->value)) {
								$this->errors[] = "Invalid";
								$success = false;
							}
							break;
						case 'isInt':
							if (!is_numeric($this->value)) {
								$this->errors[] = "Invalid integer";
								$success = false;
							}
							break;
						case 'maxInt':
							if (intval($this->value) > $rule) {
								$this->errors[] = "Maximum $rule";
								$success = false;
							}
							break;
						case 'minInt':
							if (intval($this->value) < $rule) {
								$this->errors[] = "Minimum $rule";
								$success = false;
							}
							break;
						case 'passwordLen':
							//echobr($this->value);
							if (strlen($this->value) < intval($rule)) {
								$this->errors[] = "Minimum length: $rule";
								$success = false;
							}
							break;
						case 'email':
							if (!ereg('^[a-zA-Z0-9 \._\-]+@([a-zA-Z0-9-][a-zA-Z0-9\-]*\.)+[a-zA-Z]+$', $this->value)) {
								$this->errors[] = "Invalid email";
								$success = false;
								if ($this->value == null)
									$success = true;
							}
							break;
						case 'equals':
							if ($this->value != $this->cleanup($_REQUEST["$rule"])) {
								$this->errors[] = "Doesn't match $rule";
								$success = false;
							}
							break;
					}
				}
			}

			return $success;
		}

		function getErrorsTag() {
			$errMsg = '<div class="formErrorMsg">';
			foreach ($this->errors as $error) {
				$errMsg .= $error.'<br />';
			}
			$errMsg .= '</div>';
			return $errMsg;
		}

		function getTag() {

			$tag = null;
			switch ($this->type) {
				case 'static':
					$tag = '<span class=formLabel>'.$this->value.'</span>';
					break;
				case 'text':
				case 'password':
				case 'hidden':
					$tag = "<input type='$this->type' name='$this->name' value='$this->value' $this->attrib>";
					break;

				case 'textarea':
					$rows = $this->layout['rows'];
					$cols = $this->layout['cols'];
					$tag = "<textarea name='$this->name' rows=$rows cols=$cols
					     wrap=$this->layout['wrap']>$this->value</textarea>";
					break;

				case 'select':
					$tag = "<select name='$this->name'>";
					foreach ($this->valab as $value=>$label) {
						$selected = null;
						if ($this->value == $value) {
								$selected = 'selected';
						}
						$tag .= "<option value='$value' $selected>$label</option>";
					}
					$tag .= '</select>';
					break;

				case 'checkbox':
					$tag .= '<div class=message>';
					foreach ($this->valab as $value=>$label) {
						$checked = null;
						if ($this->values != null) {
							foreach ($this->values as $checkedValue) {
								if ($checkedValue == $value) {
									$checked = 'checked';
									break;
								}
							}
						}
						$tag .= "<input type='$this->type' name='$this->name[]' value=".'"'.$value.'"'." $checked>$label";
						if ($this->layout['checkbox'] == 'v')
							$tag .= '<br />';
					}
					$tag .= '</div>';
					break;

				case 'radio':
					$tag .= '<div class=message>';
					foreach ($this->valab as $value=>$label) {
						$checked = null;
						if ($this->value == $value) {
							$checked = 'checked';
						}
						$tag .= "<input type='$this->type' name='$this->name' value='$value' $checked $this->attrib>$label";
						if ($this->layout['radio'] == 'v')
							$tag .= '<br />';
					}
					$tag .= '</div>';
					break;

				case 'submit':
					$tag .= "<input type='$this->type' name='$this->name' value='$this->value'>";
					break;

				case 'title':
					$this->label .= "<br /><b>$this->name</b><br /><br />";
					$this->layout['labelAlign'] = 'left';
					break;

				case 'line':
					$tag = '<br />';
					break;

				case 'file':
					$maxSize = $this->rules[MAX_FILE_SIZE] * 1024 * 1024;
					$tag .= "<input type='hidden' name='MAX_FILE_SIZE' value=".'"'.$maxSize.'">';
					$tag .= "<input type='file' name='$this->name'>";	// value='$this->value'>";
					break;
			}

			if ($this->layout['plugin'] == 1) {
				$tag .= $this->getErrorsTag();
			}
			return $tag;

		} // end of getTag()

		function plugin() {
			$style = 0; // element tag and error msg only

			$numArg = func_num_args();
			if ($numArg > 0)
				$style = func_get_arg(0);

			echo $style >= 1 ? $this->label.'<br />' : '';
			echo $this->getTag();
			echo '<br />'.$this->getErrorsTag();
			echo $style >= 2 ? $this->desc.'<br />' : '';

		}

		function display() {

			$tag = $this->getTag();

			if (!isset($this->layout['set'])) {		// not a grouped element

				echo '<tr class=formRow>';
					$align = $this->layout['labelAlign'];
					echo "<td class=formLabel align=$align valign=top >";
						echo $this->label;
						//echo '<div class=formErrorMsg>'.$this->getErrorsTag().'</div>';
					echo '</td>';
					echo '<td valign=top>';
						echo $tag;
						echo '<div class=formErrorMsg>'.$this->getErrorsTag().'</div>';
						echo "<div class=formDescSmall>$this->desc</div>";
					echo '</td>';
				echo '</tr>';

			} else { // is a grouped element. need to buffer output

				// todo add something here if set cardinality > 9

				$setSize = intval($this->layout['set'][0]);
				$num = intval($this->layout['set'][1]);
				if ($num == 1) {
					unset($GLOBALS['error']);
					unset($GLOBALS['desc']);
					unset($GLOBALS['tag']);
				}

				$GLOBALS['error'][] = $this->getErrorsTag();
				$GLOBALS['desc'][] = $this->desc;
				$GLOBALS['tag'][] = $tag;

				if ($num == $setSize) {	// the last element in the group, flush buffer

					$allTag = '<table border="0" cellpadding="0" cellspacing="0"><tr>';
					foreach ($GLOBALS['desc'] as $desc) {
						$allTag .= "<td style='font-size:10px' valign=top>$desc</td>";
					}
					$allTag .= '</tr><tr>';
					$i = 0;
					foreach ($GLOBALS['tag'] as $tag) {
						$allTag .= "<td valign=top>$tag";
						$allTag .= "<br /><div class=formDescSmall>".$GLOBALS['error'][$i++]."</div>";
						$allTag .= "</td>";
					}
					$allTag .= '</tr></table>';

					echo '<tr class=formRow>';
						echo '<td class=formLabel align=right valign=middle>';
							echo $this->label;
							//echo '<div class=formErrorMsg>'.$allErr.'</div>';
						echo '</td>';
						echo '<td valign=top>';
							echo $allTag;
							//echo "<div class=formDescSmall>$this->desc</div>";
						echo '</td>';
					echo '</tr>';
				}
			}
		} // end of display()

		public function getSelectValue($i) {
			if ($this->values == NULL)
				return 0;
			return $this->values[$i];
		}
	}

	class HTMLForm {
		public $name;
		public $method;
		public $action;
		public $enctype;
		public $elements = array();

		function HTMLForm ($name = null, $method = null, $action = null, $enctype = null) {
			$this->name = $name == null ? 'form1' : $name; // specify a name or don't use name attr at all
			$this->method = $method == null ? 'post' : $method;
			$this->action = $action == null ? $_SERVER['PHP_SELF'] : $action;
			$this->enctype = $enctype == null ? 'multipart/form-data' : $enctype;
		}

		function open($style = 'form') {
			echo "<div class=$style>";
			echo "<form name=$this->name method=$this->method action=$this->action enctype=$this->enctype>";
		}

		function close() {
			echo '</form></div>';
		}

		function addElement() {
			$numArgs = func_num_args();
			for ($i = 0; $i < $numArgs; $i++) {
				$this->elements[] = func_get_arg($i);
			}
		}

		function display($style = 'form') {
			$this->open($style);

			echo '<br /><table border="0" cellpadding="0" cellspacing="0">';
			$numElements = count($this->elements);
			for ($i = 0; $i < $numElements; $i++) {
				$this->elements[$i]->display();
			}
			echo '</table>';

			$this->close();
		}

		function validate() {

			$success = true;
			foreach ($this->elements as $element) {
				if (! $element->validate())
					$success = false;
			}
			return $success;
		}

		function validateWithException($exceptionMsg = 'Please correct errors in the form.') {

			$success = $this->validate();
			if (!$success)
				throw new Exception($exceptionMsg);
		}

	} // end of Form


	// data + <>  + style + layout = tag, which can be displayed, or plugged in other page elememnts;

	class HTMLTable {

		public $data = array(); 		// the cells could be text or object.
		public $title = array();		// 'column name' => 'column width'

		public $interleave = false;
		public $style = 0;				// default. todo: add more styles
		public $layout = array();
		
		public $sortCol = -1;			// -1: no sort; > -1: order by col num
		public $sortDir	= null;			// 'asc' or 'desc'
		public $sortOffCols;
		
		public $categoryCol = -1;		// categorize by a column
		public $quickLinkTitle = null;
		public $showQuickLink = false;

		function HTMLTable($data = null, $sortCol = -1, $sortDir = null) {

			$this->data = $data;
			$this->layout['titleBold'] = 1;
			
			if ($sortCol != -1) $this->sortCol = $sortCol;
			if ($sortDir != null) $this->sortDir = $sortDir;
			
			$this->sort();
			
/*
			if (!(isset($_GET['sortCol']) && isset($_GET['sortDir']))) 
*/
			if (isset($_GET['catCol']) && isset($_GET['quickLinkTitle']))
				$this->setCategory($_GET['catCol'], $_GET['quickLinkTitle']);
		}
		
		// sort $this->data according to the column number given
		function sort() {		
			
			if (!isset($_GET['sortCol']) && $this->sortCol == -1) 
				return;

			if (isset($_GET['sortCol']))
				$this->sortCol = $_GET['sortCol'];
			if (isset($_GET['sortDir']))
				$this->sortDir = $_GET['sortDir'];
			sortTable(&$this->data, $this->sortCol, $this->sortDir);		
		} 
		
		function setSortOffCols($array) {
			
			foreach ($array as $col)
				$this->sortOffCols[$col] = $col;
		}
		
		function setCategory($col, $quickLinkTitle = null ) {
			
			$this->categoryCol = $col;
			$this->quickLinkTitle = $quickLinkTitle;
		}			
		
		function setTitle() {
			$numArgs = func_num_args();
			for ($i = 0; $i < $numArgs; $i++) {
				$arg = func_get_arg($i);
				$pair = explode('=', trim($arg));
				if ($this->sortCol != -1 && !isset($this->sortOffCols[$i])) {
					$pair[0] = HTML::command($pair[0], $_SERVER['PHP_SELF']."?sortCol=$i".
					'&sortDir='.($i == $this->sortCol ? ($this->sortDir == 'asc' ? 'desc':'asc') : 'asc')).
					($i == $this->sortCol ? ($this->sortDir == 'asc' ? '+':'-'):'');
				}
				$this->title[$pair[0]] = $pair[1];
			}
		}

		function setLayout() {
		}

		function getTitleTag() {

			$tag .= '<tr>';
			$i = 0;
			foreach ($this->title as $colName=>$width) {

				if ($i++ == $this->categoryCol) continue;
				
				$tag .= "<td width=$width class=titlecell>";
				$tag .= $this->layout['titleBold'] == 1 ? '<b>':'';
				$tag .= $colName;
				$tag .= $this->layout['titleBold'] == 1 ? '</b>':'';
				$tag .= '</td>';
			}
			$tag .= '</tr>';
			return $tag;
		}

		function getNEmptyCellTag($n, $class) {
			$tag = null;
			for ($i = 1; $i <= $n; $i++) {
				$tag .= "<td $class>&nbsp;</td>";
			}
			return $tag;
		}

		function getDataTag() {
			$numCols = count($this->title); 		// $data != $this->data !!!!!
			//echo $numCols;
//if ($this->data == null) return '';

			foreach ($this->data as $row) {
				$class = $class == 'class=cell' ? "class=shadedcell" : 'class=cell';

				$tag .= "<tr>";

				// the cells in the row may not start from index 0

				$j1 = 0; // current cell to be drawn
				foreach ($row as $j2=>$cell) {
					
					if ($j1 == $this->categoryCol) continue;
					
					if ($j1 == $j2) {
						if (isset($this->style['align'][$j1]))
							$align = "align = $this->style['align'][$j1]";;
						$tag .= "<td $align $class>".(is_object($cell) ? $cell->getTag() : $cell)."</td>";
					}
					if ($j1 < $j2) {
						$tag .= $this->getNEmptyCellTag($j2 - $j1, $class);
						$tag .= "<td $class>".(is_object($cell) ? $cell->getTag() : $cell)."</td>";
					}
					$j1 = $j2 + 1;
				}
				if ($j2 < ($numCols))
					$tag .= $this->getNEmptyCellTag($numCols - 1 - $j2, $class);

				$tag .= "</tr>";
			}
			return $tag;
		}

		function getTag() {

			if ($this->categoryCol == -1 || isset($_GET['sortCol'])) {
				if ($this->categoryCol != -1)
					$tag = HTML::command("Sort by $this->quickLinkTitle", $_SERVER['PHP_SELF'].
					"?catCol=$this->categoryCol").'<br /><br />';
				
				$tag .= '<table border="0" cellpadding="0" cellspacing="0">';
				$tag .= $this->getTitleTag();
				$tag .= $this->getDataTag();
				$tag .= "</table>";
//echobr(111);
			} else {

				$d = array();
				$j = 1;
				$cnt = count($this->data);
				$cat1 = $this->data[0][$this->categoryCol];
//printArray($this->data);
				$i = 1;
				foreach ($this->data as  $row) {
					$cat2 = $row[$this->categoryCol];
//echobr("i:$i 1:$cat1  2:$cat2");
					if ($cat2 == $cat1) {
						$d[$i] = $row;
						unset($d[$i][$this->categoryCol]);
						if ($j == $cnt) {
							$tag .= $this->categoryTable($cat1, &$d);
							break;
						}
					} else {
						
						$tag .= $this->categoryTable($cat1, &$d);
						$cat1 = $cat2;	
						$d = array();
						$d[$i] = $row;
						unset($d[$i][$this->categoryCol]);
						if ($j == $cnt) {	
							$tag .= $this->categoryTable($cat1, &$d);
							break;
						} 
					}
					$j++;
					$i++;
				}	
			}

			return $tag;
		}

		function categoryTable($cat1, &$d) {
			arsort($d);
			//sortTable(&$d, 0, 'desc');	// todo
			//printArray($d);
			$t = new HTMLTable($d, $this->sortCol, $this->sortDir);
			$t->layout['titleBold'] = 0;
			$t->title = $this->title;
			$k = 0;
			foreach ($t->title as $name => $width) {
				if ($k++ == $this->categoryCol)
					unset($t->title[$name]);
			}

			return "<h4>$cat1".' ['.count($d).']</h4>'.$t->getTag().'<br />';
		}			
			
		function display() {
			if ($this->data != null)
				echo $this->getTag();
		}

	}

	class HTMLPager	{

		public $dataSource;	// has method getData($pageNum), getTotalRows
		public $table;	// has methodd setData();

		public $totalRows;
		public $totalPages;
		public $pageSize = 10;
		public $page;

		public $navBar;

		public $formName = 'pagerForm'; // onclick

		function HTMLPager() {
		}

		function setDataSource($dataSource) {

			$this->dataSource = $dataSource;

			$this->totalRows = isset($_SESSION['totalRows']) ?
				intval($_SESSION['totalRows']) : $dataSource->getTotalRows();
			$this->totalPages = $this->totalRows / $this->pageSize;
			$this->totalPages = ceil($this->totalPages);
			$this->page = isset($_GET['page']) ? intval($_GET['page']) : 0;

			$from = $this->page * $this->pageSize;
			if ($from > ($this->totalRows - 1) || !is_int($from) || $from < 0) {
				$from = 0;
				$this->page = 0;
			}
			$to = $from + $this->pageSize - 1;
			$to = $to > ($this->totalRows -1) ? ($this->totalRows -1) : $to;

			$this->table = new HTMLTable($this->dataSource->getData($from, $to));


			echobr('totalRows = '.$this->totalRows);
			echobr('totalPages = '.$this->totalPages);
			echobr('page = '.$this->page);
			echobr("from = $from finsih = $to");
			$this->buildNavBar();
		}

		function buildNavBar() {

			$curr = $this->page;
			$prev = $curr == 0 ? null : $curr - 1;
			$next = $curr == ($this->totalPages - 1) ? null : $curr + 1;
			echobr("prev:curr:next = $prev:$curr:$next");

			$prev = $prev === null ? null : HTML::a('Previous', $_SERVER['PHP_SELF'].'?page='.$prev);
			$next = $next === null ? null : HTML::a('Next', $_SERVER['PHP_SELF'].'?page='.$next, null, null,
				'onclick='.$this->formName.'.submit();');

			$this->navBar = $prev.' '.($curr + 0).' '.$next;
		}

		function displayTable() {
			echo $this->table->getTag();
		}

		function displayNavBar() {
			echo $this->navBar;
		}

		function getTag() {
			$tag = $this->table->getTag();
			$tag .= $this->navBar;
			return $tag;
		}

		function display() {
			echo $this->getTag();
		}

	}


	class HTMLMultiPageForm {

		public $fromPage;
		public $toPage;
		public $fromForm;
		public $forms = array();

		function HTMLMultiPageForm() {
			$this->fromPage = $_GET['fromPage'];
			$this->toPage = $_GET['toPage'];
			$this->fromForm = new HTMLForm();

		}

		function  isValidFromPage() {
			if (!$this->fromForm->validate()) {
				$this->toPage = $this->fromPage;
			} else {
				$this->addForm($this->fromForm);
			}
		}

		function isSubmitClicked() {
			return isset($_GET['submit']) ? true : false;
		}

	}

	class HTMLPagerForm {

		function sessionate() {
			foreach ($_POST as $name=>$value) {
				$_SESSION['pagerForm'][$name] = $value;
			}
			foreach ($_SESSION['pagerForm'] as $name=>$value) {
				$_POST[$name] = $value;
			}
			var_dump($_SESSION);
		}

	}

	class HTMLBox {

		public $title;
		public $lines;

		function HTMLBox($title) {
			$this->title = '<div class=titlecell>&nbsp;'.$title.'</div>';
		}

		function addLine($line) {
			$this->lines[] = $line;
		}

		function display() {
			echo '<b>'.$this->title.'</b><br />';
			foreach ($this->lines as $line) {
				echo '&nbsp;'.$line.'<br />';
			}
			echo '<br />';
		}
	}


	class HTML {

		public static function a($label, $link, $class = null, $script = null) {

			$class = $class == null ? null : 'class='.$class. ' ';
			$tag = '<a '.$class.'href="'.$link.'" '.$script.'>'.$label.'</a>';

			return $tag;
		}

		public static function command($label, $link, $state = 'normal', $decorate = false) {

			switch ($state) {
				case 'normal':
					$tag = HTML::a($label, $link);
					break;
				case 'disabled':
					$tag = $label;	// label only, actually not link
					break;
				case 'active':
					$tag = HTML::a($label, $link, 'activeLink');	// active menu item
					break;
			}

			if ($decorate == true)
				$tag = '['.$tag.']';

			return $tag;
		}

		public static function div($text, $class) {	// todo: nexting multiple classe divs
			return "<div class=$class>$text</div>";
		}

		public static function menu($array, $align = 'left') {
			
			$rtn = null;
			foreach ($array as $label => $cmd) {
				if ($rtn != null)
					$rtn .= ' | ';
				$rtn .= HTML::command($label,  $cmd[0], $cmd[1] == 1 ? 'normal':'disabled');
			}
			
			if ($align == 'right')
				$rtn = '<span class=right0>'.$rtn.'</span>';
			
			return $rtn;
		}
		
		public static function switchStatus($label) {
			
			$label_ = str_replace(' ', '_', $label);
			return $_GET[$label_];			
		}
		
		// $_SERVER["PHP_SELF"] should not contain any query strings.
		
		public static function switchCommand($label, $link = 'self') {
			
			$label_ = str_replace(' ', '_', $label);

			if (!isset($_GET[$label_]) || $_GET[$label_] == 'Hide') {
				$label2 = 'Show '.$label;
				$queryStr = $label_.'=Show';
			} 
			
			if ($_GET[$label_] == 'Show') {
				$label2 = 'Hide '.$label;
				$queryStr = $label_.'=Hide';
			}
			
			if ($link == 'self') $link = $_SERVER["PHP_SELF"];
			$link .= "?$queryStr";
			
			return '<span class=right0>'.HTML::command($label2, $link).'</span>';
		}
	}
?>
