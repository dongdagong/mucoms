<?
	include_once 'country_valab.php';		// for country dropdown lists
	include_once 'includes/constants.php';	// application-wide constant definitions
	include_once 'cms/Role.php';

/*
	Database
	Authentication
	Email
	File

	Handy functions
	CMS only functions
*/


/*
	At the beginning of each page, open a connection to the database. The connection will be automatically
	closed by PHP when the page finishes execution.

	Cannot establish a connection that's available across pages, since the connection will be closed
	when the page goes out of scope. Keeping the connection alive when the user is reading the page is unnecessary.

	Only in login and signup it is necessary to pass an argument to connect().
*/

class DB {

	public static $host = DB_HOST;
	public static $user = DB_USER;
	public static $pwd = DB_PASSWORD;
	public static $conn;

	// connect to the specified database, or connect to 'ci'
	public static function connect($database = NULL) {
		if ($database != NULL) {
			@ $conn = new mysqli(self::$host, self::$user, self::$pwd, DB_PREFIX.$database);
			$errno = mysqli_connect_errno();
			if ($errno == 1044)
				HTTP::message("Database $database does not exist.");
			if ($errno != 0) {
				throw new Exception("DB::connect(): Error connecting to $database. Errno: $errno");
			} else {
				$_SESSION['conn'] = $conn;
			}
		} else {
			$conn = DB::connect($_SESSION['ci']);	///////////////////////////////
		}
	}

	public static function getConn() {

		return $_SESSION['conn'];
	}

	public static function autocommit($value) {

		$conn = DB::getConn();
		$conn->autocommit($value);
	}

	public static function commit() {

		$conn = DB::getConn();
		$conn->commit();
	}

	public static function rollback() {

		$conn = DB::getConn();
		$conn->rollback();
	}

	static static function query($query, &$result, $msg = null) { // $msg: name of the method that called query()

		$conn = DB::getConn();
		$result = $conn->query($query);
		
		if (!$result) {
			$msg = "Query error.<br />$msg";
			if (DEBUG) $msg .= "Query: $query <br />";
			if (DEBUG) $msg .= "Error: $conn->error <br />";;
			$conn->rollback();
			throw new Exception($msg);
		}
	}

	public static function getCell(&$result, $row = 0, $col = 0) {

		//$array = $result->fetch_array(MYSQLI_NUM); // fetch_array: returns a ROW as an array
		if ($row >= $result->num_rows)
			return null;

		for ($i = 0; $i <= $row; $i++) {
			$resultRow = $result->fetch_row();
			if ($i == $row) {
				if ($col > count($resultRow))
					return null;
				//echo $resultRow[$col] . '<br/>';
				return $resultRow[$col];
			}
		}
	}

	public static function errno() {

		$conn = DB::getConn();
		return $conn->errno;
	}

}


class Auth {	// Authentication

	// Set the user role and id in session
	// Returns true of successful, false if not.
	static function login($uid, $pwd, $role, $ci, $hashPwd = true) {

		// ci exist?

		DB::connect('common');

		// is ci active?

		$conf = new Conference($ci);
		if ($conf->isActive == 0) {
			HTTP::message("Conference instance is inactive.");
		}
		$_SESSION['conference'] = $conf;

		// yes ci is active, log the user in

		DB::connect($ci);

		$pwdHash = $hashPwd ? sha1($pwd) : $pwd;
		$query = "
			select count(*) as num 
			from Users u, UserRole r 
			where 
				u.ID = r.UserID and 
				(u.UID = '$uid' or u.Email = '$uid') and 
				u.pwd = '$pwdHash' and 
				r.RoleID = $role";

		DB::query($query, &$result, 'Auth::login()');
		$num = DB::getCell(&$result);

		if ($num != 1)
			throw new Exception("Invalid ID or password, please try again.");

		$query = "select ID from Users where UID = '$uid' or Email = '$uid'";
		DB::query($query, &$result, 'Auth::login()');

		$_SESSION['id'] = DB::getCell(&$result);
		$_SESSION['pwd'] = $pwd;

		$_SESSION['role'] = $role;
		$_SESSION['ci'] = $ci;

		// write log
		
		$u = new Users($_SESSION['id']);
		$roles  = new Role();
		
		$log = date('Y-m-d H:i:s').', '.$u->getName().' ('.$_SESSION['id'].'), '.
		$roles->valab[$role].', '.$_SERVER['REMOTE_ADDR']."\r\n";
		$file = fopen(LOG_DIR.$_SESSION['ci'].'_log.txt', 'a');
		fwrite($file, $log);
		fclose($file);
	}

	// Unset the user role in session
	static function logout() {

		foreach ($_SESSION as $key => $value)
			unset($_SESSION[$key]);
		session_destroy();
	}

	static function switchRole($newRole) {

		$_SESSION['role'] = $newRole;
	}

	static function isLogin($role) {

		if (isset($_SESSION['id']) && $_SESSION['role'] == $role) {
			return true;
		}
		return false;
	}

	static function loginCheck($role) {

		if ($role == null || !Auth::isLogin($role)) {

			HTTP::message('Please login to access this page.');
		}

	}

	static function adminCheck() {
		if (!isset($_SESSION['adminUID'])) {
			echo ('Please login as administrator to access this page.');
			exit;
		}
	}

	static function getRole() {

		return $_SESSION['role'];
	}

	static function getId() {

		return $_SESSION['id'];
	}
}


class HTTP {

	static public function isLegalPost() {
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
			return true;
		return false;
	}

	static public function redirect($fileName) {

		header("Location: $fileName"); // Session ID is not passed with Location header
		exit();
	}

	static public function message($message, $nextURL = '') {
		$nextURL = $nextURL == ''? '':"nextURL=$nextURL";
		HTTP::redirect("message.php?message=$message&$nextURL");
	}

	// put a GET variable into SESSION
	public static function sessionate($var) {

		$_SESSION[$var] = isset($_GET[$var]) ? $_GET[$var] : $_SESSION[$var];
		return $_SESSION[$var];
	}

	// file download
	public static function d($label, $fileName) {

		return HTML::a($label, 'download.php?f='.$fileName);
	}

	public static function confirm($msg, $yesURL, $noURL, $yesCmd = 'Yes', $noCmd = 'No') {

		// have not gone through the confirmation yet
		if (!isset($_GET['confirm'])) {
			$msg = urlencode($msg);
			$yesURL = urlencode($yesURL);
			$noURL = urlencode($noURL);
			HTTP::redirect("confirm.php?msg=$msg&yesURL=$yesURL&noURL=$noURL&yesCmd=$yesCmd&noCmd=$noCmd");
		}

		// user choosed 'yes'
		if ($_GET['confirm'] == 'yes') {
			return;
		}
	}

}


// send email using templates

class Email {

	public $templateDir = TEMPLATE_DIR;
	public $logEmail;
	public $logMsg;

	public $date = null;
	public $to = null;
	public $from = null;
	public $cc = null;
	public $bcc = null;
	public $headers = null;
	public $subject = null;
	public $body = null;

	public $plugins = array();

	function Email($subject, $template, $plugins = null, $useTemplate = true, $includePwd = true) {

		// headers

		if (!isset($_SESSION['conference'])) {
			throw new Exception('Email::Conference instance not available.');
		}

		$conf = $_SESSION['conference'];
		$this->logEmail = $conf->logEmail;
		$this->from = 'From: '.$conf->fromEmail;
		if (strlen($conf->CC) > 0)
			$this->cc = "\r\nCC: ".$conf->CC;
		if (strlen($conf->BCC) > 0)
			$this->bcc = "\r\nBCC: ".$conf->BCC;

		// subject

		$this->subject = $_SESSION['conference']->shortName.' - '.$subject;

		// body

		$this->plugins = array(
			'ConferenceFullName' => $_SESSION['conference']->fullName,
			'ConferenceShortName' => $_SESSION['conference']->shortName,
			'ContactEmail' => $_SESSION['conference']->contactEmail,
			'AuthorLoginURL' => SYSTEM_DOMAIN.'a_login.php?ci='.$_SESSION['ci'],
			'MemberLoginURL' => SYSTEM_DOMAIN.'login.php?ci='.$_SESSION['ci'],
		);

		if (isset($_SESSION['id'])) {

			$inviter = new Users($_SESSION['id']);

			$this->plugins['InviterName'] = $inviter->getName();
			$this->plugins['InviterOrg'] = $inviter->org;
			$this->plugins['InviterEmail'] = $inviter->email;
		}

		if ($useTemplate) {
			$fileArray = file($this->templateDir.$_SESSION['ci'].'/'.$template.'.txt');
			$template = '';
			$msgAdded = false;
			foreach ($fileArray as $line) {
				if ($includePwd == true) {
					$template .= $line;
				} else {
					if ((strpos($line, '{UserID}') === false && strpos($line, '{Password}') === false)) {
						$template .= $line;
					} elseif ($msgAdded == false) {
						$template .= '   ID and Password: remain the same';
						$msgAdded = true;
					}
				}
			}
		}

		$this->body = $this->plug($template, $this->plugins);
	}

	public function addCC($cc) {
		if ($this->cc == null)
			$this->cc .= "\r\nCC: ".$cc;
		else
			$this->cc .= ", $cc";
	}

	public function addTo($to) {
		$this->cc .= ", $to";
	}

	public function addPlugin($plugin) {

/*
		$a = explode($separator, trim($plugin));
		$this->body = $this->plug($this->body, array($a[0] => $a[1]));
		// The assumption here is the 2nd part of $plugin contains 
		// no ':', which is not always true.
*/
		$plugin = trim($plugin);
		$sepPos = strpos($plugin, ':');
		$a = substr($plugin, 0, $sepPos);
		$b = substr($plugin, $sepPos + 1);
		$this->body = $this->plug($this->body, array($a => $b));
	}

	function plug($template, $plugins) {

		foreach ($plugins as $key => $value) {

			$template = str_ireplace('{'.$key.'}', $value, $template);
		}
		return $template;
	}

	// plug user's info (name, uid, pwd) into $body and send
	public function send($to = null) {

		$userInfoPlugins = array(
			'UserName' => $to->fname.' '.$to->lname,
			'UserID' => $to->uid,
			'Password' => $to->pwd
		);
		$body = $this->plug($this->body, $userInfoPlugins);
		$this->to = $to->email;
		$this->headers = $this->from.$this->cc.$this->bcc;



		if (SEND_EMAIL == 1)
			mail(
				$this->to,
				$this->subject,
				htmlspecialchars_decode($body, ENT_QUOTES),
				$this->headers
			);

		if ($this->logEmail == 1) {
			$this->writeLog($this->getLogMsg($body));
		}
	}

	public function groupSend($group) {

		if ($group != null) {

			foreach ($group as $to) {
				$this->send($to);
			}
		}
	}

	function getLogmsg($body) {

		$this->logMsg = "/ \r\nDate: ".date('Y-m-d H:i:s')."\r\n";
		$this->logMsg .= 'To: '.$this->to."\r\n";
		if ($this->headers != null)
			$this->logMsg .= $this->headers;
		$this->logMsg .= "\r\nSubject: ".$this->subject."\r\n";
		$this->logMsg .= "\r\n".$body."\r\n \r\n";
		$this->logMsg = htmlspecialchars_decode($this->logMsg, ENT_QUOTES);
		return $this->logMsg;
	}

	function writeLog() {

		$file = fopen(LOG_DIR.$_SESSION['ci'].'_notification_log.txt', 'a');
		fwrite($file, $this->logMsg);
		fclose($file);
	}
}

class File {

	public static function getDefaultUploadDir() {

		return UPLOAD_DIR.$_SESSION['ci'].'/';
	}


	public static function getUploadFileExt($fileInputName) {

		$file = $_FILES[$fileInputName]['name'];
		$lastDotPos = strrpos($file, '.');
		return substr($file, $lastDotPos, strlen($file) - $lastDocPos);
	}

	public static function upload($fileInputName, $newName) { 	// $newName does not contain .ext


		$oldName = $_FILES[$fileInputName]['name'];
		$tmpName = $_FILES[$fileInputName]['tmp_name'];

		$ext = File::getUploadFileExt($fileInputName);

		$newName = File::getDefaultUploadDir().$newName.$ext;

//		echobr('oldName: '.$oldName);
//		echobr('tmpName: '.$tmpName);
//		echobr('newName: '.$newName);

		if (is_uploaded_file($tmpName)) {
			if (!move_uploaded_file($tmpName, $newName)) {
				throw new Exception("Could not move $input to destination directory.");
			}
		} else {
				throw new Exception("Possible file upload attack. Filename: $oldName");
		}
	}
}


?>
<?php

	////////// Handy functions //////////

	// $input: name of the file input element

	function echoh($string) {

		$size = func_num_args() > 1 ? func_get_arg(1) : 3;
		echo "<h$size>".$string."</h$size><br />";
	}

	function echobr($string) {

		echo $string . '<br />';
		if (func_num_args() > 1) {
			$arg2 = func_get_arg(1);
			//for ($i = 1; $i < arg2; $i++)
			for ($i = 1; $i <  $arg2; $i++)
				echo '<br />';
		}
	}

	// Return the number of days from date1 to date2.
	// http://www.developertutorials.com/tutorials/php/calculating-difference-between-dates-php-051018/page1.html
	function dateDiff($date1, $date2, $delimiter = '-') { // yyyy-mm-dd

		$d1 = explode($delimiter, $date1);
		$d2 = explode($delimiter, $date2);

		$d1Jd = gregoriantojd($d1[1], $d1[2], $d1[0]);
		$d2Jd = gregoriantojd($d2[1], $d2[2], $d2[0]);

		return $d2Jd - $d1Jd;
	}

	function getDateSection($date, $section) {

		switch ($section) {
			case 'day':
				return substr($date, 8, 2);	break;
			case 'month':
				return substr($date, 5, 2);	break;
			case 'year':
				return substr($date, 0, 4);	break;
		}
	}


	function headerLocation($fileName) {

		$host  = $_SERVER['HTTP_HOST'];
		$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		header("Location: http://$host$uri/$fileName");
	}

	function warning($var, $lower = 0, $upper = 1000000) {

		if ($var < $lower || $var > $upper)
			return '<span class=warning>&nbsp;'.$var.'&nbsp</span>';

		return $var;
	}

	function debug($msg){
		if (DEBUG == true) {
			echo 'Debug: '.$msg.'<br />';
		}
	}

	function noTail($str, $tailLen){
		return substr($str, 0, strlen($str) - $tailLen);
	}

	function makeup($str, $color, $background){
		return "<span style=color:$color;background:$background>$str</span>";
	}

	function nbspStr($str, $len) {

		$i = $len - strlen($str);
		while ($i > 0) {
			$str .= '&nbsp;';
			$i--;
		}
		return $str;
	}

	// Return the referer file name without the query string.
	function referer() {
		return array_shift(explode('?', basename($_SERVER['HTTP_REFERER'])));
	}

	// recursively print the given array as a tree
	function printArray($a, $j = 0) {
		if (!is_array($a)) return;
//		if (is_object($a)) return;
		if ($j == 0) echo '/ <br />';

		foreach ($a as $id=>$e) {
			for ($i = 0; $i < $j; $i++)
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			if (is_array($e)) {
				echo "$id => <br />";
				printArray($e, $j + 1);
			} else {
				echo "$id => $e <br />";
			}
		}
		echo '<br />';
	}
	
	// sort table by column number maintaining index association.
	// column number starts at 0.
	
	function sortTable(&$array, $col, $dir) {
/*
echobr("$col $dir");		
*/
/*
printArray($array);
*/
		
		$a2 = null;
		foreach ($array as $key => $row) {
			$a2[$key] = strtolower($row[$col]); 
		}
/*
printArray($a2);
*/
		if (is_array($a2)) {
			if ($dir == 'asc')
				asort($a2);
			if ($dir == 'desc')
				arsort($a2);
		}
/*
printArray($a2);
*/

		$a3  = null;
		if ($a2 != null) {
			foreach ($a2 as $key => $row) {
				$a3[$key] = $array[$key];
			}
		}
		
		$array = $a3;		
	}

	////////// CMS only functions //////////

	function downloadLink($pId) {

		return ' <a href=c_paper_dl.php?p='.$pId.' target="_blank">'.$pId.'</a>';
	}
?>
