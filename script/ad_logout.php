<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';

	session_start();
?>
<?php include_once 'header_simple.php'; ?>
<div class="main">
<?php
	
	Auth::logout();

	echo '<br />You have logged out of '.SYSTEM_NAME.' administration.<br />';
?>
<p>
</div> <!-- end of main -->
<?php include_once 'footer_simple.php'; ?>
