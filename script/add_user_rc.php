<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';

	session_start();

	if (Auth::isLogin(40) || Auth::isLogin(31) || Auth::isLogin(30) ||Auth::isLogin(20) )
		HTTP::message('You are already logged in.');

	if (!isset($_SESSION['ci'])) {
		$msg = '';
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') > 0) { // user is using IE v6
			$msg = "This error may be due to how Internet Explorer 6 handles user sessions, please try the following and then revisit this page: <br /><br />1. In Internet Explorer, click Tools, and then click Internet Options.<br />2. Click the Privacy tab, and then click Advanced.<br />3. Click to select the Override automatic cookie handling check box.<br />4. Click Ok twice to save the setting. <br /><br />";
		}
		HTTP::message("Conference instance is not specified<br /><br />$msg");
	}

	DB::connect();
	$u = new Author();
	
	if (!isset($_SESSION['newAuthor4RC']))
		HTTP::message('newAuthor4RC missing');
	else
		$u = $_SESSION['newAuthor4RC'];
		
	if ($u->isExistingUser(40)) {
		HTTP::message('You already have an authors role.', 'a_login.php');
	}
	
	$form = new HTMLForm('signup');

	$email = new FormElement('static', 'email', "You already have a non-author account in the system, 
	registered using <b>$u->email</b>. The new author role will also be associated with this account.<br /><br />
	To continue, please provide your password to this account.");

	$pwd = new FormElement('password', 'pwd');
	$pwd->addLayout(null, null, 'size=25');

	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Submit');

	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();
			
			DB::autocommit(false);
			$u->add();
			DB::commit();
			Auth::login($u->email, $pwd->value, 40, $_SESSION['ci']);
			
			unset($_SESSION['newAuthor4RC']);
			HTTP::redirect('a_main.php');

		} catch (Exception $e) {
			//DB::rollback();
			$u->delete(40);
			$oops = $e->getMessage();
		}
	}
?>
<?php include_once 'header.php'; ?>

<div class="main">
<h3>Adding author role to existing account</h3>
<p class=red><?php echo $oops; ?><br />
<?
	$form->open();
	echo '<br />';
	$email->display();
	echo '<br /><br /><br />';
	echo 'Password: ';
	echo $pwd->getTag().'&nbsp;'.$submit->getTag();
	echo '<br /><br />';
	$form->close();
?>

</div> <!-- end of main -->

<?php include_once 'footer.php'; ?>
