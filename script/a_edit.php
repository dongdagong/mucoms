<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';

	/*
		edit basic parts of the submission
	*/

	session_start();
	Auth::loginCheck(40);
	DB::connect();

	$paperId = isset($_GET['paperId']) ? $_GET['paperId']:$_SESSION['paperId'];
	$_SESSION['paperId'] = $paperId;

	$p = new Paper($paperId);
	$t = new Track($p->trackId);

	// is an contact author of this paper
	$p->isContactAuthorCheck();

	// can edit at this stage
	if (!$p->isEditable()) {
		HTTP::message('Cannot edit paper information at this stage.', 'a_main.php');
	}

	$oldAbst = $p->abstract;

	$form = new HTMLForm('a_edit');

	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'Submit');

	$title = new FormElement('text', 'title', $p->title);
	$title->addLayout('Title', null, 'size=65');
	$title->addRule('required', 'maxLen=150');

	$abstract = new FormElement('textarea', 'abstract', $p->abstract);
	$abstract->addLayout('Abstract', 'Maximum 3000 characters.<br /><br />', 'cols=64', 'rows=4');
	$abstract->addRule('required', 'maxLen=3000');

	$keywords = new FormElement('text', 'keywords', $p->keywords);
	$keywords->addLayout('Keywords', 'Please use comma to seperate the keywords.', 'size=65');
	$keywords->addRule('required', 'maxLen=255');

	$priTopic = new FormElement('select', 'priTopic', $p->priTopic, $t->getTopicsValab());
	$priTopic->addLayout('Primary Topic');
	$priTopic->addRule('required', 'minInt=1');

	$secTopics = new FormElement('checkbox', 'secTopics', null, $t->getTopicsValab(), $p->secTopics);
	$secTopics->addLayout('Secondary Topics');

	$isStudentPaperValab = array(
		1 => 'Check if this is a student paper. Please refer to the submission notification email for more information.'
	);

	$isStudentPaper = new FormElement('checkbox', "isStudentPaper", null, $isStudentPaperValab, $p->isStudentPaper == 1 ? array(1=>1):NULL);
	$isStudentPaper->addLayout('Student Paper');

	$form->addElement($title, $line, $abstract, $keywords, $line, $priTopic, $line, $secTopics);

	if ($_SESSION['ci'] == 'ci5') $form->addElement($line, $isStudentPaper);
	
	$form->addElement($line, $line, $submit);


	if (HTTP::isLegalPost()) {

		try {

			$form->validateWithException();

			$p->title = $title->value;
			$p->abstract = $abstract->value;

			if ($oldAbst != $abstract->value) {

				$now = date('Y-m-d H:i:s');
				$p->abstLastModi = $now;
			}

			$p->keywords = $keywords->value;

			$p->priTopic = $priTopic->value;
			$p->secTopics = $secTopics->values;

			$p->isStudentPaper = $isStudentPaper->getSelectValue(0); 

			DB::autocommit(false);
			$p->update();
			DB::commit();

			HTTP::redirect('a_main.php');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<? include_once 'header.php'; ?>
<div class="main">
<? echo HTML::command('Edit Authors List', "a_edit_authors.php?pId=$paperId").'<br />'; ?>
<h3>Edit Paper Information</h3>
<? echo REQUIRED_FIELDS; ?>
<p class=red><? echo $oops; ?></p>
<? $form->display(); ?>
</div> <!-- end of main -->
<? include_once 'footer.php'; ?>
