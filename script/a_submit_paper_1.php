<?php 
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Track.php';
	
	/*
		multi-track: select a track
		single-track: detect this and skip this step
		
		output: track the paper is to be submitted.
	*/
	
	session_start();
	Auth::loginCheck(40);	
	DB::connect();
	
	$title = 'Select a Track';	
	$selectedTrack = 0;
	
	$conf = $_SESSION['conference'];
	$openTracks = array();
	
	if (!$conf->isMultiTrack()) {
		
		$title = 'Submit a Paper';
		
		try {
			$track = new Track(1);
			if ($track->isAbstOpen() == 0) {
				//$conf = $_SESSION['conference'];
				HTTP::message('Submission is not open at this stage.');
			}
			$_SESSION['selectedTrack'] = 1;
			HTTP::redirect('a_submit_paper_2.php');
		} catch (Exception $e) {
			$oops = $e->getMessage();			
		}
		
	}  
	
	// get the list of tracks that have Abstract Submission Phase opened

	
	
	$form = new HTMLForm('a_submit_paper_1');
	$line = new FormElement('line');
	$submit = new FormElement('submit', null, 'next');
	$form->addElement($line, $submit);		
		
	if (HTTP::isLegalPost()) {
		
		try {
			
			//headerLocation('a_main.php'); 
		
		} catch (Exception $e) {
		
			$oops = $e->getMessage();
			
		}
	}
?>
	
	
<?php include_once 'header.php'; ?>

<div class="main">
<h3><?php echo $title; ?>
</h3>
<p class=red><?php echo $oops; ?><br /></p>
<?php $form->display(); ?>
</div> <!-- end of main -->

<?php include_once 'footer.php'; ?>
