<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(250);

	
	// send noti to this selected paper
	if (isset($_GET['pId'])) {
		$_SESSION['_sendDecisionToOnlyOnePaper'] = true;
		$_SESSION['_pId'] = $_GET['pId'];
		HTTP::redirect('c_notification.php?n=decision');
	}			
			
			
	$form = new HTMLForm('c_selection');
	$line = new FormElement('line', 'line', null);
	$submit = new FormElement('submit', null, 'Send');
	$track = new Track(1);
	
	$msg = new FormElement('static', 'msg', '<b>Release paper status (so the authors can see it online) and send decision notification?</b>');
	
	$clear = new FormElement('checkbox', 'clear', null, array('yes' => 
	'Delete assignments for which no review has been entered.'));
	
	$form->addElement($msg, $line, $clear, $line, $submit);
	
	if (HTTP::isLegalPost()) {

		try {
			
			if ($clear->values[0] == 'yes') {
				
				// Delete those (assignments) that are in Assignment but not in Review.
				// Delete delegation as well.
				// Reviewers will be aware of these deletions.
				
				DB::query("	
					select ReviewerID, PaperID from Assignment A
					where concat(A.ReviewerID, A.PaperID) not in (
					select concat(ReviewerID, PaperID) from Review)", &$result);
				
				DB::autocommit(false);
				while ($row = $result->fetch_assoc()) {
					$track->delAssi($row['ReviewerID'], $row['PaperID']);
				}
				DB::commit();
			}
			
			HTTP::redirect('c_notification.php?n=decision&confirm=yes');

		} catch (Exception $e) {

			$oops = $e->getMessage();
		}
	}
?>
<?php include 'header.php'; ?>
<div class="main">
<h3>Decision Notification</h3>
<div class=red><? echo $oops; ?></div>
<?
	$form->display();
	echo '<br />'.HTML::command('Cancel and return to Selection page.','c_selection.php');
?>
<p></p>
</div> <!-- end of main -->
<?php include 'footer.php'; ?>
