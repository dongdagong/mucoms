<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(40);
	DB::connect();

	$pId = HTTP::sessionate('pId');
	$paper = new Paper($pId);
	$track = new Track(1);

	// privilege check

	// is an contact author of this paper
	$paper->isContactAuthorCheck();

	// can edit at this stage
	if (!$paper->isEditable()) {
		HTTP::message('Cannot edit paper information at this stage.', 'a_main.php');
	}

	// move authors

	$authorCnt = count($paper->authors);
	$moveForm = new HTMLForm('moveForm');
	$submitMove = new FormElement('submit', 'submitMove', 'Move');
	$x = new FormElement('text', 'x');
	$x->addLayout(null, null, 'size=2');
	$x->addRule('isInt', 'minInt=1', "maxInt=$authorCnt");

	$dirArray = array(
		-1 => 'before',
		1 => 'after'
	);
	$dir = new FormElement('select', 'dir', 0, $dirArray);
	$dir->addRule('minInt=-1', 'maxInt=1');

	$y = new FormElement('text', 'y');
	$y->addLayout(null, null, 'size=2');
	$y->addRule('isInt', 'minInt=1', "maxInt=$authorCnt");

	$moveForm->addElement($x, $dir, $y);

	if (HTTP::isLegalPost()) {

		try {

			$authors = $paper->getAuthors();

			if (isset($_POST['x'])) {
				$moveForm->validateWithException('Please correct errors in the Move Author form.');
				DB::autocommit(false);
				$paper->moveAuthor($x->value, $dir->value, $y->value);
				DB::commit();
			}

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br />';
		}

	}

	try {
		if (isset($_GET['c']) && isset($_GET['a'])) {

			if ($_GET['c'] == 'delete') {

				$authors = $paper->getAuthors();
				$author = new Author($_GET['a']);
				
				if (!$author->hasPaper($pId))
					throw new Exception('Author is not associated with this paper.');

				if ($_GET['a'] == $_SESSION['id'])
					throw new Exception('Cannot delete yourself.');

				if (count($authors) == 1)
					throw new Exception('There is only one author left.
					Please add another author first.');
				
				HTTP::confirm(
					'Delete author '.$author->getName().'?<br />',
					"a_edit_authors.php?pId=$pId&c=delete&a=$author->id",
					"a_edit_authors.php?pId=$pId",
					'Yes',
					'Cancel'
				);

				DB::autocommit(false);
				$paper->deleteAuthor($_GET['a']);
/*
				// You cannot delete yourself and since you are editing this paper you 
				// must be a contact author. 
				$p2 = new Paper($pId);
				if (count($p->authors) == 1) && $p->authors[0]->isContactAuthor != 1)
					throw new Exception('The only remaining author is not a contact author.');
*/
				DB::commit();

			}
		}
	} catch (Exception $e) {
		$deleteOops = 'Deletion failed<br />'.
		$e->getMessage().'<br />';
	}

	// authors list

	$authors = $paper->getAuthors();
	$i = 1;
	foreach ($authors as $author) {
		$data[$i][0] = $i;
		$data[$i][1] = (DEBUG ? $author->id.'. ':'').$author->getName().
		' &nbsp; &nbsp; '.HTML::command('Delete', "a_edit_authors.php?pId=$pId&c=delete&a=$author->id");

		$i++;
	}
	$table = new HTMLTable($data);
	$table->setTitle('#=25', 'Author=950', '=50');
?>
<?php include 'header.php'; ?>

<div class="main">
<h3>Edit Authors List for <? echo $paper->title; ?></h3>
<?php
	echo '<span class=red>'.$oops.'</span>';
	echo '<span class=red>'.$deleteOops.'</span><br />';

	$table->display();
	echo '<br />';

	if (count($authors) > 1) {

		$moveForm->open();
		echo 'Place author #'.$x->getTag().' '.$dir->getTag().
		' author #'.$y->getTag().' '.$submitMove->gettag();
		echo '<br /><span class=small><span class=red>';
		echo isset($x->errors[0]) ? '1st author number: '.$x->errors[0].' &nbsp;' : '';
		echo isset($y->errors[0]) ? '2nd author number: '.$y->errors[0] : '';
		echo '</span></span>';
		$moveForm->close();
		echo '<br />';
	}

	echo HTML::command('Add another author', "add_user.php?pId=$paper->id&nur=40").'<br />';
?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
