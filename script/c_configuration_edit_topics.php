<?php
	include_once 'library/html.php';
	include_once 'library/basic.php';
	include_once 'cms/Users.php';
	include_once 'cms/Author.php';
	include_once 'cms/Reviewer.php';
	include_once 'cms/ExternalReviewer.php';
	include_once 'cms/Track.php';
	include_once 'cms/Paper.php';
	include_once 'cms/Review.php';
	include_once 'cms/FormQuestion.php';
	include_once 'cms/PaperTopic.php';
	include_once 'cms/Conference.php';
	include_once 'cms/Chair.php';

	session_start();
	Auth::loginCheck(20);
	DB::connect();

	$c = new Chair($_SESSION['id']);
	$c->privilegeCheck(200);

	$track = new Track(1);

	// add new topic

	$form = new HTMLForm('form');
	$submit = new FormElement('submit', 'submit', 'Add this topic');
	$new = new FormElement('text', 'new');
	$new->addLayout(null, null, 'size=100');
	$new->addRule('minLen=5', 'maxLen=150');
	$form->addElement($new);

	// move topics
	
	$topics = $track->getTopicsValab();
	$topicsCnt = count($topics);

	$moveForm = new HTMLForm('moveForm');
	$submitMove = new FormElement('submit', 'submitMove', 'Move');
	$x = new FormElement('text', 'x');
	$x->addLayout(null, null, 'size=2');
	$x->addRule('isInt', 'minInt=1', "maxInt=$topicsCnt");

	$dirArray = array(
		-1 => 'before',
		1 => 'after'
	);
	$dir = new FormElement('select', 'dir', 0, $dirArray);
	$dir->addRule('minInt=-1', 'maxInt=1');

	$y = new FormElement('text', 'y');
	$y->addLayout(null, null, 'size=2');
	$y->addRule('isInt', 'minInt=1', "maxInt=$topicsCnt");

	$moveForm->addElement($x, $dir, $y);


	// processing input

	if (HTTP::isLegalPost()) {

		try {

			if (isset($_POST['new'])) {
				$form->validateWithException('Please correct errors in the New Topic form.');

				DB::autocommit(false);
				$track->addTopic(trim($_POST['new']));
				$new->value = '';
				DB::commit();
			}

			if (isset($_POST['x'])) {
				$moveForm->validateWithException('Please correct errors in the Move Topic form.');
				DB::autocommit(false);
				$track->moveTopic($x->value, $dir->value, $y->value);
				DB::commit();
			}

		} catch (Exception $e) {
			$oops = $e->getMessage().'<br />';
		}

	}

	if (isset($_GET['c']) && isset($_GET['t'])) {


		if ($_GET['c'] == 'delete') {

			$topics = $track->getTopicsValab();

			try {

				DB::autocommit(false);
				$track->deleteTopic($_GET['t']);
				DB::commit();
			} catch (Exception $e) {
				$deleteOops = 'Cannot delete <b>'.$topics[$_GET['t']].'</b> at this stage.<br />'; //$e->getMessage();
			}
		}
	}

	// topics table

	$topics = $track->getTopicsValab();

	if ($topics !=  null) {

		$i = 1;
		foreach ($topics as $tId => $tName) {

			$data[$i][0] = $i;
			$data[$i][1] = (DEBUG ? $tId.'. ':'').$tName.' '.
			HTML::command('delete', "c_configuration_edit_topics.php?c=delete&t=$tId");

			$i++;
		}

		$table = new HTMLTable($data);
		$table->setTitle('#=15', 'Topic=1000', '=50');
	}

?>
<?php include 'header.php'; ?>
<div class="main">
<?
	echo HTML::menu(array(
		'Topics' 		=> array('c_configuration_edit_topics.php', 0),
		'Paper Formats'	=> array('c_configuration_paper_formats.php', 1),
		'Review Form' 	=> array('c_configuration_form.php', 1),
		'Paper Status'	=> array('c_configuration_ps_disp.php?template=hide', 1)
	));
?>
<h3>Edit Topics</h3>
<?php
	echo '<span class=red>'.$oops.'</span>';
	echo '<span class=red>'.$deleteOops.'</span><br />';
	if ($topics != null) {
		$table->display();
		echo '<br />';
	}

	if (count($topics) > 1) {

		$moveForm->open();
		echo 'Place topic #'.$x->getTag().' '.$dir->getTag().
		' topic #'.$y->getTag().' '.$submitMove->gettag();
		echo '<br /><span class=small><span class=red>';
		echo isset($x->errors[0]) ? '1st topic number: '.$x->errors[0].' &nbsp;' : '';
		echo isset($y->errors[0]) ? '2nd topic number: '.$y->errors[0] : '';
		echo '</span></span>';
		$moveForm->close();
		echo '<br />';
	}

	$form->open();
	echo 'New topic: <br />';
	echo $new->getTag().' ';
	echo '<br /><span class=small><span class=red>'.$new->errors[0].'</span></span><br />';
	echo $submit->gettag();
	$form->close();?>
</div> <!-- end of main -->

<?php include 'footer.php'; ?>
