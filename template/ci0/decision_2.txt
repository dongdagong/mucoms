Dear {UserName},


We regret to inform you that your paper, entitled

   {PaperTitle}

submitted to {ConferenceShortName} has not been accepted for
presentation or publication.

The reviews for your paper are accessible through the conference
management system at the following URL:

   Login URL: {AuthorLoginURL}


Nevertheless, we hope you will be able to attend the {ConferenceShortName}
conference.


Best regards,

   {ConferenceShortName} Committee
   {ConferenceFullName}

