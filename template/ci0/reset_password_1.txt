
Dear {UserName},


Please follow the link below to reset your password. 

   {LinkToResetPassword}


Thanks,
   {ConferenceShortName} Committee
   {ConferenceFullName}
