
Dear {UserName},


This is to inform you that a new  message regarding paper

{PaperTitle}

has been posted to the discussion forum. 

To view the details about this review conflict/discussion, please
login and follow the link 'discuss' next to this paper.

   Login URL: {MemberLoginURL}


Thank you,
   {ConferenceShortName} Committee
   {ConferenceFullName}
