
Dear {UserName},


Your password has been changed. 

   ID: {UserID}
   New Password: {Password}

   Author Login URL: {AuthorLoginURL}
   Member Login URL: {MemberLoginURL}


Best regards,
   {ConferenceShortName} Committee
   {ConferenceFullName}
