
Dear {UserName},


This is to inform you that Review Submission for {ConferenceShortName}
is about to close on {DueReview}.

Please submit or edit your reviews before this date. 

   Login URL: {MemberLoginURL}


Thank you,
   {ConferenceShortName} Committee
   {ConferenceFullName}
