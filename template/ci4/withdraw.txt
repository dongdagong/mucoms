
Dear {UserName},


This is to inform you that the following paper has been withdrawn
from {ConferenceShortName}.

   {PaperTitle}


Best regards,
   {ConferenceShortName} Committee
   {ConferenceFullName}
