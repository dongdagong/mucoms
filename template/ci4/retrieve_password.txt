
Dear {UserName},


A new password has been created for you. 

   ID: {UserID}
   New Password: {Password}

   {AuthorLoginURLPlugin}
   {MemberLoginURLPlugin}


Best regards,
   {ConferenceShortName} Committee
   {ConferenceFullName}
