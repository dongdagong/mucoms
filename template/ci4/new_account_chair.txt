
Dear {UserName},


You have been invited as Chair to {ConferenceShortName}.

Please use the following information to log in:

   ID: {UserID}
   Password: {Password}

   Login URL: {MemberLoginURL}

To change your password, choose Edit Profile after login.


Thank you,
   {InviterName}
   {InviterOrg}
   {InviterEmail}

   {ConferenceFullName}
   {ConferenceShortName} Committee
