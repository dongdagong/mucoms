
Dear {UserName},

Your {ConferenceShortName} submission entitled

   {paperTitle}

has been removed from the {ConferenceShortName} submission and review
system. If you feel that this action has been unjustified, please,
contact 

   {ContactEmail}


Best regards,
   {ConferenceShortName} Committee
   {ConferenceFullName}
