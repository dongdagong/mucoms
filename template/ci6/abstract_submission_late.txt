
Dear {UserName},


This is to inform you that Abstract Submission for {ConferenceShortName} 
is about to close on {DueAbst}.

Please submit or edit your abstract before this date. 

   Login URL: {AuthorLoginURL}


Best regards,
   {ConferenceShortName} Committee
   {ConferenceFullName}
